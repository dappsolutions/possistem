var ZakatUsaha = {
  module: function () {
   return 'zakat_usaha';
  },
  
  setSelect2: function () {
   $('select#thresholdmas').select2();
  },
 
  getZakat: function () { 
   var threshold_id = $('select#threshold_id').val();
   $.ajax({
    type: 'POST',
    dataType: 'json',
    async: false,
    url: url.base_url(ZakatUsaha.module()) + "goThresholdMas/" + threshold_id,
 
    error: function () {
     console.log({is_valid: false});
    },
 
    success: function (resp) {
     console.log(resp);
    }
   });
  },
 
  goThresholdMasZakat: function (elm) {
   var threshold = $(elm).val();
   if(threshold != ''){
    window.location.href = url.base_url(ZakatUsaha.module()) + "index/" + threshold;
   }else{
    window.location.href = url.base_url(ZakatUsaha.module()) + "index";
   }  
  },
  
  getPostData: function () {
    var data = {
     'harga': $('#zakat_mal_harga_emas').val(),
     'start': $('#zakat_mal_periode_mash').val(),
    };
  
    return data;
   },

  updateThresholdMas : function (id){
   var data = ZakatUsaha.getPostData();
   var formData = new FormData();
   formData.append('data', JSON.stringify(data));
   formData.append("id", id);
   console.log(id);

   if (validation.run()) {
    $.ajax({
     type: 'POST',
     data: formData,
     dataType: 'json',
     processData: false,
     contentType: false,
     async: false,
     url: url.base_url(ZakatUsaha.module()) + "simpan",
     error: function (err) {
       console.log(err);
      toastr.error("Program Error");
      message.closeLoading();
     },
 
     beforeSend: function () {
      message.loadingProses("Proses Simpan...");
     },
 
     success: function (resp) {
       console.log(resp);
      if (resp.is_valid) {
       toastr.success("Berhasil Disimpan");
       var reload = function () {
        window.location.href = url.base_url(ZakatUsaha.module());
       };
 
       setTimeout(reload(), 1000);
      } else {
       toastr.error("Gagal Disimpan");
      }
      message.closeLoading();
     }
    });
   }
  }
 };
 
 $(function () {
  ZakatUsaha.setSelect2();
  var threshold_id = $('input#threshold_id').val();
  if (threshold_id != '0') {
    ZakatUsaha.getZakat();
  }
 // ref.on('value', snap => console.log(snap.val()));
 
 // $('select#salesman').on('change', function (e) {
 //  let id = $(this).find("option:selected").val();
 //  LacakSalesman.initSalesLocation(id);
 // });
 })