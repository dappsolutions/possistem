var Payment = {
 module: function () {
  return 'payment';
 },
 
 moduleFaktur: function () {
  return 'faktur_pelanggan';
 },

 add: function () {
  window.location.href = url.base_url(Payment.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Payment.moduleFaktur()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Payment.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Payment.module()) + "index";
   }
  }
 },

 getPostInvoiceItem: function () {
  var data = [];
  var tb_item = $('table#tb_item').find('tbody').find('tr');

  $.each(tb_item, function () {
   var check = $(this).find('input#check');
   if (check.is(':checked')) {
    data.push({
     'invoice': $(this).attr('data_id'),
     'sisa_hutang': $.trim($(this).find('td:eq(2)').text())
    });
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'customer': $('#customer').val(),
  //  'tanggal_faktur': $('#tanggal_faktur_fb').val(),
   'tanggal_bayar': $('#tanggal_bayar_fb').val(),
   'jumlah': $('input#jumlah').val(),
   'invoice_item': Payment.getPostInvoiceItem()
  };

  return data;
 },

 simpan: function (id) {
  var data = Payment.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Payment.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Payment.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Payment.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Payment.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Payment.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Payment.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setSelect2: function () {
  $("#customer").select2();
  $("#pembeli").select2();
  $("#metode").select2();
  $("#pajak").select2();

  var tr_product = $('table#tb_product').find('tbody').find('tr');
  $.each(tr_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    var data_id = $(this).attr('data_id');
    if (data_id != '') {
     var index = $(this).index();
     $(this).find('td:eq(0)').find('select').select2();
     $(this).find('td:eq(1)').find('select').select2();
     $(this).find('td:eq(2)').find('select').select2();
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal_faktur').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
   autoclose: true,
  });
  $('input#tanggal_bayar').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
   autoclose: true,
  });
  $('input#tanggal_faktur_fb').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
   autoclose: true,
  });
  $('input#tanggal_bayar_fb').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
   autoclose: true,
  });
 },

 addItem: function (elm, e) {
  e.preventDefault();
  var tr = $(elm).closest('tbody').find('tr:last');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: tr.index()
   },
   async: false,
   url: url.base_url(Payment.module()) + "addItem",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var tr = $(elm).closest('tbody').find('tr:last');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 hitungSubTotal: function (elm, jenis) {
  var tr = $(elm).closest('tr');

  var option_product = tr.find('td:eq(0)').find('select').find('option');
  var harga = 0;
  $.each(option_product, function () {
   if ($(this).is(':selected')) {
    harga = $(this).attr('harga');
   }
  });

  var option_pajak = tr.find('td:eq(1)').find('select').find('option');
  var persentase = 0;
  $.each(option_pajak, function () {
   if ($(this).is(':selected')) {
    persentase = $(this).attr('persentase');
   }
  });

  var jumlah = parseInt(tr.find('td:eq(3)').find('input').val());
  var sub_total_content = tr.find('td:eq(4)');

  var potong_pajak = (jumlah * harga * persentase) / 100;
  var sub_total = (jumlah * harga) - potong_pajak;
  sub_total_content.find('label#sub_total').text(sub_total);

  Payment.hitungTotal();
 },

 hitungTotal: function () {
  var tb_item = $('table#tb_item').find('tbody').find('tr');
//  console.log(tb_product);
  var total = 0;
  $.each(tb_item, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    if (!$(this).hasClass('deleted')) {
     var input_check = $(this).find('input#check');
     if (input_check.is(':checked')) {
      var sub_total = $(this).find('td:eq(2)').text().toString();
      sub_total = sub_total.replace(',', '');
      sub_total = parseInt(sub_total);
      total += sub_total;
     }
    }
   }
  });

  $('label#total').text(total);
  $('label#total').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 deleteItem: function (elm) {
  var data_id = $(elm).closest('tr').attr('data_id');
  if (data_id != '') {
   var tr = $('table#tb_product').find('tbody').find('tr[data_id="' + data_id + '"]');
   var metode = tr.find('td:eq(2)').find('select').val();
   tr.addClass('hide');
   tr.addClass('deleted');
   if (metode == 2) {
    tr.next().addClass('hide');
    tr.next().addClass('deleted');
   }
  } else {
   $(elm).closest('tr').remove();
  }

  Payment.hitungTotal();
 },

 getMetodeBayar: function (elm) {
  var metode = $(elm).val();
  var index = $(elm).closest('tr').index();
  switch (metode) {
   case "2":
    $.ajax({
     type: 'POST',
     data: {
      metode: metode,
      index: index
     },
     dataType: 'html',
     async: false,
     url: url.base_url(Payment.module()) + "getMetodeBayar",
     error: function () {
      toastr.error("Gagal");
     },

     beforeSend: function () {

     },

     success: function (resp) {
      bootbox.dialog({
       message: resp
      });
     }
    });
    break;

   default:

    break;
  }
 },

 pilihBank: function (elm) {
  message.closeDialog();
  var index_row = $(elm).attr('index_row');
  var tb_product = $('table#tb_product').find('tbody');
  var tr_product = tb_product.find('tr:eq(' + index_row + ')');

  var nama_bank = $(elm).closest('tr').find('td:eq(0)').text();
  var akun = $(elm).closest('tr').find('td:eq(2)').text();
  var no_rek = $(elm).closest('tr').find('td:eq(1)').text();
  var data_id_bank = $(elm).closest('tr').attr('data_id');
  var tr_bank = '<tr data_bank="' + data_id_bank + '">';
  tr_bank += '<td colspan="7">';
  tr_bank += '[' + nama_bank + '] - [' + akun + '] - [' + no_rek + ']';
  tr_bank += '</td>';
  tr_bank += '</tr>';
  tr_product.after(tr_bank);
 },

 cetak: function (id) {
  window.open(url.base_url(Payment.module()) + "printFaktur/" + id);
 },

 getDetailInvoice: function (elm) {
  var customer = $(elm).val();
  $.ajax({
   type: 'POST',
   data: {
    customer: customer
   },
   dataType: 'json',
   async: false,
   url: url.base_url(Payment.module()) + "getDetailInvoice",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {

   },

   success: function (resp) {
//    $('input#tanggal_faktur').val(resp.invoice.tanggal_faktur);
//    $('input#tanggal_bayar').val(resp.invoice.tanggal_bayar);
//    $('div#content-product').html(resp.view_item);
//    $('label#total').html(resp.invoice.total);    
//    $('label#total').attr('total', resp.invoice.total_ori);
//    Payment.hitungSisaBayar(elm);
    $('div#content-invoice').html(resp.view_item);
   }
  });
 },

 hitungSisaBayar: function (elm) {
  var jumlah = $('input#jumlah').val();
  var total = $('label#total').attr('total');
  var sisa_content = $('input#sisa');

  var sisa = jumlah - total;
  sisa_content.val(sisa);
//  $('#sisa').divide({
//   delimiter: '.',
//   divideThousand: true
//  });
 },

 checkAll: function (elm) {
  var is_checked = $(elm).is(':checked');
  $('input.check_invoice').prop('checked', is_checked);
  Payment.hitungTotal();
 },

 checkedInvoice: function (elm) {
  var check_detail = $('input.check_invoice');
  var total = check_detail.length;
  var checked = 0;

  $.each(check_detail, function () {
   if ($(this).is(':checked')) {
    checked += 1;
   }
  });

  if (checked == total) {
   $('input#check_all').prop('checked', true);
  } else {
   $('input#check_all').prop('checked', false);
  }

  Payment.hitungTotal();
 },
};

$(function () {
 Payment.setDate();
 Payment.setSelect2();
 Payment.setThousandSparator();
});
