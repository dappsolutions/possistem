var Internal = {
 module: function () {
  return 'kerja_sama_internal';
 },

 back: function () {
  window.location.href = url.base_url(Internal.module()) + "index";
 },

 edit: function () {
  window.location.href = url.base_url(Internal.module()) + "edit";
 },

 getPostData: function () {
  var data = [];
  var tr = $('table').find('tbody').find('tr');
  $.each(tr, function () {
   data.push({
    'id': $(this).attr('id'),
    'keterangan': $(this).find('input#keterangan').val(),
    'presentase': $(this).find('input#presentase').val()
   });
  });

  return data;
 },

 validationData: function () {
  var is_valid = true;
  var tr = $('table').find('tbody').find('tr');
  var jumlah = 0;
  $.each(tr, function () {
   var presentase = $.trim($(this).find('input#presentase').val());
   presentase = isNaN(parseInt(presentase)) ? 0 : parseInt(presentase);

   jumlah += presentase;
  });

  if (jumlah > 100) {
   is_valid = false;
   toastr.error("Presentase Melebihi 100 %");
  }

  return is_valid;
 },

 simpan: function (id) {
  var data = Internal.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));

  if (validation.run() && Internal.validationData()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Internal.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
//     console.log(resp);
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Internal.module()) + "index";
      };

      setTimeout(reload(), 1000);
     } else {
      console.log('gagal');
      toastr.error("Gagal Disimpan, " + resp.message);
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Internal.module()) + "ubah/" + id;
 },

 addRow: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  newTr.find('input').val('');
  newTr.find('td:eq(2)').html('<i class="mdi mdi-minus mdi-24px hover" onclick="Internal.removeRow(this)"></i>');
  tr.after(newTr);
 },

 removeRow: function (elm) {
  var tr = $(elm).closest('tr');
  tr.remove();
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Internal.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Internal.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 }
};