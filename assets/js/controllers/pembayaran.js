var Pembayaran = {
 module: function () {
  return 'pembayaran';
 },

 add: function () {
  window.location.href = url.base_url(Pembayaran.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Pembayaran.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Pembayaran.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Pembayaran.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'jenis': $('input#jenis').val(),
   'keterangan': $('#keterangan').val(),
   'pembayaran': $('#pembayaran').val(),
   'tgl_bayar': $('#tgl_bayar').val(),
   'jenis_pembayaran': $('#jenis_pembayaran').val()
  };

  return data;
 },

 simpanLain: function (id) {
  var data = Pembayaran.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Pembayaran.module()) + "simpanLain",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Pembayaran.module()) + "detail" + '/' + resp.lain;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 getPostDataTagihan: function () {
  var data = {
   'tagihan': $('select#tagihan').val(),
   'keterangan': $('#keterangan').val(),
   'pembayaran': $('#pembayaran').val(),
   'tgl_bayar': $('#tgl_bayar').val(),
   'jenis_pembayaran': $('#jenis_pembayaran').val(),
  };

  return data;
 },

 getPostDataVendor: function () {
  var data = {
   'vendor': $('select#vendor').val(),
   'keterangan': $('#keterangan').val(),
   'pembayaran': $('#pembayaran').val(),
   'tgl_bayar': $('#tgl_bayar').val(),
   'jenis_pembayaran': $('#jenis_pembayaran').val(),
  };

  return data;
 },

 simpanTagihan: function (id) {
  var data = Pembayaran.getPostDataTagihan();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Pembayaran.module()) + "simpanTagihan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Pembayaran.module()) + "detailTagihan" + '/' + resp.tagihan;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 simpanVendor: function (id) {
  var data = Pembayaran.getPostDataVendor();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append('file', $('input#file').prop('files')[0]);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Pembayaran.module()) + "simpanVendor",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Pembayaran.module()) + "detailVendor" + '/' + resp.vendor;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Pembayaran.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Pembayaran.module()) + "detail/" + id;
 },

 getPembayaranForm: function () {
  $.ajax({
   type: 'POST',
   data: {
    'metode': $('select#metode_bayar').val()
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Pembayaran.module()) + "formPembayaran",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },
   beforeSend: function () {
    message.loadingProses('Proses Retrieving Form Pembayaran ...');
   },

   success: function (resp) {
    message.closeLoading();
    $('div.form_pembayaran').html(resp);
    Pembayaran.setDatePickerTglBayar();
   }
  });
 },

 setDatePickerTglBayar: function () {
  $('input#tgl_bayar').datepicker({
   dateFormat: 'yy-mm-dd'
  });
 },

 reloadPage: function () {
  window.location.reload();
 },

};