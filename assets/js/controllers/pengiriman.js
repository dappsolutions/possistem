var Pengiriman = {
 module: function () {
  return 'pengiriman';
 },

 add: function () {
  window.location.href = url.base_url(Pengiriman.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Pengiriman.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Pengiriman.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Pengiriman.module()) + "index";
   }
  }
 },

 getPostItemProduct: function () {
  var data = [];
  var tb_product = $('table#tb_product').find('tbody').find('tr');
  $.each(tb_product, function () {
   var invoice_product = $(this).attr('data_id');
   var product_satuan = $(this).attr('product_satuan');
   data.push({
				invoice_product: invoice_product,
				product_satuan: product_satuan,
				qty: $.trim($(this).find('td:eq(3)').text())
   });
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'filter_by': $('#filter_by').val(),
   'tanggal': $('#tanggal').val(),
   'sopir': $('#sopir').val(),
   'filter_data': $('#filter_data').val(),
   'no_polisi': $('#no_polisi').val(),
   'total': $('#total').text(),
   'tanggal_order': $('#tanggal_order').val(),
   'data_item': Pengiriman.getPostItemProduct()
  };

  return data;
 },

 simpan: function (id) {
  var data = Pengiriman.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Pengiriman.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Pengiriman.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Pengiriman.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Pengiriman.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Pengiriman.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Pengiriman.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal').datepicker({
   format: 'yyyy-mm-dd',   
   todayHighlight: true,
   autoclose:true
  });
  
  $('#tanggal_order').daterangepicker({
   locale:{
    format: 'YYYY-MM-DD'
   }
  });
 },

 setSelect2: function () {
  $("#invoice").select2();
 },

 cetak: function (id) {
  window.open(url.base_url(Pengiriman.module()) + "printFaktur/" + id);
 },

 getDataListProduct: function (elm) {
  var formData = new FormData();
  formData.append('filter_by', $('#filter_by').val());
  formData.append('filter_data', $('#filter_data').val());
  formData.append('tanggal_order', $('#tanggal_order').val());


  $.ajax({
   type: 'POST',
   data: formData,
   dataType: 'json',
   async: false,
   processData: false,
   contentType: false,
   url: url.base_url(Pengiriman.module()) + "getDataListProduct",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    $('div.form-item').html(resp.view_item);
    $('label#total').text(resp.total);
   }
  });
 },

 getFilterBy: function (elm) {
  var filter_by = $(elm).val();

  $.ajax({
   type: 'POST',
   data: {
    filter_by: filter_by
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Pengiriman.module()) + "getFilterBy",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieving Data...");
   },

   success: function (resp) {
    message.closeLoading();
    $('div#content_filter').html(resp);
    $('select#filter_data').select2();
   }
  });
 },

 cetakFaktur: function () {
  printJS({
   printable: 'konten_faktur',
   type: 'html',
   targetStyles: ['*']
  });
 }
};

$(function () {
 Pengiriman.setDate();
 Pengiriman.setSelect2();
 Pengiriman.setThousandSparator();
});
