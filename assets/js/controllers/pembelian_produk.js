var PembelianProduk = {
 module: function () {
  return 'pembelian_produk';
 },

 add: function () {
  window.location.href = url.base_url(PembelianProduk.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(PembelianProduk.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(PembelianProduk.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(PembelianProduk.module()) + "index";
   }
  }
 },

 getDetailBeli: function () {
  var tble = $('table#list_product_taken').find('tbody').find('tr');
  var data = [];
  if (tble.length > 1) {
   $.each(tble, function () {
    if ($(this).hasClass('detail_product')) {
     var id = $(this).attr('id');
     var status_pembelian = $(this).find('#status_pembelian').val();
     var angsuran = $('tr.anguran_product' + id + '').attr('id');
     var syarat = $('tr.anguran_product' + id + '').find('#syarat_text').attr('syarat_id');

     data.push({
      'product': id,
      'status_pembelian': status_pembelian,
      'angsuran': angsuran,
      'syarat': syarat
     });
    }
   });
  } else {
   toastr.error("Belum Ada Produk Dipilih");
  }

  return data;
 },

 getDetailPostSyarat: function(){
  var data = [];
  var tr = $('tbody.persyaratan').find('tr');
  $.each(tr, function () {
   var id = $(this).attr('id');
   if(id != "empty_syarat"){
    data.push({
     'id': id,   
     'status': $(this).attr('status')
    });
   }
  });
  return data;
 },
 
 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'customer': $('#customer').val(),
   'nama': $('#nama_customer').val(),
   'no_hp': $('#no_hp').val(),
   'alamat': $('#alamat').val(),
   'email': $('#email').val(),
   'identitas': $('#identitas').val(),
   'no_identitas': $('#no_identitas').val(),
   'jatuh_tempo':$('#tgl_jatuh_tempo').val(),
   'detail': PembelianProduk.getDetailBeli(),
   'detail_syarat': PembelianProduk.getDetailPostSyarat()
  };

  return data;
 },

 proses: function (id) {
  var data = PembelianProduk.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  var inputBerkas = $('input[type="file"]');
  var i = 1;
  $.each(inputBerkas, function () {
   formData.append('berkas' + i, $(this).prop('files')[0]);
   i += 1;
  });
  
  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(PembelianProduk.module()) + "proses",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(PembelianProduk.module()) + "detail" + '/' + resp.pembelian;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(PembelianProduk.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(PembelianProduk.module()) + "detail/" + id;
 },

 getCustomer: function (elm) {
  var vlue = $(elm).val();
  if (vlue == "baru") {
   $('div.data_customer').removeClass('hide');
   $('#nama_customer').removeAttr('disabled');
   $('#no_hp').removeAttr('disabled');
   $('#alamat').removeAttr('disabled');
   $('#email').removeAttr('disabled');

   $.ajax({
    type: 'POST',
    data: {},
    dataType: 'html',
    async: false,
    url: url.base_url(PembelianProduk.module()) + "getListIDentitasData",
    error: function () {
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Retrieving Data..");
    },

    success: function (resp) {
     $('div.identitas_data').html(resp);
     message.closeLoading();
    }
   });
  } else {
   if (vlue != '') {
    $.ajax({
     type: 'POST',
     dataType: 'json',
     async: false,
     url: url.base_url(PembelianProduk.module()) + "getCustomer/" + vlue,
     error: function () {
      message.closeLoading();
     },

     beforeSend: function () {
      message.loadingProses("Proses Retrieving Data..");
     },

     success: function (resp) {
      console.log(resp);
      $('div.data_customer').removeClass('hide');
      $('#nama_customer').val(resp.nama).attr('disabled', 'disabled');
      $('#no_hp').val(resp.no_hp).attr('disabled', 'disabled');
      $('#alamat').val(resp.alamat).attr('disabled', 'disabled');
      $('#email').val(resp.email).attr('disabled', 'disabled');
//      $('div.identitas_data').html(resp.view);
      message.closeLoading();
     }
    });
   } else {
    $('div.data_customer').addClass('hide');
    $('#nama_customer').removeAttr('disabled');
    $('#no_hp').removeAttr('disabled');
    $('#alamat').removeAttr('disabled');
    $('#email').removeAttr('disabled');
   }
  }
 },

 moduleProduk: function () {
  return "produk_product";
 },

 detailProduk: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(PembelianProduk.module()) + "detailProduk/" + id,
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
//  window.open(url.base_url(PembelianProduk.moduleProduk()) + "detail/" + id);
 },

 addProduk: function (id, elm) {
  if (!$('tr#empty_produk').hasClass('hide')) {
   $('tr#empty_produk').addClass('hide');
  }

  var status_pembelian = "<select id='status_pembelian' class='form-control' onchange='PembelianProduk.getAngsuran(this)'>";
  status_pembelian += "<option value='1'>CASH</option>";
  status_pembelian += "<option value='2'>KREDIT</option>";
  status_pembelian += "</select>";

  var html = '<tr class="product_' + id + ' detail_product" id="' + id + '">';
  html += '<td>' + $.trim($(elm).closest('tr').find('td:eq(1)').text()) + '</td>';
  html += '<td>' + $.trim($(elm).closest('tr').find('td:eq(2)').text()) + '</td>';
  html += '<td>' + $.trim($(elm).closest('tr').find('td:eq(3)').text()) + '</td>';
  html += '<td>';
  html += status_pembelian
  html += '</td>';
  html += '<td class="text-center">';
  html += '<button id="" class="btn btn-succes-baru font12" ';
  html += 'onclick="PembelianProduk.cancel(this)">Batal</button>';
  html += '&nbsp;';
  html += '</td>';
  html + '</tr>';

  html += "<tr class='anguran_product" + id + "' id=''>";
  html += "<td colspan='5'>Angsuran yang dipilih : <b>Tidak Ada</b></td>";
  html += "</tr>";
  var table = $('table#list_product_taken').find('tbody');
  table.append(html);
 },

 cancel: function (elm) {
  var id = $(elm).closest('tr').attr('id');
  $(elm).closest('tr').remove();
  $('tr.anguran_product' + id + '').remove();

  if ($('table#list_product_taken').find('tr').length == 2) {
   $('tr#empty_produk').removeClass('hide');
  }
 },

 searchInTable: function (elm) {
  var value = $(elm).val();
  $("table#list_product tbody tr").filter(function () {
   $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
  });
 },

 getAngsuran: function (elm) {
  var id_product = $(elm).closest('tr').attr('id');
  if ($(elm).val() != 1) {
   $.ajax({
    type: 'POST',
    dataType: 'html',
    async: false,
    url: url.base_url(PembelianProduk.module()) + "getAngsuran/" + id_product,

    success: function (resp) {
     bootbox.dialog({
      message: resp,
      size: 'large'
     });
    }
   });
  }
 },

 getPersyaratan: function () {
  var data = $('select#syarat').find('option');
  var syarat_data = [];
  $.each(data, function () {
   if ($(this).is(':selected')) {
    syarat_data.push({
     'syarat': $(this).text(),
     'id': $(this).attr('value')
    });
   }
  });
  return syarat_data;
 },

 getDetailSyarat: function (elm) {
  var persyaratan = $(elm).val();
  if (persyaratan != '') {
   $.ajax({
    type: 'POST',
    dataType: 'html',
    async: false,
    url: url.base_url(PembelianProduk.module()) + "getDetailSyarat/" + persyaratan,
    error: function () {
     toastr.error("Gagal");
    },

    success: function (resp) {
     $('tbody.persyaratan').html(resp);
    }
   });
  } else {
   var html = "<tr class='' id='empty_syarat'>";
   html += '<td colspan="3" class="text-center">Belum Ada Persyaratan Dipilih</td>';
   html += "</tr>";
   $('tbody.persyaratan').html(html);
  }
 },

 pilihAngsuran: function (elm) {
  var product = $(elm).closest('tr').attr('product');
  var id_angsuran = $(elm).closest('tr').attr('id');

  var tenor = $.trim($(elm).closest('tr').find('td:eq(2)').text());
  var harga = $.trim($(elm).closest('tr').find('td:eq(1)').text());
  

  var html = "Angsuran yang dipilih : <b>" + tenor + "</b> (" + harga + ")<br/>";
  $('tr.anguran_product' + product + '').attr('id', id_angsuran);
  $('tr.anguran_product' + product + '').find('td').html(html);
  message.closeDialog();
 },

 showAnsuran: function (elm) {
  var id = $(elm).closest('tr').attr('id');
  if ($('tr.detail_ansuran' + id).hasClass('hide')) {
   $('tr.detail_ansuran' + id).removeClass('hide');
  } else {
   $('tr.detail_ansuran' + id).addClass('hide');
  }
 },

 moduleProduk: function () {
  return 'produk';
 },

 listFotoProduk: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(PembelianProduk.moduleProduk()) + "listFotoProduk/" + id,
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 detailSyarat: function (persyaratan) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(PembelianProduk.module()) + "detailSyarat/" + persyaratan,
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },
 
 setDateJatuhTempo: function(){
  $('input#tgl_jatuh_tempo').datepicker({
   dateFormat: 'dd'
  });
 }
};

$(function(){
 PembelianProduk.setDateJatuhTempo();
});