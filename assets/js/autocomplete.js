//Module yang memakai Event Autocomplete



//Permintaan Barang
var pBarang = {
    module: function(){
        return 'permintaan/Permintaan_barang';
    },

    checkStrtusItem : function(elm, value){
        var item_name = value;
        $.ajax({
            type: 'POST',
            data: {
                'item_name': item_name
            },
            dataType: 'json',
            async: false,
            url: url.base_url(pBarang.module()) + 'checkStatusItem',
            beforeSend: function () {
                message.loadingProses("Mohon Tunggu..");
            },
            success: function (resp) {
                console.log(resp);
                $(elm).closest('tr').find('.input-status').val(resp.item.status);
                $(elm).closest('tr').find('.input-item-code').val(resp.item.item_code);

                if (resp.item.status == 'Available') {
                    $(elm).closest('tr').find('.input-status').addClass('lb-success').text(resp.item.status);
                } else {
                    $(elm).closest('tr').find('.input-status').addClass('lb-danger').text("Not Available");
                }

                if (parseInt(resp.item.kg_nett) <= 0 || resp.item.kg_nett == '.00000') {
                    $(elm).closest('tbody').find('label#message:last').removeClass('display-none');
                    $(elm).closest('tbody').find('tr#input-1:last').attr('data-status', '0');
                    $(elm).closest('tr').find('.input-stock').val('0');
                    $(elm).closest('tr').find('.input-stock').text('0');
                    $('#nomor-draft').val(resp.dpp.id);
                    $('.btn-save-usage').addClass('display-none');
                    $('.btn-save').removeClass('display-none');
                } else {
                    $(elm).closest('tbody').find('label#message:last').addClass('display-none');
                    $(elm).closest('tbody').find('tr#input-1:last').attr('data-status', '1');
                    $(elm).closest('tr').find('.input-stock').val(parseInt(resp.item.kg_nett));
                    $(elm).closest('tr').find('.input-stock').text(parseInt(resp.item.kg_nett));
                    $('.btn-save').addClass('display-none');
                    $('.btn-save-usage').removeClass('display-none');
                }
                /*PermintaanBarang.checkRequestStatus(function(data){
                console.log("masuk");
                console.log(data);
            });*/
            //---- check status ---
            var status_request_avail = 0;
            var status_request_empty = 0;
            $.each($('tr#input-1'), function () {
                console.log($(this));
                if ($(this).attr('data-status') == 1) {
                    status_request_avail = 1;
                } else {
                    status_request_empty = 1
                }
            });
            console.log(status_request_avail);
            console.log(status_request_empty);
            if ((status_request_avail == 1 && status_request_empty == 0) || (status_request_avail == 0 && status_request_empty == 1)) {
                message.success(".message", "Data Valid");
                $('.btn-save').removeAttr('disabled');
                $('.btn-save-usage').removeAttr('disabled');

            } else {
                message.error(".message", "Jenis barang harus sama");
                //$('.btn-save').attr('disabled', '');

            }
            //---- end check status ---
            message.closeLoading();
        }
    });
}
};
//--------------------------------------------------------------------


//Library Auocomplete
function autocomplete(inp, arr, modules) {
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) {
            return false;
        }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        //      console.log(arr.length);
        for (i = 0; i < arr.length; i++) {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i]['item'].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i]['item'].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i]['item'].substr(val.length);
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i]['item'] + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function (e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;

                    switch (modules) {
                        case 'permintaan_barang':
                        if($(inp).attr('error') != 'Package'){
                            pBarang.checkStrtusItem(inp, inp.value);
                        }
                        break;

                        default:

                        break;
                    }
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x)
        x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x)
                x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x)
        return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length)
        currentFocus = 0;
        if (currentFocus < 0)
        currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}
