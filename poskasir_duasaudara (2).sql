-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 05, 2021 at 07:17 PM
-- Server version: 5.7.34-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poskasir_duasaudara`
--

-- --------------------------------------------------------

--
-- Table structure for table `ansuran`
--

CREATE TABLE `ansuran` (
  `id` int(11) NOT NULL,
  `periode_tahun` varchar(50) DEFAULT NULL,
  `ansuran` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `akun` varchar(255) DEFAULT NULL,
  `nama_bank` varchar(255) DEFAULT NULL,
  `no_rekening` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coa`
--

CREATE TABLE `coa` (
  `id` int(11) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coa_type`
--

CREATE TABLE `coa_type` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `data_notifikasi_tagihan`
--

CREATE TABLE `data_notifikasi_tagihan` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `message` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `jenis` varchar(100) DEFAULT NULL COMMENT 'FAKTUR\nRETUR\nPENGADAAN',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document_pengiriman`
--

CREATE TABLE `document_pengiriman` (
  `id` int(11) NOT NULL,
  `no_dokumen` varchar(155) DEFAULT NULL,
  `tgl_pengiriman` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_pengiriman_status`
--

CREATE TABLE `dokumen_pengiriman_status` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'SENT\nBROKE\nRECEIVED',
  `document_pengiriman` int(11) NOT NULL,
  `keterangan` varchar(155) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `nama` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `id` int(11) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `logo` text,
  `alamat` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`id`, `title`, `logo`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Toko Bangunan', NULL, 'Suruhwadang', '2021-08-06', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `id` int(11) NOT NULL,
  `kode` varchar(155) DEFAULT NULL,
  `nama_gudang` varchar(255) DEFAULT NULL,
  `panjang` int(11) DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `tinggi` int(11) DEFAULT NULL,
  `max_tonase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`id`, `kode`, `nama_gudang`, `panjang`, `lebar`, `tinggi`, `max_tonase`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '001', 'UMUM', 12, 12, 12, 100, '2021-08-07', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hari`
--

CREATE TABLE `hari` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `indetitas`
--

CREATE TABLE `indetitas` (
  `id` int(11) NOT NULL,
  `identitas` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investor`
--

CREATE TABLE `investor` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `alamat` text,
  `keterangan` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `potongan` int(11) NOT NULL,
  `metode_bayar` int(11) NOT NULL,
  `ref` int(11) DEFAULT NULL COMMENT 'oder table',
  `pot_faktur` int(11) DEFAULT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal_faktur` datetime DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `no_rekening` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0',
  `uang_kembali` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `pembeli`, `potongan`, `metode_bayar`, `ref`, `pot_faktur`, `no_faktur`, `tanggal_faktur`, `tanggal_bayar`, `no_rekening`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`, `uang_kembali`) VALUES
(1, 1, 3, 1, 1, 0, 'INV21AUG001', '2021-08-17 08:06:34', '2021-08-17', NULL, 24000, '2021-08-17', 3, NULL, NULL, 0, NULL),
(2, 1, 3, 1, 2, 0, 'INV21AUG002', '2021-08-17 08:33:36', '2021-08-17', NULL, 24000, '2021-08-17', 3, NULL, NULL, 0, NULL),
(3, 1, 3, 1, 4, 0, 'INV21AUG003', '2021-08-17 08:41:54', '2021-08-17', NULL, 24000, '2021-08-17', 3, NULL, NULL, 0, NULL),
(4, 1, 3, 1, 5, 0, 'INV21AUG004', '2021-08-17 08:45:25', '2021-08-17', NULL, 24000, '2021-08-17', 3, NULL, NULL, 0, NULL),
(5, 1, 3, 1, 8, 0, 'INV21SEP001', '2021-09-30 18:10:07', '2021-09-30', NULL, 12000, '2021-09-30', 3, NULL, NULL, 0, NULL),
(6, 1, 3, 1, 9, 0, 'INV21OCT001', '2021-10-07 19:14:00', '2021-10-07', NULL, 26000, '2021-10-07', 3, NULL, NULL, 0, NULL),
(7, 1, 3, 1, 10, 0, 'INV21NOV001', '2021-11-01 19:54:20', '2021-11-01', NULL, 10000, '2021-11-01', 3, NULL, NULL, 0, NULL),
(8, 1, 3, 1, 11, 0, 'INV21NOV002', '2021-11-05 19:02:13', '2021-11-05', NULL, 26000, '2021-11-05', 3, NULL, NULL, 0, NULL),
(9, 1, 3, 1, 12, 0, 'INV21NOV003', '2021-11-05 19:15:23', '2021-11-05', NULL, 26000, '2021-11-05', 3, NULL, NULL, 0, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_pot_product`
--

CREATE TABLE `invoice_pot_product` (
  `id` int(11) NOT NULL,
  `invoice_product` int(11) NOT NULL,
  `nilai` int(11) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_print`
--

CREATE TABLE `invoice_print` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `cetak` int(11) DEFAULT NULL,
  `approve` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_product`
--

CREATE TABLE `invoice_product` (
  `id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `bank` int(11) DEFAULT '0',
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_product`
--

INSERT INTO `invoice_product` (`id`, `invoice`, `product_satuan`, `bank`, `qty`, `sub_total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1675, 0, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(2, 2, 1675, 0, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(3, 3, 1675, 0, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(4, 4, 1675, 0, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(5, 5, 1684, 0, 1, 12000, '2021-09-30', 3, NULL, NULL, 0),
(6, 6, 1675, 0, 1, 26000, '2021-10-07', 3, NULL, NULL, 0),
(7, 7, 2511, 0, 1, 10000, '2021-11-01', 3, NULL, NULL, 0),
(8, 8, 1675, 0, 1, 26000, '2021-11-05', 3, NULL, NULL, 0),
(9, 9, 1675, 0, 1, 26000, '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_sisa`
--

CREATE TABLE `invoice_sisa` (
  `id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_sisa`
--

INSERT INTO `invoice_sisa` (`id`, `invoice`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 0, '2021-08-17', 3, NULL, NULL, 0),
(2, 2, 0, '2021-08-17', 3, NULL, NULL, 0),
(3, 3, 0, '2021-08-17', 3, NULL, NULL, 0),
(4, 4, 0, '2021-08-17', 3, NULL, NULL, 0),
(5, 5, 0, '2021-09-30', 3, NULL, NULL, 0),
(6, 6, 0, '2021-10-07', 3, NULL, NULL, 0),
(7, 7, 0, '2021-11-01', 3, NULL, NULL, 0),
(8, 8, 0, '2021-11-05', 3, NULL, NULL, 0),
(9, 9, 0, '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_status`
--

CREATE TABLE `invoice_status` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'PAID\nDRAFT\nVALID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_status`
--

INSERT INTO `invoice_status` (`id`, `user`, `invoice`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3, 1, 'PAID', '2021-08-17', 3, NULL, NULL, 0),
(2, 3, 2, 'PAID', '2021-08-17', 3, NULL, NULL, 0),
(3, 3, 3, 'PAID', '2021-08-17', 3, NULL, NULL, 0),
(4, 3, 4, 'PAID', '2021-08-17', 3, NULL, NULL, 0),
(5, 3, 5, 'PAID', '2021-09-30', 3, NULL, NULL, 0),
(6, 3, 6, 'PAID', '2021-10-07', 3, NULL, NULL, 0),
(7, 3, 7, 'PAID', '2021-11-01', 3, NULL, NULL, 0),
(8, 3, 8, 'PAID', '2021-11-05', 3, NULL, NULL, 0),
(9, 3, 9, 'PAID', '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_akad`
--

CREATE TABLE `jenis_akad` (
  `id` int(11) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_notif_pengiriman`
--

CREATE TABLE `jenis_notif_pengiriman` (
  `id` int(11) NOT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal`
--

CREATE TABLE `jurnal` (
  `id` int(11) NOT NULL,
  `no_jurnal` varchar(155) DEFAULT NULL,
  `ref` varchar(155) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_detail`
--

CREATE TABLE `jurnal_detail` (
  `id` int(11) NOT NULL,
  `jurnal` int(11) NOT NULL,
  `jurnal_struktur` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_struktur`
--

CREATE TABLE `jurnal_struktur` (
  `id` int(11) NOT NULL,
  `feature` int(11) NOT NULL,
  `coa` int(11) NOT NULL,
  `coa_type` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kas`
--

CREATE TABLE `kas` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon`
--

CREATE TABLE `kasbon` (
  `id` int(11) NOT NULL,
  `no_kasbon` varchar(155) DEFAULT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_paid`
--

CREATE TABLE `kasbon_paid` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_payment`
--

CREATE TABLE `kasbon_payment` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_payment_item`
--

CREATE TABLE `kasbon_payment_item` (
  `id` int(11) NOT NULL,
  `kasbon_payment` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_sisa`
--

CREATE TABLE `kasbon_sisa` (
  `id` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_status`
--

CREATE TABLE `kasbon_status` (
  `id` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nPAID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_akad`
--

CREATE TABLE `kategori_akad` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_product`
--

CREATE TABLE `kategori_product` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_product`
--

INSERT INTO `kategori_product` (`id`, `kategori`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UMUM', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kerja_sama_internal`
--

CREATE TABLE `kerja_sama_internal` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `presentase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_user_position`
--

CREATE TABLE `log_user_position` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `metode_bayar`
--

CREATE TABLE `metode_bayar` (
  `id` int(11) NOT NULL,
  `metode` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metode_bayar`
--

INSERT INTO `metode_bayar` (`id`, `metode`) VALUES
(1, 'CASH'),
(2, 'TRANSFER'),
(3, 'KREDIT');

-- --------------------------------------------------------

--
-- Table structure for table `metode_pembayaran`
--

CREATE TABLE `metode_pembayaran` (
  `id` int(11) NOT NULL,
  `metode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `minggu`
--

CREATE TABLE `minggu` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notif_has_jenis_pengiriman`
--

CREATE TABLE `notif_has_jenis_pengiriman` (
  `id` int(11) NOT NULL,
  `notif_jatuh_tempo` int(11) NOT NULL,
  `jenis_notif_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notif_jatuh_tempo`
--

CREATE TABLE `notif_jatuh_tempo` (
  `id` int(11) NOT NULL,
  `nama_notif` varchar(150) DEFAULT NULL,
  `jadwal_notif` int(11) DEFAULT NULL COMMENT 'Jumlah Hari',
  `jenis_notif` varchar(1) DEFAULT NULL COMMENT '+ = Lebih\n- = Kurang',
  `status` int(11) DEFAULT '0' COMMENT '0 : Tidak Aktif\n1 : Aktif',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `no_order` varchar(155) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `pot_faktur` int(11) DEFAULT NULL,
  `tanggal_faktur` datetime DEFAULT NULL,
  `metode_bayar` int(11) NOT NULL,
  `no_rekening` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `pembeli`, `no_order`, `potongan`, `pot_faktur`, `tanggal_faktur`, `metode_bayar`, `no_rekening`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'OR21AUG001', 3, 0, '2021-08-17 08:06:34', 1, NULL, 24000, '2021-08-17', 3, NULL, NULL, 0),
(2, 1, 'OR21AUG002', 3, 0, '2021-08-17 08:33:36', 1, NULL, 24000, '2021-08-17', 3, NULL, NULL, 0),
(3, 1, 'OR21AUG003', 3, 0, '2021-08-17 08:40:57', 1, NULL, 24000, '2021-08-17', 3, NULL, NULL, 0),
(4, 1, 'OR21AUG004', 3, 0, '2021-08-17 08:41:54', 1, NULL, 24000, '2021-08-17', 3, NULL, NULL, 0),
(5, 1, 'OR21AUG005', 3, 0, '2021-08-17 08:45:25', 1, NULL, 24000, '2021-08-17', 3, NULL, NULL, 0),
(6, 1, 'OR21AUG006', 3, 0, '2021-08-23 08:17:56', 1, NULL, 24000, '2021-08-23', 3, NULL, NULL, 0),
(7, 1, 'OR21AUG007', 3, 0, '2021-08-23 08:22:45', 1, NULL, 24000, '2021-08-23', 3, NULL, NULL, 0),
(8, 1, 'OR21SEP001', 3, 0, '2021-09-30 18:10:07', 1, NULL, 12000, '2021-09-30', 3, NULL, NULL, 0),
(9, 1, 'OR21OCT001', 3, 0, '2021-10-07 19:14:00', 1, NULL, 26000, '2021-10-07', 3, NULL, NULL, 0),
(10, 1, 'OR21NOV001', 3, 0, '2021-11-01 19:54:20', 1, NULL, 10000, '2021-11-01', 3, NULL, NULL, 0),
(11, 1, 'OR21NOV002', 3, 0, '2021-11-05 19:02:13', 1, NULL, 26000, '2021-11-05', 3, NULL, NULL, 0),
(12, 1, 'OR21NOV003', 3, 0, '2021-11-05 19:15:23', 1, NULL, 26000, '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_pot_product`
--

CREATE TABLE `order_pot_product` (
  `id` int(11) NOT NULL,
  `order_product` int(11) NOT NULL,
  `nilai` int(11) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `product_satuan`, `order`, `qty`, `sub_total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1675, 1, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(2, 1675, 2, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(3, 1675, 3, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(4, 1675, 4, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(5, 1675, 5, 1, 24000, '2021-08-17', 3, NULL, NULL, 0),
(6, 1675, 6, 1, 24000, '2021-08-23', 3, NULL, NULL, 0),
(7, 1675, 7, 1, 24000, '2021-08-23', 3, NULL, NULL, 0),
(8, 1684, 8, 1, 12000, '2021-09-30', 3, NULL, NULL, 0),
(9, 1675, 9, 1, 26000, '2021-10-07', 3, NULL, NULL, 0),
(10, 2511, 10, 1, 10000, '2021-11-01', 3, NULL, NULL, 0),
(11, 1675, 11, 1, 26000, '2021-11-05', 3, NULL, NULL, 0),
(12, 1675, 12, 1, 26000, '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `order`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'DRAFT', '2021-08-17', 3, NULL, NULL, 0),
(2, 1, 'CONFIRM', '2021-08-17', 3, NULL, NULL, 0),
(3, 1, 'VALIDATE', '2021-08-17', 3, NULL, NULL, 0),
(4, 2, 'DRAFT', '2021-08-17', 3, NULL, NULL, 0),
(5, 2, 'CONFIRM', '2021-08-17', 3, NULL, NULL, 0),
(6, 2, 'VALIDATE', '2021-08-17', 3, NULL, NULL, 0),
(7, 3, 'DRAFT', '2021-08-17', 3, NULL, NULL, 0),
(8, 3, 'CANCEL', '2021-08-17', 3, NULL, NULL, 0),
(9, 4, 'DRAFT', '2021-08-17', 3, NULL, NULL, 0),
(10, 4, 'CONFIRM', '2021-08-17', 3, NULL, NULL, 0),
(11, 4, 'VALIDATE', '2021-08-17', 3, NULL, NULL, 0),
(12, 5, 'DRAFT', '2021-08-17', 3, NULL, NULL, 0),
(13, 5, 'CONFIRM', '2021-08-17', 3, NULL, NULL, 0),
(14, 5, 'VALIDATE', '2021-08-17', 3, NULL, NULL, 0),
(15, 6, 'DRAFT', '2021-08-23', 3, NULL, NULL, 0),
(16, 7, 'DRAFT', '2021-08-23', 3, NULL, NULL, 0),
(17, 8, 'DRAFT', '2021-09-30', 3, NULL, NULL, 0),
(18, 8, 'CONFIRM', '2021-09-30', 3, NULL, NULL, 0),
(19, 8, 'VALIDATE', '2021-09-30', 3, NULL, NULL, 0),
(20, 9, 'DRAFT', '2021-10-07', 3, NULL, NULL, 0),
(21, 9, 'CONFIRM', '2021-10-07', 3, NULL, NULL, 0),
(22, 9, 'VALIDATE', '2021-10-07', 3, NULL, NULL, 0),
(23, 10, 'DRAFT', '2021-11-01', 3, NULL, NULL, 0),
(24, 10, 'CONFIRM', '2021-11-01', 3, NULL, NULL, 0),
(25, 10, 'VALIDATE', '2021-11-01', 3, NULL, NULL, 0),
(26, 11, 'DRAFT', '2021-11-05', 3, NULL, NULL, 0),
(27, 11, 'CONFIRM', '2021-11-05', 3, NULL, NULL, 0),
(28, 11, 'VALIDATE', '2021-11-05', 3, NULL, NULL, 0),
(29, 12, 'DRAFT', '2021-11-05', 3, NULL, NULL, 0),
(30, 12, 'CONFIRM', '2021-11-05', 3, NULL, NULL, 0),
(31, 12, 'VALIDATE', '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pajak`
--

CREATE TABLE `pajak` (
  `id` int(11) NOT NULL,
  `jenis` varchar(155) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `persentase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `partial_payment`
--

CREATE TABLE `partial_payment` (
  `id` int(11) NOT NULL,
  `no_faktur_bayar` varchar(150) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `partial_payment_status`
--

CREATE TABLE `partial_payment_status` (
  `id` int(11) NOT NULL,
  `partial_payment` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'LUNAS\nKREDIT',
  `sisa_hutang` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `no_faktur_bayar` varchar(155) DEFAULT NULL,
  `tanggal_bayar` datetime DEFAULT NULL,
  `tanggal_faktur` datetime DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `no_faktur_bayar`, `tanggal_bayar`, `tanggal_faktur`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'PAY21AUG001', '2021-08-17 00:00:00', '2021-08-17 08:33:04', 90000, '2021-08-17', 3, NULL, NULL, 0),
(2, 'PAY21AUG002', '2021-08-17 00:00:00', '2021-08-17 08:33:45', 24000, '2021-08-17', 3, NULL, NULL, 0),
(3, 'PAY21AUG003', '2021-08-17 00:00:00', '2021-08-17 08:44:29', 24000, '2021-08-17', 3, NULL, NULL, 0),
(4, 'PAY21AUG004', '2021-08-17 00:00:00', '2021-08-17 08:45:44', 24000, '2021-08-17', 3, NULL, NULL, 0),
(5, 'PAY21SEP001', '2021-09-30 00:00:00', '2021-09-30 18:10:21', 12000, '2021-09-30', 3, NULL, NULL, 0),
(6, 'PAY21OCT001', '2021-10-07 00:00:00', '2021-10-07 19:14:09', 26000, '2021-10-07', 3, NULL, NULL, 0),
(7, 'PAY21NOV001', '2021-11-01 00:00:00', '2021-11-01 19:54:27', 10000, '2021-11-01', 3, NULL, NULL, 0),
(8, 'PAY21NOV002', '2021-11-05 00:00:00', '2021-11-05 19:09:15', 27000, '2021-11-05', 3, NULL, NULL, 0),
(9, 'PAY21NOV003', '2021-11-05 00:00:00', '2021-11-05 19:15:32', 28000, '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment_item`
--

CREATE TABLE `payment_item` (
  `id` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `jumlah_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_item`
--

INSERT INTO `payment_item` (`id`, `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1, 90000, '2021-08-17', 3, NULL, NULL, 0),
(2, 2, 2, 24000, '2021-08-17', 3, NULL, NULL, 0),
(3, 3, 3, 24000, '2021-08-17', 3, NULL, NULL, 0),
(4, 4, 4, 24000, '2021-08-17', 3, NULL, NULL, 0),
(5, 5, 5, 12000, '2021-09-30', 3, NULL, NULL, 0),
(6, 6, 6, 26000, '2021-10-07', 3, NULL, NULL, 0),
(7, 7, 7, 10000, '2021-11-01', 3, NULL, NULL, 0),
(8, 8, 8, 27000, '2021-11-05', 3, NULL, NULL, 0),
(9, 9, 9, 28000, '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `periode` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `keterangan` text,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_category`
--

CREATE TABLE `payroll_category` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL COMMENT 'POTONGAN\nTUNJANGAN\nDLL',
  `action` varchar(12) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_item`
--

CREATE TABLE `payroll_item` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `payroll_category` int(11) NOT NULL,
  `keterangan` text,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_workdays`
--

CREATE TABLE `payroll_workdays` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `keterangan` text,
  `qty_hari` int(11) DEFAULT NULL,
  `qty_jam` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` text,
  `alamat` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `no_hp`, `email`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(5, 'Reni', '085812193273', '-', 'Suruhwadang', '2021-08-06', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai_kontrak`
--

CREATE TABLE `pegawai_kontrak` (
  `id` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_angsuran`
--

CREATE TABLE `pembayaran_has_angsuran` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tgl_angsuran` date DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_cash`
--

CREATE TABLE `pembayaran_has_cash` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_product`
--

CREATE TABLE `pembayaran_has_product` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tgl_angsuran` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_lain_lain`
--

CREATE TABLE `pembayaran_lain_lain` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(45) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_product`
--

CREATE TABLE `pembayaran_product` (
  `id` int(11) NOT NULL,
  `pembeli_has_product` int(11) NOT NULL,
  `status_pembayaran` int(11) NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_tagihan`
--

CREATE TABLE `pembayaran_tagihan` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `tagihan` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_vendor`
--

CREATE TABLE `pembayaran_vendor` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `vendor` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id` int(11) NOT NULL,
  `pembeli_kategori` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `foto` text,
  `kelurahan` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kota` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id`, `pembeli_kategori`, `nama`, `alamat`, `email`, `no_hp`, `foto`, `kelurahan`, `kecamatan`, `kota`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'UMUM', '-', '-', '-', NULL, '-', '-', '-', '2021-08-17', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `jatuh_tempo` int(11) DEFAULT NULL COMMENT 'Hari',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_angsuran`
--

CREATE TABLE `pembeli_has_angsuran` (
  `id` int(11) NOT NULL,
  `pembeli_has_product` int(11) NOT NULL,
  `product_has_harga_angsuran` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_identitas`
--

CREATE TABLE `pembeli_has_identitas` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `indetitas` int(11) NOT NULL,
  `no_identitas` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_persyaratan`
--

CREATE TABLE `pembeli_has_persyaratan` (
  `id` int(11) NOT NULL,
  `pembelian` int(11) NOT NULL,
  `persyaratan_has_berkas` int(11) NOT NULL,
  `berkas` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_product`
--

CREATE TABLE `pembeli_has_product` (
  `id` int(11) NOT NULL,
  `pembelian` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembeli` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `status_pembelian` int(11) NOT NULL,
  `tgl_beli` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_kategori`
--

CREATE TABLE `pembeli_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli_kategori`
--

INSERT INTO `pembeli_kategori` (`id`, `kategori`) VALUES
(2, 'UMUM');

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman`
--

CREATE TABLE `pengiriman` (
  `id` int(11) NOT NULL,
  `no_pengiriman` varchar(150) DEFAULT NULL,
  `tanggal_pengiriman` date DEFAULT NULL,
  `tanggal_awal_order` date DEFAULT NULL,
  `tanggal_akhir_order` date DEFAULT NULL,
  `sopir` varchar(255) DEFAULT NULL,
  `no_polisi` varchar(45) DEFAULT NULL,
  `filter_by` varchar(45) DEFAULT NULL COMMENT 'SALES\nCUSTOMER',
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_detail`
--

CREATE TABLE `pengiriman_detail` (
  `id` int(11) NOT NULL,
  `pengiriman` int(11) NOT NULL,
  `invoice_product` int(11) NOT NULL,
  `product_satuan` int(11) DEFAULT NULL,
  `qty` varchar(50) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_has_customer`
--

CREATE TABLE `pengiriman_has_customer` (
  `id` int(11) NOT NULL,
  `pengiriman` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_has_sales`
--

CREATE TABLE `pengiriman_has_sales` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengiriman_status`
--

CREATE TABLE `pengiriman_status` (
  `id` int(11) NOT NULL,
  `pengiriman` int(11) NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan`
--

CREATE TABLE `persyaratan` (
  `id` int(11) NOT NULL,
  `syarat` varchar(150) DEFAULT NULL,
  `kategori_akad` int(11) NOT NULL,
  `jenis_akad` int(11) NOT NULL,
  `lampirkan_berkas` int(11) DEFAULT NULL COMMENT '0 : Tidak\n1: YA',
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan_has_berkas`
--

CREATE TABLE `persyaratan_has_berkas` (
  `id` int(11) NOT NULL,
  `persyaratan` int(11) NOT NULL,
  `nama_berkas` varchar(150) DEFAULT NULL COMMENT 'Nama Berkas Misal (KTP, SIM, dLL)',
  `status` varchar(150) DEFAULT NULL COMMENT 'Status Tanpa Upload Berkas atau Tidak\nN : Tanpa Berkas\nY : Dengan Berkas',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `potongan`
--

CREATE TABLE `potongan` (
  `id` int(11) NOT NULL,
  `potongan` varchar(45) DEFAULT NULL COMMENT 'Nominal\nPersentase'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `potongan`
--

INSERT INTO `potongan` (`id`, `potongan`) VALUES
(1, 'Nominal'),
(2, 'Persentase'),
(3, 'Tidak ada potongan');

-- --------------------------------------------------------

--
-- Table structure for table `priveledge`
--

CREATE TABLE `priveledge` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priveledge`
--

INSERT INTO `priveledge` (`id`, `hak_akses`) VALUES
(5, 'superadmin'),
(6, 'kasir');

-- --------------------------------------------------------

--
-- Table structure for table `procurement`
--

CREATE TABLE `procurement` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(150) DEFAULT NULL,
  `vendor` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement`
--

INSERT INTO `procurement` (`id`, `no_faktur`, `vendor`, `total`, `tanggal`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'PROC21OCT001', 1, 20000, '2021-10-20', '-', '2021-10-20', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_item`
--

CREATE TABLE `procurement_item` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `satuan` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_item`
--

INSERT INTO `procurement_item` (`id`, `procurement`, `product`, `satuan`, `harga`, `qty`, `sub_total`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1677, 1, 20000, 1, 20000, NULL, '2021-10-20', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `procurement_retur`
--

CREATE TABLE `procurement_retur` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_retur_item`
--

CREATE TABLE `procurement_retur_item` (
  `id` int(11) NOT NULL,
  `procurement_item` int(11) NOT NULL,
  `procurement_retur` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_status`
--

CREATE TABLE `procurement_status` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nPAID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `procurement_status`
--

INSERT INTO `procurement_status` (`id`, `procurement`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'DRAFT', '2021-10-20', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product` varchar(150) DEFAULT NULL,
  `kode_product` varchar(50) DEFAULT NULL,
  `kodebarcode` text,
  `tipe_product` int(11) NOT NULL,
  `kategori_product` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product`, `kode_product`, `kodebarcode`, `tipe_product`, `kategori_product`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1677, 'Surya F16', '8998989110167', '8998989110167', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1678, 'Surya F12', '8998989110129', '8998989110129', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1679, 'Surya Pro Merah', '8998989121163', '8998989121163', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1680, 'Surya Pro Putih', '8998989300391', '8998989300391', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1681, 'Signature biru', '8998989300230', '8998989300230', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1682, 'Gudang garam patra', '8998989300292', '8998989300292', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1683, 'Gudang garam inter', '8998989100120', '8998989100120', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1684, 'Signature coklat', '8998989300155', '8998989300155', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1685, 'GG Merah', '8998989500128', '8998989500128', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1686, 'GG Hijau', '8998989502122', '8998989502122', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1687, 'Halim', '8998968400203', '8998968400203', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1688, 'LA Bold F20', '8991906105758', '8991906105758', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1689, 'LA Bold F12', '8991906108896', '8991906108896', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1690, 'LA Light F16', '8991906101057', '8991906101057', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1691, 'LA Light F12', '8991906101132', '8991906101132', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1692, 'LA Ice F16', '8991906101101', '8991906101101', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1693, 'LA Menthol F16', '8991906101071', '8991906101071', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1694, 'On Bold F20', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1695, 'On Bold F12', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1696, 'Grendel F12', '8992729129891', '8992729129891', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1697, 'Grendel F16', '8992729168692', '8992729168692', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1698, 'Track Menthol', '8997079803842', '8997079803842', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1699, 'Track Bold', '8997079808151', '8997079808151', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1700, 'UP Menthol F20', '8997079808946', '8997079808946', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1701, 'UP Berry F16', '8997079804023', '8997079804023', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1702, 'Sampoerna AGA', '8999909000988', '8999909000988', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1703, 'Sampoerna Mild', '8999909096004', '8999909096004', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1704, 'Djarum black capp', '8991906101170', '8991906101170', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1705, 'On line Bold', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1706, 'Tali Roso', '8998127655130', '8998127655130', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1707, 'Fajar Berlian', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1708, 'Apache F16', '8994729100122', '8994729100122', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1709, 'Apache F12', '8994729100245', '8994729100245', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1710, 'Apache Kretek', '8994729400178', '8994729400178', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1711, 'Marlboro Red', '76164217', '76164217', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1712, 'Marlboro Kretek', '8999909003439', '8999909003439', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1713, 'Dji Sam Soe', '8999909000629', '8999909000629', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1714, 'MLD F16', '8991906109992', '8991906109992', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1715, 'MLD F12', '8991906108384', '8991906108384', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1716, 'Marcopolo', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1717, 'Diplomat', '8994214110124', '8994214110124', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1718, 'Andalan', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1719, 'Sabun Zwitsal', '8992694247163', '8992694247163', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1720, 'Cusson Baby', '8888103200013', '8888103200013', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1721, 'Sabun Sehat', '8992929130253', '8992929130253', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1722, 'Sabun GIV batang', '8998866602389', '8998866602389', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1723, 'Nuvo batang', '8998866618144', '8998866618144', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1724, 'Lifebuoy batang', '8999999059316', '8999999059316', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1725, 'Sabun claudia', '8999908009302', '8999908009302', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1726, 'Sabun lux batang', '8999999036638', '8999999036638', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1727, 'BDL Papaya 60g', '8992803662771', '8992803662771', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1728, 'RDL Papaya 135g', '8997003822307', '8997003822307', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1729, 'Sabun Hijau 40g', '8998183216122', '8998183216122', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1730, 'Asepso 80g', '7640129890040', '7640129890040', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1731, 'shinzui batang 85g', '8992946511790', '8992946511790', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1732, 'Sabun Citra btng 70g', '8999999533724', '8999999533724', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1733, 'Sabun Harmoni 70g', '8993379200855', '8993379200855', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1734, 'Dettol 100g', '8993560156053', '8993560156053', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1735, 'Dettol 60g', '8993560155810', '8993560155810', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1736, 'Shampo Lifebuoy Botol 340ml', '8999999033224', '8999999033224', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1737, 'Shampo Lifebuoy Botol 170ml', '8999999033170', '8999999033170', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1738, 'Shampo Cussons kids 100ml', '8998103017495', '8998103017495', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1739, 'Baby oil Mitu 95ml', '8992745380542', '8992745380542', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1740, 'Hair n Body Cologne 100ml', '8999908764201', '8999908764201', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1741, 'Eskulin Cologne Gell 50ml', '8993417212116', '8993417212116', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1742, 'Swana Parfume 100ml', '8998824551759', '8998824551759', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1743, 'Surya Kaleng', '8998989110501', '8998989110501', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1744, 'Viva Milk Cleanser 100ml', '8992796011136', '8992796011136', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1745, 'Hand Body citra 120ml', '8999999528812', '8999999528812', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1746, 'Hand Body citra 230ml', '8999999528881', '8999999528881', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1747, 'Cologne Mitu 100ml', '8992745326670', '8992745326670', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1748, 'Shampo Zinc Botol 170ml', '8998866105750', '8998866105750', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1749, 'Hand Body Vaseline 200ml', '8999999719418', '8999999719418', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1750, 'Hand Body Kris 100ml', '8992929751090', '8992929751090', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1751, 'Metro Snow 60g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1752, 'Gillete Goal', '8992765101004', '8992765101004', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1753, 'London Bridge', '4800037120339', '4800037120339', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1754, 'Rexona SC Men', '8934868015031', '8934868015031', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1755, 'Rexona SC Woman', '8934868015024', '8934868015024', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1756, 'Fair n Lovely Cream SC', '8999999007768', '8999999007768', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1757, 'Fair n Lovely Foam SC pink', '8999999055752', '8999999055752', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1758, 'Fair n Lovely Foam SC Orange', '8999999566661', '8999999566661', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1759, 'Garnier Cream Sakura Pink SC', '8992304049743', '8992304049743', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1760, 'Garnier Cream Men SC', '8992304078446', '8992304078446', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1761, 'Stella 7ml', '8992745326694', '8992745326694', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1762, 'Stella Pinguin', '8992745999935', '8992745999935', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1763, 'Kit Hitam 25ml', '8992779269202', '8992779269202', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1764, 'Nosy 25ml', '8992803658217', '8992803658217', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1765, 'Hit Mat 5w', '8992745120186', '8992745120186', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1766, 'Isi Ulang Hit Mat 18 + 6', '8992745120636', '8992745120636', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1767, 'Semprot Burung', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1768, 'Skakel Gantung IOS-A', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1769, 'Tutup Panci biasa', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1770, 'Tutup Panci Bagus', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1771, 'Senter Matsugi', '6946723408423', '6946723408423', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1772, 'Alat Lem Tembak Goztar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1773, 'Tepung Terigu', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1774, 'Gunting M2000 SMD680', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1775, 'cutter Safari', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1776, 'Cussons Baby Powder 500g', '8888103201317', '8888103201317', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1777, 'Cussons Baby Powder 75g', '8998103000565', '8998103000565', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1778, 'My Baby Powder 100g+25g', '8999908318305', '8999908318305', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1779, 'Zwitsal Baby Powder 100gr', '8992694242717', '8992694242717', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1780, 'My Baby Minyak Telon 60ml', '8999908204202', '8999908204202', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1781, 'Rexona Men Rol', '8999999049669', '8999999049669', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1782, 'Fresh & Natural Parfume', '8998866611237', '8998866611237', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1783, 'Posh Men Parfume 150ml', '8998866107150', '8998866107150', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1784, 'Fair & Lovely Cream 25g', '8999999007782', '8999999007782', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1785, 'Fair & lovely Cream 50g', '8999999007799', '8999999007799', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1786, 'Fair & Lovely FW 50g', '8999999055769', '8999999055769', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1787, 'Ponds White Beauty crm', '8999999056841', '8999999056841', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1788, 'Garnier Sakura White FW 50ML', '8992304047169', '8992304047169', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1789, 'Garnier men FW 15ml', '609252558', '609252558', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1790, 'Cussons Newborn bdywash 100ml', '8998103017327', '8998103017327', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1791, 'Miranda Hair Color', '8997016370390', '8997016370390', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1792, 'Garnier Men FW 100ML', '8992304039614', '8992304039614', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1793, 'Sikat Gigi Ciptadent', '8998866103978', '8998866103978', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1794, 'Sikat Gigi Formula', '8991102022347', '8991102022347', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1795, 'Pasta Gigi Pepsodent 225g', '8999999037765', '8999999037765', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1796, 'Pasta Gigi Pepsodent 190g', '8999999706180', '8999999706180', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1797, 'Pasta Gigi Pepsodent 120g', '8999999706173', '8999999706173', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1798, 'Pasta Gigi Pepsodent 75g', '8999999706081', '8999999706081', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1799, 'Pasta Gigi Close UP 160g', '8999999707859', '8999999707859', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1800, 'Pasta Gigi Close Up 65g+10g', '8999999574376', '8999999574376', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1801, 'Pasta Gigi Ciptadent 190g', '8998866181082', '8998866181082', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1802, 'Pasta Gigi Formula 160g', '8991102100434', '8991102100434', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1803, 'Minyak Kayu Putih 60ml', '8993176110074', '8993176110074', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1804, 'Minyak Kayu Putih 30ml', '8993176110081', '8993176110081', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1805, 'Minyak Kayu Putih 15ml', '8993176110098', '8993176110098', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1806, 'Senter Luby 7w', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1807, 'Senter Push On 30w', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1808, 'Lampu Focus 5w', '8997011050037', '8997011050037', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1809, 'Lampu Philip LED 6w', '8718696822845', '8718696822845', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1810, 'Lampu Philip LED 12w', '8718696822968', '8718696822968', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1811, 'Lampu Philip LED 14.5w', '8718699640491', '8718699640491', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1812, 'Lampu Endora 5w', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1813, 'Lampu Ecolink 10w', '8710619960917', '8710619960917', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1814, 'Lampu Ecolinnk 15w', '8710619963871', '8710619963871', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1815, 'Lampu Sloveens 5w', '8990126599057', '8990126599057', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1816, 'Lampu Ossio LED 5W', '8997213205686', '8997213205686', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1817, 'Lampu jastec 12w', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1818, 'Lampu Endora 25w', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1819, 'Lem rajawali 70g', '8997006900064', '8997006900064', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1820, 'Lem Eternity 40g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1821, 'Solatip Hitam Nasional', '8997777080323', '8997777080323', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1822, 'Skakel Gantung Cahaya', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1823, 'Colokan lampu Matsuka', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1824, 'Solatip Bening', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1825, 'Solatip Kertas', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1826, 'Solatip Hitam', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1827, 'Solatip Bening', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1828, 'lilin tanggung', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1829, 'lilin', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1830, 'Lakban hitam', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1831, 'Lakban bening', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1832, 'Lakban coklat', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1833, 'Baterai jam', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1834, 'Baterai remot', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1835, 'Tissu Paseo', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1836, 'Tissu basah mitu 50 sheets', '8992745550525', '8992745550525', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1837, 'Tissu Basah mamypoko', '8993189320767', '8993189320767', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1838, 'Laurier night 30cm', '8992727007283', '8992727007283', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1839, 'Pagoda Lem Lalat', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1840, 'Racun tikus cair', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1841, 'Gandasil', '8997076800295', '8997076800295', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1842, 'Temix racun tikus', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1843, 'Baterai ABC R20S', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1844, 'Baterai ABC R14c', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1845, 'Colokan lampu biasa', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1846, 'Pitingan Gantung', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1847, 'Terminal listrik pelangi', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1848, 'Terminal listrik isi 5', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1849, 'Pompa galon otomatis', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1850, 'Aston clip kabel', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1851, 'Terminal listrik isi 3', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1852, 'Obat nyamuk bakar Nomos', '8997027300089', '8997027300089', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1853, 'Semir HITOP 35ml', '8999338313888', '8999338313888', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1854, 'Semir HITOP 15ml', '8999338136883', '8999338136883', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1855, 'Semir Bigen 6g', '8992866110608', '8992866110608', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1856, 'Hemaviton jreng bubuk', '8999908045812', '8999908045812', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1857, 'Kukubima energi', '8998898830101', '8998898830101', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1858, 'Exstrajoss sachet', '8993058000684', '8993058000684', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1859, 'Vicks vaporub', '4987176600554', '4987176600554', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1860, 'Tissu multi 200 sheets', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1861, 'Madu TJ Sachet', '8993365170025', '8993365170025', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1862, 'Montalin', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1863, 'Salonpas koyo', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1864, 'Tissu paseo 12 sheets', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1865, 'Betadine 5ml', '8992843103050', '8992843103050', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1866, 'Charm safe night 35cm', '8993189320279', '8993189320279', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1867, 'Hers protex  comfort night 30cm', '8998866500180', '8998866500180', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1868, 'Panty liners daun sirih', '8992959974209', '8992959974209', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1869, 'Aito tetes mata', '8997219420038', '8997219420038', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1870, 'Herocyn dewasa', '8996200900092', '8996200900092', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1871, 'Herocyn baby powder', '8996200900047', '8996200900047', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1872, 'Hot in cream red', '8997021870571', '8997021870571', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1873, 'Hot in cream green', '8997021870595', '8997021870595', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1874, 'Hot in cream strong cair 20g', '8997021870892', '8997021870892', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1875, 'Hot in cream red cair 60g', '8997021870540', '8997021870540', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1876, 'GPU minyak urut 60ml', '8993176110104', '8993176110104', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1877, 'Waisan bintang tujuh sachet', '8993058200107', '8993058200107', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1878, 'Caladine cair 60ml', '8993005123015', '8993005123015', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1879, 'Tolak angin sachet', '8998898101409', '8998898101409', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1880, 'Tolak linu mint herbal sachet', '8998898338409', '8998898338409', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1881, 'Antangin sachet', '8992003782354', '8992003782354', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1882, 'Komik OBH sachet', '8993058301200', '8993058301200', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1883, 'Minyak angin 1001 2,5ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1884, 'Balsem Geliga 10g', '8993176812039', '8993176812039', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1885, 'Balsem Geliga 20g', '8993176812022', '8993176812022', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1886, 'Balsem lang 20g', '8993176120028', '8993176120028', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1887, 'Minyak kapak 10ml', '8994472000038', '8994472000038', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1888, 'Minyak kapak 5ml', '8994472000021', '8994472000021', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1889, 'Minyak kapak 56ml', '8994472000069', '8994472000069', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1890, 'Minyak kapak 14ml', '8994472000045', '8994472000045', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1891, 'Tolak linu herbal', '8998898335408', '8998898335408', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1892, 'Gulkol eds-on', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1893, 'Stabilo van-art', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1894, 'Gunting kuku safari', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1895, 'Gunting kuku rige', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1896, 'Correction pen joyko', '8993988050223', '8993988050223', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1897, 'Joyko Eraser B40P', '8993988690047', '8993988690047', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1898, 'Joyko Eraser ER-30W', '8993988090106', '8993988090106', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1899, 'Joyko Eraser B20', '8993988090021', '8993988090021', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1900, 'Joyko Stapler HD-10', '8993988350002', '8993988350002', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1901, 'Joyko Stapler HD-10D', '8993988350040', '8993988350040', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1902, 'Joyko Stapler HD-50CL', '8993988350354', '8993988350354', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1903, 'Amplop 110x230mm', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1904, 'Amplop 95x152mm', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1905, 'Vision buku kotak besar', '501167689454', '501167689454', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1906, 'Vision buku kotak kecil', '501177895531', '501177895531', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1907, 'Sidu buku tulis halus latin', '8991389220030', '8991389220030', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1908, 'Vision buku tulis 38lmbr', '501060049867', '501060049867', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1909, 'Vision buku tulis B5 42lmbr', '501117896574', '501117896574', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1910, 'Buku paper line 50lmbar', '8991389241646', '8991389241646', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1911, 'Buku paper line 100lmbar', '8991389241585', '8991389241585', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1912, 'Sampul buku unedo besar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1913, 'Sampul buku unedo kecil', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1914, 'Sampul buku batik', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1915, 'Crayon Titi 12', '8801076910352', '8801076910352', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1916, 'Kertas folio', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1917, 'Snowman permanent marking ink', '4970129731511', '4970129731511', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1918, 'Goztar isi ulang stapler besar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1919, 'Tempat pensil all about me', '6927427500026', '6927427500026', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1920, 'Tempat pensil rabbit', '6922468221770', '6922468221770', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1921, 'Origami 12x12', '890855042214', '890855042214', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1922, 'Origami 16x16', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1923, 'Modis kapas', '8991038773344', '8991038773344', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1924, 'Joyko pensil warna 12', '8993988048329', '8993988048329', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1925, 'Bodrex bulat', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1926, 'Bodrex Migrain', '8999908071903', '8999908071903', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1927, 'Bodrex Extra', '8999908285003', '8999908285003', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1928, 'Promag kaplet', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1929, 'Sangobion', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1930, 'Ultrasiline cream', '8997014050096', '8997014050096', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1931, 'Diapet kapsul', '8998777110218', '8998777110218', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1932, 'Bodrexin', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1933, 'Contrexin anak', '8999908039101', '8999908039101', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1934, 'Antangin junior', '8992003783665', '8992003783665', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1935, 'Tolak angin anak', '8998898151404', '8998898151404', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1936, 'Koolfever anak', '4987072061817', '4987072061817', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1937, 'CTM 12 tablet', '8993515100537', '8993515100537', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1938, 'Neo Napacin kaplet', '8998667100169', '8998667100169', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1939, 'Ultraflu Kaplet', '8997014050010', '8997014050010', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1940, 'Poldanmig kaplet', '8993008235043', '8993008235043', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1941, 'Paramex kaplet', '8998667100206', '8998667100206', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1942, 'Mixagrip Flu & batuk', '8995858192620', '8995858192620', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1943, 'Mixagrip flu', '8995858999991', '8995858999991', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1944, 'Oskadon sakit kepala', '8999908039309', '8999908039309', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1945, 'Oskadon SP nyeri otot', '8999908043801', '8999908043801', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1946, 'Konidin kaplet', '8998667100084', '8998667100084', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1947, 'Pilkita kaplet', '8997013070019', '8997013070019', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1948, 'Panadol hijau kaplet', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1949, 'Panadol merah', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1950, 'Panadol biru', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1951, 'Hemaviton stamina kapsul', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1952, 'Hemaviton action kaplet', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1953, 'Supertetra kaplet', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1954, 'Komik OBH Kids', '8993058302900', '8993058302900', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1955, 'Cotton Bud dewasa', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1956, 'Cotton Bud baby', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1957, 'Amplop linen merak', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1958, 'Plastik es lilin 4,5x15', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1959, 'Plastik es lilin jerapah 5x15', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1960, 'Kantong plastik keong 11x22', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1961, 'Plastik hitam sinar tanggung 38', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1962, 'Plastik ungu OZO', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1963, 'Plastik joyoboyo 15x27', '8993393600037', '8993393600037', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1964, 'Plastik joyoboyo 12x24', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1965, 'Plastik joyoboyo 10x17', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1966, 'Plastik KA-TUP 7x10', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1967, 'Plastik KA-TUP 5X8', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1968, 'Plastik KA-TUP 4x6', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1969, 'Laurier Day 22cm', '8992727006200', '8992727006200', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1970, 'Laurier Day 25cm', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1971, 'Charm fragrance 23cm', '8993189321849', '8993189321849', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1972, 'Handsaplast standar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1973, 'Vanili cair 20ml', '8997013990072', '8997013990072', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1974, 'Vanili bubuk', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1975, 'Vegeta Herbal sachet', '8992772401012', '8992772401012', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1976, 'Ademsari sachet', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1977, 'The sari wangi sachet', '8999999540333', '8999999540333', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1978, 'The sari murni sachet', '8999999502003', '8999999502003', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1979, 'Autan sachet 6ml', '8998899004105', '8998899004105', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1980, 'Silet cukur EDS-ON', '6941009624511', '6941009624511', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1981, 'Gillette goal II', '8992765301008', '8992765301008', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1982, 'Gillette Blue II', '4902430923590', '4902430923590', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1983, 'Gunting Milton', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1984, 'Gunting M2000', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1985, 'Cutter Amanda', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1986, 'Kapur barus Azane 25g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1987, 'Pantene anti ketombe rtg', '4902430563864', '4902430563864', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1988, 'Pantene anti lepek rtg', '4902430799614', '4902430799614', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1989, 'Pantene rambut rontok  rtg', '4902430563871', '4902430563871', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1990, 'Pantene rambut rusak rtg', '4902430563901', '4902430563901', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1991, 'kondisioner pantene hitam rtg', '4902430415750', '4902430415750', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1992, 'kondisioner pantene ungu rtg', '4902430415743', '4902430415743', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1993, 'kondisioner pantene pink rtg', '4902430415729', '4902430415729', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1994, 'Pantene hitam & panjang rtg', '4902430563888', '4902430563888', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1995, 'Clear menthol sachet', '8999999529833', '8999999529833', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1996, 'Clear nutrisi komplit sachet', '8999999529819', '8999999529819', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1997, 'Dove biru jumbo sachet', '8999999533274', '8999999533274', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1998, 'Kondisioner dove hijau sachet', '8999999037130', '8999999037130', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(1999, 'Kondisioner dove kuning sachet', '8999999534806', '8999999534806', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2000, 'Rejoice rich sachet', '4902430697941', '4902430697941', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2001, 'Rejoice hijab sachet', '4902430753333', '4902430753333', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2002, 'Lifebuoy hijau sachet', '8999999027032', '8999999027032', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2003, 'Lifebuoy biru sachet', '8999999027056', '8999999027056', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2004, 'Head & shoulders lemon sachet', '4902430566889', '4902430566889', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2005, 'Head & shoulders menthol sachet', '4902430566896', '4902430566896', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2006, 'Zinc active fresh sachet', '8998866108799', '8998866108799', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2007, 'Zinc refreshing cool sachet', '8998866107488', '8998866107488', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2008, 'Emeron damage care sachet', '8998866107587', '8998866107587', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2009, 'Emeron hair fall sachet', '8998866107549', '8998866107549', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2010, 'Emeron soft  smooth sachet', '8998866107556', '8998866107556', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2011, 'Emeron anti dandruft sachet', '8998866107563', '8998866107563', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2012, 'Emeron hijab clean fresh sachet', '8998866108003', '8998866108003', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2013, 'Tolak linu herbal', '8998898335408', '8998898335408', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2014, 'Madu TJ strowberry sachet', '8993365130029', '8993365130029', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2015, 'Madurasa jeruk nipis sachet', '8993014730112', '8993014730112', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2016, 'Downy parfum daring 20ml', '4902430693455', '4902430693455', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2017, 'Downy mild & gentle 19ml', '4902430928748', '4902430928748', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2018, 'Downy sunrise fresh 20ml', '4902430557122', '4902430557122', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2019, 'Downy parfum mystique 20ml', '4902430504454', '4902430504454', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2020, 'Downy parfum sweetheart 20ml', '4902430685856', '4902430685856', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2021, 'Downy parfum passion 20ml', '4902430542753', '4902430542753', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2022, 'Downy floral pink 20ml', '4902430894609', '4902430894609', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2023, 'Downy secret garden 25ml', '4902430853200', '4902430853200', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2024, 'Downy parfum passion 10ml', '4987176051349', '4987176051349', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2025, 'Downy floral pink 10ml', '4902430803380', '4902430803380', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2026, 'Downy sport 10ml', '4902430821827', '4902430821827', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2027, 'Downy anti bau 10ml', '4902430897112', '4902430897112', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2028, 'Mie panda', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2029, 'Sek', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2030, 'Cengkeh', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2031, 'Tepung beras putih 500g', '8993093115008', '8993093115008', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2032, 'Tepung ketan putih 500g', '8993093135006', '8993093135006', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2033, 'Mbako no 1', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2034, 'Mbako no 2', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2035, 'Sedotan pelangi', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2036, 'Benang butterfly', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2037, 'Mylanta kunyah', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2038, 'OB Herbal 60ml', '8992003782453', '8992003782453', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2039, 'Isi ulang staples kecil', '8994354100399', '8994354100399', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2040, 'Spidol snowman permanent red', '4970129004523', '4970129004523', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2041, 'Spidol snowman permanent blue', '4970129004530', '4970129004530', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2042, 'Snowman white board black', '4970129000518', '4970129000518', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2043, 'Joyko permanent marker blck', '8993988240242', '8993988240242', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2044, 'Spidoll snowman mini blck', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2045, 'Spidoll snowman mini red', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2046, 'Spidoll snowman mini blue', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2047, 'Bolpoin snowman V-1', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2048, 'Bolpoin snowman V-5', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2049, 'Bolpoin HI-TECH-H', '6956953588079', '6956953588079', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2050, 'Bolpoin LOL', '6925473852793', '6925473852793', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2051, 'Bolpoin mickey mouse', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2052, 'Bolpoin hijab series', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2053, 'Bolpoin lovein', '6925473852946', '6925473852946', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2054, 'Bolpoin lovein music', '6925473852649', '6925473852649', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2055, 'Bolpoin batik snake', '6932913800184', '6932913800184', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2056, 'Bolpoin batik ketupat', '6955114607628', '6955114607628', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2057, 'Bolpoin O&Q', '6955114619713', '6955114619713', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2058, 'Bolpoin youmei office', '6932808788887', '6932808788887', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2059, 'Pensil staedler 2B', '4007817104118', '4007817104118', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2060, 'Pensil joyko 2B', '8993988286066', '8993988286066', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2061, 'Marina compact powder 02', '8999908041203', '8999908041203', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2062, 'Marcks beauty powder crème', '8995026801019', '8995026801019', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2063, 'Marcks beauty powder rose', '8995026801033', '8995026801033', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2064, 'Lipstik Implora  01', '1212040001001', '1212040001001', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2065, 'Lipstik Implora  02', '1212040002008', '1212040002008', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2066, 'Lipstik Implora 03', '1212040003005', '1212040003005', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2067, 'Implora eyeliner pen', '1205030000160', '1205030000160', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2068, 'Implora deep black mascara', '1213030000028', '1213030000028', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2069, 'Mukka Eyeliner Liquid', '8994990013930', '8994990013930', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2070, 'Implora eyeshadow pallet', '1205077002004', '1205077002004', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2071, 'Christina puct puff', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2072, 'Aini powder puff', '6920850300225', '6920850300225', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2073, 'Nice look kontak lensa 60ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2074, 'Implora eyebrow pencil 002', '1252010002003', '1252010002003', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2075, 'Implora eyebrow pencill 004', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2076, 'DNM eyebrow pencil brown', '6970469380002', '6970469380002', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2077, 'Etude house eyebrow brown', '8806382623132', '8806382623132', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2078, 'Madame gie eyebrow pencil 02', '6933994120420', '6933994120420', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2079, 'Davis eyebrow pencil brown', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2080, 'Viva eyebrow pencil brown', '8992796157315', '8992796157315', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2081, 'DNM eyebrow pencil brown', '857289008353', '857289008353', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2082, 'Tineke eye brow razor 3pcs', '8801038123097', '8801038123097', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2083, 'Focallure liquid concealer 02', '6927545980502', '6927545980502', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2084, 'KCC BB Cream 15ml', '8997224912092', '8997224912092', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2085, 'Bioaqua eyeliner', '6947790781020', '6947790781020', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2086, 'Brastomolo', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2087, 'Kejetit keseleo', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2088, 'Top gusi', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2089, 'Korek jress white', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2090, 'Korek jress black', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2091, 'Korek cetek', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2092, 'Korek gas bara pelangi', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2093, 'Korek gas bara simple', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2094, 'Korek senter', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2095, 'Kertas laminasi dua elang', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2096, 'Kertas bungkus cap naga', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2097, 'Samir cap merak', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2098, 'Tepung kanji cap tiga bola', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2099, 'Sabun daia violet 290g', '8998866610261', '8998866610261', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2100, 'Sabun daia pink 290g', '8998866608718', '8998866608718', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2101, 'Sabun rinso rose fresh 240g', '8999999500641', '8999999500641', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2102, 'Soklin Smart color care 800g', '8998866605816', '8998866605816', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2103, 'Soklin liquid white&bright 750ml', '8998866808859', '8998866808859', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2104, 'Soklin liquid softergent 750ml', '8998866609180', '8998866609180', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2105, 'Soklin lantai marine mint 780ml', '8998866679596', '8998866679596', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2106, 'Molto pewangi floral bliss 820ml', '8999999571580', '8999999571580', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2107, 'Sunlight jeruk nipis 755ml', '8999999390198', '8999999390198', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2108, 'Mama lemon daun mint 780ml', '8998866100700', '8998866100700', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2109, 'Mama lime & mineral salt 780ml', '8998866100717', '8998866100717', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2110, 'Shampoo mobil 400ml', '8992746700844', '8992746700844', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2111, 'Cussons baby liquid cleanser 100ml', '8998103015972', '8998103015972', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2112, 'Toilet colour ball swallow isi 5', '8886020001096', '8886020001096', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2113, 'Vixal pembersih 190ml', '8999999555450', '8999999555450', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2114, 'Wipol karbol cemara 450ml', '8999999407896', '8999999407896', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2115, 'Sunlight jeruk nipis 105ml', '8999999050009', '8999999050009', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2116, 'Mama lemon jeruk nipis 115ml', '8998866105132', '8998866105132', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2117, 'Mama lime & mineral salt 58ml', '8998866104456', '8998866104456', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2118, 'Minyak goreng sovia 1 liter', '8993496107051', '8993496107051', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2119, 'Minyak goreng fraisswell 1 liter', '8992946513701', '8992946513701', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2120, 'Minyak goreng bimoli 1 liter', '8992628022156', '8992628022156', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2121, 'Minyak goreng bimoli 2 liter', '8992628020152', '8992628020152', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2122, 'Minyak goreng sania 2 liter', '8993496001076', '8993496001076', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2123, 'Minyak goreng sovia 2 liter', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2124, 'Ekonomi sabun cream 80g', '8998866603829', '8998866603829', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2125, 'Wings sabun cream 174g', '8998866670722', '8998866670722', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2126, 'Rinso anti noda 44g', '8999999558062', '8999999558062', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2127, 'Sunlight cream 270g', '8999999549480', '8999999549480', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2128, 'Kuku Bima vit C sachet', '8998898865103', '8998898865103', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2129, 'Minyak goreng sovia 1 liter', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2130, 'Kapal api spesial 165g', '8991002105423', '8991002105423', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2131, 'Kapal api khusus 65g', '8991002105447', '8991002105447', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2132, 'Kapal api spesial mix 24g', '8991002105485', '8991002105485', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2133, 'Fresco kopi bubuk 158g', '8991002109353', '8991002109353', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2134, 'Top kopi murni 158g', '8998866200646', '8998866200646', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2135, 'ABC kopi susu 31g', '8991002101630', '8991002101630', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2136, 'Teh sari murni kotak', '8999999031602', '8999999031602', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2137, 'Teh sariwangi kotak', '8999999195649', '8999999195649', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2138, 'Teh 999 23gram', '8997002670015', '8997002670015', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2139, 'Madu klanceng pegal linu 100ml', '8997007570396', '8997007570396', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2140, 'Indomilk sachet putih', '8993007001557', '8993007001557', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2141, 'Indomilk sachet coklat', '8993007001359', '8993007001359', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2142, 'Millo sachet 22g', '8992696521797', '8992696521797', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2143, 'Energen vanilla sachet', '8996001440124', '8996001440124', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2144, 'Energen jahe sachet', '8996001440353', '8996001440353', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2145, 'Energen kacang hijau sachet', '8996001440087', '8996001440087', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2146, 'Energen coklat sachet', '8996001440049', '8996001440049', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2147, 'Luwak white kopi sachet', '8994171101289', '8994171101289', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2148, 'Top white kopi sachet', '8998866609234', '8998866609234', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2149, 'Top kopi susu sachet', '8998866200745', '8998866200745', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2150, 'fresco kopi susu', '8991002109148', '8991002109148', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2151, 'Top kopi plus sachet', '8998866803281', '8998866803281', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2152, 'Ademsari chingqu kaleng', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2153, 'Larutan kaleng cap badak', '8999988888866', '8999988888866', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2154, 'Larutan botol cap badak 500ml', '8999988888972', '8999988888972', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2155, 'Larutan botol cap badak 200ml', '8999988888989', '8999988888989', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2156, 'Larutan kaleng anak rasa leci', '8999988808628', '8999988808628', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2157, 'Larutan kaleng anak rasa jambu', '8999988808642', '8999988808642', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2158, 'Bear brand susu steril 189ml', '8992696404441', '8992696404441', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2159, 'Tujuh kurma susu steril 200ml', '8996001600696', '8996001600696', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2160, 'Tebs kaleng mix fruit', '8996006853127', '8996006853127', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2161, 'Susu kaleng bendera coklat 370ml', '8992753102204', '8992753102204', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2162, 'Susu kaleng bendera putih 370ml', '8992753101207', '8992753101207', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2163, 'Indomilk kaleng putih 370ml', '8992702000018', '8992702000018', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2164, 'Indomilk kaleng coklat 370ml', '8992702000063', '8992702000063', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2165, 'Hemaviton energy drink 150ml', '8999908034205', '8999908034205', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2166, 'Kratingdeng energy drink 150ml', '8886057883665', '8886057883665', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2167, 'Nutrijell strowberry sachet 10g', '8992933321111', '8992933321111', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2168, 'Nutrijell anggur sachet 10g', '8992933328110', '8992933328110', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2169, 'Nutrijell lychee sachet 10g', '8992933325119', '8992933325119', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2170, 'Nutrijell melon sachet 10g', '8992933323115', '8992933323115', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2171, 'Nutrijell mangga sachet 10g', '8992933326116', '8992933326116', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2172, 'Nutrijell cincau sachet 10g', '8992933329117', '8992933329117', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2173, 'Nutrijell coklat sachet 10g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2174, 'Agar lumba-lumba merah 5g', '8994021000021', '8994021000021', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2175, 'Agar lumba-lumba hijau 5g', '8994021000045', '8994021000045', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2176, 'Nutrisari sweet orange sachet', '749921005946', '749921005946', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2177, 'Nutrisari kelapa muda sachet', '749921010780', '749921010780', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2178, 'Nutrisari leci sachet', '749921006226', '749921006226', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2179, 'Nutrisari lemon tea sachet', '749921010575', '749921010575', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2180, 'Nutrisari jeruk peras sachet', '749921010711', '749921010711', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2181, 'Marimas sirsak sachet', '8993500019943', '8993500019943', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2182, 'Marimas pink leci sachet', '8993500019141', '8993500019141', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2183, 'Marimas jeruk sachet', '8993500019912', '8993500019912', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2184, 'Marimas mangga sachet', '8993500019103', '8993500019103', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2185, 'Marimas strowberry sachet', '8993500019189', '8993500019189', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2186, 'Marimas melon sachet', '8993500019974', '8993500019974', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2187, 'Jasjus jambu sachet', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2188, 'Susu Jahe sidomuncul sachet', '8998898853100', '8998898853100', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2189, 'Milo kotak 180ml', '8992696523067', '8992696523067', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2190, 'Milo kotak 110ml', '8992696523081', '8992696523081', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2191, 'Indomilk kotak strowberry 115ml', '8993007000253', '8993007000253', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2192, 'Indomilk kotak cokelat 115ml', '8993007000239', '8993007000239', 1, 1, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `product` (`id`, `product`, `kode_product`, `kodebarcode`, `tipe_product`, `kategori_product`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2193, 'Indomilk botol cokelat', '8992702005945', '8992702005945', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2194, 'Indomilk botol strowberry', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2195, 'Indomilk botol melon', '8992702006003', '8992702006003', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2196, 'Tango kido banana kotak 115ml', '8991102714709', '8991102714709', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2197, 'Tango kido cokelat kotak 115ml', '8991102714624', '8991102714624', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2198, 'Tango kido strowberry kotak 115ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2199, 'Sasa santan kelapa 65ml', '8991188943536', '8991188943536', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2200, 'Sun kara santan kelapa 65ml', '8992717781025', '8992717781025', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2201, 'Bango kecap manis 60ml', '8999999514006', '8999999514006', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2202, 'Bango kecap manis 40ml', '8999999567583', '8999999567583', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2203, 'Bango kecap manis 20ml', '8999999533496', '8999999533496', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2204, 'Bango kecap manis 210ml', '8999999012625', '8999999012625', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2205, 'Sedap kecap manis 63ml', '8998866608039', '8998866608039', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2206, 'Tjapar kecap manis 500ml', '8997209080266', '8997209080266', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2207, 'Extra kecap manis 500ml', '8997209080259', '8997209080259', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2208, 'Extra kecap manis 625ml', '8997209080099', '8997209080099', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2209, 'Tjapar kecap manis 625ml', '8997209080143', '8997209080143', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2210, 'Indofood sambal pedas 135ml', '89686400427', '89686400427', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2211, 'Indofood saus tomat 135ml', '89686401721', '89686401721', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2212, 'Magic bumbu penyedap 7g', '8992696522664', '8992696522664', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2213, 'Masako ayam 9g', '8992770033130', '8992770033130', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2214, 'Royko rasa ayam 8g', '8999999502393', '8999999502393', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2215, 'Rinso detergen cair gold 20ml', '8999999556112', '8999999556112', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2216, 'Rinso detergen cair ungu 20ml', '8999999556129', '8999999556129', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2217, 'Sajiku tepung bumbu 240g', '8992770084064', '8992770084064', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2218, 'Sajiku tepung bumbu 80g', '8992770061010', '8992770061010', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2219, 'Mama suka bakwan krispi 100g', '8995102702117', '8995102702117', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2220, 'Sasa pisang goreng 75g', '8992736010168', '8992736010168', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2221, 'Sajiku nasi goreng ayam 20g', '8992770054012', '8992770054012', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2222, 'Sajiku nasi goreng pedas 20g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2223, 'Sasa micin 250g', '8991188943062', '8991188943062', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2224, 'Sasa micin 120g', '8991188943369', '8991188943369', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2225, 'Sasa micin 50g', '8991188943017', '8991188943017', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2226, 'Sasa micin 18g', '8991188943055', '8991188943055', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2227, 'Antaka bumbu bbq 100gram', '8997028630024', '8997028630024', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2228, 'Tepung maizena hawai 200g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2229, 'Desaku ketumbar bubuk 12,5g', '8997011931107', '8997011931107', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2230, 'Ladaku merica bubuk 4g', '8997011930612', '8997011930612', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2231, 'Sagu mutiara 100g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2232, 'Bihun jagung padamu 350g', '8997011700031', '8997011700031', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2233, 'Mie burung dara pipih 140g', '8991688890484', '8991688890484', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2234, 'Ekomie kering isi 2 120g', '8998866200981', '8998866200981', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2235, 'Mie telur 3 ayam isi 2 120g', '89686000108', '89686000108', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2236, 'Indomie goreng jumbo 129g', '89686041705', '89686041705', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2237, 'Sukses goreng  isi 2 ayam kremes', '8998866200882', '8998866200882', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2238, 'Indomie goreng 85g', '89686010947', '89686010947', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2239, 'Sedap goreng 90g', '8998866200301', '8998866200301', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2240, 'Sedap korean spicy', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2241, 'Sedap kuah ayam bawang 70g', '8998866200318', '8998866200318', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2242, 'Sedap kuah rasa soto 75g', '8998866200325', '8998866200325', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2243, 'Sedap kuah baso spesial 77g', '8998866200929', '8998866200929', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2244, 'Sedap kuah kari spesial 76g', '8998866200578', '8998866200578', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2245, 'Sarimi gelas soto ayam', '89686923063', '89686923063', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2246, 'Sarimi gelas baso sapi', '89686923117', '89686923117', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2247, 'Sarimi gelas ayam bawang', '89686923001', '89686923001', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2248, 'Pop mie instan cup rasa ayam', '89686060027', '89686060027', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2249, 'Pop mie instan cup kari ayam', '89686060461', '89686060461', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2250, 'Pop mie mini soto mi', '89686061079', '89686061079', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2251, 'Pop mie mini baso sapi', '89686061123', '89686061123', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2252, 'Pop mie mini ayam bawang', '89686061024', '89686061024', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2253, 'Felibite', '8994409101371', '8994409101371', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2254, 'Teh gelas 170ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2255, 'Teh rio 180ml', '8998866500388', '8998866500388', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2256, 'Kopikap cappucino 150ml', '8996001600153', '8996001600153', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2257, 'Iso cup leci 175ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2258, 'Minute maid jeruk 270ml', '8992761110048', '8992761110048', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2259, 'Torpedo rasa aneka buah 170ml', '8997007300016', '8997007300016', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2260, 'Floridina orange 350ml', '8998866500708', '8998866500708', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2261, 'Floridina coco 350ml', '8998866202923', '8998866202923', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2262, 'Teh pucuk harum 350ml', '8996001600146', '8996001600146', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2263, 'Milku strowberry 200ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2264, 'Milku cokelat 200ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2265, 'Iso plus isotonic 350ml', '8998866610377', '8998866610377', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2266, 'Sprite lemon-lime 250ml', '8992761145026', '8992761145026', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2267, 'Sprite lemon-lime 390ml', '8992761002022', '8992761002022', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2268, 'Fanta soda water drink 250ml', '8992761145118', '8992761145118', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2269, 'Fanta strowberry 390ml', '8992761002039', '8992761002039', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2270, 'Pocari sweat 500ml', '8997035563414', '8997035563414', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2271, 'Pocari sweat 350ml', '8997035563544', '8997035563544', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2272, 'Mizone lychee lemon 500ml', '8992752112013', '8992752112013', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2273, 'Aqua botol 600ml', '8886008101053', '8886008101053', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2274, 'Aqua botol 1,5 L', '8886008101091', '8886008101091', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2275, 'Oronaminc drink 120ml', '8997035601222', '8997035601222', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2276, 'Yakult 65ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2277, 'Roma biskuit kelapa 300g', '8996001301142', '8996001301142', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2278, 'Roma malkist kelapa kopyor 252g', '8996001312353', '8996001312353', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2279, 'Roma malkist cokelat kelapa 135g', '8996001301579', '8996001301579', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2280, 'Roma malkist abon 250g', '8996001302392', '8996001302392', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2281, 'Goodbis strowberry cream 200g', '8997028380196', '8997028380196', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2282, 'Goodbis blueberry cream 200g', '8997028380202', '8997028380202', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2283, 'Goodbis melon cream 200g', '8997028380219', '8997028380219', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2284, 'Goodbis chocolate cream 200g', '8997028380189', '8997028380189', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2285, 'Gangsar kacang atom 225g', '8994222112325', '8994222112325', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2286, 'Gangsar kacang atom 140g', '8994222112318', '8994222112318', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2287, 'Club gelas 48x150 ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2288, 'Club gelas 48x220 ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2289, 'Aqua gelas 220 ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2290, 'AICE  Semangka', '8997033170164', '8997033170164', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2291, 'AICE Nanas', '8885013130546', '8885013130546', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2292, 'AICE Miki-Miki', '8885013130393', '8885013130393', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2293, 'AICE Chocolate Stick', '8997033170140', '8997033170140', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2294, 'AICE 2 colour', '8885013130997', '8885013130997', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2295, 'AICE Es Jeruk', '8885013130676', '8885013130676', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2296, 'AICE Freezy Choco', '8885013130805', '8885013130805', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2297, 'AICE Milk Stick', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2298, 'AICE Miki-Miki Strowberry', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2299, 'AICE Melon', '8885013130249', '8885013130249', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2300, 'AICE Blueberry Yogurt', '8885013131406', '8885013131406', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2301, 'AICE Mochi Vanilla', '8885013130201', '8885013130201', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2302, 'AICE Mochi Strawberry', '8885013130690', '8885013130690', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2303, 'AICE Mochi Coklat', '8885013130645', '8885013130645', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2304, 'AICE Mochi Durian', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2305, 'AICE Sweet Corn', '8997033170027', '8997033170027', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2306, 'AICE Coffee Cryspy', '8885013130485', '8885013130485', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2307, 'AICE Taro Cryspy', '8885013131123', '8885013131123', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2308, 'AICE Fruits Twister', '8885013130751', '8885013130751', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2309, 'AICE Strawberry Cryspy', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2310, 'AICE Chocalate Cryspy', '8885013130058', '8885013130058', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2311, 'AICE Choco Melt', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2312, 'AICE Strowberry Cone', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2313, 'AICE Mango Slush', '8885013130041', '8885013130041', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2314, 'AICE Taro Cone', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2315, 'AICE Sunday Chocolate', '8885013130706', '8885013130706', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2316, 'AICE Sunday Strowberry', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2317, 'AICE Sunday Avocado', '8885013130874', '8885013130874', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2318, 'AICE Choco cookies', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2319, 'AICE  Sandwich', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2320, 'Beras ikan mas 3kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2321, 'Beras 99 3kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2322, 'Beras mangga 3kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2323, 'Beras koi 5kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2324, 'Beras 99 5kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2325, 'Beras pak tani 5kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2326, 'Beras jambu 5kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2327, 'Beras bintang 5kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2328, 'Jarum hijau 10kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2329, 'Beras 99 10kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2330, 'Beras pak tani 10kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2331, 'Beras koi 10kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2332, 'Beras gurami 10kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2333, 'Jarum hijau 25kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2334, 'Beras 99 25kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2335, 'Beras polos 25kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2336, 'Beras gurami 25kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2337, 'Beras Koi 25kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2338, 'Soklin Softergen Cheerful Red 770g', '8998866610438', '8998866610438', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2339, 'Swalow Black Tggng', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2340, 'Swalow Black Seri', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2341, 'Swalo Seri', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2342, 'Swalow Khusus ( 10, 10,5)', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2343, 'Swalow no 11', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2344, 'Swalow Army', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2345, 'Mely Khusus ( 10, 10,5)', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2346, 'Mely Seri', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2347, 'Mely Tnggng', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2348, 'Batle siip', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2349, 'New Era Tanggung', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2350, 'New Era Seri', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2351, 'New Era Khusus', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2352, 'Gramaxone 1l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2353, 'Calaris 250ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2354, 'Calaris 500ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2355, 'Kayabas 250ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2356, 'Kayabas 500', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2357, 'Amexone', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2358, 'Primarin B500', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2359, 'Santaquat 1l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2360, 'Elang 1l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2361, 'Starmin 500ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2362, 'Rambo 1l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2363, 'Gempur 1l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2364, 'Starmin 400ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2365, 'Starmin 1l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2366, 'Bassa 100ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2367, 'Decis 100ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2368, 'Decis 50ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2369, 'VitaChicks 5g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2370, 'Furadan 2kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2371, 'Furadan 1kg', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2372, 'Fast Up 500ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2373, 'Mip Cinta 100g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2374, 'Metindo 100g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2375, 'Rafia 1k', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2376, 'Rafia 2,5k', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2377, 'Rafia 5k', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2378, 'Rafia 10k', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2379, 'Remi Las Vegas', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2380, 'Domino Gunting', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2381, 'Ceki Jitu', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2382, 'Krupuk Uyel', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2383, 'Kerupuk Uyel', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2384, 'Kerupuk Bintang', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2385, 'Kok Semeru', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2386, 'Kok National', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2387, 'Keset Cendol', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2388, 'Keset Karakter', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2389, 'Sikat WC Gragon pndk', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2390, 'Sikat Dragon Panjng', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2391, 'Sapu Slamet Merang', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2392, 'Sapu Slamet Hitam', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2393, 'Garuk Air Kayu', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2394, 'Sapu Korek pnjng 2ikan', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2395, 'Rigoletto 2,5l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2396, 'Rigoletto 3l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2397, 'Rigoletto 5l', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2398, 'Leser Atom no1', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2399, 'Leser Atom no2', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2400, 'Leser Atom no 3', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2401, 'Leser Atom no 4', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2402, 'Termos stainless steal 0,35L', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2403, 'Termos stainless steal 0,75L', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2404, 'Termos stainless steal 1L', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2405, 'Ember asul2 888', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2406, 'Hydro bottle 1500ml', '8999979040730', '8999979040730', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2407, 'AKVO Bottle 1000ml', '8999979040600', '8999979040600', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2408, 'Sport drink bottle 1,5', '8999979002585', '8999979002585', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2409, 'AKVO Bottle 800ml', '8999979040488', '8999979040488', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2410, 'Rolex bottle', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2411, 'Vian bottle', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2412, 'Mini bottle', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2413, 'My bottle 500ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2414, 'Botol saus kangguru', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2415, 'Botol saus mama 400ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2416, 'Botol saus mama 600ml', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2417, 'Cetakan jelly stik/ice isi 6', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2418, 'Torong air besar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2419, 'Tikar karakter bagus', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2420, 'Tikar karakter biasa', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2421, 'Perlak lantai cap banteng', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2422, 'Hunger baju lestari besar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2423, 'Hunger baju lestari kecil', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2424, 'Gayung mandi besar biasa', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2425, 'Gayung mandi besar bagus', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2426, 'Gayung mandi anak kecil', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2427, 'Cantolan baju besi', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2428, 'Gantungan popok maspion isi 24', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2429, 'Wadah takar air', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2430, 'Parut keju biasa', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2431, 'Parut keju bagus', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2432, 'Irus jangan kecil', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2433, 'Irus jangan tanggung', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2434, 'Irus jangan besar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2435, 'Sotel kecil', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2436, 'Sotel tanggung', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2437, 'Sotel besar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2438, 'Asbak stainless', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2439, 'Tempat sabun', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2440, 'Pisau kecil', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2441, 'Pisau stainless steel', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2442, 'Parutan kelapa', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2443, 'Timbo segi enam', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2444, 'Timbo hitam polos kecil', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2445, 'Timbo hitam polos tanggung', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2446, 'Timbo hitam polos besar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2447, 'Bak hitam kecil', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2448, 'Bak hitam tanggung', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2449, 'Bak hitam besar', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2450, 'Gentong merah 40L', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2451, 'Gentong merah 30L', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2452, 'Gentong merah 80L', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2453, 'Gerry salut wafer coconut 21g', '8992775001011', '8992775001011', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2454, 'Beng-beng', '8886001038011', '8886001038011', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2455, 'Slai olai strowberry 24g', '8996001304549', '8996001304549', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2456, 'Roma malkist abon 27g', '8996001302316', '8996001302316', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2457, 'Coki-coki choco cashew', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2458, 'Nabati wafer coklat 7,5g', '8993175532891', '8993175532891', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2459, 'Tango wafer coklat 7g', '8991102385084', '8991102385084', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2460, 'Tango wafer strowberry 7g', '8991102989305', '8991102989305', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2461, 'Nabati ahh recheese 5,5g', '8993175532297', '8993175532297', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2462, 'Deo go choco crepes', '8886013459248', '8886013459248', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2463, 'Roma malkist cappucino', '8996001312438', '8996001312438', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2464, 'Deo chocopie', '8994834003288', '8994834003288', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2465, 'Go potato original', '8994834000331', '8994834000331', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2466, 'Golden chips 17g', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2467, 'Spix mie goreng 10g', '8994834000430', '8994834000430', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2468, 'Goriorio vanila', '8994834000218', '8994834000218', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2469, 'Goriorio coklat', '8994834000225', '8994834000225', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2470, 'Chocho coklat compound', '8992952951092', '8992952951092', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2471, 'Nit nut kacang panggang 5g', '8991002501683', '8991002501683', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2472, 'Gemez enak snack mi', '8886013700104', '8886013700104', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2473, 'Miko banana flavour 8g', '8997009463054', '8997009463054', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2474, 'spix soba ayam bakar', '8994834000478', '8994834000478', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2475, 'Manis madu', '8997009461012', '8997009461012', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2476, 'Kripstos sambal balado', '8997006170054', '8997006170054', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2477, 'Putra bali', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2478, 'Biskuat original 8,4g', '7622210437617', '7622210437617', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2479, 'Mayasi kuaci 5g', '8991002503533', '8991002503533', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2480, 'Kacang koro dua kelinci original', '8995077600890', '8995077600890', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2481, 'Kacang koro dua kelinci rumputlaut', '8995077603075', '8995077603075', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2482, 'Twistko 12g', '8886013257400', '8886013257400', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2483, 'Tango wafer vanila milk 47g', '8991102387262', '8991102387262', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2484, 'Tango waffle cranch chox', '8991102386586', '8991102386586', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2485, 'Nabati rolls chocolate stik 40g', '8993175538633', '8993175538633', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2486, 'Kevin brownis pisang', '8997026180620', '8997026180620', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2487, 'kevin bronis cokelat', '8997026180620', '8997026180620', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2488, 'Kevin brownis pandan', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2489, 'Mooncake original', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2490, 'Roti kevin cocopandan', '8997026180309', '8997026180309', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2491, 'Roti kevin bluberry', '8997026180309', '8997026180309', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2492, 'Roti yuli kacang coklat', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2493, 'Roti yuli messes coklat', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2494, 'Roti yuli keju', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2495, 'Roti pia wahyu bakery', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2496, 'Roti sisir krisna', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2497, 'Suki ayam kecap 17g', '8994834001895', '8994834001895', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2498, 'French fries kentang goreng 24g', '8886013281481', '8886013281481', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2499, 'Nabati wafer keju 50g', '8993175535878', '8993175535878', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2500, 'Nabati pink lava 50g', '8993175542241', '8993175542241', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2501, 'Nabati wafer white 50g', '8993175539517', '8993175539517', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2502, 'Kripik usus bakar barbeque', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2503, 'Nabati siip keju', '8993175531702', '8993175531702', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2504, 'Nabati siip cokelat', '8993175531696', '8993175531696', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2505, 'Nextar strawberry', '8993175548014', '8993175548014', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2506, 'Nextar nanas', '8993175538541', '8993175538541', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2507, 'Nextar cokelat', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2508, 'Mister kentang goreng', '8994834005510', '8994834005510', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2509, 'Makaroni ikan kembung picsus', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2510, 'Nugget goreng raja udang', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2511, 'Keripik kaca pakset', '-', '-', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2512, 'Indomilk kental manis 545g', '8993007003902', '8993007003902', 1, 1, NULL, NULL, NULL, NULL, NULL, 0),
(2513, 'Pertamax', '-', '-', 1, 1, '-', '2021-11-01', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_distriburst`
--

CREATE TABLE `product_distriburst` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_foto`
--

CREATE TABLE `product_has_foto` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `foto` longtext,
  `status` varchar(45) DEFAULT NULL COMMENT 'updated : File Diganti dengan file lain',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_angsuran`
--

CREATE TABLE `product_has_harga_angsuran` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `ansuran` int(11) NOT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `harga_total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_grosir`
--

CREATE TABLE `product_has_harga_grosir` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual`
--

CREATE TABLE `product_has_harga_jual` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_has_harga_jual`
--

INSERT INTO `product_has_harga_jual` (`id`, `product_satuan`, `harga`, `createddate`, `createdby`) VALUES
(1675, 1675, 24000, NULL, NULL),
(1676, 1676, 18500, NULL, NULL),
(1677, 1677, 21000, NULL, NULL),
(1678, 1678, 21000, NULL, NULL),
(1679, 1679, 21000, NULL, NULL),
(1680, 1680, 11000, NULL, NULL),
(1681, 1681, 18500, NULL, NULL),
(1682, 1682, 17500, NULL, NULL),
(1683, 1683, 13000, NULL, NULL),
(1684, 1684, 12000, NULL, NULL),
(1685, 1685, 17500, NULL, NULL),
(1686, 1686, 24500, NULL, NULL),
(1687, 1687, 17500, NULL, NULL),
(1688, 1688, 23500, NULL, NULL),
(1689, 1689, 17000, NULL, NULL),
(1690, 1690, 23500, NULL, NULL),
(1691, 1691, 23500, NULL, NULL),
(1692, 1692, 18500, NULL, NULL),
(1693, 1693, 12000, NULL, NULL),
(1694, 1694, 12000, NULL, NULL),
(1695, 1695, 15000, NULL, NULL),
(1696, 1696, 19500, NULL, NULL),
(1697, 1697, 19500, NULL, NULL),
(1698, 1698, 20000, NULL, NULL),
(1699, 1699, 18000, NULL, NULL),
(1700, 1700, 13000, NULL, NULL),
(1701, 1701, 24000, NULL, NULL),
(1702, 1702, 23500, NULL, NULL),
(1703, 1703, 15000, NULL, NULL),
(1704, 1704, 7500, NULL, NULL),
(1705, 1705, 8500, NULL, NULL),
(1706, 1706, 18000, NULL, NULL),
(1707, 1707, 15000, NULL, NULL),
(1708, 1708, 10000, NULL, NULL),
(1709, 1709, 30000, NULL, NULL),
(1710, 1710, 8000, NULL, NULL),
(1711, 1711, 17500, NULL, NULL),
(1712, 1712, 22000, NULL, NULL),
(1713, 1713, 16500, NULL, NULL),
(1714, 1714, 18500, NULL, NULL),
(1715, 1715, 18500, NULL, NULL),
(1716, 1716, 10000, NULL, NULL),
(1717, 1717, 4000, NULL, NULL),
(1718, 1718, 3500, NULL, NULL),
(1719, 1719, 2500, NULL, NULL),
(1720, 1720, 2000, NULL, NULL),
(1721, 1721, 3000, NULL, NULL),
(1722, 1722, 3000, NULL, NULL),
(1723, 1723, 2500, NULL, NULL),
(1724, 1724, 3500, NULL, NULL),
(1725, 1725, 6500, NULL, NULL),
(1726, 1726, 10000, NULL, NULL),
(1727, 1727, 3500, NULL, NULL),
(1728, 1728, 7500, NULL, NULL),
(1729, 1729, 4000, NULL, NULL),
(1730, 1730, 2500, NULL, NULL),
(1731, 1731, 2000, NULL, NULL),
(1732, 1732, 5500, NULL, NULL),
(1733, 1733, 3500, NULL, NULL),
(1734, 1734, 27000, NULL, NULL),
(1735, 1735, 16000, NULL, NULL),
(1736, 1736, 10000, NULL, NULL),
(1737, 1737, 12500, NULL, NULL),
(1738, 1738, 14000, NULL, NULL),
(1739, 1739, 10000, NULL, NULL),
(1740, 1740, 15000, NULL, NULL),
(1741, 1741, 1500, NULL, NULL),
(1742, 1742, 7000, NULL, NULL),
(1743, 1743, 12500, NULL, NULL),
(1744, 1744, 18500, NULL, NULL),
(1745, 1745, 16000, NULL, NULL),
(1746, 1746, 16500, NULL, NULL),
(1747, 1747, 23000, NULL, NULL),
(1748, 1748, 3000, NULL, NULL),
(1749, 1749, 4000, NULL, NULL),
(1750, 1750, 3500, NULL, NULL),
(1751, 1751, 2500, NULL, NULL),
(1752, 1752, 2500, NULL, NULL),
(1753, 1753, 2500, NULL, NULL),
(1754, 1754, 4500, NULL, NULL),
(1755, 1755, 3500, NULL, NULL),
(1756, 1756, 3500, NULL, NULL),
(1757, 1757, 4500, NULL, NULL),
(1758, 1758, 4500, NULL, NULL),
(1759, 1759, 15000, NULL, NULL),
(1760, 1760, 12000, NULL, NULL),
(1761, 1761, 2000, NULL, NULL),
(1762, 1762, 2000, NULL, NULL),
(1763, 1763, 10000, NULL, NULL),
(1764, 1764, 6500, NULL, NULL),
(1765, 1765, 12000, NULL, NULL),
(1766, 1766, 4000, NULL, NULL),
(1767, 1767, 1500, NULL, NULL),
(1768, 1768, 2500, NULL, NULL),
(1769, 1769, 25000, NULL, NULL),
(1770, 1770, 20000, NULL, NULL),
(1771, 1771, 3500, NULL, NULL),
(1772, 1772, 10000, NULL, NULL),
(1773, 1773, 1000, NULL, NULL),
(1774, 1774, 17000, NULL, NULL),
(1775, 1775, 4500, NULL, NULL),
(1776, 1776, 9000, NULL, NULL),
(1777, 1777, 8500, NULL, NULL),
(1778, 1778, 17000, NULL, NULL),
(1779, 1779, 15000, NULL, NULL),
(1780, 1780, 12000, NULL, NULL),
(1781, 1781, 25000, NULL, NULL),
(1782, 1782, 17000, NULL, NULL),
(1783, 1783, 27500, NULL, NULL),
(1784, 1784, 13000, NULL, NULL),
(1785, 1785, 0, NULL, NULL),
(1786, 1786, 18000, NULL, NULL),
(1787, 1787, 7000, NULL, NULL),
(1788, 1788, 12500, NULL, NULL),
(1789, 1789, 10000, NULL, NULL),
(1790, 1790, 24000, NULL, NULL),
(1791, 1791, 3500, NULL, NULL),
(1792, 1792, 3500, NULL, NULL),
(1793, 1793, 12000, NULL, NULL),
(1794, 1794, 9500, NULL, NULL),
(1795, 1795, 6500, NULL, NULL),
(1796, 1796, 4500, NULL, NULL),
(1797, 1797, 13500, NULL, NULL),
(1798, 1798, 7500, NULL, NULL),
(1799, 1799, 7500, NULL, NULL),
(1800, 1800, 11000, NULL, NULL),
(1801, 1801, 21000, NULL, NULL),
(1802, 1802, 0, NULL, NULL),
(1803, 1803, 6500, NULL, NULL),
(1804, 1804, 75000, NULL, NULL),
(1805, 1805, 65000, NULL, NULL),
(1806, 1806, 6000, NULL, NULL),
(1807, 1807, 34000, NULL, NULL),
(1808, 1808, 51000, NULL, NULL),
(1809, 1809, 57500, NULL, NULL),
(1810, 1810, 12000, NULL, NULL),
(1811, 1811, 22500, NULL, NULL),
(1812, 1812, 38000, NULL, NULL),
(1813, 1813, 8000, NULL, NULL),
(1814, 1814, 8000, NULL, NULL),
(1815, 1815, 38000, NULL, NULL),
(1816, 1816, 45000, NULL, NULL),
(1817, 1817, 8000, NULL, NULL),
(1818, 1818, 7000, NULL, NULL),
(1819, 1819, 6000, NULL, NULL),
(1820, 1820, 4000, NULL, NULL),
(1821, 1821, 7500, NULL, NULL),
(1822, 1822, 1000, NULL, NULL),
(1823, 1823, 2000, NULL, NULL),
(1824, 1824, 2000, NULL, NULL),
(1825, 1825, 2000, NULL, NULL),
(1826, 1826, 2000, NULL, NULL),
(1827, 1827, 1000, NULL, NULL),
(1828, 1828, 12000, NULL, NULL),
(1829, 1829, 10000, NULL, NULL),
(1830, 1830, 10000, NULL, NULL),
(1831, 1831, 2500, NULL, NULL),
(1832, 1832, 2500, NULL, NULL),
(1833, 1833, 10000, NULL, NULL),
(1834, 1834, 10000, NULL, NULL),
(1835, 1835, 17500, NULL, NULL),
(1836, 1836, 16000, NULL, NULL),
(1837, 1837, 1000, NULL, NULL),
(1838, 1838, 14000, NULL, NULL),
(1839, 1839, 10000, NULL, NULL),
(1840, 1840, 5000, NULL, NULL),
(1841, 1841, 6500, NULL, NULL),
(1842, 1842, 4500, NULL, NULL),
(1843, 1843, 4000, NULL, NULL),
(1844, 1844, 5000, NULL, NULL),
(1845, 1845, 0, NULL, NULL),
(1846, 1846, 28000, NULL, NULL),
(1847, 1847, 45000, NULL, NULL),
(1848, 1848, 16000, NULL, NULL),
(1849, 1849, 11000, NULL, NULL),
(1850, 1850, 4000, NULL, NULL),
(1851, 1851, 12000, NULL, NULL),
(1852, 1852, 10000, NULL, NULL),
(1853, 1853, 8000, NULL, NULL),
(1854, 1854, 1000, NULL, NULL),
(1855, 1855, 1000, NULL, NULL),
(1856, 1856, 1000, NULL, NULL),
(1857, 1857, 8000, NULL, NULL),
(1858, 1858, 5000, NULL, NULL),
(1859, 1859, 1000, NULL, NULL),
(1860, 1860, 5000, NULL, NULL),
(1861, 1861, 1500, NULL, NULL),
(1862, 1862, 1000, NULL, NULL),
(1863, 1863, 6000, NULL, NULL),
(1864, 1864, 8000, NULL, NULL),
(1865, 1865, 15000, NULL, NULL),
(1866, 1866, 8000, NULL, NULL),
(1867, 1867, 12000, NULL, NULL),
(1868, 1868, 14500, NULL, NULL),
(1869, 1869, 9000, NULL, NULL),
(1870, 1870, 15000, NULL, NULL),
(1871, 1871, 15000, NULL, NULL),
(1872, 1872, 21000, NULL, NULL),
(1873, 1873, 14000, NULL, NULL),
(1874, 1874, 16000, NULL, NULL),
(1875, 1875, 750, NULL, NULL),
(1876, 1876, 15000, NULL, NULL),
(1877, 1877, 1000, NULL, NULL),
(1878, 1878, 1000, NULL, NULL),
(1879, 1879, 1000, NULL, NULL),
(1880, 1880, 1500, NULL, NULL),
(1881, 1881, 3500, NULL, NULL),
(1882, 1882, 5000, NULL, NULL),
(1883, 1883, 9000, NULL, NULL),
(1884, 1884, 9000, NULL, NULL),
(1885, 1885, 9000, NULL, NULL),
(1886, 1886, 7500, NULL, NULL),
(1887, 1887, 0, NULL, NULL),
(1888, 1888, 12500, NULL, NULL),
(1889, 1889, 3000, NULL, NULL),
(1890, 1890, 1000, NULL, NULL),
(1891, 1891, 0, NULL, NULL),
(1892, 1892, 5000, NULL, NULL),
(1893, 1893, 5000, NULL, NULL),
(1894, 1894, 0, NULL, NULL),
(1895, 1895, 500, NULL, NULL),
(1896, 1896, 1000, NULL, NULL),
(1897, 1897, 2000, NULL, NULL),
(1898, 1898, 10000, NULL, NULL),
(1899, 1899, 15000, NULL, NULL),
(1900, 1900, 21000, NULL, NULL),
(1901, 1901, 0, NULL, NULL),
(1902, 1902, 0, NULL, NULL),
(1903, 1903, 0, NULL, NULL),
(1904, 1904, 0, NULL, NULL),
(1905, 1905, 0, NULL, NULL),
(1906, 1906, 3500, NULL, NULL),
(1907, 1907, 4500, NULL, NULL),
(1908, 1908, 10000, NULL, NULL),
(1909, 1909, 16500, NULL, NULL),
(1910, 1910, 500, NULL, NULL),
(1911, 1911, 500, NULL, NULL),
(1912, 1912, 500, NULL, NULL),
(1913, 1913, 5000, NULL, NULL),
(1914, 1914, 0, NULL, NULL),
(1915, 1915, 0, NULL, NULL),
(1916, 1916, 3500, NULL, NULL),
(1917, 1917, 0, NULL, NULL),
(1918, 1918, 0, NULL, NULL),
(1919, 1919, 4000, NULL, NULL),
(1920, 1920, 5000, NULL, NULL),
(1921, 1921, 5000, NULL, NULL),
(1922, 1922, 12000, NULL, NULL),
(1923, 1923, 500, NULL, NULL),
(1924, 1924, 2500, NULL, NULL),
(1925, 1925, 2500, NULL, NULL),
(1926, 1926, 750, NULL, NULL),
(1927, 1927, 1500, NULL, NULL),
(1928, 1928, 8500, NULL, NULL),
(1929, 1929, 2500, NULL, NULL),
(1930, 1930, 500, NULL, NULL),
(1931, 1931, 1000, NULL, NULL),
(1932, 1932, 3000, NULL, NULL),
(1933, 1933, 3000, NULL, NULL),
(1934, 1934, 7500, NULL, NULL),
(1935, 1935, 2000, NULL, NULL),
(1936, 1936, 2500, NULL, NULL),
(1937, 1937, 3000, NULL, NULL),
(1938, 1938, 3000, NULL, NULL),
(1939, 1939, 3000, NULL, NULL),
(1940, 1940, 3000, NULL, NULL),
(1941, 1941, 3000, NULL, NULL),
(1942, 1942, 3000, NULL, NULL),
(1943, 1943, 2500, NULL, NULL),
(1944, 1944, 2500, NULL, NULL),
(1945, 1945, 2500, NULL, NULL),
(1946, 1946, 1500, NULL, NULL),
(1947, 1947, 1500, NULL, NULL),
(1948, 1948, 1500, NULL, NULL),
(1949, 1949, 1500, NULL, NULL),
(1950, 1950, 1500, NULL, NULL),
(1951, 1951, 1750, NULL, NULL),
(1952, 1952, 1500, NULL, NULL),
(1953, 1953, 3500, NULL, NULL),
(1954, 1954, 2500, NULL, NULL),
(1955, 1955, 4500, NULL, NULL),
(1956, 1956, 4000, NULL, NULL),
(1957, 1957, 0, NULL, NULL),
(1958, 1958, 9000, NULL, NULL),
(1959, 1959, 4000, NULL, NULL),
(1960, 1960, 9000, NULL, NULL),
(1961, 1961, 5000, NULL, NULL),
(1962, 1962, 3500, NULL, NULL),
(1963, 1963, 3000, NULL, NULL),
(1964, 1964, 3000, NULL, NULL),
(1965, 1965, 2000, NULL, NULL),
(1966, 1966, 1500, NULL, NULL),
(1967, 1967, 20000, NULL, NULL),
(1968, 1968, 18000, NULL, NULL),
(1969, 1969, 15000, NULL, NULL),
(1970, 1970, 500, NULL, NULL),
(1971, 1971, 4500, NULL, NULL),
(1972, 1972, 750, NULL, NULL),
(1973, 1973, 2500, NULL, NULL),
(1974, 1974, 2500, NULL, NULL),
(1975, 1975, 1000, NULL, NULL),
(1976, 1976, 1500, NULL, NULL),
(1977, 1977, 1000, NULL, NULL),
(1978, 1978, 7000, NULL, NULL),
(1979, 1979, 6000, NULL, NULL),
(1980, 1980, 8000, NULL, NULL),
(1981, 1981, 4000, NULL, NULL),
(1982, 1982, 5000, NULL, NULL),
(1983, 1983, 2000, NULL, NULL),
(1984, 1984, 0, NULL, NULL),
(1985, 1985, 500, NULL, NULL),
(1986, 1986, 500, NULL, NULL),
(1987, 1987, 500, NULL, NULL),
(1988, 1988, 500, NULL, NULL),
(1989, 1989, 500, NULL, NULL),
(1990, 1990, 500, NULL, NULL),
(1991, 1991, 500, NULL, NULL),
(1992, 1992, 500, NULL, NULL),
(1993, 1993, 500, NULL, NULL),
(1994, 1994, 500, NULL, NULL),
(1995, 1995, 1000, NULL, NULL),
(1996, 1996, 1000, NULL, NULL),
(1997, 1997, 1000, NULL, NULL),
(1998, 1998, 500, NULL, NULL),
(1999, 1999, 500, NULL, NULL),
(2000, 2000, 250, NULL, NULL),
(2001, 2001, 250, NULL, NULL),
(2002, 2002, 0, NULL, NULL),
(2003, 2003, 0, NULL, NULL),
(2004, 2004, 250, NULL, NULL),
(2005, 2005, 250, NULL, NULL),
(2006, 2006, 250, NULL, NULL),
(2007, 2007, 250, NULL, NULL),
(2008, 2008, 250, NULL, NULL),
(2009, 2009, 250, NULL, NULL),
(2010, 2010, 250, NULL, NULL),
(2011, 2011, 3000, NULL, NULL),
(2012, 2012, 1000, NULL, NULL),
(2013, 2013, 1000, NULL, NULL),
(2014, 2014, 1000, NULL, NULL),
(2015, 2015, 1000, NULL, NULL),
(2016, 2016, 1000, NULL, NULL),
(2017, 2017, 1000, NULL, NULL),
(2018, 2018, 1000, NULL, NULL),
(2019, 2019, 1000, NULL, NULL),
(2020, 2020, 1000, NULL, NULL),
(2021, 2021, 1500, NULL, NULL),
(2022, 2022, 500, NULL, NULL),
(2023, 2023, 500, NULL, NULL),
(2024, 2024, 500, NULL, NULL),
(2025, 2025, 500, NULL, NULL),
(2026, 2026, 5500, NULL, NULL),
(2027, 2027, 500, NULL, NULL),
(2028, 2028, 1500, NULL, NULL),
(2029, 2029, 6500, NULL, NULL),
(2030, 2030, 12000, NULL, NULL),
(2031, 2031, 7500, NULL, NULL),
(2032, 2032, 6500, NULL, NULL),
(2033, 2033, 3500, NULL, NULL),
(2034, 2034, 1000, NULL, NULL),
(2035, 2035, 750, NULL, NULL),
(2036, 2036, 15000, NULL, NULL),
(2037, 2037, 2500, NULL, NULL),
(2038, 2038, 7000, NULL, NULL),
(2039, 2039, 7000, NULL, NULL),
(2040, 2040, 7000, NULL, NULL),
(2041, 2041, 5000, NULL, NULL),
(2042, 2042, 2500, NULL, NULL),
(2043, 2043, 2500, NULL, NULL),
(2044, 2044, 2500, NULL, NULL),
(2045, 2045, 2500, NULL, NULL),
(2046, 2046, 3500, NULL, NULL),
(2047, 2047, 3000, NULL, NULL),
(2048, 2048, 2500, NULL, NULL),
(2049, 2049, 3000, NULL, NULL),
(2050, 2050, 2500, NULL, NULL),
(2051, 2051, 2000, NULL, NULL),
(2052, 2052, 2500, NULL, NULL),
(2053, 2053, 2500, NULL, NULL),
(2054, 2054, 2500, NULL, NULL),
(2055, 2055, 2500, NULL, NULL),
(2056, 2056, 3000, NULL, NULL),
(2057, 2057, 2500, NULL, NULL),
(2058, 2058, 2000, NULL, NULL),
(2059, 2059, 11000, NULL, NULL),
(2060, 2060, 15000, NULL, NULL),
(2061, 2061, 15000, NULL, NULL),
(2062, 2062, 19000, NULL, NULL),
(2063, 2063, 19000, NULL, NULL),
(2064, 2064, 19000, NULL, NULL),
(2065, 2065, 18000, NULL, NULL),
(2066, 2066, 18000, NULL, NULL),
(2067, 2067, 10000, NULL, NULL),
(2068, 2068, 22000, NULL, NULL),
(2069, 2069, 2500, NULL, NULL),
(2070, 2070, 2500, NULL, NULL),
(2071, 2071, 10000, NULL, NULL),
(2072, 2072, 7000, NULL, NULL),
(2073, 2073, 7000, NULL, NULL),
(2074, 2074, 7000, NULL, NULL),
(2075, 2075, 7000, NULL, NULL),
(2076, 2076, 7000, NULL, NULL),
(2077, 2077, 3000, NULL, NULL),
(2078, 2078, 3000, NULL, NULL),
(2079, 2079, 7000, NULL, NULL),
(2080, 2080, 7000, NULL, NULL),
(2081, 2081, 0, NULL, NULL),
(2082, 2082, 15000, NULL, NULL),
(2083, 2083, 10000, NULL, NULL),
(2084, 2084, 3000, NULL, NULL),
(2085, 2085, 2500, NULL, NULL),
(2086, 2086, 2000, NULL, NULL),
(2087, 2087, 1000, NULL, NULL),
(2088, 2088, 2000, NULL, NULL),
(2089, 2089, 2000, NULL, NULL),
(2090, 2090, 3000, NULL, NULL),
(2091, 2091, 3000, NULL, NULL),
(2092, 2092, 2500, NULL, NULL),
(2093, 2093, 6000, NULL, NULL),
(2094, 2094, 14000, NULL, NULL),
(2095, 2095, 10000, NULL, NULL),
(2096, 2096, 2500, NULL, NULL),
(2097, 2097, 5000, NULL, NULL),
(2098, 2098, 5000, NULL, NULL),
(2099, 2099, 5000, NULL, NULL),
(2100, 2100, 19500, NULL, NULL),
(2101, 2101, 16500, NULL, NULL),
(2102, 2102, 16500, NULL, NULL),
(2103, 2103, 12500, NULL, NULL),
(2104, 2104, 12500, NULL, NULL),
(2105, 2105, 16000, NULL, NULL),
(2106, 2106, 12000, NULL, NULL),
(2107, 2107, 12000, NULL, NULL),
(2108, 2108, 10000, NULL, NULL),
(2109, 2109, 4000, NULL, NULL),
(2110, 2110, 22000, NULL, NULL),
(2111, 2111, 5000, NULL, NULL),
(2112, 2112, 17000, NULL, NULL),
(2113, 2113, 2000, NULL, NULL),
(2114, 2114, 2000, NULL, NULL),
(2115, 2115, 1000, NULL, NULL),
(2116, 2116, 15000, NULL, NULL),
(2117, 2117, 15000, NULL, NULL),
(2118, 2118, 16000, NULL, NULL),
(2119, 2119, 30000, NULL, NULL),
(2120, 2120, 28500, NULL, NULL),
(2121, 2121, 28500, NULL, NULL),
(2122, 2122, 1000, NULL, NULL),
(2123, 2123, 2000, NULL, NULL),
(2124, 2124, 1000, NULL, NULL),
(2125, 2125, 5000, NULL, NULL),
(2126, 2126, 1000, NULL, NULL),
(2127, 2127, 15000, NULL, NULL),
(2128, 2128, 12000, NULL, NULL),
(2129, 2129, 4000, NULL, NULL),
(2130, 2130, 1250, NULL, NULL),
(2131, 2131, 10000, NULL, NULL),
(2132, 2132, 10000, NULL, NULL),
(2133, 2133, 1500, NULL, NULL),
(2134, 2134, 6000, NULL, NULL),
(2135, 2135, 5500, NULL, NULL),
(2136, 2136, 3500, NULL, NULL),
(2137, 2137, 2500, NULL, NULL),
(2138, 2138, 1500, NULL, NULL),
(2139, 2139, 1500, NULL, NULL),
(2140, 2140, 2000, NULL, NULL),
(2141, 2141, 1500, NULL, NULL),
(2142, 2142, 1500, NULL, NULL),
(2143, 2143, 1500, NULL, NULL),
(2144, 2144, 1500, NULL, NULL),
(2145, 2145, 1250, NULL, NULL),
(2146, 2146, 1000, NULL, NULL),
(2147, 2147, 1000, NULL, NULL),
(2148, 2148, 1000, NULL, NULL),
(2149, 2149, 500, NULL, NULL),
(2150, 2150, 6000, NULL, NULL),
(2151, 2151, 6000, NULL, NULL),
(2152, 2152, 6500, NULL, NULL),
(2153, 2153, 3500, NULL, NULL),
(2154, 2154, 5000, NULL, NULL),
(2155, 2155, 5000, NULL, NULL),
(2156, 2156, 15000, NULL, NULL),
(2157, 2157, 8000, NULL, NULL),
(2158, 2158, 4500, NULL, NULL),
(2159, 2159, 11000, NULL, NULL),
(2160, 2160, 11000, NULL, NULL),
(2161, 2161, 10000, NULL, NULL),
(2162, 2162, 10000, NULL, NULL),
(2163, 2163, 4500, NULL, NULL),
(2164, 2164, 5000, NULL, NULL),
(2165, 2165, 2000, NULL, NULL),
(2166, 2166, 2000, NULL, NULL),
(2167, 2167, 2000, NULL, NULL),
(2168, 2168, 2000, NULL, NULL),
(2169, 2169, 2000, NULL, NULL),
(2170, 2170, 2000, NULL, NULL),
(2171, 2171, 3000, NULL, NULL),
(2172, 2172, 2000, NULL, NULL),
(2173, 2173, 2000, NULL, NULL),
(2174, 2174, 1500, NULL, NULL),
(2175, 2175, 1500, NULL, NULL),
(2176, 2176, 1500, NULL, NULL),
(2177, 2177, 1500, NULL, NULL),
(2178, 2178, 1500, NULL, NULL),
(2179, 2179, 500, NULL, NULL),
(2180, 2180, 500, NULL, NULL),
(2181, 2181, 500, NULL, NULL),
(2182, 2182, 500, NULL, NULL),
(2183, 2183, 500, NULL, NULL),
(2184, 2184, 500, NULL, NULL),
(2185, 2185, 500, NULL, NULL),
(2186, 2186, 1500, NULL, NULL),
(2187, 2187, 4000, NULL, NULL),
(2188, 2188, 3000, NULL, NULL),
(2189, 2189, 3000, NULL, NULL),
(2190, 2190, 3000, NULL, NULL),
(2191, 2191, 4000, NULL, NULL),
(2192, 2192, 4000, NULL, NULL),
(2193, 2193, 4000, NULL, NULL),
(2194, 2194, 3000, NULL, NULL),
(2195, 2195, 3000, NULL, NULL),
(2196, 2196, 3000, NULL, NULL),
(2197, 2197, 4000, NULL, NULL),
(2198, 2198, 4000, NULL, NULL),
(2199, 2199, 3000, NULL, NULL),
(2200, 2200, 2000, NULL, NULL),
(2201, 2201, 1000, NULL, NULL),
(2202, 2202, 11500, NULL, NULL),
(2203, 2203, 2000, NULL, NULL),
(2204, 2204, 15000, NULL, NULL),
(2205, 2205, 17500, NULL, NULL),
(2206, 2206, 22000, NULL, NULL),
(2207, 2207, 16000, NULL, NULL),
(2208, 2208, 6500, NULL, NULL),
(2209, 2209, 6500, NULL, NULL),
(2210, 2210, 500, NULL, NULL),
(2211, 2211, 500, NULL, NULL),
(2212, 2212, 500, NULL, NULL),
(2213, 2213, 500, NULL, NULL),
(2214, 2214, 500, NULL, NULL),
(2215, 2215, 5500, NULL, NULL),
(2216, 2216, 2500, NULL, NULL),
(2217, 2217, 2000, NULL, NULL),
(2218, 2218, 2500, NULL, NULL),
(2219, 2219, 2000, NULL, NULL),
(2220, 2220, 2000, NULL, NULL),
(2221, 2221, 10000, NULL, NULL),
(2222, 2222, 5000, NULL, NULL),
(2223, 2223, 2500, NULL, NULL),
(2224, 2224, 1000, NULL, NULL),
(2225, 2225, 6000, NULL, NULL),
(2226, 2226, 5000, NULL, NULL),
(2227, 2227, 1000, NULL, NULL),
(2228, 2228, 1000, NULL, NULL),
(2229, 2229, 2500, NULL, NULL),
(2230, 2230, 5500, NULL, NULL),
(2231, 2231, 2500, NULL, NULL),
(2232, 2232, 2000, NULL, NULL),
(2233, 2233, 2000, NULL, NULL),
(2234, 2234, 3500, NULL, NULL),
(2235, 2235, 3500, NULL, NULL),
(2236, 2236, 2500, NULL, NULL),
(2237, 2237, 2500, NULL, NULL),
(2238, 2238, 2500, NULL, NULL),
(2239, 2239, 2500, NULL, NULL),
(2240, 2240, 2500, NULL, NULL),
(2241, 2241, 2500, NULL, NULL),
(2242, 2242, 2500, NULL, NULL),
(2243, 2243, 1000, NULL, NULL),
(2244, 2244, 1000, NULL, NULL),
(2245, 2245, 1000, NULL, NULL),
(2246, 2246, 5000, NULL, NULL),
(2247, 2247, 5000, NULL, NULL),
(2248, 2248, 3500, NULL, NULL),
(2249, 2249, 3500, NULL, NULL),
(2250, 2250, 3500, NULL, NULL),
(2251, 2251, 13000, NULL, NULL),
(2252, 2252, 1000, NULL, NULL),
(2253, 2253, 1000, NULL, NULL),
(2254, 2254, 1000, NULL, NULL),
(2255, 2255, 1000, NULL, NULL),
(2256, 2256, 2000, NULL, NULL),
(2257, 2257, 1000, NULL, NULL),
(2258, 2258, 3000, NULL, NULL),
(2259, 2259, 3000, NULL, NULL),
(2260, 2260, 3000, NULL, NULL),
(2261, 2261, 3000, NULL, NULL),
(2262, 2262, 3000, NULL, NULL),
(2263, 2263, 3000, NULL, NULL),
(2264, 2264, 3000, NULL, NULL),
(2265, 2265, 5000, NULL, NULL),
(2266, 2266, 4500, NULL, NULL),
(2267, 2267, 5000, NULL, NULL),
(2268, 2268, 7500, NULL, NULL),
(2269, 2269, 6000, NULL, NULL),
(2270, 2270, 4000, NULL, NULL),
(2271, 2271, 3000, NULL, NULL),
(2272, 2272, 5000, NULL, NULL),
(2273, 2273, 5000, NULL, NULL),
(2274, 2274, 2000, NULL, NULL),
(2275, 2275, 7500, NULL, NULL),
(2276, 2276, 6500, NULL, NULL),
(2277, 2277, 6000, NULL, NULL),
(2278, 2278, 8000, NULL, NULL),
(2279, 2279, 5500, NULL, NULL),
(2280, 2280, 5500, NULL, NULL),
(2281, 2281, 5500, NULL, NULL),
(2282, 2282, 5500, NULL, NULL),
(2283, 2283, 7500, NULL, NULL),
(2284, 2284, 5000, NULL, NULL),
(2285, 2285, 18000, NULL, NULL),
(2286, 2286, 22000, NULL, NULL),
(2287, 2287, 32000, NULL, NULL),
(2288, 2288, 2000, NULL, NULL),
(2289, 2289, 2000, NULL, NULL),
(2290, 2290, 2000, NULL, NULL),
(2291, 2291, 2000, NULL, NULL),
(2292, 2292, 2000, NULL, NULL),
(2293, 2293, 2000, NULL, NULL),
(2294, 2294, 2000, NULL, NULL),
(2295, 2295, 2000, NULL, NULL),
(2296, 2296, 2000, NULL, NULL),
(2297, 2297, 2500, NULL, NULL),
(2298, 2298, 2500, NULL, NULL),
(2299, 2299, 2500, NULL, NULL),
(2300, 2300, 3000, NULL, NULL),
(2301, 2301, 3000, NULL, NULL),
(2302, 2302, 3000, NULL, NULL),
(2303, 2303, 3000, NULL, NULL),
(2304, 2304, 3000, NULL, NULL),
(2305, 2305, 3000, NULL, NULL),
(2306, 2306, 3000, NULL, NULL),
(2307, 2307, 3000, NULL, NULL),
(2308, 2308, 4000, NULL, NULL),
(2309, 2309, 4000, NULL, NULL),
(2310, 2310, 4000, NULL, NULL),
(2311, 2311, 4000, NULL, NULL),
(2312, 2312, 4000, NULL, NULL),
(2313, 2313, 4000, NULL, NULL),
(2314, 2314, 4000, NULL, NULL),
(2315, 2315, 4000, NULL, NULL),
(2316, 2316, 4500, NULL, NULL),
(2317, 2317, 5000, NULL, NULL),
(2318, 2318, 30000, NULL, NULL),
(2319, 2319, 31000, NULL, NULL),
(2320, 2320, 32000, NULL, NULL),
(2321, 2321, 50000, NULL, NULL),
(2322, 2322, 49000, NULL, NULL),
(2323, 2323, 48000, NULL, NULL),
(2324, 2324, 47500, NULL, NULL),
(2325, 2325, 47500, NULL, NULL),
(2326, 2326, 102000, NULL, NULL),
(2327, 2327, 99000, NULL, NULL),
(2328, 2328, 94000, NULL, NULL),
(2329, 2329, 105000, NULL, NULL),
(2330, 2330, 100000, NULL, NULL),
(2331, 2331, 240000, NULL, NULL),
(2332, 2332, 237000, NULL, NULL),
(2333, 2333, 230000, NULL, NULL),
(2334, 2334, 242000, NULL, NULL),
(2335, 2335, 242000, NULL, NULL),
(2336, 2336, 16500, NULL, NULL),
(2337, 2337, 10000, NULL, NULL),
(2338, 2338, 11000, NULL, NULL),
(2339, 2339, 10000, NULL, NULL),
(2340, 2340, 11000, NULL, NULL),
(2341, 2341, 12000, NULL, NULL),
(2342, 2342, 12000, NULL, NULL),
(2343, 2343, 7000, NULL, NULL),
(2344, 2344, 7000, NULL, NULL),
(2345, 2345, 6000, NULL, NULL),
(2346, 2346, 8000, NULL, NULL),
(2347, 2347, 0, NULL, NULL),
(2348, 2348, 0, NULL, NULL),
(2349, 2349, 0, NULL, NULL),
(2350, 2350, 0, NULL, NULL),
(2351, 2351, 0, NULL, NULL),
(2352, 2352, 0, NULL, NULL),
(2353, 2353, 0, NULL, NULL),
(2354, 2354, 0, NULL, NULL),
(2355, 2355, 0, NULL, NULL),
(2356, 2356, 0, NULL, NULL),
(2357, 2357, 0, NULL, NULL),
(2358, 2358, 0, NULL, NULL),
(2359, 2359, 0, NULL, NULL),
(2360, 2360, 0, NULL, NULL),
(2361, 2361, 0, NULL, NULL),
(2362, 2362, 0, NULL, NULL),
(2363, 2363, 0, NULL, NULL),
(2364, 2364, 12000, NULL, NULL),
(2365, 2365, 26000, NULL, NULL),
(2366, 2366, 16000, NULL, NULL),
(2367, 2367, 1500, NULL, NULL),
(2368, 2368, 36000, NULL, NULL),
(2369, 2369, 22500, NULL, NULL),
(2370, 2370, 0, NULL, NULL),
(2371, 2371, 11500, NULL, NULL),
(2372, 2372, 25000, NULL, NULL),
(2373, 2373, 1000, NULL, NULL),
(2374, 2374, 2500, NULL, NULL),
(2375, 2375, 5000, NULL, NULL),
(2376, 2376, 10000, NULL, NULL),
(2377, 2377, 5000, NULL, NULL),
(2378, 2378, 2000, NULL, NULL),
(2379, 2379, 3000, NULL, NULL),
(2380, 2380, 3000, NULL, NULL),
(2381, 2381, 2000, NULL, NULL),
(2382, 2382, 1000, NULL, NULL),
(2383, 2383, 3000, NULL, NULL),
(2384, 2384, 5000, NULL, NULL),
(2385, 2385, 25000, NULL, NULL),
(2386, 2386, 20000, NULL, NULL),
(2387, 2387, 13000, NULL, NULL),
(2388, 2388, 25000, NULL, NULL),
(2389, 2389, 22000, NULL, NULL),
(2390, 2390, 22000, NULL, NULL),
(2391, 2391, 15000, NULL, NULL),
(2392, 2392, 12000, NULL, NULL),
(2393, 2393, 62500, NULL, NULL),
(2394, 2394, 65000, NULL, NULL),
(2395, 2395, 80000, NULL, NULL),
(2396, 2396, 3333, NULL, NULL),
(2397, 2397, 4500, NULL, NULL),
(2398, 2398, 7000, NULL, NULL),
(2399, 2399, 8500, NULL, NULL),
(2400, 2400, 57500, NULL, NULL),
(2401, 2401, 84000, NULL, NULL),
(2402, 2402, 90000, NULL, NULL),
(2403, 2403, 5000, NULL, NULL),
(2404, 2404, 20000, NULL, NULL),
(2405, 2405, 18000, NULL, NULL),
(2406, 2406, 21000, NULL, NULL),
(2407, 2407, 16500, NULL, NULL),
(2408, 2408, 10000, NULL, NULL),
(2409, 2409, 12000, NULL, NULL),
(2410, 2410, 5000, NULL, NULL),
(2411, 2411, 13000, NULL, NULL),
(2412, 2412, 5000, NULL, NULL),
(2413, 2413, 6000, NULL, NULL),
(2414, 2414, 7000, NULL, NULL),
(2415, 2415, 5000, NULL, NULL),
(2416, 2416, 7500, NULL, NULL),
(2417, 2417, 38000, NULL, NULL),
(2418, 2418, 24000, NULL, NULL),
(2419, 2419, 11000, NULL, NULL),
(2420, 2420, 15000, NULL, NULL),
(2421, 2421, 9000, NULL, NULL),
(2422, 2422, 6000, NULL, NULL),
(2423, 2423, 10000, NULL, NULL),
(2424, 2424, 4000, NULL, NULL),
(2425, 2425, 10000, NULL, NULL),
(2426, 2426, 40000, NULL, NULL),
(2427, 2427, 6000, NULL, NULL),
(2428, 2428, 3000, NULL, NULL),
(2429, 2429, 5000, NULL, NULL),
(2430, 2430, 3000, NULL, NULL),
(2431, 2431, 5000, NULL, NULL),
(2432, 2432, 7000, NULL, NULL),
(2433, 2433, 5000, NULL, NULL),
(2434, 2434, 6000, NULL, NULL),
(2435, 2435, 7000, NULL, NULL),
(2436, 2436, 4000, NULL, NULL),
(2437, 2437, 5000, NULL, NULL),
(2438, 2438, 3000, NULL, NULL),
(2439, 2439, 13000, NULL, NULL),
(2440, 2440, 10000, NULL, NULL),
(2441, 2441, 33000, NULL, NULL),
(2442, 2442, 12000, NULL, NULL),
(2443, 2443, 13000, NULL, NULL),
(2444, 2444, 15000, NULL, NULL),
(2445, 2445, 10000, NULL, NULL),
(2446, 2446, 15000, NULL, NULL),
(2447, 2447, 19000, NULL, NULL),
(2448, 2448, 0, NULL, NULL),
(2449, 2449, 48000, NULL, NULL),
(2450, 2450, 94000, NULL, NULL),
(2451, 2451, 2000, NULL, NULL),
(2452, 2452, 1500, NULL, NULL),
(2453, 2453, 1000, NULL, NULL),
(2454, 2454, 1000, NULL, NULL),
(2455, 2455, 1000, NULL, NULL),
(2456, 2456, 500, NULL, NULL),
(2457, 2457, 500, NULL, NULL),
(2458, 2458, 500, NULL, NULL),
(2459, 2459, 500, NULL, NULL),
(2460, 2460, 500, NULL, NULL),
(2461, 2461, 1000, NULL, NULL),
(2462, 2462, 500, NULL, NULL),
(2463, 2463, 500, NULL, NULL),
(2464, 2464, 500, NULL, NULL),
(2465, 2465, 500, NULL, NULL),
(2466, 2466, 500, NULL, NULL),
(2467, 2467, 500, NULL, NULL),
(2468, 2468, 500, NULL, NULL),
(2469, 2469, 500, NULL, NULL),
(2470, 2470, 1000, NULL, NULL),
(2471, 2471, 500, NULL, NULL),
(2472, 2472, 500, NULL, NULL),
(2473, 2473, 500, NULL, NULL),
(2474, 2474, 500, NULL, NULL),
(2475, 2475, 500, NULL, NULL),
(2476, 2476, 500, NULL, NULL),
(2477, 2477, 500, NULL, NULL),
(2478, 2478, 1000, NULL, NULL),
(2479, 2479, 1000, NULL, NULL),
(2480, 2480, 1000, NULL, NULL),
(2481, 2481, 2000, NULL, NULL),
(2482, 2482, 2000, NULL, NULL),
(2483, 2483, 2000, NULL, NULL),
(2484, 2484, 1000, NULL, NULL),
(2485, 2485, 1000, NULL, NULL),
(2486, 2486, 1000, NULL, NULL),
(2487, 2487, 2000, NULL, NULL),
(2488, 2488, 2000, NULL, NULL),
(2489, 2489, 2000, NULL, NULL),
(2490, 2490, 3000, NULL, NULL),
(2491, 2491, 3000, NULL, NULL),
(2492, 2492, 3000, NULL, NULL),
(2493, 2493, 1000, NULL, NULL),
(2494, 2494, 1000, NULL, NULL),
(2495, 2495, 1000, NULL, NULL),
(2496, 2496, 0, NULL, NULL),
(2497, 2497, 2000, NULL, NULL),
(2498, 2498, 2000, NULL, NULL),
(2499, 2499, 2000, NULL, NULL),
(2500, 2500, 500, NULL, NULL),
(2501, 2501, 500, NULL, NULL),
(2502, 2502, 500, NULL, NULL),
(2503, 2503, 2000, NULL, NULL),
(2504, 2504, 2000, NULL, NULL),
(2505, 2505, 2000, NULL, NULL),
(2506, 2506, 2000, NULL, NULL),
(2507, 2507, 500, NULL, NULL),
(2508, 2508, 500, NULL, NULL),
(2509, 2509, 500, NULL, NULL),
(2510, 2510, 0, NULL, NULL),
(2511, 1675, 25000, '2021-10-07 19:08:31', 3),
(2512, 1675, 26000, '2021-10-07 19:10:06', 3),
(2513, 2511, 10000, '2021-11-01 19:52:43', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual_pokok`
--

CREATE TABLE `product_has_harga_jual_pokok` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual_tunai`
--

CREATE TABLE `product_has_harga_jual_tunai` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_price_procurement`
--

CREATE TABLE `product_has_price_procurement` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `procurement` int(11) DEFAULT NULL,
  `procurement_item` int(11) DEFAULT NULL,
  `createddate` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_has_price_procurement`
--

INSERT INTO `product_has_price_procurement` (`id`, `product_satuan`, `procurement`, `procurement_item`, `createddate`, `createdby`) VALUES
(1, 1675, 1, 1, '2021-10-20 17:36:35', 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_kirim`
--

CREATE TABLE `product_kirim` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `document_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_kirim_status`
--

CREATE TABLE `product_kirim_status` (
  `id` int(11) NOT NULL,
  `product_kirim` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'SENT\nRECEIVED\nBROKE',
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_log_stock`
--

CREATE TABLE `product_log_stock` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'OUT',
  `qty` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL COMMENT 'PEMBELIAN\nPENGIRIMAN',
  `reference_id` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_log_stock`
--

INSERT INTO `product_log_stock` (`id`, `product_satuan`, `status`, `qty`, `stok_kategori`, `keterangan`, `reference_id`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1674, 1675, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1675, 1676, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1676, 1677, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1677, 1678, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1678, 1679, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1679, 1680, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1680, 1681, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1681, 1682, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1682, 1683, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1683, 1684, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1684, 1685, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1685, 1686, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1686, 1687, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1687, 1688, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1688, 1689, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1689, 1690, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1690, 1691, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1691, 1692, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1692, 1693, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1693, 1694, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1694, 1695, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1695, 1696, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1696, 1697, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1697, 1698, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1698, 1699, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1699, 1700, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1700, 1701, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1701, 1702, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1702, 1703, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1703, 1704, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1704, 1705, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1705, 1706, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1706, 1707, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1707, 1708, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1708, 1709, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1709, 1710, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1710, 1711, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1711, 1712, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1712, 1713, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1713, 1714, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1714, 1715, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1715, 1716, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1716, 1717, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1717, 1718, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1718, 1719, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1719, 1720, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1720, 1721, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1721, 1722, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1722, 1723, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1723, 1724, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1724, 1725, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1725, 1726, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1726, 1727, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1727, 1728, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1728, 1729, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1729, 1730, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1730, 1731, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1731, 1732, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1732, 1733, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1733, 1734, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1734, 1735, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1735, 1736, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1736, 1737, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1737, 1738, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1738, 1739, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1739, 1740, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1740, 1741, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1741, 1742, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1742, 1743, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1743, 1744, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1744, 1745, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1745, 1746, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1746, 1747, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1747, 1748, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1748, 1749, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1749, 1750, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1750, 1751, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1751, 1752, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1752, 1753, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1753, 1754, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1754, 1755, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1755, 1756, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1756, 1757, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1757, 1758, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1758, 1759, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1759, 1760, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1760, 1761, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1761, 1762, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1762, 1763, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1763, 1764, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1764, 1765, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1765, 1766, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1766, 1767, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1767, 1768, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1768, 1769, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1769, 1770, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1770, 1771, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1771, 1772, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1772, 1773, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1773, 1774, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1774, 1775, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1775, 1776, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1776, 1777, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1777, 1778, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1778, 1779, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1779, 1780, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1780, 1781, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1781, 1782, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1782, 1783, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1783, 1784, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1784, 1785, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1785, 1786, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1786, 1787, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1787, 1788, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1788, 1789, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1789, 1790, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1790, 1791, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1791, 1792, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1792, 1793, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1793, 1794, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1794, 1795, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1795, 1796, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1796, 1797, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1797, 1798, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1798, 1799, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1799, 1800, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1800, 1801, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1801, 1802, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1802, 1803, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1803, 1804, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1804, 1805, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1805, 1806, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1806, 1807, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1807, 1808, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1808, 1809, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1809, 1810, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1810, 1811, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1811, 1812, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1812, 1813, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1813, 1814, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1814, 1815, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1815, 1816, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1816, 1817, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1817, 1818, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1818, 1819, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1819, 1820, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1820, 1821, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1821, 1822, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1822, 1823, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1823, 1824, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1824, 1825, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1825, 1826, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1826, 1827, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1827, 1828, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1828, 1829, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1829, 1830, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1830, 1831, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1831, 1832, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1832, 1833, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1833, 1834, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1834, 1835, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1835, 1836, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1836, 1837, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1837, 1838, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1838, 1839, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1839, 1840, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1840, 1841, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1841, 1842, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1842, 1843, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1843, 1844, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1844, 1845, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1845, 1846, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1846, 1847, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1847, 1848, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1848, 1849, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1849, 1850, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1850, 1851, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1851, 1852, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1852, 1853, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1853, 1854, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1854, 1855, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1855, 1856, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1856, 1857, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1857, 1858, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1858, 1859, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1859, 1860, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1860, 1861, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1861, 1862, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1862, 1863, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1863, 1864, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1864, 1865, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1865, 1866, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1866, 1867, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1867, 1868, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1868, 1869, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1869, 1870, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1870, 1871, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1871, 1872, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1872, 1873, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1873, 1874, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1874, 1875, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1875, 1876, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1876, 1877, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1877, 1878, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1878, 1879, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1879, 1880, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1880, 1881, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1881, 1882, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1882, 1883, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1883, 1884, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1884, 1885, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1885, 1886, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1886, 1887, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1887, 1888, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1888, 1889, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1889, 1890, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1890, 1891, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1891, 1892, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1892, 1893, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1893, 1894, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1894, 1895, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1895, 1896, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1896, 1897, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1897, 1898, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1898, 1899, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1899, 1900, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1900, 1901, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1901, 1902, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1902, 1903, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1903, 1904, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1904, 1905, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1905, 1906, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1906, 1907, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1907, 1908, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1908, 1909, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1909, 1910, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1910, 1911, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1911, 1912, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1912, 1913, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1913, 1914, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1914, 1915, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1915, 1916, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1916, 1917, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1917, 1918, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1918, 1919, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1919, 1920, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1920, 1921, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1921, 1922, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1922, 1923, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1923, 1924, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1924, 1925, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1925, 1926, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1926, 1927, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1927, 1928, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1928, 1929, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1929, 1930, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1930, 1931, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1931, 1932, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1932, 1933, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1933, 1934, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1934, 1935, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1935, 1936, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1936, 1937, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1937, 1938, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1938, 1939, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1939, 1940, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1940, 1941, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1941, 1942, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1942, 1943, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1943, 1944, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1944, 1945, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1945, 1946, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1946, 1947, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1947, 1948, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1948, 1949, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1949, 1950, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1950, 1951, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1951, 1952, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1952, 1953, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1953, 1954, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1954, 1955, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1955, 1956, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1956, 1957, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1957, 1958, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1958, 1959, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1959, 1960, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1960, 1961, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1961, 1962, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1962, 1963, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1963, 1964, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1964, 1965, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1965, 1966, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1966, 1967, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1967, 1968, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1968, 1969, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1969, 1970, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1970, 1971, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1971, 1972, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1972, 1973, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1973, 1974, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1974, 1975, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1975, 1976, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1976, 1977, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1977, 1978, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1978, 1979, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1979, 1980, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1980, 1981, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1981, 1982, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1982, 1983, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1983, 1984, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1984, 1985, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1985, 1986, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1986, 1987, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1987, 1988, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1988, 1989, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1989, 1990, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1990, 1991, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1991, 1992, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1992, 1993, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1993, 1994, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1994, 1995, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1995, 1996, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1996, 1997, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1997, 1998, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1998, 1999, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(1999, 2000, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2000, 2001, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2001, 2002, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2002, 2003, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2003, 2004, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2004, 2005, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2005, 2006, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2006, 2007, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2007, 2008, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2008, 2009, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2009, 2010, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2010, 2011, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2011, 2012, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2012, 2013, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2013, 2014, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2014, 2015, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2015, 2016, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2016, 2017, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2017, 2018, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2018, 2019, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2019, 2020, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2020, 2021, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2021, 2022, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2022, 2023, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2023, 2024, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2024, 2025, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2025, 2026, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2026, 2027, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2027, 2028, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2028, 2029, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2029, 2030, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2030, 2031, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2031, 2032, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2032, 2033, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2033, 2034, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2034, 2035, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2035, 2036, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2036, 2037, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2037, 2038, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2038, 2039, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2039, 2040, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2040, 2041, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2041, 2042, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2042, 2043, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2043, 2044, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2044, 2045, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2045, 2046, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2046, 2047, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2047, 2048, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2048, 2049, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2049, 2050, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2050, 2051, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2051, 2052, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2052, 2053, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2053, 2054, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2054, 2055, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2055, 2056, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2056, 2057, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2057, 2058, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2058, 2059, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2059, 2060, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2060, 2061, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2061, 2062, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2062, 2063, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2063, 2064, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2064, 2065, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2065, 2066, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2066, 2067, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2067, 2068, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2068, 2069, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2069, 2070, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2070, 2071, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2071, 2072, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2072, 2073, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2073, 2074, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2074, 2075, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2075, 2076, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2076, 2077, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2077, 2078, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2078, 2079, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2079, 2080, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2080, 2081, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2081, 2082, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2082, 2083, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2083, 2084, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2084, 2085, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2085, 2086, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2086, 2087, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2087, 2088, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2088, 2089, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2089, 2090, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2090, 2091, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2091, 2092, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2092, 2093, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2093, 2094, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2094, 2095, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2095, 2096, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2096, 2097, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2097, 2098, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2098, 2099, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2099, 2100, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2100, 2101, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2101, 2102, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2102, 2103, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2103, 2104, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2104, 2105, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2105, 2106, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2106, 2107, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2107, 2108, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2108, 2109, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2109, 2110, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2110, 2111, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2111, 2112, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2112, 2113, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2113, 2114, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2114, 2115, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2115, 2116, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2116, 2117, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2117, 2118, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2118, 2119, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2119, 2120, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2120, 2121, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2121, 2122, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2122, 2123, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2123, 2124, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2124, 2125, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2125, 2126, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2126, 2127, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2127, 2128, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2128, 2129, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2129, 2130, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2130, 2131, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2131, 2132, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2132, 2133, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2133, 2134, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2134, 2135, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2135, 2136, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2136, 2137, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2137, 2138, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2138, 2139, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2139, 2140, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2140, 2141, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2141, 2142, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2142, 2143, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2143, 2144, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2144, 2145, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2145, 2146, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2146, 2147, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2147, 2148, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2148, 2149, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2149, 2150, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2150, 2151, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2151, 2152, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2152, 2153, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2153, 2154, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2154, 2155, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2155, 2156, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2156, 2157, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2157, 2158, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2158, 2159, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2159, 2160, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2160, 2161, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2161, 2162, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2162, 2163, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2163, 2164, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2164, 2165, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2165, 2166, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2166, 2167, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2167, 2168, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2168, 2169, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2169, 2170, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2170, 2171, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2171, 2172, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2172, 2173, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2173, 2174, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2174, 2175, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2175, 2176, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2176, 2177, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2177, 2178, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2178, 2179, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2179, 2180, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2180, 2181, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2181, 2182, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2182, 2183, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2183, 2184, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2184, 2185, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2185, 2186, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2186, 2187, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2187, 2188, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2188, 2189, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2189, 2190, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2190, 2191, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2191, 2192, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2192, 2193, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2193, 2194, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2194, 2195, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2195, 2196, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2196, 2197, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2197, 2198, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2198, 2199, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2199, 2200, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2200, 2201, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2201, 2202, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2202, 2203, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2203, 2204, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2204, 2205, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2205, 2206, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2206, 2207, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2207, 2208, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2208, 2209, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2209, 2210, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2210, 2211, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2211, 2212, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2212, 2213, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2213, 2214, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2214, 2215, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2215, 2216, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2216, 2217, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2217, 2218, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2218, 2219, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2219, 2220, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2220, 2221, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2221, 2222, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2222, 2223, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2223, 2224, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2224, 2225, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2225, 2226, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2226, 2227, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2227, 2228, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2228, 2229, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2229, 2230, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2230, 2231, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2231, 2232, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2232, 2233, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2233, 2234, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2234, 2235, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2235, 2236, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2236, 2237, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2237, 2238, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2238, 2239, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2239, 2240, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2240, 2241, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2241, 2242, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2242, 2243, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2243, 2244, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2244, 2245, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2245, 2246, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2246, 2247, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2247, 2248, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2248, 2249, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2249, 2250, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2250, 2251, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2251, 2252, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2252, 2253, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2253, 2254, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2254, 2255, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2255, 2256, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2256, 2257, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2257, 2258, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2258, 2259, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2259, 2260, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2260, 2261, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2261, 2262, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2262, 2263, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2263, 2264, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2264, 2265, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2265, 2266, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2266, 2267, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2267, 2268, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2268, 2269, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2269, 2270, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2270, 2271, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2271, 2272, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2272, 2273, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2273, 2274, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2274, 2275, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2275, 2276, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2276, 2277, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2277, 2278, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2278, 2279, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2279, 2280, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2280, 2281, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2281, 2282, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2282, 2283, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2283, 2284, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2284, 2285, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2285, 2286, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2286, 2287, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2287, 2288, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2288, 2289, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2289, 2290, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2290, 2291, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2291, 2292, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2292, 2293, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2293, 2294, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2294, 2295, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2295, 2296, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2296, 2297, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2297, 2298, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2298, 2299, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2299, 2300, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2300, 2301, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2301, 2302, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2302, 2303, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2303, 2304, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2304, 2305, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2305, 2306, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2306, 2307, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2307, 2308, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2308, 2309, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2309, 2310, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2310, 2311, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2311, 2312, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2312, 2313, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2313, 2314, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2314, 2315, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2315, 2316, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2316, 2317, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2317, 2318, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2318, 2319, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2319, 2320, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2320, 2321, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2321, 2322, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2322, 2323, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2323, 2324, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2324, 2325, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2325, 2326, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2326, 2327, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2327, 2328, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2328, 2329, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2329, 2330, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2330, 2331, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2331, 2332, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2332, 2333, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2333, 2334, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2334, 2335, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2335, 2336, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2336, 2337, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2337, 2338, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2338, 2339, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2339, 2340, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2340, 2341, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2341, 2342, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2342, 2343, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2343, 2344, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2344, 2345, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2345, 2346, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2346, 2347, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2347, 2348, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2348, 2349, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2349, 2350, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2350, 2351, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2351, 2352, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2352, 2353, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2353, 2354, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2354, 2355, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2355, 2356, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2356, 2357, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2357, 2358, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2358, 2359, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2359, 2360, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2360, 2361, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2361, 2362, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2362, 2363, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2363, 2364, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2364, 2365, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2365, 2366, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2366, 2367, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2367, 2368, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2368, 2369, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2369, 2370, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2370, 2371, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2371, 2372, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2372, 2373, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2373, 2374, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2374, 2375, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2375, 2376, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2376, 2377, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2377, 2378, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2378, 2379, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2379, 2380, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2380, 2381, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2381, 2382, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2382, 2383, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2383, 2384, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2384, 2385, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2385, 2386, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2386, 2387, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2387, 2388, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2388, 2389, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2389, 2390, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2390, 2391, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2391, 2392, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2392, 2393, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2393, 2394, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2394, 2395, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `product_log_stock` (`id`, `product_satuan`, `status`, `qty`, `stok_kategori`, `keterangan`, `reference_id`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2395, 2396, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2396, 2397, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2397, 2398, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2398, 2399, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2399, 2400, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2400, 2401, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2401, 2402, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2402, 2403, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2403, 2404, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2404, 2405, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2405, 2406, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2406, 2407, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2407, 2408, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2408, 2409, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2409, 2410, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2410, 2411, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2411, 2412, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2412, 2413, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2413, 2414, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2414, 2415, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2415, 2416, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2416, 2417, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2417, 2418, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2418, 2419, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2419, 2420, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2420, 2421, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2421, 2422, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2422, 2423, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2423, 2424, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2424, 2425, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2425, 2426, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2426, 2427, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2427, 2428, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2428, 2429, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2429, 2430, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2430, 2431, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2431, 2432, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2432, 2433, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2433, 2434, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2434, 2435, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2435, 2436, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2436, 2437, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2437, 2438, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2438, 2439, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2439, 2440, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2440, 2441, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2441, 2442, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2442, 2443, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2443, 2444, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2444, 2445, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2445, 2446, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2446, 2447, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2447, 2448, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2448, 2449, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2449, 2450, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2450, 2451, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2451, 2452, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2452, 2453, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2453, 2454, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2454, 2455, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2455, 2456, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2456, 2457, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2457, 2458, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2458, 2459, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2459, 2460, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2460, 2461, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2461, 2462, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2462, 2463, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2463, 2464, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2464, 2465, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2465, 2466, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2466, 2467, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2467, 2468, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2468, 2469, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2469, 2470, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2470, 2471, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2471, 2472, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2472, 2473, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2473, 2474, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2474, 2475, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2475, 2476, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2476, 2477, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2477, 2478, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2478, 2479, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2479, 2480, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2480, 2481, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2481, 2482, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2482, 2483, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2483, 2484, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2484, 2485, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2485, 2486, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2486, 2487, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2487, 2488, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2488, 2489, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2489, 2490, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2490, 2491, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2491, 2492, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2492, 2493, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2493, 2494, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2494, 2495, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2495, 2496, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2496, 2497, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2497, 2498, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2498, 2499, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2499, 2500, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2500, 2501, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2501, 2502, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2502, 2503, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2503, 2504, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2504, 2505, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2505, 2506, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2506, 2507, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2507, 2508, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2508, 2509, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2509, 2510, 'IN', 10, NULL, 'Lain', NULL, NULL, NULL, NULL, NULL, 0),
(2510, 1675, 'ORDER', 1, NULL, 'Order Pelanggan', 1, '2021-08-17', 3, NULL, NULL, 0),
(2511, 1675, 'ORDER', 1, NULL, 'Order Pelanggan', 2, '2021-08-17', 3, NULL, NULL, 0),
(2512, 1675, 'ORDER', 1, NULL, 'Order Pelanggan', 4, '2021-08-17', 3, NULL, NULL, 0),
(2513, 1675, 'ORDER', 1, NULL, 'Order Pelanggan', 5, '2021-08-17', 3, NULL, NULL, 0),
(2514, 1684, 'ORDER', 1, NULL, 'Order Pelanggan', 8, '2021-09-30', 3, NULL, NULL, 0),
(2515, 1675, 'ORDER', 1, NULL, 'Order Pelanggan', 9, '2021-10-07', 3, NULL, NULL, 0),
(2516, 1675, 'IN', 1, 1, 'Pengadaan', NULL, '2021-10-20', 3, NULL, NULL, 0),
(2517, 2511, 'ORDER', 1, NULL, 'Order Pelanggan', 10, '2021-11-01', 3, NULL, NULL, 0),
(2518, 1675, 'ORDER', 1, NULL, 'Order Pelanggan', 11, '2021-11-05', 3, NULL, NULL, 0),
(2519, 1675, 'ORDER', 1, NULL, 'Order Pelanggan', 12, '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_satuan`
--

CREATE TABLE `product_satuan` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `satuan` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL COMMENT 'Isi Dari Satuan',
  `harga` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `ket_harga` varchar(150) DEFAULT NULL,
  `satuan_terkecil` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_satuan`
--

INSERT INTO `product_satuan` (`id`, `product`, `satuan`, `qty`, `harga`, `harga_beli`, `ket_harga`, `satuan_terkecil`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1675, 1677, 1, 1, 26000, 20000, '', 1, NULL, NULL, '2021-10-20 17:40:29', 3, 0),
(1676, 1678, 1, 1, 18500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1677, 1679, 1, 1, 21000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1678, 1680, 1, 1, 21000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1679, 1681, 1, 1, 21000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1680, 1682, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1681, 1683, 1, 1, 18500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1682, 1684, 1, 1, 17500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1683, 1685, 1, 1, 13000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1684, 1686, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1685, 1687, 1, 1, 17500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1686, 1688, 1, 1, 24500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1687, 1689, 1, 1, 17500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1688, 1690, 1, 1, 23500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1689, 1691, 1, 1, 17000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1690, 1692, 1, 1, 23500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1691, 1693, 1, 1, 23500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1692, 1694, 1, 1, 18500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1693, 1695, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1694, 1696, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1695, 1697, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1696, 1698, 1, 1, 19500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1697, 1699, 1, 1, 19500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1698, 1700, 1, 1, 20000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1699, 1701, 1, 1, 18000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1700, 1702, 1, 1, 13000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1701, 1703, 1, 1, 24000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1702, 1704, 1, 1, 23500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1703, 1705, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1704, 1706, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1705, 1707, 1, 1, 8500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1706, 1708, 1, 1, 18000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1707, 1709, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1708, 1710, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1709, 1711, 1, 1, 30000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1710, 1712, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1711, 1713, 1, 1, 17500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1712, 1714, 1, 1, 22000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1713, 1715, 1, 1, 16500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1714, 1716, 1, 1, 18500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1715, 1717, 1, 1, 18500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1716, 1718, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1717, 1719, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1718, 1720, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1719, 1721, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1720, 1722, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1721, 1723, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1722, 1724, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1723, 1725, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1724, 1726, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1725, 1727, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1726, 1728, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1727, 1729, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1728, 1730, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1729, 1731, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1730, 1732, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1731, 1733, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1732, 1734, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1733, 1735, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1734, 1736, 1, 1, 27000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1735, 1737, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1736, 1738, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1737, 1739, 1, 1, 12500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1738, 1740, 1, 1, 14000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1739, 1741, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1740, 1742, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1741, 1743, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1742, 1744, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1743, 1745, 1, 1, 12500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1744, 1746, 1, 1, 18500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1745, 1747, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1746, 1748, 1, 1, 16500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1747, 1749, 1, 1, 23000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1748, 1750, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1749, 1751, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1750, 1752, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1751, 1753, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1752, 1754, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1753, 1755, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1754, 1756, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1755, 1757, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1756, 1758, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1757, 1759, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1758, 1760, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1759, 1761, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1760, 1762, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1761, 1763, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1762, 1764, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1763, 1765, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1764, 1766, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1765, 1767, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1766, 1768, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1767, 1769, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1768, 1770, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1769, 1771, 1, 1, 25000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1770, 1772, 1, 1, 20000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1771, 1773, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1772, 1774, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1773, 1775, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1774, 1776, 1, 1, 17000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1775, 1777, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1776, 1778, 1, 1, 9000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1777, 1779, 1, 1, 8500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1778, 1780, 1, 1, 17000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1779, 1781, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1780, 1782, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1781, 1783, 1, 1, 25000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1782, 1784, 1, 1, 17000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1783, 1785, 1, 1, 27500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1784, 1786, 1, 1, 13000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1785, 1787, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1786, 1788, 1, 1, 18000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1787, 1789, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1788, 1790, 1, 1, 12500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1789, 1791, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1790, 1792, 1, 1, 24000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1791, 1793, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1792, 1794, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1793, 1795, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1794, 1796, 1, 1, 9500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1795, 1797, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1796, 1798, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1797, 1799, 1, 1, 13500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1798, 1800, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1799, 1801, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1800, 1802, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1801, 1803, 1, 1, 21000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1802, 1804, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1803, 1805, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1804, 1806, 1, 1, 75000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1805, 1807, 1, 1, 65000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1806, 1808, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1807, 1809, 1, 1, 34000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1808, 1810, 1, 1, 51000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1809, 1811, 1, 1, 57500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1810, 1812, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1811, 1813, 1, 1, 22500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1812, 1814, 1, 1, 38000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1813, 1815, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1814, 1816, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1815, 1817, 1, 1, 38000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1816, 1818, 1, 1, 45000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1817, 1819, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1818, 1820, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1819, 1821, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1820, 1822, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1821, 1823, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1822, 1824, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1823, 1825, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1824, 1826, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1825, 1827, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1826, 1828, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1827, 1829, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1828, 1830, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1829, 1831, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1830, 1832, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1831, 1833, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1832, 1834, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1833, 1835, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1834, 1836, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1835, 1837, 1, 1, 17500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1836, 1838, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1837, 1839, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1838, 1840, 1, 1, 14000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1839, 1841, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1840, 1842, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1841, 1843, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1842, 1844, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1843, 1845, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1844, 1846, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1845, 1847, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1846, 1848, 1, 1, 28000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1847, 1849, 1, 1, 45000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1848, 1850, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1849, 1851, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1850, 1852, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1851, 1853, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1852, 1854, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1853, 1855, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1854, 1856, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1855, 1857, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1856, 1858, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1857, 1859, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1858, 1860, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1859, 1861, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1860, 1862, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1861, 1863, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1862, 1864, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1863, 1865, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1864, 1866, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1865, 1867, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1866, 1868, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1867, 1869, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1868, 1870, 1, 1, 14500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1869, 1871, 1, 1, 9000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1870, 1872, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1871, 1873, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1872, 1874, 1, 1, 21000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1873, 1875, 1, 1, 14000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1874, 1876, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1875, 1877, 1, 1, 750, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1876, 1878, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1877, 1879, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1878, 1880, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1879, 1881, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1880, 1882, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1881, 1883, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1882, 1884, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1883, 1885, 1, 1, 9000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1884, 1886, 1, 1, 9000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1885, 1887, 1, 1, 9000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1886, 1888, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1887, 1889, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1888, 1890, 1, 1, 12500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1889, 1891, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1890, 1892, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1891, 1893, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1892, 1894, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1893, 1895, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1894, 1896, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1895, 1897, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1896, 1898, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1897, 1899, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1898, 1900, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1899, 1901, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1900, 1902, 1, 1, 21000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1901, 1903, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1902, 1904, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1903, 1905, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1904, 1906, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1905, 1907, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1906, 1908, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1907, 1909, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1908, 1910, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1909, 1911, 1, 1, 16500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1910, 1912, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1911, 1913, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1912, 1914, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1913, 1915, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1914, 1916, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1915, 1917, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1916, 1918, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1917, 1919, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1918, 1920, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1919, 1921, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1920, 1922, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1921, 1923, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1922, 1924, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1923, 1925, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1924, 1926, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1925, 1927, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1926, 1928, 1, 1, 750, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1927, 1929, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1928, 1930, 1, 1, 8500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1929, 1931, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1930, 1932, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1931, 1933, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1932, 1934, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1933, 1935, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1934, 1936, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1935, 1937, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1936, 1938, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1937, 1939, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1938, 1940, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1939, 1941, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1940, 1942, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1941, 1943, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1942, 1944, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1943, 1945, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1944, 1946, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1945, 1947, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1946, 1948, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1947, 1949, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1948, 1950, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1949, 1951, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1950, 1952, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1951, 1953, 1, 1, 1750, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1952, 1954, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1953, 1955, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1954, 1956, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1955, 1957, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1956, 1958, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1957, 1959, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1958, 1960, 1, 1, 9000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1959, 1961, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1960, 1962, 1, 1, 9000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1961, 1963, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1962, 1964, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1963, 1965, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1964, 1966, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1965, 1967, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1966, 1968, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1967, 1969, 1, 1, 20000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1968, 1970, 1, 1, 18000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1969, 1971, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1970, 1972, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1971, 1973, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1972, 1974, 1, 1, 750, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1973, 1975, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1974, 1976, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1975, 1977, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1976, 1978, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1977, 1979, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1978, 1980, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1979, 1981, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1980, 1982, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1981, 1983, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1982, 1984, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1983, 1985, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1984, 1986, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1985, 1987, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1986, 1988, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1987, 1989, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1988, 1990, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1989, 1991, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1990, 1992, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1991, 1993, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1992, 1994, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1993, 1995, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1994, 1996, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1995, 1997, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1996, 1998, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1997, 1999, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1998, 2000, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(1999, 2001, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2000, 2002, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2001, 2003, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2002, 2004, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2003, 2005, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2004, 2006, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2005, 2007, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2006, 2008, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2007, 2009, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2008, 2010, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2009, 2011, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2010, 2012, 1, 1, 250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2011, 2013, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2012, 2014, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2013, 2015, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2014, 2016, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2015, 2017, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2016, 2018, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2017, 2019, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2018, 2020, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2019, 2021, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2020, 2022, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2021, 2023, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2022, 2024, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2023, 2025, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2024, 2026, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2025, 2027, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2026, 2028, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2027, 2029, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2028, 2030, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2029, 2031, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2030, 2032, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2031, 2033, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2032, 2034, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2033, 2035, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2034, 2036, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2035, 2037, 1, 1, 750, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2036, 2038, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2037, 2039, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2038, 2040, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2039, 2041, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2040, 2042, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2041, 2043, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2042, 2044, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2043, 2045, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2044, 2046, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2045, 2047, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2046, 2048, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2047, 2049, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2048, 2050, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2049, 2051, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2050, 2052, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2051, 2053, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2052, 2054, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2053, 2055, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2054, 2056, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2055, 2057, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2056, 2058, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2057, 2059, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2058, 2060, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2059, 2061, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2060, 2062, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2061, 2063, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2062, 2064, 1, 1, 19000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2063, 2065, 1, 1, 19000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2064, 2066, 1, 1, 19000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2065, 2067, 1, 1, 18000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2066, 2068, 1, 1, 18000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2067, 2069, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2068, 2070, 1, 1, 22000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2069, 2071, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2070, 2072, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2071, 2073, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2072, 2074, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2073, 2075, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2074, 2076, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2075, 2077, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2076, 2078, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2077, 2079, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2078, 2080, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2079, 2081, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2080, 2082, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2081, 2083, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2082, 2084, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2083, 2085, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2084, 2086, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2085, 2087, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2086, 2088, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2087, 2089, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2088, 2090, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2089, 2091, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2090, 2092, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2091, 2093, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2092, 2094, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2093, 2095, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2094, 2096, 1, 1, 14000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2095, 2097, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2096, 2098, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2097, 2099, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2098, 2100, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2099, 2101, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2100, 2102, 1, 1, 19500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2101, 2103, 1, 1, 16500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2102, 2104, 1, 1, 16500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2103, 2105, 1, 1, 12500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2104, 2106, 1, 1, 12500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2105, 2107, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2106, 2108, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2107, 2109, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2108, 2110, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2109, 2111, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2110, 2112, 1, 1, 22000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2111, 2113, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2112, 2114, 1, 1, 17000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2113, 2115, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2114, 2116, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2115, 2117, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2116, 2118, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2117, 2119, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2118, 2120, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2119, 2121, 1, 1, 30000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2120, 2122, 1, 1, 28500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2121, 2123, 1, 1, 28500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2122, 2124, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2123, 2125, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2124, 2126, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2125, 2127, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2126, 2128, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2127, 2129, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2128, 2130, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2129, 2131, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2130, 2132, 1, 1, 1250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2131, 2133, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2132, 2134, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2133, 2135, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2134, 2136, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2135, 2137, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2136, 2138, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2137, 2139, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2138, 2140, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2139, 2141, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2140, 2142, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2141, 2143, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2142, 2144, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2143, 2145, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2144, 2146, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2145, 2147, 1, 1, 1250, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2146, 2148, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2147, 2149, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2148, 2150, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2149, 2151, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2150, 2152, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2151, 2153, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2152, 2154, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2153, 2155, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2154, 2156, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2155, 2157, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2156, 2158, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2157, 2159, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2158, 2160, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2159, 2161, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2160, 2162, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2161, 2163, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2162, 2164, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2163, 2165, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2164, 2166, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2165, 2167, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2166, 2168, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2167, 2169, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2168, 2170, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2169, 2171, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2170, 2172, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2171, 2173, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2172, 2174, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2173, 2175, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2174, 2176, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2175, 2177, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2176, 2178, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2177, 2179, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2178, 2180, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2179, 2181, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2180, 2182, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2181, 2183, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2182, 2184, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2183, 2185, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2184, 2186, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2185, 2187, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2186, 2188, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2187, 2189, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2188, 2190, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2189, 2191, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2190, 2192, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2191, 2193, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2192, 2194, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2193, 2195, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2194, 2196, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2195, 2197, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2196, 2198, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2197, 2199, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2198, 2200, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2199, 2201, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2200, 2202, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2201, 2203, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2202, 2204, 1, 1, 11500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2203, 2205, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2204, 2206, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2205, 2207, 1, 1, 17500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2206, 2208, 1, 1, 22000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2207, 2209, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2208, 2210, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2209, 2211, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2210, 2212, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2211, 2213, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2212, 2214, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2213, 2215, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2214, 2216, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2215, 2217, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2216, 2218, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2217, 2219, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2218, 2220, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2219, 2221, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2220, 2222, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2221, 2223, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2222, 2224, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2223, 2225, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2224, 2226, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2225, 2227, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2226, 2228, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2227, 2229, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2228, 2230, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2229, 2231, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2230, 2232, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2231, 2233, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2232, 2234, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2233, 2235, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2234, 2236, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2235, 2237, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2236, 2238, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2237, 2239, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2238, 2240, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2239, 2241, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2240, 2242, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2241, 2243, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2242, 2244, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2243, 2245, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2244, 2246, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2245, 2247, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2246, 2248, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2247, 2249, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2248, 2250, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2249, 2251, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2250, 2252, 1, 1, 3500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2251, 2253, 1, 1, 13000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2252, 2254, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2253, 2255, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2254, 2256, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2255, 2257, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2256, 2258, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2257, 2259, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2258, 2260, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2259, 2261, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2260, 2262, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2261, 2263, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2262, 2264, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2263, 2265, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2264, 2266, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2265, 2267, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2266, 2268, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2267, 2269, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2268, 2270, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2269, 2271, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2270, 2272, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2271, 2273, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2272, 2274, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2273, 2275, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2274, 2276, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2275, 2277, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2276, 2278, 1, 1, 6500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2277, 2279, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2278, 2280, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2279, 2281, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2280, 2282, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2281, 2283, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2282, 2284, 1, 1, 5500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2283, 2285, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2284, 2286, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2285, 2287, 1, 1, 18000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2286, 2288, 1, 1, 22000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2287, 2289, 1, 1, 32000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2288, 2290, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2289, 2291, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2290, 2292, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2291, 2293, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2292, 2294, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2293, 2295, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2294, 2296, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2295, 2297, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2296, 2298, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2297, 2299, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2298, 2300, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2299, 2301, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2300, 2302, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2301, 2303, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2302, 2304, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2303, 2305, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2304, 2306, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2305, 2307, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2306, 2308, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2307, 2309, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2308, 2310, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2309, 2311, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2310, 2312, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2311, 2313, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2312, 2314, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2313, 2315, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2314, 2316, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2315, 2317, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2316, 2318, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2317, 2319, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2318, 2320, 1, 1, 30000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2319, 2321, 1, 1, 31000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2320, 2322, 1, 1, 32000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2321, 2323, 1, 1, 50000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2322, 2324, 1, 1, 49000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2323, 2325, 1, 1, 48000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2324, 2326, 1, 1, 47500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2325, 2327, 1, 1, 47500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2326, 2328, 1, 1, 102000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2327, 2329, 1, 1, 99000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2328, 2330, 1, 1, 94000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2329, 2331, 1, 1, 105000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2330, 2332, 1, 1, 100000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2331, 2333, 1, 1, 240000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2332, 2334, 1, 1, 237000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2333, 2335, 1, 1, 230000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2334, 2336, 1, 1, 242000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2335, 2337, 1, 1, 242000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2336, 2338, 1, 1, 16500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2337, 2339, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2338, 2340, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2339, 2341, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2340, 2342, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2341, 2343, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2342, 2344, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2343, 2345, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2344, 2346, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2345, 2347, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2346, 2348, 1, 1, 8000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2347, 2349, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2348, 2350, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2349, 2351, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2350, 2352, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2351, 2353, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2352, 2354, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2353, 2355, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2354, 2356, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2355, 2357, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2356, 2358, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2357, 2359, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2358, 2360, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2359, 2361, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2360, 2362, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2361, 2363, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2362, 2364, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2363, 2365, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2364, 2366, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2365, 2367, 1, 1, 26000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2366, 2368, 1, 1, 16000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2367, 2369, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2368, 2370, 1, 1, 36000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2369, 2371, 1, 1, 22500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2370, 2372, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2371, 2373, 1, 1, 11500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2372, 2374, 1, 1, 25000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2373, 2375, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2374, 2376, 1, 1, 2500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2375, 2377, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2376, 2378, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2377, 2379, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2378, 2380, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2379, 2381, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2380, 2382, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2381, 2383, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2382, 2384, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2383, 2385, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2384, 2386, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2385, 2387, 1, 1, 25000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2386, 2388, 1, 1, 20000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2387, 2389, 1, 1, 13000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2388, 2390, 1, 1, 25000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2389, 2391, 1, 1, 22000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2390, 2392, 1, 1, 22000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2391, 2393, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2392, 2394, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2393, 2395, 1, 1, 62500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2394, 2396, 1, 1, 65000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2395, 2397, 1, 1, 80000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2396, 2398, 1, 1, 3333, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2397, 2399, 1, 1, 4500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2398, 2400, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2399, 2401, 1, 1, 8500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2400, 2402, 1, 1, 57500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2401, 2403, 1, 1, 84000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2402, 2404, 1, 1, 90000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2403, 2405, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2404, 2406, 1, 1, 20000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2405, 2407, 1, 1, 18000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2406, 2408, 1, 1, 21000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2407, 2409, 1, 1, 16500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2408, 2410, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2409, 2411, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2410, 2412, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2411, 2413, 1, 1, 13000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2412, 2414, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2413, 2415, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2414, 2416, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2415, 2417, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2416, 2418, 1, 1, 7500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2417, 2419, 1, 1, 38000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2418, 2420, 1, 1, 24000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2419, 2421, 1, 1, 11000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2420, 2422, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2421, 2423, 1, 1, 9000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2422, 2424, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2423, 2425, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2424, 2426, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2425, 2427, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2426, 2428, 1, 1, 40000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2427, 2429, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2428, 2430, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2429, 2431, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2430, 2432, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2431, 2433, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2432, 2434, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2433, 2435, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2434, 2436, 1, 1, 6000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2435, 2437, 1, 1, 7000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2436, 2438, 1, 1, 4000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2437, 2439, 1, 1, 5000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2438, 2440, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2439, 2441, 1, 1, 13000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2440, 2442, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2441, 2443, 1, 1, 33000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2442, 2444, 1, 1, 12000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2443, 2445, 1, 1, 13000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2444, 2446, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2445, 2447, 1, 1, 10000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2446, 2448, 1, 1, 15000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2447, 2449, 1, 1, 19000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2448, 2450, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2449, 2451, 1, 1, 48000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2450, 2452, 1, 1, 94000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2451, 2453, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2452, 2454, 1, 1, 1500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2453, 2455, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2454, 2456, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2455, 2457, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2456, 2458, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2457, 2459, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2458, 2460, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2459, 2461, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2460, 2462, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2461, 2463, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2462, 2464, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2463, 2465, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `product_satuan` (`id`, `product`, `satuan`, `qty`, `harga`, `harga_beli`, `ket_harga`, `satuan_terkecil`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2464, 2466, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2465, 2467, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2466, 2468, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2467, 2469, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2468, 2470, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2469, 2471, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2470, 2472, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2471, 2473, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2472, 2474, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2473, 2475, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2474, 2476, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2475, 2477, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2476, 2478, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2477, 2479, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2478, 2480, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2479, 2481, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2480, 2482, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2481, 2483, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2482, 2484, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2483, 2485, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2484, 2486, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2485, 2487, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2486, 2488, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2487, 2489, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2488, 2490, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2489, 2491, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2490, 2492, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2491, 2493, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2492, 2494, 1, 1, 3000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2493, 2495, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2494, 2496, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2495, 2497, 1, 1, 1000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2496, 2498, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2497, 2499, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2498, 2500, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2499, 2501, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2500, 2502, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2501, 2503, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2502, 2504, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2503, 2505, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2504, 2506, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2505, 2507, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2506, 2508, 1, 1, 2000, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2507, 2509, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2508, 2510, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2509, 2511, 1, 1, 500, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2510, 2512, 1, 1, 0, 0, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2511, 2513, 1, 1, 10000, 8000, '', 1, '2021-11-01', 3, '2021-11-01 19:55:55', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE `product_stock` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `gudang` int(11) DEFAULT NULL,
  `rak` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_stock`
--

INSERT INTO `product_stock` (`id`, `product`, `product_satuan`, `stock`, `gudang`, `rak`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1673, 1677, 1675, 11, 1, 1, NULL, NULL, '2021-10-20 17:36:35', 3, 0),
(1674, 1678, 1676, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1675, 1679, 1677, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1676, 1680, 1678, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1677, 1681, 1679, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1678, 1682, 1680, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1679, 1683, 1681, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1680, 1684, 1682, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1681, 1685, 1683, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1682, 1686, 1684, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1683, 1687, 1685, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1684, 1688, 1686, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1685, 1689, 1687, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1686, 1690, 1688, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1687, 1691, 1689, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1688, 1692, 1690, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1689, 1693, 1691, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1690, 1694, 1692, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1691, 1695, 1693, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1692, 1696, 1694, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1693, 1697, 1695, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1694, 1698, 1696, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1695, 1699, 1697, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1696, 1700, 1698, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1697, 1701, 1699, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1698, 1702, 1700, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1699, 1703, 1701, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1700, 1704, 1702, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1701, 1705, 1703, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1702, 1706, 1704, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1703, 1707, 1705, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1704, 1708, 1706, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1705, 1709, 1707, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1706, 1710, 1708, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1707, 1711, 1709, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1708, 1712, 1710, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1709, 1713, 1711, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1710, 1714, 1712, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1711, 1715, 1713, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1712, 1716, 1714, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1713, 1717, 1715, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1714, 1718, 1716, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1715, 1719, 1717, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1716, 1720, 1718, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1717, 1721, 1719, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1718, 1722, 1720, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1719, 1723, 1721, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1720, 1724, 1722, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1721, 1725, 1723, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1722, 1726, 1724, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1723, 1727, 1725, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1724, 1728, 1726, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1725, 1729, 1727, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1726, 1730, 1728, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1727, 1731, 1729, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1728, 1732, 1730, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1729, 1733, 1731, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1730, 1734, 1732, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1731, 1735, 1733, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1732, 1736, 1734, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1733, 1737, 1735, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1734, 1738, 1736, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1735, 1739, 1737, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1736, 1740, 1738, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1737, 1741, 1739, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1738, 1742, 1740, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1739, 1743, 1741, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1740, 1744, 1742, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1741, 1745, 1743, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1742, 1746, 1744, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1743, 1747, 1745, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1744, 1748, 1746, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1745, 1749, 1747, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1746, 1750, 1748, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1747, 1751, 1749, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1748, 1752, 1750, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1749, 1753, 1751, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1750, 1754, 1752, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1751, 1755, 1753, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1752, 1756, 1754, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1753, 1757, 1755, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1754, 1758, 1756, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1755, 1759, 1757, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1756, 1760, 1758, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1757, 1761, 1759, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1758, 1762, 1760, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1759, 1763, 1761, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1760, 1764, 1762, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1761, 1765, 1763, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1762, 1766, 1764, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1763, 1767, 1765, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1764, 1768, 1766, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1765, 1769, 1767, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1766, 1770, 1768, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1767, 1771, 1769, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1768, 1772, 1770, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1769, 1773, 1771, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1770, 1774, 1772, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1771, 1775, 1773, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1772, 1776, 1774, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1773, 1777, 1775, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1774, 1778, 1776, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1775, 1779, 1777, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1776, 1780, 1778, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1777, 1781, 1779, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1778, 1782, 1780, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1779, 1783, 1781, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1780, 1784, 1782, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1781, 1785, 1783, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1782, 1786, 1784, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1783, 1787, 1785, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1784, 1788, 1786, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1785, 1789, 1787, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1786, 1790, 1788, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1787, 1791, 1789, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1788, 1792, 1790, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1789, 1793, 1791, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1790, 1794, 1792, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1791, 1795, 1793, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1792, 1796, 1794, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1793, 1797, 1795, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1794, 1798, 1796, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1795, 1799, 1797, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1796, 1800, 1798, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1797, 1801, 1799, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1798, 1802, 1800, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1799, 1803, 1801, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1800, 1804, 1802, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1801, 1805, 1803, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1802, 1806, 1804, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1803, 1807, 1805, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1804, 1808, 1806, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1805, 1809, 1807, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1806, 1810, 1808, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1807, 1811, 1809, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1808, 1812, 1810, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1809, 1813, 1811, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1810, 1814, 1812, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1811, 1815, 1813, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1812, 1816, 1814, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1813, 1817, 1815, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1814, 1818, 1816, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1815, 1819, 1817, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1816, 1820, 1818, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1817, 1821, 1819, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1818, 1822, 1820, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1819, 1823, 1821, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1820, 1824, 1822, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1821, 1825, 1823, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1822, 1826, 1824, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1823, 1827, 1825, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1824, 1828, 1826, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1825, 1829, 1827, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1826, 1830, 1828, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1827, 1831, 1829, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1828, 1832, 1830, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1829, 1833, 1831, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1830, 1834, 1832, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1831, 1835, 1833, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1832, 1836, 1834, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1833, 1837, 1835, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1834, 1838, 1836, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1835, 1839, 1837, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1836, 1840, 1838, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1837, 1841, 1839, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1838, 1842, 1840, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1839, 1843, 1841, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1840, 1844, 1842, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1841, 1845, 1843, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1842, 1846, 1844, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1843, 1847, 1845, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1844, 1848, 1846, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1845, 1849, 1847, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1846, 1850, 1848, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1847, 1851, 1849, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1848, 1852, 1850, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1849, 1853, 1851, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1850, 1854, 1852, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1851, 1855, 1853, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1852, 1856, 1854, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1853, 1857, 1855, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1854, 1858, 1856, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1855, 1859, 1857, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1856, 1860, 1858, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1857, 1861, 1859, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1858, 1862, 1860, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1859, 1863, 1861, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1860, 1864, 1862, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1861, 1865, 1863, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1862, 1866, 1864, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1863, 1867, 1865, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1864, 1868, 1866, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1865, 1869, 1867, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1866, 1870, 1868, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1867, 1871, 1869, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1868, 1872, 1870, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1869, 1873, 1871, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1870, 1874, 1872, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1871, 1875, 1873, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1872, 1876, 1874, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1873, 1877, 1875, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1874, 1878, 1876, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1875, 1879, 1877, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1876, 1880, 1878, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1877, 1881, 1879, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1878, 1882, 1880, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1879, 1883, 1881, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1880, 1884, 1882, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1881, 1885, 1883, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1882, 1886, 1884, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1883, 1887, 1885, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1884, 1888, 1886, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1885, 1889, 1887, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1886, 1890, 1888, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1887, 1891, 1889, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1888, 1892, 1890, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1889, 1893, 1891, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1890, 1894, 1892, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1891, 1895, 1893, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1892, 1896, 1894, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1893, 1897, 1895, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1894, 1898, 1896, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1895, 1899, 1897, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1896, 1900, 1898, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1897, 1901, 1899, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1898, 1902, 1900, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1899, 1903, 1901, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1900, 1904, 1902, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1901, 1905, 1903, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1902, 1906, 1904, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1903, 1907, 1905, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1904, 1908, 1906, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1905, 1909, 1907, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1906, 1910, 1908, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1907, 1911, 1909, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1908, 1912, 1910, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1909, 1913, 1911, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1910, 1914, 1912, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1911, 1915, 1913, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1912, 1916, 1914, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1913, 1917, 1915, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1914, 1918, 1916, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1915, 1919, 1917, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1916, 1920, 1918, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1917, 1921, 1919, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1918, 1922, 1920, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1919, 1923, 1921, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1920, 1924, 1922, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1921, 1925, 1923, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1922, 1926, 1924, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1923, 1927, 1925, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1924, 1928, 1926, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1925, 1929, 1927, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1926, 1930, 1928, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1927, 1931, 1929, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1928, 1932, 1930, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1929, 1933, 1931, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1930, 1934, 1932, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1931, 1935, 1933, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1932, 1936, 1934, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1933, 1937, 1935, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1934, 1938, 1936, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1935, 1939, 1937, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1936, 1940, 1938, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1937, 1941, 1939, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1938, 1942, 1940, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1939, 1943, 1941, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1940, 1944, 1942, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1941, 1945, 1943, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1942, 1946, 1944, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1943, 1947, 1945, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1944, 1948, 1946, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1945, 1949, 1947, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1946, 1950, 1948, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1947, 1951, 1949, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1948, 1952, 1950, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1949, 1953, 1951, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1950, 1954, 1952, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1951, 1955, 1953, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1952, 1956, 1954, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1953, 1957, 1955, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1954, 1958, 1956, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1955, 1959, 1957, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1956, 1960, 1958, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1957, 1961, 1959, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1958, 1962, 1960, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1959, 1963, 1961, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1960, 1964, 1962, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1961, 1965, 1963, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1962, 1966, 1964, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1963, 1967, 1965, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1964, 1968, 1966, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1965, 1969, 1967, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1966, 1970, 1968, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1967, 1971, 1969, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1968, 1972, 1970, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1969, 1973, 1971, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1970, 1974, 1972, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1971, 1975, 1973, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1972, 1976, 1974, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1973, 1977, 1975, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1974, 1978, 1976, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1975, 1979, 1977, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1976, 1980, 1978, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1977, 1981, 1979, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1978, 1982, 1980, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1979, 1983, 1981, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1980, 1984, 1982, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1981, 1985, 1983, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1982, 1986, 1984, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1983, 1987, 1985, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1984, 1988, 1986, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1985, 1989, 1987, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1986, 1990, 1988, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1987, 1991, 1989, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1988, 1992, 1990, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1989, 1993, 1991, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1990, 1994, 1992, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1991, 1995, 1993, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1992, 1996, 1994, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1993, 1997, 1995, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1994, 1998, 1996, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1995, 1999, 1997, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1996, 2000, 1998, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1997, 2001, 1999, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1998, 2002, 2000, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(1999, 2003, 2001, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2000, 2004, 2002, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2001, 2005, 2003, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2002, 2006, 2004, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2003, 2007, 2005, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2004, 2008, 2006, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2005, 2009, 2007, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2006, 2010, 2008, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2007, 2011, 2009, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2008, 2012, 2010, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2009, 2013, 2011, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2010, 2014, 2012, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2011, 2015, 2013, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2012, 2016, 2014, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2013, 2017, 2015, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2014, 2018, 2016, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2015, 2019, 2017, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2016, 2020, 2018, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2017, 2021, 2019, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2018, 2022, 2020, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2019, 2023, 2021, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2020, 2024, 2022, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2021, 2025, 2023, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2022, 2026, 2024, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2023, 2027, 2025, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2024, 2028, 2026, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2025, 2029, 2027, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2026, 2030, 2028, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2027, 2031, 2029, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2028, 2032, 2030, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2029, 2033, 2031, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2030, 2034, 2032, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2031, 2035, 2033, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2032, 2036, 2034, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2033, 2037, 2035, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2034, 2038, 2036, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2035, 2039, 2037, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2036, 2040, 2038, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2037, 2041, 2039, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2038, 2042, 2040, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2039, 2043, 2041, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2040, 2044, 2042, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2041, 2045, 2043, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2042, 2046, 2044, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2043, 2047, 2045, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2044, 2048, 2046, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2045, 2049, 2047, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2046, 2050, 2048, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2047, 2051, 2049, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2048, 2052, 2050, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2049, 2053, 2051, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2050, 2054, 2052, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2051, 2055, 2053, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2052, 2056, 2054, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2053, 2057, 2055, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2054, 2058, 2056, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2055, 2059, 2057, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2056, 2060, 2058, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2057, 2061, 2059, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2058, 2062, 2060, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2059, 2063, 2061, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2060, 2064, 2062, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2061, 2065, 2063, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2062, 2066, 2064, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2063, 2067, 2065, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2064, 2068, 2066, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2065, 2069, 2067, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2066, 2070, 2068, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2067, 2071, 2069, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2068, 2072, 2070, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2069, 2073, 2071, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2070, 2074, 2072, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2071, 2075, 2073, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2072, 2076, 2074, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2073, 2077, 2075, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2074, 2078, 2076, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2075, 2079, 2077, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2076, 2080, 2078, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2077, 2081, 2079, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2078, 2082, 2080, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2079, 2083, 2081, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2080, 2084, 2082, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2081, 2085, 2083, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2082, 2086, 2084, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2083, 2087, 2085, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2084, 2088, 2086, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2085, 2089, 2087, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2086, 2090, 2088, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2087, 2091, 2089, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2088, 2092, 2090, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2089, 2093, 2091, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2090, 2094, 2092, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2091, 2095, 2093, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2092, 2096, 2094, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2093, 2097, 2095, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2094, 2098, 2096, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2095, 2099, 2097, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2096, 2100, 2098, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2097, 2101, 2099, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2098, 2102, 2100, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2099, 2103, 2101, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2100, 2104, 2102, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2101, 2105, 2103, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2102, 2106, 2104, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2103, 2107, 2105, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2104, 2108, 2106, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2105, 2109, 2107, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2106, 2110, 2108, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2107, 2111, 2109, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2108, 2112, 2110, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2109, 2113, 2111, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2110, 2114, 2112, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2111, 2115, 2113, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2112, 2116, 2114, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2113, 2117, 2115, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2114, 2118, 2116, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2115, 2119, 2117, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2116, 2120, 2118, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2117, 2121, 2119, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2118, 2122, 2120, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2119, 2123, 2121, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2120, 2124, 2122, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2121, 2125, 2123, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2122, 2126, 2124, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2123, 2127, 2125, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2124, 2128, 2126, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2125, 2129, 2127, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2126, 2130, 2128, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2127, 2131, 2129, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2128, 2132, 2130, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2129, 2133, 2131, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2130, 2134, 2132, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2131, 2135, 2133, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2132, 2136, 2134, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2133, 2137, 2135, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2134, 2138, 2136, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2135, 2139, 2137, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2136, 2140, 2138, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2137, 2141, 2139, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2138, 2142, 2140, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2139, 2143, 2141, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2140, 2144, 2142, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2141, 2145, 2143, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2142, 2146, 2144, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2143, 2147, 2145, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2144, 2148, 2146, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2145, 2149, 2147, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2146, 2150, 2148, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2147, 2151, 2149, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2148, 2152, 2150, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2149, 2153, 2151, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2150, 2154, 2152, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2151, 2155, 2153, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2152, 2156, 2154, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2153, 2157, 2155, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2154, 2158, 2156, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2155, 2159, 2157, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2156, 2160, 2158, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2157, 2161, 2159, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2158, 2162, 2160, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2159, 2163, 2161, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2160, 2164, 2162, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2161, 2165, 2163, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2162, 2166, 2164, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2163, 2167, 2165, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2164, 2168, 2166, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2165, 2169, 2167, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2166, 2170, 2168, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2167, 2171, 2169, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2168, 2172, 2170, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2169, 2173, 2171, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2170, 2174, 2172, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2171, 2175, 2173, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2172, 2176, 2174, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2173, 2177, 2175, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2174, 2178, 2176, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2175, 2179, 2177, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2176, 2180, 2178, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2177, 2181, 2179, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2178, 2182, 2180, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2179, 2183, 2181, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2180, 2184, 2182, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2181, 2185, 2183, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2182, 2186, 2184, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2183, 2187, 2185, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2184, 2188, 2186, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2185, 2189, 2187, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2186, 2190, 2188, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2187, 2191, 2189, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2188, 2192, 2190, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2189, 2193, 2191, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2190, 2194, 2192, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2191, 2195, 2193, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2192, 2196, 2194, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2193, 2197, 2195, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2194, 2198, 2196, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2195, 2199, 2197, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2196, 2200, 2198, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2197, 2201, 2199, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2198, 2202, 2200, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2199, 2203, 2201, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2200, 2204, 2202, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2201, 2205, 2203, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2202, 2206, 2204, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2203, 2207, 2205, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2204, 2208, 2206, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2205, 2209, 2207, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2206, 2210, 2208, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2207, 2211, 2209, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2208, 2212, 2210, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2209, 2213, 2211, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2210, 2214, 2212, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2211, 2215, 2213, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2212, 2216, 2214, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2213, 2217, 2215, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2214, 2218, 2216, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2215, 2219, 2217, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2216, 2220, 2218, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2217, 2221, 2219, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2218, 2222, 2220, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2219, 2223, 2221, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2220, 2224, 2222, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2221, 2225, 2223, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2222, 2226, 2224, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2223, 2227, 2225, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2224, 2228, 2226, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2225, 2229, 2227, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2226, 2230, 2228, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2227, 2231, 2229, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2228, 2232, 2230, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2229, 2233, 2231, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2230, 2234, 2232, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2231, 2235, 2233, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2232, 2236, 2234, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2233, 2237, 2235, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2234, 2238, 2236, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2235, 2239, 2237, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2236, 2240, 2238, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2237, 2241, 2239, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2238, 2242, 2240, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2239, 2243, 2241, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2240, 2244, 2242, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2241, 2245, 2243, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2242, 2246, 2244, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2243, 2247, 2245, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2244, 2248, 2246, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2245, 2249, 2247, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2246, 2250, 2248, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2247, 2251, 2249, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2248, 2252, 2250, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2249, 2253, 2251, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2250, 2254, 2252, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2251, 2255, 2253, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2252, 2256, 2254, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2253, 2257, 2255, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2254, 2258, 2256, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2255, 2259, 2257, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2256, 2260, 2258, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2257, 2261, 2259, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2258, 2262, 2260, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2259, 2263, 2261, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2260, 2264, 2262, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2261, 2265, 2263, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2262, 2266, 2264, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2263, 2267, 2265, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2264, 2268, 2266, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2265, 2269, 2267, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2266, 2270, 2268, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2267, 2271, 2269, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2268, 2272, 2270, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2269, 2273, 2271, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2270, 2274, 2272, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2271, 2275, 2273, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2272, 2276, 2274, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2273, 2277, 2275, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2274, 2278, 2276, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2275, 2279, 2277, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2276, 2280, 2278, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2277, 2281, 2279, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2278, 2282, 2280, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2279, 2283, 2281, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2280, 2284, 2282, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2281, 2285, 2283, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2282, 2286, 2284, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2283, 2287, 2285, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2284, 2288, 2286, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2285, 2289, 2287, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2286, 2290, 2288, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2287, 2291, 2289, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2288, 2292, 2290, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2289, 2293, 2291, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2290, 2294, 2292, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2291, 2295, 2293, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2292, 2296, 2294, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2293, 2297, 2295, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2294, 2298, 2296, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2295, 2299, 2297, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2296, 2300, 2298, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2297, 2301, 2299, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2298, 2302, 2300, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2299, 2303, 2301, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2300, 2304, 2302, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2301, 2305, 2303, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2302, 2306, 2304, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2303, 2307, 2305, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2304, 2308, 2306, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2305, 2309, 2307, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2306, 2310, 2308, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2307, 2311, 2309, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2308, 2312, 2310, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2309, 2313, 2311, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2310, 2314, 2312, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2311, 2315, 2313, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2312, 2316, 2314, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2313, 2317, 2315, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2314, 2318, 2316, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2315, 2319, 2317, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2316, 2320, 2318, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2317, 2321, 2319, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2318, 2322, 2320, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2319, 2323, 2321, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2320, 2324, 2322, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2321, 2325, 2323, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2322, 2326, 2324, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2323, 2327, 2325, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2324, 2328, 2326, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2325, 2329, 2327, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2326, 2330, 2328, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2327, 2331, 2329, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2328, 2332, 2330, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2329, 2333, 2331, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2330, 2334, 2332, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2331, 2335, 2333, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2332, 2336, 2334, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2333, 2337, 2335, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2334, 2338, 2336, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2335, 2339, 2337, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2336, 2340, 2338, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2337, 2341, 2339, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2338, 2342, 2340, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2339, 2343, 2341, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2340, 2344, 2342, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2341, 2345, 2343, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2342, 2346, 2344, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2343, 2347, 2345, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2344, 2348, 2346, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2345, 2349, 2347, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2346, 2350, 2348, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2347, 2351, 2349, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2348, 2352, 2350, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2349, 2353, 2351, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2350, 2354, 2352, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2351, 2355, 2353, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2352, 2356, 2354, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2353, 2357, 2355, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2354, 2358, 2356, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2355, 2359, 2357, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2356, 2360, 2358, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2357, 2361, 2359, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2358, 2362, 2360, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2359, 2363, 2361, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2360, 2364, 2362, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2361, 2365, 2363, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2362, 2366, 2364, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2363, 2367, 2365, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2364, 2368, 2366, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2365, 2369, 2367, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2366, 2370, 2368, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2367, 2371, 2369, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2368, 2372, 2370, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2369, 2373, 2371, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2370, 2374, 2372, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2371, 2375, 2373, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2372, 2376, 2374, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2373, 2377, 2375, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2374, 2378, 2376, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2375, 2379, 2377, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2376, 2380, 2378, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2377, 2381, 2379, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2378, 2382, 2380, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2379, 2383, 2381, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2380, 2384, 2382, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2381, 2385, 2383, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2382, 2386, 2384, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2383, 2387, 2385, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2384, 2388, 2386, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2385, 2389, 2387, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2386, 2390, 2388, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2387, 2391, 2389, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2388, 2392, 2390, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2389, 2393, 2391, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2390, 2394, 2392, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2391, 2395, 2393, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2392, 2396, 2394, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2393, 2397, 2395, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2394, 2398, 2396, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2395, 2399, 2397, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2396, 2400, 2398, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2397, 2401, 2399, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2398, 2402, 2400, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2399, 2403, 2401, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2400, 2404, 2402, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2401, 2405, 2403, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2402, 2406, 2404, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2403, 2407, 2405, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2404, 2408, 2406, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2405, 2409, 2407, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2406, 2410, 2408, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2407, 2411, 2409, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2408, 2412, 2410, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2409, 2413, 2411, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2410, 2414, 2412, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2411, 2415, 2413, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2412, 2416, 2414, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2413, 2417, 2415, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2414, 2418, 2416, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2415, 2419, 2417, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2416, 2420, 2418, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2417, 2421, 2419, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2418, 2422, 2420, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2419, 2423, 2421, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2420, 2424, 2422, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2421, 2425, 2423, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2422, 2426, 2424, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2423, 2427, 2425, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2424, 2428, 2426, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2425, 2429, 2427, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2426, 2430, 2428, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2427, 2431, 2429, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2428, 2432, 2430, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2429, 2433, 2431, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2430, 2434, 2432, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2431, 2435, 2433, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2432, 2436, 2434, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2433, 2437, 2435, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2434, 2438, 2436, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2435, 2439, 2437, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2436, 2440, 2438, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2437, 2441, 2439, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2438, 2442, 2440, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2439, 2443, 2441, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2440, 2444, 2442, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2441, 2445, 2443, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2442, 2446, 2444, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2443, 2447, 2445, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2444, 2448, 2446, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2445, 2449, 2447, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2446, 2450, 2448, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2447, 2451, 2449, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2448, 2452, 2450, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2449, 2453, 2451, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2450, 2454, 2452, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2451, 2455, 2453, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2452, 2456, 2454, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2453, 2457, 2455, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2454, 2458, 2456, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2455, 2459, 2457, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2456, 2460, 2458, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2457, 2461, 2459, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2458, 2462, 2460, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2459, 2463, 2461, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2460, 2464, 2462, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2461, 2465, 2463, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2462, 2466, 2464, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2463, 2467, 2465, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2464, 2468, 2466, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2465, 2469, 2467, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2466, 2470, 2468, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2467, 2471, 2469, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2468, 2472, 2470, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2469, 2473, 2471, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2470, 2474, 2472, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2471, 2475, 2473, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2472, 2476, 2474, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2473, 2477, 2475, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2474, 2478, 2476, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2475, 2479, 2477, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2476, 2480, 2478, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2477, 2481, 2479, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2478, 2482, 2480, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2479, 2483, 2481, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2480, 2484, 2482, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2481, 2485, 2483, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2482, 2486, 2484, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2483, 2487, 2485, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2484, 2488, 2486, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2485, 2489, 2487, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2486, 2490, 2488, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2487, 2491, 2489, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2488, 2492, 2490, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2489, 2493, 2491, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2490, 2494, 2492, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2491, 2495, 2493, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2492, 2496, 2494, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2493, 2497, 2495, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2494, 2498, 2496, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2495, 2499, 2497, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2496, 2500, 2498, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2497, 2501, 2499, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2498, 2502, 2500, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2499, 2503, 2501, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2500, 2504, 2502, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2501, 2505, 2503, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2502, 2506, 2504, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2503, 2507, 2505, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2504, 2508, 2506, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2505, 2509, 2507, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2506, 2510, 2508, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2507, 2511, 2509, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2508, 2512, 2510, 10, 1, 1, NULL, NULL, NULL, NULL, 0),
(2509, 1677, 1675, 9, 1, 1, '2021-08-17', 3, NULL, NULL, 0),
(2510, 1677, 1675, 8, 1, 1, '2021-08-17', 3, NULL, NULL, 0),
(2511, 1677, 1675, 7, 1, 1, '2021-08-17', 3, NULL, NULL, 0),
(2512, 1677, 1675, 6, 1, 1, '2021-08-17', 3, NULL, NULL, 0),
(2513, 1686, 1684, 9, 1, 1, '2021-09-30', 3, NULL, NULL, 0),
(2514, 1677, 1675, 5, 1, 1, '2021-10-07', 3, NULL, NULL, 0),
(2515, 1677, 1675, 6, 1, 1, '2021-10-20', 3, NULL, NULL, 0),
(2516, 2513, 2511, 120, 1, 1, '2021-11-01', 3, NULL, NULL, 0),
(2517, 2513, 2511, 119, 1, 1, '2021-11-01', 3, NULL, NULL, 0),
(2518, 1677, 1675, 5, 1, 1, '2021-11-05', 3, NULL, NULL, 0),
(2519, 1677, 1675, 4, 1, 1, '2021-11-05', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_stock_minus`
--

CREATE TABLE `product_stock_minus` (
  `id` int(11) NOT NULL,
  `product_stock` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_surat_jalan`
--

CREATE TABLE `product_surat_jalan` (
  `id` int(11) NOT NULL,
  `no_sj` varchar(155) DEFAULT NULL,
  `document_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_vendor`
--

CREATE TABLE `product_vendor` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `vendor` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama_rak` varchar(255) DEFAULT NULL,
  `panjang` int(11) DEFAULT NULL,
  `lebar` int(11) DEFAULT NULL,
  `tinggi` int(11) DEFAULT NULL,
  `max_tonase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `kode`, `nama_rak`, `panjang`, `lebar`, `tinggi`, `max_tonase`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'RAK001', 'RAK UTAMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reimburse`
--

CREATE TABLE `reimburse` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `return_penjualan`
--

CREATE TABLE `return_penjualan` (
  `id` int(11) NOT NULL,
  `no_retur` varchar(255) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `return_penjualan_item`
--

CREATE TABLE `return_penjualan_item` (
  `id` int(11) NOT NULL,
  `return_penjualan` int(11) NOT NULL,
  `invoice_product` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_barang`
--

CREATE TABLE `retur_barang` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `no_retur` varchar(155) DEFAULT NULL,
  `tanggal_retur` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_barang_item`
--

CREATE TABLE `retur_barang_item` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `retur_barang` int(11) NOT NULL,
  `stok_kategori` int(11) NOT NULL,
  `gudang` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_barang_status`
--

CREATE TABLE `retur_barang_status` (
  `id` int(11) NOT NULL,
  `retur_barang` int(11) NOT NULL,
  `status` varchar(150) DEFAULT NULL COMMENT 'VALIDATE\nDRAFT',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_order`
--

CREATE TABLE `retur_order` (
  `id` int(11) NOT NULL,
  `no_retur` varchar(155) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_order_item`
--

CREATE TABLE `retur_order_item` (
  `id` int(11) NOT NULL,
  `retur_order` int(11) NOT NULL,
  `order_product` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute`
--

CREATE TABLE `sales_rute` (
  `id` int(11) NOT NULL,
  `no_rute` varchar(155) DEFAULT NULL,
  `minggu` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_customer`
--

CREATE TABLE `sales_rute_customer` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_hari`
--

CREATE TABLE `sales_rute_hari` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `hari` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_user`
--

CREATE TABLE `sales_rute_user` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `nama_satuan` varchar(155) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `nama_satuan`, `parent`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'PCS', NULL, '2021-08-08', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status_pembayaran`
--

CREATE TABLE `status_pembayaran` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_pembelian`
--

CREATE TABLE `status_pembelian` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stok_kategori`
--

CREATE TABLE `stok_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surat_jalan`
--

CREATE TABLE `surat_jalan` (
  `id` int(11) NOT NULL,
  `no_surat_jalan` varchar(155) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id` int(11) NOT NULL,
  `tagihan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `threshold_mas`
--

CREATE TABLE `threshold_mas` (
  `id` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_product`
--

CREATE TABLE `tipe_product` (
  `id` int(11) NOT NULL,
  `tipe` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_product`
--

INSERT INTO `tipe_product` (`id`, `tipe`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UMUM', '2021-08-07', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` text,
  `pegawai` int(11) DEFAULT NULL,
  `priveledge` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `pegawai`, `priveledge`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(3, 'admin', '21232f297a57a5a743894a0e4a801fc3', 5, 5, '2021-08-06', NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_sms_gateway`
--

CREATE TABLE `user_sms_gateway` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `url` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `nama_vendor` varchar(150) DEFAULT NULL,
  `vendor_category` int(11) NOT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `nama_vendor`, `vendor_category`, `no_hp`, `keterangan`, `alamat`, `email`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UMUM', 2, '-', '-', '-', '-', 1, '2021-10-20', 3, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_category`
--

CREATE TABLE `vendor_category` (
  `id` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL COMMENT 'COMPANY\nSUPPLIER'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_category`
--

INSERT INTO `vendor_category` (`id`, `kategori`) VALUES
(1, 'COMPANY'),
(2, 'SUPPLIER');

-- --------------------------------------------------------

--
-- Table structure for table `zakat_mal`
--

CREATE TABLE `zakat_mal` (
  `id` int(11) NOT NULL,
  `threshold_mas` int(11) NOT NULL,
  `tot_kas_bank` int(15) NOT NULL,
  `tot_inventori` int(15) NOT NULL,
  `tot_hutang` int(15) NOT NULL,
  `tot_bruto` int(15) NOT NULL,
  `tot_net` int(15) NOT NULL,
  `zakat_threshold` int(15) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ansuran`
--
ALTER TABLE `ansuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coa`
--
ALTER TABLE `coa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coa_type`
--
ALTER TABLE `coa_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_pengiriman`
--
ALTER TABLE `document_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  ADD PRIMARY KEY (`id`,`document_pengiriman`),
  ADD KEY `fk_dokumen_pengiriman_status_document_pengiriman1_idx` (`document_pengiriman`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indetitas`
--
ALTER TABLE `indetitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investor`
--
ALTER TABLE `investor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`,`pembeli`,`potongan`,`metode_bayar`),
  ADD KEY `fk_invoice_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_invoice_potongan1_idx` (`potongan`),
  ADD KEY `fk_invoice_metode_bayar1_idx` (`metode_bayar`);

--
-- Indexes for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  ADD PRIMARY KEY (`id`,`invoice_product`,`potongan`),
  ADD KEY `fk_invoice_pot_product_potongan1_idx` (`potongan`),
  ADD KEY `fk_invoice_pot_product_invoice_product1_idx` (`invoice_product`);

--
-- Indexes for table `invoice_print`
--
ALTER TABLE `invoice_print`
  ADD PRIMARY KEY (`id`,`user`,`invoice`),
  ADD KEY `fk_invoice_print_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_print_user1_idx` (`user`);

--
-- Indexes for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD PRIMARY KEY (`id`,`invoice`,`product_satuan`),
  ADD KEY `fk_invoice_product_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_product_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_invoice_sisa_invoice1_idx` (`invoice`);

--
-- Indexes for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD PRIMARY KEY (`id`,`user`,`invoice`),
  ADD KEY `fk_invoice_status_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_status_user1_idx` (`user`);

--
-- Indexes for table `jenis_akad`
--
ALTER TABLE `jenis_akad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_notif_pengiriman`
--
ALTER TABLE `jenis_notif_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  ADD PRIMARY KEY (`id`,`jurnal`,`jurnal_struktur`),
  ADD KEY `fk_jurnal_detail_jurnal1_idx` (`jurnal`),
  ADD KEY `fk_jurnal_detail_jurnal_struktur1_idx` (`jurnal_struktur`);

--
-- Indexes for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  ADD PRIMARY KEY (`id`,`feature`,`coa`,`coa_type`),
  ADD KEY `fk_jurnal_struktur_coa_type1_idx` (`coa_type`),
  ADD KEY `fk_jurnal_struktur_coa1_idx` (`coa`),
  ADD KEY `fk_jurnal_struktur_feature1_idx` (`feature`);

--
-- Indexes for table `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kasbon`
--
ALTER TABLE `kasbon`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_kasbon_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  ADD PRIMARY KEY (`id`,`payroll`,`kasbon`),
  ADD KEY `fk_kasbon_paid_kasbon1_idx` (`kasbon`),
  ADD KEY `fk_kasbon_paid_payroll1_idx` (`payroll`);

--
-- Indexes for table `kasbon_payment`
--
ALTER TABLE `kasbon_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  ADD PRIMARY KEY (`id`,`kasbon_payment`,`kasbon`),
  ADD KEY `fk_kasbon_payment_item_kasbon_payment1_idx` (`kasbon_payment`),
  ADD KEY `fk_kasbon_payment_item_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  ADD PRIMARY KEY (`id`,`kasbon`),
  ADD KEY `fk_kasbon_sisa_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  ADD PRIMARY KEY (`id`,`kasbon`),
  ADD KEY `fk_kasbon_status_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kategori_akad`
--
ALTER TABLE `kategori_akad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_product`
--
ALTER TABLE `kategori_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerja_sama_internal`
--
ALTER TABLE `kerja_sama_internal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_user_position`
--
ALTER TABLE `log_user_position`
  ADD PRIMARY KEY (`id`,`user`),
  ADD KEY `fk_log_user_position_user1_idx` (`user`);

--
-- Indexes for table `metode_bayar`
--
ALTER TABLE `metode_bayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `minggu`
--
ALTER TABLE `minggu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  ADD PRIMARY KEY (`id`,`notif_jatuh_tempo`,`jenis_notif_pengiriman`),
  ADD KEY `fk_notif_has_jenis_pengiriman_notif_jatuh_tempo1_idx` (`notif_jatuh_tempo`),
  ADD KEY `fk_notif_has_jenis_pengiriman_jenis_notif_pengiriman1_idx` (`jenis_notif_pengiriman`);

--
-- Indexes for table `notif_jatuh_tempo`
--
ALTER TABLE `notif_jatuh_tempo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`,`pembeli`,`potongan`,`metode_bayar`),
  ADD KEY `fk_order_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_order_potongan1_idx` (`potongan`),
  ADD KEY `fk_order_metode_bayar1_idx` (`metode_bayar`);

--
-- Indexes for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  ADD PRIMARY KEY (`id`,`order_product`,`potongan`),
  ADD KEY `fk_order_pot_product_order_product1_idx` (`order_product`),
  ADD KEY `fk_order_pot_product_potongan1_idx` (`potongan`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`,`product_satuan`,`order`),
  ADD KEY `fk_order_product_order1_idx` (`order`),
  ADD KEY `fk_order_product_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`,`order`),
  ADD KEY `fk_order_status_order1_idx` (`order`);

--
-- Indexes for table `pajak`
--
ALTER TABLE `pajak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partial_payment`
--
ALTER TABLE `partial_payment`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_partial_payment_invoice1_idx` (`invoice`);

--
-- Indexes for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  ADD PRIMARY KEY (`id`,`partial_payment`),
  ADD KEY `fk_partial_payment_status_partial_payment1_idx` (`partial_payment`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_item`
--
ALTER TABLE `payment_item`
  ADD PRIMARY KEY (`id`,`payment`,`invoice`),
  ADD KEY `fk_payment_item_payment1_idx` (`payment`),
  ADD KEY `fk_payment_item_invoice1_idx` (`invoice`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`,`periode`,`pegawai`),
  ADD KEY `fk_payroll_pegawai1_idx` (`pegawai`),
  ADD KEY `fk_payroll_periode1_idx` (`periode`);

--
-- Indexes for table `payroll_category`
--
ALTER TABLE `payroll_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_item`
--
ALTER TABLE `payroll_item`
  ADD PRIMARY KEY (`id`,`payroll`,`payroll_category`),
  ADD KEY `fk_payroll_item_payroll_category1_idx` (`payroll_category`),
  ADD KEY `fk_payroll_item_payroll1_idx` (`payroll`);

--
-- Indexes for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  ADD PRIMARY KEY (`id`,`payroll`),
  ADD KEY `fk_payroll_workdays_payroll1_idx` (`payroll`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_pegawai_kontrak_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_angsuran_pembayaran_product1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_cash_pembayaran_rumah1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_angsuran_pembayaran1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  ADD PRIMARY KEY (`id`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_lain_lain_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  ADD PRIMARY KEY (`id`,`pembeli_has_product`,`status_pembayaran`),
  ADD KEY `fk_pembayaran_pembeli_has_rumah1_idx` (`pembeli_has_product`),
  ADD KEY `fk_pembayaran_status_pembayaran1_idx` (`status_pembayaran`);

--
-- Indexes for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  ADD PRIMARY KEY (`id`,`tagihan`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_tagihan_tagihan1_idx` (`tagihan`),
  ADD KEY `fk_pembayaran_tagihan_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  ADD PRIMARY KEY (`id`,`vendor`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_vendor_vendor1_idx` (`vendor`),
  ADD KEY `fk_pembayaran_vendor_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id`,`pembeli_kategori`),
  ADD KEY `fk_pembeli_pembeli_kategori1_idx` (`pembeli_kategori`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  ADD PRIMARY KEY (`id`,`pembeli_has_product`,`product_has_harga_angsuran`),
  ADD KEY `fk_pembeli_has_angsuran_pembeli_has_rumah1_idx` (`pembeli_has_product`),
  ADD KEY `fk_pembeli_has_angsuran_rumah_has_harga_angsuran1_idx` (`product_has_harga_angsuran`);

--
-- Indexes for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  ADD PRIMARY KEY (`id`,`pembeli`,`indetitas`),
  ADD KEY `fk_kreditur_has_identitas_kreditur1_idx` (`pembeli`),
  ADD KEY `fk_kreditur_has_identitas_indetitas1_idx` (`indetitas`);

--
-- Indexes for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  ADD PRIMARY KEY (`id`,`pembelian`,`persyaratan_has_berkas`),
  ADD KEY `fk_pembeli_has_persyaratan_persyaratan_has_berkas1_idx` (`persyaratan_has_berkas`),
  ADD KEY `fk_pembeli_has_persyaratan_pembelian1_idx` (`pembelian`);

--
-- Indexes for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  ADD PRIMARY KEY (`id`,`pembelian`,`pembeli`,`product`,`status_pembelian`),
  ADD KEY `fk_kreditur_has_rumah_kreditur1_idx` (`pembeli`),
  ADD KEY `fk_kreditur_has_rumah_rumah1_idx` (`product`),
  ADD KEY `fk_pembeli_has_rumah_status_pembelian1_idx` (`status_pembelian`),
  ADD KEY `fk_pembeli_has_rumah_pembelian1_idx` (`pembelian`);

--
-- Indexes for table `pembeli_kategori`
--
ALTER TABLE `pembeli_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengiriman`
--
ALTER TABLE `pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengiriman_detail`
--
ALTER TABLE `pengiriman_detail`
  ADD PRIMARY KEY (`id`,`pengiriman`,`invoice_product`),
  ADD KEY `fk_pengiriman_detail_pengiriman1_idx` (`pengiriman`),
  ADD KEY `fk_pengiriman_detail_invoice_product1_idx` (`invoice_product`);

--
-- Indexes for table `pengiriman_has_customer`
--
ALTER TABLE `pengiriman_has_customer`
  ADD PRIMARY KEY (`id`,`pengiriman`,`pembeli`),
  ADD KEY `fk_pengiriman_has_customer_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_pengiriman_has_customer_pengiriman1_idx` (`pengiriman`);

--
-- Indexes for table `pengiriman_has_sales`
--
ALTER TABLE `pengiriman_has_sales`
  ADD PRIMARY KEY (`id`,`user`,`pengiriman`),
  ADD KEY `fk_pengiriman_has_sales_pengiriman1_idx` (`pengiriman`),
  ADD KEY `fk_pengiriman_has_sales_user1_idx` (`user`);

--
-- Indexes for table `pengiriman_status`
--
ALTER TABLE `pengiriman_status`
  ADD PRIMARY KEY (`id`,`pengiriman`),
  ADD KEY `fk_pengiriman_status_pengiriman1_idx` (`pengiriman`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD PRIMARY KEY (`id`,`kategori_akad`,`jenis_akad`),
  ADD KEY `fk_persyaratan_kategori_akad1_idx` (`kategori_akad`),
  ADD KEY `fk_persyaratan_jenis_akad1_idx` (`jenis_akad`);

--
-- Indexes for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  ADD PRIMARY KEY (`id`,`persyaratan`),
  ADD KEY `fk_persyaratan_has_berkas_persyaratan1_idx` (`persyaratan`);

--
-- Indexes for table `potongan`
--
ALTER TABLE `potongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priveledge`
--
ALTER TABLE `priveledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement`
--
ALTER TABLE `procurement`
  ADD PRIMARY KEY (`id`,`vendor`),
  ADD KEY `fk_procurement_vendor1_idx` (`vendor`);

--
-- Indexes for table `procurement_item`
--
ALTER TABLE `procurement_item`
  ADD PRIMARY KEY (`id`,`procurement`,`product`,`satuan`),
  ADD KEY `fk_procurement_item_product1_idx` (`product`),
  ADD KEY `fk_procurement_item_satuan1_idx` (`satuan`),
  ADD KEY `fk_procurement_item_procurement1_idx` (`procurement`);

--
-- Indexes for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  ADD PRIMARY KEY (`id`,`procurement`),
  ADD KEY `fk_procurement_retur_procurement1_idx` (`procurement`);

--
-- Indexes for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  ADD PRIMARY KEY (`id`,`procurement_item`,`procurement_retur`),
  ADD KEY `fk_procurement_return_item_procurement_retur1_idx` (`procurement_retur`),
  ADD KEY `fk_procurement_return_item_procurement_item1_idx` (`procurement_item`);

--
-- Indexes for table `procurement_status`
--
ALTER TABLE `procurement_status`
  ADD PRIMARY KEY (`id`,`procurement`),
  ADD KEY `fk_procurement_status_procurement1_idx` (`procurement`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`,`tipe_product`,`kategori_product`),
  ADD KEY `fk_rumah_kategori_rumah1_idx` (`kategori_product`),
  ADD KEY `fk_rumah_tipe_rumah1_idx` (`tipe_product`);

--
-- Indexes for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  ADD PRIMARY KEY (`id`,`pembeli`,`product`),
  ADD KEY `fk_product_distriburst_product1_idx` (`product`),
  ADD KEY `fk_product_distriburst_pembeli1_idx` (`pembeli`);

--
-- Indexes for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_foto_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  ADD PRIMARY KEY (`id`,`product`,`ansuran`),
  ADD KEY `fk_rumah_has_harga_angsuran_rumah1_idx` (`product`),
  ADD KEY `fk_rumah_has_harga_angsuran_ansuran1_idx` (`ansuran`);

--
-- Indexes for table `product_has_harga_grosir`
--
ALTER TABLE `product_has_harga_grosir`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_has_harga_grosir_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_has_harga_jual`
--
ALTER TABLE `product_has_harga_jual`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_has_harga_jual_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_harga_jual_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_harga_jual_kredit_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_price_procurement`
--
ALTER TABLE `product_has_price_procurement`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_has_price_procurement_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_kirim`
--
ALTER TABLE `product_kirim`
  ADD PRIMARY KEY (`id`,`product`,`document_pengiriman`),
  ADD KEY `fk_product_kirim_document_pengiriman1_idx` (`document_pengiriman`),
  ADD KEY `fk_product_kirim_product1_idx` (`product`);

--
-- Indexes for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  ADD PRIMARY KEY (`id`,`product_kirim`),
  ADD KEY `fk_product_kirim_status_product_kirim1_idx` (`product_kirim`);

--
-- Indexes for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_log_stock_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_satuan`
--
ALTER TABLE `product_satuan`
  ADD PRIMARY KEY (`id`,`product`,`satuan`),
  ADD KEY `fk_product_satuan_product1_idx` (`product`),
  ADD KEY `fk_product_satuan_satuan1_idx` (`satuan`);

--
-- Indexes for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD PRIMARY KEY (`id`,`product`,`product_satuan`),
  ADD KEY `fk_product_stock_product_satuan1_idx` (`product_satuan`),
  ADD KEY `fk_product_stock_product1_idx` (`product`);

--
-- Indexes for table `product_stock_minus`
--
ALTER TABLE `product_stock_minus`
  ADD PRIMARY KEY (`id`,`product_stock`),
  ADD KEY `fk_product_stock_minus_product_stock1_idx` (`product_stock`);

--
-- Indexes for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  ADD PRIMARY KEY (`id`,`document_pengiriman`),
  ADD KEY `fk_product_surat_jalan_document_pengiriman1_idx` (`document_pengiriman`);

--
-- Indexes for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD PRIMARY KEY (`id`,`product`,`vendor`),
  ADD KEY `fk_product_vendor_product1_idx` (`product`),
  ADD KEY `fk_product_vendor_vendor1_idx` (`vendor`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reimburse`
--
ALTER TABLE `reimburse`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_reimburse_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_return_penjualan_invoice1_idx` (`invoice`);

--
-- Indexes for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  ADD PRIMARY KEY (`id`,`return_penjualan`,`invoice_product`),
  ADD KEY `fk_return_penjualan_item_return_penjualan1_idx` (`return_penjualan`),
  ADD KEY `fk_return_penjualan_item_invoice_product1_idx` (`invoice_product`);

--
-- Indexes for table `retur_barang`
--
ALTER TABLE `retur_barang`
  ADD PRIMARY KEY (`id`,`pembeli`),
  ADD KEY `fk_retur_barang_pembeli1_idx` (`pembeli`);

--
-- Indexes for table `retur_barang_item`
--
ALTER TABLE `retur_barang_item`
  ADD PRIMARY KEY (`id`,`product_satuan`,`retur_barang`,`stok_kategori`),
  ADD KEY `fk_retur_barang_item_retur_barang1_idx` (`retur_barang`),
  ADD KEY `fk_retur_barang_item_stok_kategori1_idx` (`stok_kategori`),
  ADD KEY `fk_retur_barang_item_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `retur_barang_status`
--
ALTER TABLE `retur_barang_status`
  ADD PRIMARY KEY (`id`,`retur_barang`),
  ADD KEY `fk_retur_barang_status_retur_barang1_idx` (`retur_barang`);

--
-- Indexes for table `retur_order`
--
ALTER TABLE `retur_order`
  ADD PRIMARY KEY (`id`,`order`),
  ADD KEY `fk_retur_order_order1_idx` (`order`);

--
-- Indexes for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  ADD PRIMARY KEY (`id`,`retur_order`,`order_product`),
  ADD KEY `fk_retur_order_item_order_product1_idx` (`order_product`),
  ADD KEY `fk_retur_order_item_retur_order1_idx` (`retur_order`);

--
-- Indexes for table `sales_rute`
--
ALTER TABLE `sales_rute`
  ADD PRIMARY KEY (`id`,`minggu`),
  ADD KEY `fk_sales_rute_minggu1_idx` (`minggu`);

--
-- Indexes for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  ADD PRIMARY KEY (`id`,`sales_rute`,`pembeli`),
  ADD KEY `fk_sales_rute_customer_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_sales_rute_customer_sales_rute1_idx` (`sales_rute`);

--
-- Indexes for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  ADD PRIMARY KEY (`id`,`sales_rute`,`hari`),
  ADD KEY `fk_sales_rute_hari_sales_rute1_idx` (`sales_rute`),
  ADD KEY `fk_sales_rute_hari_hari1_idx` (`hari`);

--
-- Indexes for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  ADD PRIMARY KEY (`id`,`sales_rute`,`user`),
  ADD KEY `fk_sales_rute_user_user1_idx` (`user`),
  ADD KEY `fk_sales_rute_user_sales_rute1_idx` (`sales_rute`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pembelian`
--
ALTER TABLE `status_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok_kategori`
--
ALTER TABLE `stok_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_surat_jalan_invoice1_idx` (`invoice`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `threshold_mas`
--
ALTER TABLE `threshold_mas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_product`
--
ALTER TABLE `tipe_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`priveledge`),
  ADD KEY `fk_user_priveledge_idx` (`priveledge`);

--
-- Indexes for table `user_sms_gateway`
--
ALTER TABLE `user_sms_gateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`,`vendor_category`),
  ADD KEY `fk_vendor_vendor_category1_idx` (`vendor_category`);

--
-- Indexes for table `vendor_category`
--
ALTER TABLE `vendor_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ansuran`
--
ALTER TABLE `ansuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coa`
--
ALTER TABLE `coa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coa_type`
--
ALTER TABLE `coa_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `document_pengiriman`
--
ALTER TABLE `document_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hari`
--
ALTER TABLE `hari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `indetitas`
--
ALTER TABLE `indetitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `investor`
--
ALTER TABLE `investor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_print`
--
ALTER TABLE `invoice_print`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_product`
--
ALTER TABLE `invoice_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `invoice_status`
--
ALTER TABLE `invoice_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `jenis_akad`
--
ALTER TABLE `jenis_akad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jenis_notif_pengiriman`
--
ALTER TABLE `jenis_notif_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jurnal`
--
ALTER TABLE `jurnal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kas`
--
ALTER TABLE `kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kasbon`
--
ALTER TABLE `kasbon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kasbon_payment`
--
ALTER TABLE `kasbon_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kategori_akad`
--
ALTER TABLE `kategori_akad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kategori_product`
--
ALTER TABLE `kategori_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kerja_sama_internal`
--
ALTER TABLE `kerja_sama_internal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `log_user_position`
--
ALTER TABLE `log_user_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `metode_bayar`
--
ALTER TABLE `metode_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `minggu`
--
ALTER TABLE `minggu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `notif_jatuh_tempo`
--
ALTER TABLE `notif_jatuh_tempo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `pajak`
--
ALTER TABLE `pajak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `partial_payment`
--
ALTER TABLE `partial_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `payment_item`
--
ALTER TABLE `payment_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payroll_category`
--
ALTER TABLE `payroll_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payroll_item`
--
ALTER TABLE `payroll_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pembeli_kategori`
--
ALTER TABLE `pembeli_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pengiriman`
--
ALTER TABLE `pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengiriman_detail`
--
ALTER TABLE `pengiriman_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengiriman_has_customer`
--
ALTER TABLE `pengiriman_has_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengiriman_has_sales`
--
ALTER TABLE `pengiriman_has_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengiriman_status`
--
ALTER TABLE `pengiriman_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `persyaratan`
--
ALTER TABLE `persyaratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `potongan`
--
ALTER TABLE `potongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `priveledge`
--
ALTER TABLE `priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `procurement`
--
ALTER TABLE `procurement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `procurement_item`
--
ALTER TABLE `procurement_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `procurement_status`
--
ALTER TABLE `procurement_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2514;
--
-- AUTO_INCREMENT for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_has_harga_grosir`
--
ALTER TABLE `product_has_harga_grosir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_has_harga_jual`
--
ALTER TABLE `product_has_harga_jual`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2514;
--
-- AUTO_INCREMENT for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_has_price_procurement`
--
ALTER TABLE `product_has_price_procurement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `product_kirim`
--
ALTER TABLE `product_kirim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2520;
--
-- AUTO_INCREMENT for table `product_satuan`
--
ALTER TABLE `product_satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2512;
--
-- AUTO_INCREMENT for table `product_stock`
--
ALTER TABLE `product_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2520;
--
-- AUTO_INCREMENT for table `product_stock_minus`
--
ALTER TABLE `product_stock_minus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_vendor`
--
ALTER TABLE `product_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `reimburse`
--
ALTER TABLE `reimburse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retur_barang`
--
ALTER TABLE `retur_barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retur_barang_item`
--
ALTER TABLE `retur_barang_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retur_barang_status`
--
ALTER TABLE `retur_barang_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retur_order`
--
ALTER TABLE `retur_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sales_rute`
--
ALTER TABLE `sales_rute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `status_pembelian`
--
ALTER TABLE `status_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stok_kategori`
--
ALTER TABLE `stok_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `threshold_mas`
--
ALTER TABLE `threshold_mas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tipe_product`
--
ALTER TABLE `tipe_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_sms_gateway`
--
ALTER TABLE `user_sms_gateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vendor_category`
--
ALTER TABLE `vendor_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  ADD CONSTRAINT `fk_dokumen_pengiriman_status_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_invoice_metode_bayar1` FOREIGN KEY (`metode_bayar`) REFERENCES `metode_bayar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  ADD CONSTRAINT `fk_invoice_pot_product_invoice_product1` FOREIGN KEY (`invoice_product`) REFERENCES `invoice_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_pot_product_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_print`
--
ALTER TABLE `invoice_print`
  ADD CONSTRAINT `fk_invoice_print_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_print_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD CONSTRAINT `fk_invoice_product_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  ADD CONSTRAINT `fk_invoice_sisa_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD CONSTRAINT `fk_invoice_status_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_status_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  ADD CONSTRAINT `fk_jurnal_detail_jurnal1` FOREIGN KEY (`jurnal`) REFERENCES `jurnal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_detail_jurnal_struktur1` FOREIGN KEY (`jurnal_struktur`) REFERENCES `jurnal_struktur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  ADD CONSTRAINT `fk_jurnal_struktur_coa1` FOREIGN KEY (`coa`) REFERENCES `coa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_struktur_coa_type1` FOREIGN KEY (`coa_type`) REFERENCES `coa_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_struktur_feature1` FOREIGN KEY (`feature`) REFERENCES `feature` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon`
--
ALTER TABLE `kasbon`
  ADD CONSTRAINT `fk_kasbon_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  ADD CONSTRAINT `fk_kasbon_paid_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kasbon_paid_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  ADD CONSTRAINT `fk_kasbon_payment_item_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kasbon_payment_item_kasbon_payment1` FOREIGN KEY (`kasbon_payment`) REFERENCES `kasbon_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  ADD CONSTRAINT `fk_kasbon_sisa_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  ADD CONSTRAINT `fk_kasbon_status_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_user_position`
--
ALTER TABLE `log_user_position`
  ADD CONSTRAINT `fk_log_user_position_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  ADD CONSTRAINT `fk_notif_has_jenis_pengiriman_jenis_notif_pengiriman1` FOREIGN KEY (`jenis_notif_pengiriman`) REFERENCES `jenis_notif_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_notif_has_jenis_pengiriman_notif_jatuh_tempo1` FOREIGN KEY (`notif_jatuh_tempo`) REFERENCES `notif_jatuh_tempo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_metode_bayar1` FOREIGN KEY (`metode_bayar`) REFERENCES `metode_bayar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  ADD CONSTRAINT `fk_order_pot_product_order_product1` FOREIGN KEY (`order_product`) REFERENCES `order_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_pot_product_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `fk_order_product_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_product_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_status`
--
ALTER TABLE `order_status`
  ADD CONSTRAINT `fk_order_status_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `partial_payment`
--
ALTER TABLE `partial_payment`
  ADD CONSTRAINT `fk_partial_payment_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  ADD CONSTRAINT `fk_partial_payment_status_partial_payment1` FOREIGN KEY (`partial_payment`) REFERENCES `partial_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payment_item`
--
ALTER TABLE `payment_item`
  ADD CONSTRAINT `fk_payment_item_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payment_item_payment1` FOREIGN KEY (`payment`) REFERENCES `payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll`
--
ALTER TABLE `payroll`
  ADD CONSTRAINT `fk_payroll_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payroll_periode1` FOREIGN KEY (`periode`) REFERENCES `periode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll_item`
--
ALTER TABLE `payroll_item`
  ADD CONSTRAINT `fk_payroll_item_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payroll_item_payroll_category1` FOREIGN KEY (`payroll_category`) REFERENCES `payroll_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  ADD CONSTRAINT `fk_payroll_workdays_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  ADD CONSTRAINT `fk_pegawai_kontrak_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  ADD CONSTRAINT `fk_pembayaran_has_angsuran_pembayaran_product1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  ADD CONSTRAINT `fk_pembayaran_has_cash_pembayaran_rumah1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  ADD CONSTRAINT `fk_pembayaran_has_angsuran_pembayaran1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  ADD CONSTRAINT `fk_pembayaran_lain_lain_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  ADD CONSTRAINT `fk_pembayaran_pembeli_has_rumah1` FOREIGN KEY (`pembeli_has_product`) REFERENCES `pembeli_has_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_status_pembayaran1` FOREIGN KEY (`status_pembayaran`) REFERENCES `status_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  ADD CONSTRAINT `fk_pembayaran_tagihan_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_tagihan_tagihan1` FOREIGN KEY (`tagihan`) REFERENCES `tagihan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  ADD CONSTRAINT `fk_pembayaran_vendor_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_vendor_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD CONSTRAINT `fk_pembeli_pembeli_kategori1` FOREIGN KEY (`pembeli_kategori`) REFERENCES `pembeli_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  ADD CONSTRAINT `fk_pembeli_has_angsuran_pembeli_has_rumah1` FOREIGN KEY (`pembeli_has_product`) REFERENCES `pembeli_has_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_angsuran_rumah_has_harga_angsuran1` FOREIGN KEY (`product_has_harga_angsuran`) REFERENCES `product_has_harga_angsuran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  ADD CONSTRAINT `fk_kreditur_has_identitas_indetitas1` FOREIGN KEY (`indetitas`) REFERENCES `indetitas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kreditur_has_identitas_kreditur1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  ADD CONSTRAINT `fk_pembeli_has_persyaratan_pembelian1` FOREIGN KEY (`pembelian`) REFERENCES `pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_persyaratan_persyaratan_has_berkas1` FOREIGN KEY (`persyaratan_has_berkas`) REFERENCES `persyaratan_has_berkas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  ADD CONSTRAINT `fk_kreditur_has_rumah_kreditur1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kreditur_has_rumah_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_rumah_pembelian1` FOREIGN KEY (`pembelian`) REFERENCES `pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_rumah_status_pembelian1` FOREIGN KEY (`status_pembelian`) REFERENCES `status_pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengiriman_detail`
--
ALTER TABLE `pengiriman_detail`
  ADD CONSTRAINT `fk_pengiriman_detail_invoice_product1` FOREIGN KEY (`invoice_product`) REFERENCES `invoice_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pengiriman_detail_pengiriman1` FOREIGN KEY (`pengiriman`) REFERENCES `pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengiriman_has_customer`
--
ALTER TABLE `pengiriman_has_customer`
  ADD CONSTRAINT `fk_pengiriman_has_customer_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pengiriman_has_customer_pengiriman1` FOREIGN KEY (`pengiriman`) REFERENCES `pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengiriman_has_sales`
--
ALTER TABLE `pengiriman_has_sales`
  ADD CONSTRAINT `fk_pengiriman_has_sales_pengiriman1` FOREIGN KEY (`pengiriman`) REFERENCES `pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pengiriman_has_sales_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pengiriman_status`
--
ALTER TABLE `pengiriman_status`
  ADD CONSTRAINT `fk_pengiriman_status_pengiriman1` FOREIGN KEY (`pengiriman`) REFERENCES `pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD CONSTRAINT `fk_persyaratan_jenis_akad1` FOREIGN KEY (`jenis_akad`) REFERENCES `jenis_akad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persyaratan_kategori_akad1` FOREIGN KEY (`kategori_akad`) REFERENCES `kategori_akad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  ADD CONSTRAINT `fk_persyaratan_has_berkas_persyaratan1` FOREIGN KEY (`persyaratan`) REFERENCES `persyaratan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement`
--
ALTER TABLE `procurement`
  ADD CONSTRAINT `fk_procurement_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_item`
--
ALTER TABLE `procurement_item`
  ADD CONSTRAINT `fk_procurement_item_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_item_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_item_satuan1` FOREIGN KEY (`satuan`) REFERENCES `satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  ADD CONSTRAINT `fk_procurement_retur_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  ADD CONSTRAINT `fk_procurement_return_item_procurement_item1` FOREIGN KEY (`procurement_item`) REFERENCES `procurement_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_return_item_procurement_retur1` FOREIGN KEY (`procurement_retur`) REFERENCES `procurement_retur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_status`
--
ALTER TABLE `procurement_status`
  ADD CONSTRAINT `fk_procurement_status_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_rumah_kategori_rumah1` FOREIGN KEY (`kategori_product`) REFERENCES `kategori_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rumah_tipe_rumah1` FOREIGN KEY (`tipe_product`) REFERENCES `tipe_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  ADD CONSTRAINT `fk_product_distriburst_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_distriburst_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  ADD CONSTRAINT `fk_rumah_has_foto_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  ADD CONSTRAINT `fk_rumah_has_harga_angsuran_ansuran1` FOREIGN KEY (`ansuran`) REFERENCES `ansuran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rumah_has_harga_angsuran_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_grosir`
--
ALTER TABLE `product_has_harga_grosir`
  ADD CONSTRAINT `fk_product_has_harga_grosir_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual`
--
ALTER TABLE `product_has_harga_jual`
  ADD CONSTRAINT `fk_product_has_harga_jual_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  ADD CONSTRAINT `fk_rumah_has_harga_jual_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  ADD CONSTRAINT `fk_rumah_has_harga_jual_kredit_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_kirim`
--
ALTER TABLE `product_kirim`
  ADD CONSTRAINT `fk_product_kirim_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_kirim_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  ADD CONSTRAINT `fk_product_kirim_status_product_kirim1` FOREIGN KEY (`product_kirim`) REFERENCES `product_kirim` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  ADD CONSTRAINT `fk_product_log_stock_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_satuan`
--
ALTER TABLE `product_satuan`
  ADD CONSTRAINT `fk_product_satuan_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_satuan_satuan1` FOREIGN KEY (`satuan`) REFERENCES `satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD CONSTRAINT `fk_product_stock_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_stock_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_stock_minus`
--
ALTER TABLE `product_stock_minus`
  ADD CONSTRAINT `fk_product_stock_minus_product_stock1` FOREIGN KEY (`product_stock`) REFERENCES `product_stock` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  ADD CONSTRAINT `fk_product_surat_jalan_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD CONSTRAINT `fk_product_vendor_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_vendor_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reimburse`
--
ALTER TABLE `reimburse`
  ADD CONSTRAINT `fk_reimburse_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  ADD CONSTRAINT `fk_return_penjualan_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  ADD CONSTRAINT `fk_return_penjualan_item_invoice_product1` FOREIGN KEY (`invoice_product`) REFERENCES `invoice_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_penjualan_item_return_penjualan1` FOREIGN KEY (`return_penjualan`) REFERENCES `return_penjualan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_barang`
--
ALTER TABLE `retur_barang`
  ADD CONSTRAINT `fk_retur_barang_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_barang_item`
--
ALTER TABLE `retur_barang_item`
  ADD CONSTRAINT `fk_retur_barang_item_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retur_barang_item_retur_barang1` FOREIGN KEY (`retur_barang`) REFERENCES `retur_barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retur_barang_item_stok_kategori1` FOREIGN KEY (`stok_kategori`) REFERENCES `stok_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_barang_status`
--
ALTER TABLE `retur_barang_status`
  ADD CONSTRAINT `fk_retur_barang_status_retur_barang1` FOREIGN KEY (`retur_barang`) REFERENCES `retur_barang` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_order`
--
ALTER TABLE `retur_order`
  ADD CONSTRAINT `fk_retur_order_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  ADD CONSTRAINT `fk_retur_order_item_order_product1` FOREIGN KEY (`order_product`) REFERENCES `order_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retur_order_item_retur_order1` FOREIGN KEY (`retur_order`) REFERENCES `retur_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute`
--
ALTER TABLE `sales_rute`
  ADD CONSTRAINT `fk_sales_rute_minggu1` FOREIGN KEY (`minggu`) REFERENCES `minggu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  ADD CONSTRAINT `fk_sales_rute_customer_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_customer_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  ADD CONSTRAINT `fk_sales_rute_hari_hari1` FOREIGN KEY (`hari`) REFERENCES `hari` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_hari_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  ADD CONSTRAINT `fk_sales_rute_user_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_user_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  ADD CONSTRAINT `fk_surat_jalan_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_priveledge` FOREIGN KEY (`priveledge`) REFERENCES `priveledge` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vendor`
--
ALTER TABLE `vendor`
  ADD CONSTRAINT `fk_vendor_vendor_category1` FOREIGN KEY (`vendor_category`) REFERENCES `vendor_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
