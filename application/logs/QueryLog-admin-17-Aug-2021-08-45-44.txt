
----------../modules/database/controllers/order/confirmBayar----------
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `product_log_stock` ( `product_satuan`, `status`, `qty`, `keterangan`, `reference_id`, `createddate`, `createdby`)  
    VALUES ( '1675', 'ORDER', 1, 'Order Pelanggan', '5', '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.00065398216247559, Seconds : 0.65398216247559
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT `ps`.* 
    FROM `product_stock` `ps` 
    JOIN `product_satuan` `pst` ON `pst`.`id` = `ps`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `pst`.`product` 
    JOIN `satuan` `s` ON `s`.`id` = `pst`.`satuan` 
    WHERE `ps`.`deleted` =0 
    AND `p`.`id` = '1677' 
    AND `pst`.`satuan_terkecil` = 1 
    ORDER     BY `ps`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0011987686157227, Seconds : 1.1987686157227
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `product_stock` ( `stock`, `product`, `product_satuan`, `gudang`, `rak`, `createddate`, `createdby`)  
    VALUES ( 6, '1677', '1675', 1, 1, '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.00053095817565918, Seconds : 0.53095817565918
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '5', 'CONFIRM', '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.00041484832763672, Seconds : 0.41484832763672

----------../modules/no_generator/controllers/order/validateBayar----------
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '5' 
    LIMIT 1000
 --> Execution Time: 0.001539945602417, Seconds : 1.539945602417
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT op.* , roi.qty as retur_qty , roi.sub_total as retur_sub_total , ps.harga 
    FROM order_product op 
    JOIN `order` o on o.id = op.`order` 
    LEFT     JOIN retur_order_item roi on roi.order_product = op.id 
    JOIN product_satuan ps on ps.id = op.product_satuan 
    WHERE o.id = '5' 
    LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.00075602531433105, Seconds : 0.75602531433105
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT * 
    FROM `invoice` 
    WHERE `no_faktur` 
    LIKE '%INV21AUG%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00050497055053711, Seconds : 0.50497055053711
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `invoice` ( `no_faktur`, `pembeli`, `tanggal_faktur`, `tanggal_bayar`, `ref`, `potongan`, `metode_bayar`, `pot_faktur`, `total`, `createddate`, `createdby`)  
    VALUES ( 'INV21AUG004', '1', '2021-08-17 08:45:25', '2021-08-17', '5', '3', '1', '0', '24000', '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.0011770725250244, Seconds : 1.1770725250244
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `invoice_product` ( `qty`, `sub_total`, `product_satuan`, `invoice`, `createddate`, `createdby`)  
    VALUES ( 1, 24000, '1675', 4, '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.0035250186920166, Seconds : 3.5250186920166
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '5' 
    LIMIT 1000
 --> Execution Time: 0.0018599033355713, Seconds : 1.8599033355713
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `invoice_status` ( `invoice`, `user`, `status`, `createddate`, `createdby`)  
    VALUES ( 4, '3', 'PAID', '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.00099897384643555, Seconds : 0.99897384643555
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `invoice_sisa` ( `invoice`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 4, '0', '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.0030457973480225, Seconds : 3.0457973480225
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '5', 'VALIDATE', '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.0018548965454102, Seconds : 1.8548965454102
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT * 
    FROM `payment` 
    WHERE `no_faktur_bayar` 
    LIKE '%PAY21AUG%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0018470287322998, Seconds : 1.8470287322998
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `payment` ( `no_faktur_bayar`, `tanggal_faktur`, `tanggal_bayar`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 'PAY21AUG004', '2021-08-17 08:45:44', '2021-08-17', '24000', '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.00032210350036621, Seconds : 0.32210350036621
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    INSERT     INTO `payment_item` ( `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`)  
    VALUES ( 4, 4, '24000', '2021-08-17 08:45:44', '3') 
 --> Execution Time: 0.004472017288208, Seconds : 4.472017288208

----------../modules/template/controllers/order/add----------
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT `p`.* 
    FROM `pembeli` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     AND `p`.`pembeli_kategori` = 2 
    LIMIT 1000
 --> Execution Time: 0.0018458366394043, Seconds : 1.8458366394043
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT * 
    FROM `metode_bayar` 
    LIMIT 1000
 --> Execution Time: 0.00050115585327148, Seconds : 0.50115585327148
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT `p`.* 
    FROM `pajak` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.00074076652526855, Seconds : 0.74076652526855
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT * 
    FROM `potongan` 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0005340576171875, Seconds : 0.5340576171875
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT p.* , ps.stock 
    FROM product p 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  ps_max on ps_max.product = p.id 
    LEFT     JOIN product_stock ps on ps.id = ps_max.id 
    LEFT     JOIN product_satuan pst on ps.product_satuan = pst.id 
    LEFT     JOIN satuan s on s.id = pst.satuan 
    WHERE p.deleted = 0 
    AND pst.satuan_terkecil = 1
 --> Execution Time: 0.025542974472046, Seconds : 25.542974472046
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00052094459533691, Seconds : 0.52094459533691
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0008389949798584, Seconds : 0.8389949798584
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00082612037658691, Seconds : 0.82612037658691
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00098395347595215, Seconds : 0.98395347595215

----------../modules/template/controllers/faktur_pelanggan/detail----------
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT `i`.*, `p`.`nama` as `nama_pembeli`, `ist`.`status`, `o`.`no_order`, `p`.`alamat`, `pt`.`potongan` as `jenis_potongan`, `jp`.`metode`, `pg`.`nama` as `sales` 
    FROM `invoice` `i` 
    JOIN `pembeli` `p` ON `i`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `ist` ON `ist`.`id` = `iss`.`id` 
    LEFT     JOIN `order` `o` ON `o`.`id` = `i`.`ref` 
    LEFT     JOIN `metode_bayar` `jp` ON `jp`.`id` = `i`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `i`.`potongan` 
    LEFT     JOIN `user` `usr` ON `usr`.`id` = `o`.`createdby` 
    LEFT     JOIN `pegawai` `pg` ON `pg`.`id` = `usr`.`pegawai` 
    WHERE `i`.`id` = '4' 
    LIMIT 1000
 --> Execution Time: 0.0012040138244629, Seconds : 1.2040138244629
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `p`.`kode_product`, `b`.`nama_bank`, `b`.`akun`, `b`.`no_rekening`, `s`.`nama_satuan` 
    FROM `invoice_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `bank` `b` ON `b`.`id` = `ip`.`bank` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`invoice` = '4' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.00083613395690918, Seconds : 0.83613395690918
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `invoice_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`invoice_product` = '4' 
    LIMIT 1000
 --> Execution Time: 0.00059795379638672, Seconds : 0.59795379638672
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT * 
    FROM `jenis_pembayaran` 
    LIMIT 1000
 --> Execution Time: 0.0003669261932373, Seconds : 0.3669261932373
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00039505958557129, Seconds : 0.39505958557129
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00086212158203125, Seconds : 0.86212158203125
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00082302093505859, Seconds : 0.82302093505859
admin - Tanggal/Waktu : 17-Aug-2021/08:45:44 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00079917907714844, Seconds : 0.79917907714844
