
----------../modules/database/controllers/satuan/simpan----------
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:39 AM => 
 --> 
    INSERT     INTO `product_satuan` ( `product`, `satuan`, `qty`, `harga`, `harga_beli`, `ket_harga`, `satuan_terkecil`, `createddate`, `createdby`)  
    VALUES ( '3161', '959', '30', '45000', '40000', '-', 0, '2021-08-07 08:56:39', '1') 
 --> Execution Time: 0.0005338191986084, Seconds : 0.5338191986084
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:39 AM => 
 --> 
    INSERT     INTO `product_has_harga_jual` ( `product_satuan`, `harga`, `createddate`, `createdby`)  
    VALUES ( 15, '45000', '2021-08-07 08:56:39', '1') 
 --> Execution Time: 0.00075697898864746, Seconds : 0.75697898864746

----------../modules/template/controllers/satuan/detail----------
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:39 AM => 
 --> 
    SELECT `kr`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `sp`.`nama_satuan` as `satuan_parent`, `hg`.`harga` as `harga_jual_fix`, `hgs`.`harga` `harga_grosir_fix` 
    FROM `product_satuan` `kr` 
    JOIN `product` `p` ON `kr`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `kr`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_grosir 
    GROUP     BY product_satuan)  hgs_max ON `hgs_max`.`product_satuan` = `kr`.`id` 
    LEFT     JOIN `product_has_harga_grosir` `hgs` ON `hgs`.`id` = `hgs_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `kr`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    WHERE `kr`.`id` = '15' 
    LIMIT 1000
 --> Execution Time: 0.0017240047454834, Seconds : 1.7240047454834
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:39 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0022740364074707, Seconds : 2.2740364074707
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:39 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0011508464813232, Seconds : 1.1508464813232
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:39 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.001291036605835, Seconds : 1.291036605835
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:39 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00065112113952637, Seconds : 0.65112113952637
