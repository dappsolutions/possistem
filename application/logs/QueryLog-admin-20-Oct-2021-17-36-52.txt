
----------../modules/template/controllers/satuan/index----------
admin - Tanggal/Waktu : 20-Oct-2021/17:36:52 PM => 
 --> 
    SELECT `k`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `hg`.`harga` as `harga_jual_fix` 
    FROM `product_satuan` `k` 
    JOIN `product` `p` ON `k`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `k`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `k`.`satuan` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    LIMIT 10
 --> Execution Time: 0.0076792240142822, Seconds : 7.6792240142822
admin - Tanggal/Waktu : 20-Oct-2021/17:36:52 PM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `product_satuan` `k` 
    JOIN `product` `p` ON `k`.`product` = `p`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `k`.`satuan` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0
 --> Execution Time: 0.0066771507263184, Seconds : 6.6771507263184
admin - Tanggal/Waktu : 20-Oct-2021/17:36:52 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00052285194396973, Seconds : 0.52285194396973
admin - Tanggal/Waktu : 20-Oct-2021/17:36:52 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.001082181930542, Seconds : 1.082181930542
admin - Tanggal/Waktu : 20-Oct-2021/17:36:52 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0013411045074463, Seconds : 1.3411045074463
admin - Tanggal/Waktu : 20-Oct-2021/17:36:52 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0016069412231445, Seconds : 1.6069412231445
