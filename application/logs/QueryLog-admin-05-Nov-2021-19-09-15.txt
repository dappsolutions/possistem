
----------../modules/database/controllers/order/confirmBayar----------
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `product_log_stock` ( `product_satuan`, `status`, `qty`, `keterangan`, `reference_id`, `createddate`, `createdby`)  
    VALUES ( '1675', 'ORDER', 1, 'Order Pelanggan', '11', '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.0031001567840576, Seconds : 3.1001567840576
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT `ps`.* 
    FROM `product_stock` `ps` 
    JOIN `product_satuan` `pst` ON `pst`.`id` = `ps`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `pst`.`product` 
    JOIN `satuan` `s` ON `s`.`id` = `pst`.`satuan` 
    WHERE `ps`.`deleted` =0 
    AND `p`.`id` = '1677' 
    AND `pst`.`satuan_terkecil` = 1 
    ORDER     BY `ps`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0012929439544678, Seconds : 1.2929439544678
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `product_stock` ( `stock`, `product`, `product_satuan`, `gudang`, `rak`, `createddate`, `createdby`)  
    VALUES ( 5, '1677', '1675', 1, 1, '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.0004420280456543, Seconds : 0.4420280456543
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '11', 'CONFIRM', '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.0012428760528564, Seconds : 1.2428760528564

----------../modules/no_generator/controllers/order/validateBayar----------
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '11' 
    LIMIT 1000
 --> Execution Time: 0.0023529529571533, Seconds : 2.3529529571533
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT op.* , roi.qty as retur_qty , roi.sub_total as retur_sub_total , ps.harga 
    FROM order_product op 
    JOIN `order` o on o.id = op.`order` 
    LEFT     JOIN retur_order_item roi on roi.order_product = op.id 
    JOIN product_satuan ps on ps.id = op.product_satuan 
    WHERE o.id = '11' 
    LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.0023758411407471, Seconds : 2.3758411407471
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT * 
    FROM `invoice` 
    WHERE `no_faktur` 
    LIKE '%INV21NOV%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00057101249694824, Seconds : 0.57101249694824
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `invoice` ( `no_faktur`, `pembeli`, `tanggal_faktur`, `tanggal_bayar`, `ref`, `potongan`, `metode_bayar`, `pot_faktur`, `total`, `createddate`, `createdby`)  
    VALUES ( 'INV21NOV002', '1', '2021-11-05 19:02:13', '2021-11-05', '11', '3', '1', '0', '26000', '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.00094318389892578, Seconds : 0.94318389892578
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `invoice_product` ( `qty`, `sub_total`, `product_satuan`, `invoice`, `createddate`, `createdby`)  
    VALUES ( 1, 26000, '1675', 8, '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.00046586990356445, Seconds : 0.46586990356445
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '11' 
    LIMIT 1000
 --> Execution Time: 0.0006108283996582, Seconds : 0.6108283996582
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `invoice_status` ( `invoice`, `user`, `status`, `createddate`, `createdby`)  
    VALUES ( 8, '3', 'PAID', '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.00047087669372559, Seconds : 0.47087669372559
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `invoice_sisa` ( `invoice`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 8, '0', '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.00032305717468262, Seconds : 0.32305717468262
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '11', 'VALIDATE', '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.00025606155395508, Seconds : 0.25606155395508
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT * 
    FROM `payment` 
    WHERE `no_faktur_bayar` 
    LIKE '%PAY21NOV%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00053000450134277, Seconds : 0.53000450134277
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `payment` ( `no_faktur_bayar`, `tanggal_faktur`, `tanggal_bayar`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 'PAY21NOV002', '2021-11-05 19:09:15', '2021-11-05', '27000', '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.00029897689819336, Seconds : 0.29897689819336
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    INSERT     INTO `payment_item` ( `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`)  
    VALUES ( 8, 8, '27000', '2021-11-05 19:09:15', '3') 
 --> Execution Time: 0.0003960132598877, Seconds : 0.3960132598877

----------../modules/template/controllers/order/add----------
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT `p`.* 
    FROM `pembeli` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     AND `p`.`pembeli_kategori` = 2 
    LIMIT 1000
 --> Execution Time: 0.0019030570983887, Seconds : 1.9030570983887
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT * 
    FROM `metode_bayar` 
    LIMIT 1000
 --> Execution Time: 0.0003509521484375, Seconds : 0.3509521484375
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT `p`.* 
    FROM `pajak` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.0004420280456543, Seconds : 0.4420280456543
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT * 
    FROM `potongan` 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00041317939758301, Seconds : 0.41317939758301
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT p.* , ps.stock 
    FROM product p 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  ps_max on ps_max.product = p.id 
    LEFT     JOIN product_stock ps on ps.id = ps_max.id 
    LEFT     JOIN product_satuan pst on ps.product_satuan = pst.id 
    LEFT     JOIN satuan s on s.id = pst.satuan 
    WHERE p.deleted = 0 
    AND pst.satuan_terkecil = 1 
    AND pst.deleted = 0
 --> Execution Time: 0.039451837539673, Seconds : 39.451837539673
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0026819705963135, Seconds : 2.6819705963135
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00089693069458008, Seconds : 0.89693069458008
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00095891952514648, Seconds : 0.95891952514648
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00073385238647461, Seconds : 0.73385238647461

----------../modules/template/controllers/faktur_pelanggan/detail----------
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT `i`.*, `p`.`nama` as `nama_pembeli`, `ist`.`status`, `o`.`no_order`, `p`.`alamat`, `pt`.`potongan` as `jenis_potongan`, `jp`.`metode`, `pg`.`nama` as `sales` 
    FROM `invoice` `i` 
    JOIN `pembeli` `p` ON `i`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `ist` ON `ist`.`id` = `iss`.`id` 
    LEFT     JOIN `order` `o` ON `o`.`id` = `i`.`ref` 
    LEFT     JOIN `metode_bayar` `jp` ON `jp`.`id` = `i`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `i`.`potongan` 
    LEFT     JOIN `user` `usr` ON `usr`.`id` = `o`.`createdby` 
    LEFT     JOIN `pegawai` `pg` ON `pg`.`id` = `usr`.`pegawai` 
    WHERE `i`.`id` = '8' 
    LIMIT 1000
 --> Execution Time: 0.0040631294250488, Seconds : 4.0631294250488
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `p`.`kode_product`, `b`.`nama_bank`, `b`.`akun`, `b`.`no_rekening`, `s`.`nama_satuan` 
    FROM `invoice_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `bank` `b` ON `b`.`id` = `ip`.`bank` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`invoice` = '8' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.0028510093688965, Seconds : 2.8510093688965
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `invoice_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`invoice_product` = '8' 
    LIMIT 1000
 --> Execution Time: 0.0021090507507324, Seconds : 2.1090507507324
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT * 
    FROM `jenis_pembayaran` 
    LIMIT 1000
 --> Execution Time: 0.0018808841705322, Seconds : 1.8808841705322
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.001676082611084, Seconds : 1.676082611084
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00084090232849121, Seconds : 0.84090232849121
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0011768341064453, Seconds : 1.1768341064453
admin - Tanggal/Waktu : 05-Nov-2021/19:09:15 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0007941722869873, Seconds : 0.7941722869873
