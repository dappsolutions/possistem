
----------../modules/database/controllers/satuan/delete----------
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:08 AM => 
 --> 
    UPDATE `product_satuan` 
    SET `deleted` = 1, `updateddate` = '2021-08-07 08:56:08', `updatedby` = '1' 
    WHERE `id` = '12'
 --> Execution Time: 0.00083303451538086, Seconds : 0.83303451538086

----------../modules/template/controllers/satuan/index----------
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:08 AM => 
 --> 
    SELECT `k`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `hg`.`harga` as `harga_jual_fix` 
    FROM `product_satuan` `k` 
    JOIN `product` `p` ON `k`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `k`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `k`.`satuan` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0 
    LIMIT 10
 --> Execution Time: 0.0024309158325195, Seconds : 2.4309158325195
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:08 AM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `product_satuan` `k` 
    JOIN `product` `p` ON `k`.`product` = `p`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `k`.`satuan` 
    WHERE `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0
 --> Execution Time: 0.0006110668182373, Seconds : 0.6110668182373
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:08 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00038790702819824, Seconds : 0.38790702819824
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:08 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010230541229248, Seconds : 1.0230541229248
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:08 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.001539945602417, Seconds : 1.539945602417
administrator - Tanggal/Waktu : 07-Aug-2021/08:56:08 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00076413154602051, Seconds : 0.76413154602051
