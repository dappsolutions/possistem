
----------../modules/template/controllers/faktur_pelanggan/detail----------
admin - Tanggal/Waktu : 30-Sep-2021/18:11:20 PM => 
 --> 
    SELECT `i`.*, `p`.`nama` as `nama_pembeli`, `ist`.`status`, `o`.`no_order`, `p`.`alamat`, `pt`.`potongan` as `jenis_potongan`, `jp`.`metode`, `pg`.`nama` as `sales` 
    FROM `invoice` `i` 
    JOIN `pembeli` `p` ON `i`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss ON `iss`.`invoice` = `i`.`id` 
    JOIN `invoice_status` `ist` ON `ist`.`id` = `iss`.`id` 
    LEFT     JOIN `order` `o` ON `o`.`id` = `i`.`ref` 
    LEFT     JOIN `metode_bayar` `jp` ON `jp`.`id` = `i`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `i`.`potongan` 
    LEFT     JOIN `user` `usr` ON `usr`.`id` = `o`.`createdby` 
    LEFT     JOIN `pegawai` `pg` ON `pg`.`id` = `usr`.`pegawai` 
    WHERE `i`.`id` = '5' 
    LIMIT 1000
 --> Execution Time: 0.0017468929290771, Seconds : 1.7468929290771
admin - Tanggal/Waktu : 30-Sep-2021/18:11:20 PM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `p`.`kode_product`, `b`.`nama_bank`, `b`.`akun`, `b`.`no_rekening`, `s`.`nama_satuan` 
    FROM `invoice_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `bank` `b` ON `b`.`id` = `ip`.`bank` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`invoice` = '5' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.0037341117858887, Seconds : 3.7341117858887
admin - Tanggal/Waktu : 30-Sep-2021/18:11:20 PM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `invoice_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`invoice_product` = '5' 
    LIMIT 1000
 --> Execution Time: 0.00064802169799805, Seconds : 0.64802169799805
admin - Tanggal/Waktu : 30-Sep-2021/18:11:20 PM => 
 --> 
    SELECT * 
    FROM `jenis_pembayaran` 
    LIMIT 1000
 --> Execution Time: 0.00042486190795898, Seconds : 0.42486190795898
admin - Tanggal/Waktu : 30-Sep-2021/18:11:20 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00046920776367188, Seconds : 0.46920776367188
admin - Tanggal/Waktu : 30-Sep-2021/18:11:20 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0015220642089844, Seconds : 1.5220642089844
admin - Tanggal/Waktu : 30-Sep-2021/18:11:20 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0012340545654297, Seconds : 1.2340545654297
admin - Tanggal/Waktu : 30-Sep-2021/18:11:20 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00085616111755371, Seconds : 0.85616111755371
