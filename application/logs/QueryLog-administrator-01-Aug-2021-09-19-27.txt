
----------../modules/template/controllers/lappenjualan/index----------
administrator - Tanggal/Waktu : 01-Aug-2021/09:19:27 AM => 
 --> 
    SELECT i.no_faktur , ipt.createddate as tgl_cetak , i.tanggal_bayar as tgl_jatuh_tempo , pb.nama as customer , i.tanggal_faktur , pb.alamat , 'RETAIL' as jenis_customer , o.no_order , pg.nama as nama_sales , gd.nama_gudang , p.kode_product , p.product as nama_product , kp.kategori , tp.tipe as tipe_product , vd.nama_vendor , ip.qty as total_qty , st.nama_satuan as current_satuan , ps.qty as total_current_qty , ip.sub_total , ipp.nilai total_potongan , pt.potongan as jenis_potongan , 'Penjualan' as jenis_transaksi , ps.harga as harga_jual 
    FROM invoice i 
    JOIN invoice_product ip on ip.invoice = i.id 
    LEFT     JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_print 
    GROUP     BY invoice)  iprt on iprt.invoice = i.id 
    LEFT     JOIN invoice_print ipt on ipt.id = iprt.id 
    JOIN pembeli pb on pb.id = i.pembeli 
    LEFT     JOIN `order` o on i.`ref` = o.id 
    LEFT     JOIN `user` us on us.id = o.createdby 
    LEFT     JOIN pegawai pg on pg.id = us.pegawai 
    JOIN product_satuan ps on ps.id = ip.product_satuan 
    JOIN product_stock psst on psst.product_satuan = ps.id 
    JOIN product p on p.id = ps.product 
    JOIN gudang gd on gd.id = psst.gudang 
    JOIN kategori_product kp on kp.id = p.kategori_product 
    JOIN tipe_product tp on tp.id = p.tipe_product 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM procurement_item 
    GROUP     BY product)  proc_item on proc_item.product = p.id 
    LEFT     JOIN procurement_item pit on pit.id = proc_item.id 
    LEFT     JOIN procurement pr on pr.id = pit.procurement 
    LEFT     JOIN vendor vd on vd.id = pr.vendor 
    JOIN satuan st on ps.satuan = st.id 
    LEFT     JOIN ( 
    SELECT max( id)  id, invoice_product 
    FROM invoice_pot_product 
    GROUP     BY invoice_product)  ippt on ippt.invoice_product = ip.id 
    LEFT     JOIN invoice_pot_product ipp on ipp.id = ippt.id 
    LEFT     JOIN potongan pt on pt.id = ipp.potongan 
    WHERE i.deleted = 0 
    LIMIT 1000 
    OFFSET 0
 --> Execution Time: 4.9083790779114, Seconds : 4908.3790779114
administrator - Tanggal/Waktu : 01-Aug-2021/09:19:27 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00097918510437012, Seconds : 0.97918510437012
administrator - Tanggal/Waktu : 01-Aug-2021/09:19:27 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0013611316680908, Seconds : 1.3611316680908
administrator - Tanggal/Waktu : 01-Aug-2021/09:19:27 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0013060569763184, Seconds : 1.3060569763184
administrator - Tanggal/Waktu : 01-Aug-2021/09:19:27 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00069093704223633, Seconds : 0.69093704223633
