
----------../modules/database/controllers/satuan/simpan----------
admin - Tanggal/Waktu : 20-Oct-2021/17:37:58 PM => 
 --> 
    UPDATE `product_satuan` 
    SET `product` = '1677', `satuan` = '1', `qty` = '1', `harga` = '26000', `harga_beli` = '0', `ket_harga` = '', `satuan_terkecil` = 1, `updateddate` = '2021-10-20 17:37:58', `updatedby` = '3' 
    WHERE `id` = '1675'
 --> Execution Time: 0.00062108039855957, Seconds : 0.62108039855957

----------../modules/template/controllers/satuan/detail----------
admin - Tanggal/Waktu : 20-Oct-2021/17:37:58 PM => 
 --> 
    SELECT `kr`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `sp`.`nama_satuan` as `satuan_parent`, `hg`.`harga` as `harga_jual_fix`, `hgs`.`harga` `harga_grosir_fix` 
    FROM `product_satuan` `kr` 
    JOIN `product` `p` ON `kr`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `kr`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_grosir 
    GROUP     BY product_satuan)  hgs_max ON `hgs_max`.`product_satuan` = `kr`.`id` 
    LEFT     JOIN `product_has_harga_grosir` `hgs` ON `hgs`.`id` = `hgs_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `kr`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    WHERE `kr`.`id` = '1675' 
    LIMIT 1000
 --> Execution Time: 0.003399133682251, Seconds : 3.399133682251
admin - Tanggal/Waktu : 20-Oct-2021/17:37:58 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00049901008605957, Seconds : 0.49901008605957
admin - Tanggal/Waktu : 20-Oct-2021/17:37:58 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00090813636779785, Seconds : 0.90813636779785
admin - Tanggal/Waktu : 20-Oct-2021/17:37:58 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00092101097106934, Seconds : 0.92101097106934
admin - Tanggal/Waktu : 20-Oct-2021/17:37:58 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00070095062255859, Seconds : 0.70095062255859
