
----------../modules/template/controllers/product_stock/search----------
admin - Tanggal/Waktu : 01-Nov-2021/19:58:08 PM => 
 --> 
    SELECT `k`.*, `p`.`product` as `nama_product`, `ps`.`satuan`, `g`.`nama_gudang`, `r`.`nama_rak`, `s`.`nama_satuan`, `p`.`kode_product` 
    FROM `product` `p` 
    JOIN `product_satuan` `ps` ON `ps`.`product` = `p`.`id` 
    AND `ps`.`deleted` = 0 
    JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  k_max ON `k_max`.`product` = `p`.`id` 
    JOIN `product_stock` `k` ON `k`.`id` = `k_max`.`id` 
    LEFT     JOIN `gudang` `g` ON `k`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `k`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    WHERE ( `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0)  
    AND `ps`.`satuan_terkecil` = '1' 
    AND `p`.`kode_product` 
    LIKE '%pertamax%' 
    ESCAPE '!' 
    OR `p`.`product` 
    LIKE '%pertamax%' 
    ESCAPE '!' 
    OR `k`.`stock` 
    LIKE '%pertamax%' 
    ESCAPE '!' 
    OR `s`.`nama_satuan` 
    LIKE '%pertamax%' 
    ESCAPE '!' 
    LIMIT 10
 --> Execution Time: 0.02167797088623, Seconds : 21.67797088623
admin - Tanggal/Waktu : 01-Nov-2021/19:58:08 PM => 
 --> 
    SELECT COUNT( *)  AS `numrows` 
    FROM `product` `p` 
    JOIN `product_satuan` `ps` ON `ps`.`product` = `p`.`id` 
    AND `ps`.`deleted` = 0 
    JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  k_max ON `k_max`.`product` = `p`.`id` 
    JOIN `product_stock` `k` ON `k`.`id` = `k_max`.`id` 
    LEFT     JOIN `gudang` `g` ON `k`.`gudang` = `g`.`id` 
    LEFT     JOIN `rak` `r` ON `k`.`rak` = `r`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    WHERE ( `k`.`deleted` 
    IS     NULL     OR `k`.`deleted` =0)  
    AND `ps`.`satuan_terkecil` = '1' 
    AND `p`.`product` 
    LIKE '%pertamax%' 
    ESCAPE '!' 
    OR `p`.`kode_product` 
    LIKE '%pertamax%' 
    ESCAPE '!' 
    OR `k`.`stock` 
    LIKE '%pertamax%' 
    ESCAPE '!' 
    OR `s`.`nama_satuan` 
    LIKE '%pertamax%' 
    ESCAPE '!'
 --> Execution Time: 0.024775981903076, Seconds : 24.775981903076
admin - Tanggal/Waktu : 01-Nov-2021/19:58:08 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00043797492980957, Seconds : 0.43797492980957
admin - Tanggal/Waktu : 01-Nov-2021/19:58:08 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00084090232849121, Seconds : 0.84090232849121
admin - Tanggal/Waktu : 01-Nov-2021/19:58:08 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00098490715026855, Seconds : 0.98490715026855
admin - Tanggal/Waktu : 01-Nov-2021/19:58:08 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0018329620361328, Seconds : 1.8329620361328
