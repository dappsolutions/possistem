
----------../modules/database/controllers/order/confirmBayar----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `product_log_stock` ( `product_satuan`, `status`, `qty`, `keterangan`, `reference_id`, `createddate`, `createdby`)  
    VALUES ( '6', 'ORDER', 1, 'Order Pelanggan', '24', '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00054311752319336, Seconds : 0.54311752319336
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    SELECT `ps`.* 
    FROM `product_stock` `ps` 
    JOIN `product_satuan` `pst` ON `pst`.`id` = `ps`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `pst`.`product` 
    JOIN `satuan` `s` ON `s`.`id` = `pst`.`satuan` 
    WHERE `ps`.`deleted` =0 
    AND `p`.`id` = '3159' 
    AND `s`.`nama_satuan` = 'PCS' 
    ORDER     BY `ps`.`id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0010790824890137, Seconds : 1.0790824890137
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `product_stock` ( `stock`, `product`, `product_satuan`, `gudang`, `rak`, `createddate`, `createdby`)  
    VALUES ( 95, '3159', '6', 1, 1, '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.0005030632019043, Seconds : 0.5030632019043
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '24', 'CONFIRM', '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00031495094299316, Seconds : 0.31495094299316

----------../modules/no_generator/controllers/order/validateBayar----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '24' 
    LIMIT 1000
 --> Execution Time: 0.0016238689422607, Seconds : 1.6238689422607
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    SELECT op.* , roi.qty as retur_qty , roi.sub_total as retur_sub_total , ps.harga 
    FROM order_product op 
    JOIN `order` o on o.id = op.`order` 
    LEFT     JOIN retur_order_item roi on roi.order_product = op.id 
    JOIN product_satuan ps on ps.id = op.product_satuan 
    WHERE o.id = '24' 
    LIMIT 1000 
    OFFSET 0
 --> Execution Time: 0.00098109245300293, Seconds : 0.98109245300293
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    SELECT * 
    FROM `invoice` 
    WHERE `no_faktur` 
    LIKE '%INV21AUG%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.0004880428314209, Seconds : 0.4880428314209
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `invoice` ( `no_faktur`, `pembeli`, `tanggal_faktur`, `tanggal_bayar`, `ref`, `potongan`, `metode_bayar`, `pot_faktur`, `total`, `createddate`, `createdby`)  
    VALUES ( 'INV21AUG001', '380', '2021-08-01 10:09:32', '2021-08-01', '24', '3', '1', '0', '5000', '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00037002563476562, Seconds : 0.37002563476562
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `invoice_product` ( `qty`, `sub_total`, `product_satuan`, `invoice`, `createddate`, `createdby`)  
    VALUES ( 1, 5000, '6', 27, '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00027704238891602, Seconds : 0.27704238891602
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '26' 
    LIMIT 1000
 --> Execution Time: 0.00062298774719238, Seconds : 0.62298774719238
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `invoice_status` ( `invoice`, `user`, `status`, `createddate`, `createdby`)  
    VALUES ( 27, '1', 'PAID', '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00039196014404297, Seconds : 0.39196014404297
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `invoice_sisa` ( `invoice`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 27, '0', '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00040102005004883, Seconds : 0.40102005004883
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `order_status` ( `order`, `status`, `createddate`, `createdby`)  
    VALUES ( '24', 'VALIDATE', '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00023293495178223, Seconds : 0.23293495178223
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    SELECT * 
    FROM `payment` 
    WHERE `no_faktur_bayar` 
    LIKE '%PAY21AUG%' 
    ESCAPE '!' 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00054597854614258, Seconds : 0.54597854614258
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `payment` ( `no_faktur_bayar`, `tanggal_faktur`, `tanggal_bayar`, `jumlah`, `createddate`, `createdby`)  
    VALUES ( 'PAY21AUG001', '2021-08-01 10:10:04', '2021-08-01', '5000', '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00032305717468262, Seconds : 0.32305717468262
administrator - Tanggal/Waktu : 01-Aug-2021/10:10:04 AM => 
 --> 
    INSERT     INTO `payment_item` ( `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`)  
    VALUES ( 71, 27, '5000', '2021-08-01 10:10:04', '1') 
 --> Execution Time: 0.00040507316589355, Seconds : 0.40507316589355
