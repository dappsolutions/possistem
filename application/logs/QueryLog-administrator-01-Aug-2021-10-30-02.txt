
----------../modules/template/controllers/order/detail----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '28' 
    LIMIT 1000
 --> Execution Time: 0.0024018287658691, Seconds : 2.4018287658691
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `ps`.`qty` as `konversi`, `p`.`id` as `product_id` 
    FROM `order_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`order` = '28' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.0018620491027832, Seconds : 1.8620491027832
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '30' 
    LIMIT 1000
 --> Execution Time: 0.00061202049255371, Seconds : 0.61202049255371
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT `ps`.*, `pst`.`stock`, `s`.`nama_satuan` as `current_satuan`, `sp`.`nama_satuan` as `parent_satuan`, `psp`.`id` as `parent_product_satuan`, `psp`.`qty` as `qty_parent`, `pstp`.`stock` as `parent_stock`, `pst`.`id` as `cur_product_stock`, `pstp`.`id` as `parent_product_stock`, `pdc`.`product` as `nama_product` 
    FROM `product_satuan` `ps` 
    JOIN `product_stock` `pst` ON `pst`.`product_satuan` = `ps`.`id` 
    JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    LEFT     JOIN `product_satuan` `psp` ON `psp`.`satuan` = `sp`.`id` 
    AND `psp`.`product` = `ps`.`product` 
    LEFT     JOIN `product_stock` `pstp` ON `pstp`.`product_satuan` = `psp`.`id` 
    JOIN `product` `pdc` ON `pdc`.`id` = `ps`.`product` 
    WHERE `ps`.`deleted` 
    IS     NULL     OR `ps`.`deleted` =0 
    AND `ps`.`id` = '9' 
    LIMIT 1000
 --> Execution Time: 0.0024509429931641, Seconds : 2.4509429931641
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00044703483581543, Seconds : 0.44703483581543
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.001032829284668, Seconds : 1.032829284668
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0015041828155518, Seconds : 1.5041828155518
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00071406364440918, Seconds : 0.71406364440918

----------../modules/template/controllers/order/add----------
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT `ps`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `pst`.`stock`, `p`.`kode_product` 
    FROM `product_satuan` `ps` 
    JOIN `product` `p` ON `ps`.`product` = `p`.`id` 
    LEFT     JOIN `satuan` `s` ON `ps`.`satuan` = `s`.`id` 
    JOIN `product_stock` `pst` ON `p`.`id` = `pst`.`product` 
    WHERE `ps`.`deleted` =0 
    OR `ps`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.0025629997253418, Seconds : 2.5629997253418
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT `p`.* 
    FROM `pembeli` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     AND `p`.`pembeli_kategori` = 2 
    LIMIT 1000
 --> Execution Time: 0.00061297416687012, Seconds : 0.61297416687012
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT * 
    FROM `metode_bayar` 
    LIMIT 1000
 --> Execution Time: 0.00034904479980469, Seconds : 0.34904479980469
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT `p`.* 
    FROM `pajak` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.00041913986206055, Seconds : 0.41913986206055
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT * 
    FROM `potongan` 
    ORDER     BY `id` 
    DESC     LIMIT 1000
 --> Execution Time: 0.00039100646972656, Seconds : 0.39100646972656
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT p.* , ps.stock 
    FROM product p 
    LEFT     JOIN ( 
    SELECT max( id)  id, product 
    FROM product_stock 
    GROUP     BY product)  ps_max on ps_max.product = p.id 
    LEFT     JOIN product_stock ps on ps.id = ps_max.id 
    LEFT     JOIN product_satuan pst on ps.product_satuan = pst.id 
    LEFT     JOIN satuan s on s.id = pst.satuan 
    WHERE p.deleted = 0 
    AND s.nama_satuan = 'PCS'
 --> Execution Time: 0.00092911720275879, Seconds : 0.92911720275879
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00037407875061035, Seconds : 0.37407875061035
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00099682807922363, Seconds : 0.99682807922363
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0012550354003906, Seconds : 1.2550354003906
administrator - Tanggal/Waktu : 01-Aug-2021/10:30:02 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00070309638977051, Seconds : 0.70309638977051
