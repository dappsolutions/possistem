
----------../modules/template/controllers/order/detail----------
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT `o`.*, `p`.`nama` as `nama_pembeli`, `isa`.`status`, `pt`.`potongan` as `jenis_potongan`, `mb`.`metode` 
    FROM `order` `o` 
    JOIN `pembeli` `p` ON `o`.`pembeli` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  iss ON `iss`.`order` = `o`.`id` 
    JOIN `order_status` `isa` ON `isa`.`id` = `iss`.`id` 
    JOIN `metode_bayar` `mb` ON `mb`.`id` = `o`.`metode_bayar` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `o`.`potongan` 
    WHERE `o`.`id` = '1' 
    LIMIT 1000
 --> Execution Time: 0.0010278224945068, Seconds : 1.0278224945068
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT `ip`.*, `ps`.`satuan`, `ps`.`harga`, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `ps`.`qty` as `konversi`, `p`.`id` as `product_id` 
    FROM `order_product` `ip` 
    JOIN `product_satuan` `ps` ON `ps`.`id` = `ip`.`product_satuan` 
    JOIN `product` `p` ON `p`.`id` = `ps`.`product` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    WHERE `ip`.`order` = '1' 
    AND `ip`.`deleted` =0 
    ORDER     BY `ip`.`id` 
    LIMIT 1000
 --> Execution Time: 0.00074601173400879, Seconds : 0.74601173400879
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT `ipp`.*, `pt`.`potongan` as `jenis_potongan` 
    FROM `order_pot_product` `ipp` 
    LEFT     JOIN `potongan` `pt` ON `pt`.`id` = `ipp`.`potongan` 
    WHERE `ipp`.`deleted` =0 
    AND `ipp`.`order_product` = '1' 
    LIMIT 1000
 --> Execution Time: 0.0006101131439209, Seconds : 0.6101131439209
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT `ps`.*, `pst`.`stock`, `s`.`nama_satuan` as `current_satuan`, `sp`.`nama_satuan` as `parent_satuan`, `psp`.`id` as `parent_product_satuan`, `psp`.`qty` as `qty_parent`, `pstp`.`stock` as `parent_stock`, `pst`.`id` as `cur_product_stock`, `pstp`.`id` as `parent_product_stock`, `pdc`.`product` as `nama_product` 
    FROM `product_satuan` `ps` 
    JOIN `product_stock` `pst` ON `pst`.`product_satuan` = `ps`.`id` 
    JOIN `satuan` `s` ON `s`.`id` = `ps`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    LEFT     JOIN `product_satuan` `psp` ON `psp`.`satuan` = `sp`.`id` 
    AND `psp`.`product` = `ps`.`product` 
    LEFT     JOIN `product_stock` `pstp` ON `pstp`.`product_satuan` = `psp`.`id` 
    JOIN `product` `pdc` ON `pdc`.`id` = `ps`.`product` 
    WHERE `ps`.`deleted` 
    IS     NULL     OR `ps`.`deleted` =0 
    AND `ps`.`id` = '963' 
    LIMIT 1000
 --> Execution Time: 0.0021018981933594, Seconds : 2.1018981933594
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT ps.* , s.nama_satuan , ps.qty as konversi , ps.ket_harga as keterangan 
    FROM product_satuan ps 
    JOIN satuan s on s.id = ps.satuan 
    WHERE ps.product = 966 
    AND ps.deleted = 0 
    AND ps.satuan_terkecil = 1
 --> Execution Time: 0.00061798095703125, Seconds : 0.61798095703125
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00044989585876465, Seconds : 0.44989585876465
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00085616111755371, Seconds : 0.85616111755371
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0008540153503418, Seconds : 0.8540153503418
admin - Tanggal/Waktu : 08-Aug-2021/15:59:40 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010480880737305, Seconds : 1.0480880737305
