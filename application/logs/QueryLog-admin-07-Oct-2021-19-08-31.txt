
----------../modules/database/controllers/satuan/execUbahHargaJual----------
admin - Tanggal/Waktu : 07-Oct-2021/19:08:31 PM => 
 --> 
    INSERT     INTO `product_has_harga_jual` ( `product_satuan`, `harga`, `createddate`, `createdby`)  
    VALUES ( '1675', '25000', '2021-10-07 19:08:31', '3') 
 --> Execution Time: 0.00099515914916992, Seconds : 0.99515914916992

----------../modules/template/controllers/satuan/ubah----------
admin - Tanggal/Waktu : 07-Oct-2021/19:08:31 PM => 
 --> 
    SELECT `kr`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `sp`.`nama_satuan` as `satuan_parent`, `hg`.`harga` as `harga_jual_fix`, `hgs`.`harga` `harga_grosir_fix` 
    FROM `product_satuan` `kr` 
    JOIN `product` `p` ON `kr`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `kr`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_grosir 
    GROUP     BY product_satuan)  hgs_max ON `hgs_max`.`product_satuan` = `kr`.`id` 
    LEFT     JOIN `product_has_harga_grosir` `hgs` ON `hgs`.`id` = `hgs_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `kr`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    WHERE `kr`.`id` = '1675' 
    LIMIT 1000
 --> Execution Time: 0.0035171508789062, Seconds : 3.5171508789062
admin - Tanggal/Waktu : 07-Oct-2021/19:08:31 PM => 
 --> 
    SELECT `p`.* 
    FROM `product` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.00478196144104, Seconds : 4.78196144104
admin - Tanggal/Waktu : 07-Oct-2021/19:08:31 PM => 
 --> 
    SELECT `sa`.*, `s`.`nama_satuan` as `satuan_parent` 
    FROM `satuan` `sa` 
    LEFT     JOIN `satuan` `s` ON `sa`.`parent` = `s`.`id` 
    WHERE `sa`.`deleted` =0 
    LIMIT 1000
 --> Execution Time: 0.00078296661376953, Seconds : 0.78296661376953
admin - Tanggal/Waktu : 07-Oct-2021/19:08:31 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.00050592422485352, Seconds : 0.50592422485352
admin - Tanggal/Waktu : 07-Oct-2021/19:08:31 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00092601776123047, Seconds : 0.92601776123047
admin - Tanggal/Waktu : 07-Oct-2021/19:08:31 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010390281677246, Seconds : 1.0390281677246
admin - Tanggal/Waktu : 07-Oct-2021/19:08:31 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00079202651977539, Seconds : 0.79202651977539
