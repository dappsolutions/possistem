
----------../modules/database/controllers/satuan/execUbahHargaJual----------
admin - Tanggal/Waktu : 07-Oct-2021/19:10:06 PM => 
 --> 
    INSERT     INTO `product_has_harga_jual` ( `product_satuan`, `harga`, `createddate`, `createdby`)  
    VALUES ( '1675', '26000', '2021-10-07 19:10:06', '3') 
 --> Execution Time: 0.00041604042053223, Seconds : 0.41604042053223

----------../modules/template/controllers/satuan/ubah----------
admin - Tanggal/Waktu : 07-Oct-2021/19:10:06 PM => 
 --> 
    SELECT `kr`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `sp`.`nama_satuan` as `satuan_parent`, `hg`.`harga` as `harga_jual_fix`, `hgs`.`harga` `harga_grosir_fix` 
    FROM `product_satuan` `kr` 
    JOIN `product` `p` ON `kr`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `kr`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_grosir 
    GROUP     BY product_satuan)  hgs_max ON `hgs_max`.`product_satuan` = `kr`.`id` 
    LEFT     JOIN `product_has_harga_grosir` `hgs` ON `hgs`.`id` = `hgs_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `kr`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    WHERE `kr`.`id` = '1675' 
    LIMIT 1000
 --> Execution Time: 0.0032720565795898, Seconds : 3.2720565795898
admin - Tanggal/Waktu : 07-Oct-2021/19:10:06 PM => 
 --> 
    SELECT `p`.* 
    FROM `product` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.0046231746673584, Seconds : 4.6231746673584
admin - Tanggal/Waktu : 07-Oct-2021/19:10:06 PM => 
 --> 
    SELECT `sa`.*, `s`.`nama_satuan` as `satuan_parent` 
    FROM `satuan` `sa` 
    LEFT     JOIN `satuan` `s` ON `sa`.`parent` = `s`.`id` 
    WHERE `sa`.`deleted` =0 
    LIMIT 1000
 --> Execution Time: 0.0013940334320068, Seconds : 1.3940334320068
admin - Tanggal/Waktu : 07-Oct-2021/19:10:06 PM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0022568702697754, Seconds : 2.2568702697754
admin - Tanggal/Waktu : 07-Oct-2021/19:10:06 PM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00097298622131348, Seconds : 0.97298622131348
admin - Tanggal/Waktu : 07-Oct-2021/19:10:06 PM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0011000633239746, Seconds : 1.1000633239746
admin - Tanggal/Waktu : 07-Oct-2021/19:10:06 PM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00080084800720215, Seconds : 0.80084800720215
