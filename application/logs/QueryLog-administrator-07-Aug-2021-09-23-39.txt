
----------../modules/template/controllers/satuan/ubah----------
administrator - Tanggal/Waktu : 07-Aug-2021/09:23:39 AM => 
 --> 
    SELECT `kr`.*, `p`.`product` as `nama_product`, `s`.`nama_satuan`, `sp`.`nama_satuan` as `satuan_parent`, `hg`.`harga` as `harga_jual_fix`, `hgs`.`harga` `harga_grosir_fix` 
    FROM `product_satuan` `kr` 
    JOIN `product` `p` ON `kr`.`product` = `p`.`id` 
    JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_jual 
    GROUP     BY product_satuan)  hg_max ON `hg_max`.`product_satuan` = `kr`.`id` 
    JOIN `product_has_harga_jual` `hg` ON `hg`.`id` = `hg_max`.`id` 
    LEFT     JOIN ( 
    SELECT max( id)  id, product_satuan 
    FROM product_has_harga_grosir 
    GROUP     BY product_satuan)  hgs_max ON `hgs_max`.`product_satuan` = `kr`.`id` 
    LEFT     JOIN `product_has_harga_grosir` `hgs` ON `hgs`.`id` = `hgs_max`.`id` 
    LEFT     JOIN `satuan` `s` ON `s`.`id` = `kr`.`satuan` 
    LEFT     JOIN `satuan` `sp` ON `sp`.`id` = `s`.`parent` 
    WHERE `kr`.`id` = '11' 
    LIMIT 1000
 --> Execution Time: 0.0014040470123291, Seconds : 1.4040470123291
administrator - Tanggal/Waktu : 07-Aug-2021/09:23:39 AM => 
 --> 
    SELECT `p`.* 
    FROM `product` `p` 
    WHERE `p`.`deleted` =0 
    OR `p`.`deleted` 
    IS     NULL     LIMIT 1000
 --> Execution Time: 0.00050687789916992, Seconds : 0.50687789916992
administrator - Tanggal/Waktu : 07-Aug-2021/09:23:39 AM => 
 --> 
    SELECT `sa`.*, `s`.`nama_satuan` as `satuan_parent` 
    FROM `satuan` `sa` 
    LEFT     JOIN `satuan` `s` ON `sa`.`parent` = `s`.`id` 
    WHERE `sa`.`deleted` =0 
    LIMIT 1000
 --> Execution Time: 0.00051999092102051, Seconds : 0.51999092102051
administrator - Tanggal/Waktu : 07-Aug-2021/09:23:39 AM => 
 --> 
    SELECT * 
    FROM `general` 
    LIMIT 1000
 --> Execution Time: 0.0003960132598877, Seconds : 0.3960132598877
administrator - Tanggal/Waktu : 07-Aug-2021/09:23:39 AM => 
 --> 
    SELECT i.* 
    FROM invoice i 
    JOIN ( 
    SELECT max( id)  id, invoice 
    FROM invoice_status 
    GROUP     BY invoice)  iss on iss.invoice = i.id 
    JOIN invoice_status isa on isa.id = iss.id 
    WHERE isa.status = 'DRAFT' 
    AND i.deleted = 0 
    ORDER     BY i.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0010731220245361, Seconds : 1.0731220245361
administrator - Tanggal/Waktu : 07-Aug-2021/09:23:39 AM => 
 --> 
    SELECT o.* 
    FROM `order` o 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE os.status = 'DRAFT' 
    AND o.deleted = 0 
    ORDER     BY o.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.0013370513916016, Seconds : 1.3370513916016
administrator - Tanggal/Waktu : 07-Aug-2021/09:23:39 AM => 
 --> 
    SELECT ro.* 
    FROM retur_order ro 
    JOIN `order` o on o.id = ro.id 
    JOIN ( 
    SELECT max( id)  id, `order` 
    FROM order_status 
    GROUP     BY `order`)  oss on oss.order = o.id 
    JOIN order_status os on os.id = oss.id 
    WHERE ro.deleted = 0 
    AND os.status = 'DRAFT' 
    AND ro.deleted = 0 
    ORDER     BY ro.id 
    DESC     LIMIT 5 
    OFFSET 0
 --> Execution Time: 0.00072884559631348, Seconds : 0.72884559631348
