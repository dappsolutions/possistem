<?php

class Pajak extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pajak';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/pajak.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pajak';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pajak";
  $data['title_content'] = 'Data Pajak';
  $content = $this->getDataPajak();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPajak($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('b.jenis', $keyword),
       array('b.keterangan', $keyword),
       array('b.persentase', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' b',
              'field' => array('b.*'),
              'like' => $like,
              'is_or_like' => true,
              'where' => "b.deleted is null or b.deleted = 0"
  ));

  return $total;
 }

 public function getDataPajak($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('b.jenis', $keyword),
       array('b.keterangan', $keyword),
       array('b.persentase', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' b',
              'field' => array('b.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "b.deleted is null or b.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataPajak($keyword)
  );
 }

 public function getDetailDataPajak($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*'),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pajak";
  $data['title_content'] = 'Tambah Pajak';
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPajak($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pajak";
  $data['title_content'] = 'Ubah Pajak';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPajak($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pajak";
  $data['title_content'] = 'Detail Pajak';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['jenis'] = $value->jenis;
  $data['persentase'] = $value->persentase;
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pajak";
  $data['title_content'] = 'Data Pajak';
  $content = $this->getDataPajak($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
