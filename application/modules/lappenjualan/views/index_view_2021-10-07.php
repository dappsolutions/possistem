<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class="row">
					<div class="col-md-4">
						<input type="text" id="tanggal" class="form-control" readonly>
					</div>
					<div class="col-md-8">
						<button id="tampil" class="btn btn-primary" onclick="LapPenjualan.tampilkan(this)">Tampilkan</button>
						&nbsp;
						<a class="btn btn-success" download="<?php echo 'Laporan Penjualan' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Penjualan');">Export</a>
					</div>
				</div>
				<br />
				<br />
				<div class="row">
					<div class="col-md-12">
						<div class='table-responsive' id="data_detail">
							<table class="table table-bordered table-list-draft" id="tb_laporan">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>No Nota</th>
										<!-- <th>Tanggal Cetak Nota</th> -->
										<th>Tanggal Order</th>
										<th>Tanggal Jatuh Tempo</th>
										<th>Customer</th>
										<th>Alamat Customer</th>
										<th>Jenis Customer</th>
										<th>Sales</th>
										<th>Gudang</th>
										<th>Kode Barang</th>
										<th>Nama Barang</th>
										<th>Tipe Barang</th>
										<th>Nama Supplier</th>
										<th>Total Pcs</th>
										<th>Total Beli</th>
										<th>Harga Sebelum Pajak</th>
										<th>Harga Jual Ppn</th>
										<th>Potongan</th>
										<th>Nilai Potongan</th>
										<th>Jenis Transaksi</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($data_Penjualan)) { ?>
										<?php foreach ($data_Penjualan as $value) { ?>
											<tr>
												<td><?php echo $value['no_faktur'] ?></td>
												<!-- <td><?php echo $value['tgl_cetak'] ?></td> -->
												<td><?php echo $value['tanggal_faktur'] ?></td>
												<td><?php echo $value['tgl_jatuh_tempo'] ?></td>
												<td><?php echo $value['customer'] ?></td>
												<td><?php echo $value['alamat'] ?></td>
												<td><?php echo $value['jenis_customer'] ?></td>
												<td><?php echo $value['nama_sales'] ?></td>
												<td><?php echo $value['nama_gudang'] ?></td>
												<td><?php echo $value['kode_product'] ?></td>
												<td><?php echo $value['nama_product'] ?></td>
												<td><?php echo $value['tipe_product'] ?></td>
												<td><?php echo $value['nama_vendor'] ?></td>
												<td><?php echo $value['total_current_qty'] ?></td>
												<td><?php echo $value['total_qty'] ?></td>
												<td><?php echo $value['harga_jual'] * $value['total_qty'] ?></td>
												<td><?php echo $value['harga_jual'] * $value['total_qty'] ?></td>
												<td><?php echo $value['jenis_potongan'] ?></td>
												<td><?php echo $value['total_potongan'] ?></td>
												<td><?php echo $value['jenis_transaksi'] ?></td>
											</tr>
										<?php } ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
