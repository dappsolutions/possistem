<?php if (!empty($data_Penjualan)) { ?>
	<?php foreach ($data_Penjualan as $value) { ?>
		<tr>
			<td><?php echo $value['no_faktur'] ?></td>
			<td><?php echo $value['tanggal_faktur'] ?></td>
			<td><?php echo $value['tanggal_bayar'] ?></td>
			<td><?php echo $value['product'] ?></td>
			<td><?php echo $value['kode_product'] ?></td>
			<td><?php echo $value['kodebarcode'] ?></td>
			<td><?php echo number_format($value['harga'], 0) ?></td>
			<td><?php echo $value['satuan'] ?></td>
			<td><?php echo $value['qty'] ?></td>
			<td><?php echo number_format($value['sub_total'], 0) ?></td>
		</tr>
	<?php } ?>
	<tr>
		<td class="text-right" colspan="9">Total</td>
		<td class=""><?php echo number_format($total_penjualan, 0) ?></td>
	</tr>
<?php } ?>