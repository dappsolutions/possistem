<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class="row">
					<div class="col-md-4">
						<input type="text" id="tanggal" class="form-control" readonly>
					</div>
					<div class="col-md-8">
						<button id="tampil" class="btn btn-primary" onclick="LapPenjualan.tampilkan(this)">Tampilkan</button>
						&nbsp;
						<a class="btn btn-success" download="<?php echo 'Laporan Penjualan' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Penjualan');">Export</a>
					</div>
				</div>
				<br />
				<br />
				<div class="row">
					<div class="col-md-12">
						<div class='table-responsive' id="data_detail">
							<table class="table table-bordered table-list-draft" id="tb_laporan">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>No Nota</th>
										<th>Tanggal Faktur</th>
										<th>Tanggal Bayar</th>
										<th>Produk</th>
										<th>Kode Produk</th>
										<th>Kode Barcode</th>
										<th>Harga</th>
										<th>Satuan</th>
										<th>Jumlah</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($data_Penjualan)) { ?>
										<?php foreach ($data_Penjualan as $value) { ?>
											<tr>
												<td><?php echo $value['no_faktur'] ?></td>
												<td><?php echo $value['tanggal_faktur'] ?></td>
												<td><?php echo $value['tanggal_bayar'] ?></td>
												<td><?php echo $value['product'] ?></td>
												<td><?php echo $value['kode_product'] ?></td>
												<td><?php echo $value['kodebarcode'] ?></td>
												<td><?php echo number_format($value['harga'], 0) ?></td>
												<td><?php echo $value['satuan'] ?></td>
												<td><?php echo $value['qty'] ?></td>
												<td><?php echo number_format($value['sub_total'], 0) ?></td>
											</tr>
										<?php } ?>
										<tr>
											<td class="text-right" colspan="9">Total</td>
											<td class=""><b><?php echo number_format($total_penjualan, 0) ?></b></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>