<table class="table table-bordered table-list-draft" id="tb_laba_rugi">
	<tbody>
		<tr class="bg-primary-light text-white">
			<th>&nbsp;</th>
			<th class="text-center"><b>Jan - Mar</b></th>
			<th class="text-center"><b>Apr - Jun</b></th>
			<th class="text-center"><b>Jul - Sep</b></th>
			<th class="text-center"><b>Okt - Des</b></th>
			<th class="text-center"><b>Total</b></th>
		</tr>
		<tr>
			<td colspan="6" onclick="toggleContent('penjualan')" class="text-left" style="background:#cccccc;color:#555555;">Pemasukan</td>
		</tr>
		<!-- <tr role="row" class="heading penjualan">
          <td>Kas</td>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total_satu'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total_dua'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total_tiga'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total_empat'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($kas['total'], 2, ',', '.') ?></td>  
         </tr> -->
		<!--         <tr role="row" class="heading penjualan">
          <td class="no-sort" width="70px">Faktur (Bersyarat)</td>
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total_satu'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total_dua'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total_tiga'], 2, ',', '.') ?></td>  
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total_empat'], 2, ',', '.') ?></td>           
          <td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($faktur['total'], 2, ',', '.') ?></td>  
         </tr>	-->
		<tr role="row" class="heading penjualan">
			<td class="no-sort" width="70px">Faktur Pelanggan</td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['pemasukan']['satu'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['pemasukan']['dua'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['pemasukan']['tiga'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['pemasukan']['empat'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['pemasukan_fix'], 2, ',', '.') ?></td>
		</tr>
		<tr role="row" class="heading penjualan">
			<td class="no-sort" width="70px"><b>Laba Kotor</b></td>
			<?php $total_lk_satu = $summary['pemasukan']['satu'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_satu, 2, ',', '.') ?></td>
			<?php $total_lk_dua = $summary['pemasukan']['dua'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_dua, 2, ',', '.') ?></td>
			<?php $total_lk_tiga = $summary['pemasukan']['tiga'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_tiga, 2, ',', '.') ?></td>
			<?php $total_lk_empat = $summary['pemasukan']['empat'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_lk_empat, 2, ',', '.') ?></td>
			<?php
			//          $total_pen = $kas['total'] + $faktur['total'] + $faktur_pelanggan['total'];
			$total_pen = $summary['pemasukan_fix'];
			?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_pen, 2, ',', '.') ?></td>
		</tr>

		<tr>
			<td colspan="6" onclick="toggleContent('stok_keluar')" class="text-left" style="background:#cccccc;color:#555555;">Pembayaran</td>
		</tr>
		<tr role="row" class="heading stok_keluar">
			<td class="no-sort" width="70px">Pembayaran Lunas</td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['total_faktur']['satu'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['total_faktur']['dua'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['total_faktur']['tiga'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['total_faktur']['empat'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['total_faktur_fix'], 2, ',', '.') ?></td>
		</tr>
		<tr role="row" class="heading stok_keluar">
			<td class="no-sort" width="70px">Pembayaran Hutang</td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['hutang_terbayar']['satu'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['hutang_terbayar']['dua'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['hutang_terbayar']['tiga'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['hutang_terbayar']['empat'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($summary['hutang_terbayar_fix'], 2, ',', '.') ?></td>
		</tr>
		<!-- <tr role="row" class="heading stok_keluar">
										<td class="no-sort" width="70px">Pembelian Lain - Lain</td>
										<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total_satu'], 2, ',', '.') ?></td>
										<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total_dua'], 2, ',', '.') ?></td>
										<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total_tiga'], 2, ',', '.') ?></td>
										<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total_empat'], 2, ',', '.') ?></td>
										<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($lain['total'], 2, ',', '.') ?></td>
									</tr> -->
		<tr role="row" class="heading stok_keluar">
			<td class="no-sort" width="70px"><b>Total Biaya</b></td>
			<?php $total_b_satu = $summary['total_faktur']['satu'] + $summary['hutang_terbayar']['satu'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_satu, 2, ',', '.') ?></td>
			<?php $total_b_dua = $summary['total_faktur']['dua'] + $summary['hutang_terbayar']['dua'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_dua, 2, ',', '.') ?></td>
			<?php $total_b_tiga = $summary['total_faktur']['tiga'] + $summary['hutang_terbayar']['tiga'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_tiga, 2, ',', '.') ?></td>
			<?php $total_b_empat = $summary['total_faktur']['empat'] + $summary['hutang_terbayar']['empat'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_empat, 2, ',', '.') ?></td>
			<?php
			$total_peng = $summary['total_faktur_fix'] + $summary['hutang_terbayar_fix'];
			?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_peng, 2, ',', '.') ?></td>
		</tr>

		<tr>
			<td colspan="6" onclick="toggleContent('pembelian')" class="text-left" style="background:#cccccc;color:#555555;">Pembelian</td>
		</tr>
		<tr role="row" class="heading pembelian">
			<td class="no-sort" width="70px">Pengadaan Barang</td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($pembelian['harga_beli']['satu'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($pembelian['harga_beli']['dua'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($pembelian['harga_beli']['tiga'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($pembelian['harga_beli']['empat'], 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($pembelian['harga_beli_fix'], 2, ',', '.') ?></td>
		</tr>
		<tr role="row" class="heading pembelian">
			<td class="no-sort" width="70px"><b>Total Biaya</b></td>
			<?php $total_c_satu = $pembelian['harga_beli']['satu'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_c_satu, 2, ',', '.') ?></td>
			<?php $total_c_dua = $pembelian['harga_beli']['dua'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_c_dua, 2, ',', '.') ?></td>
			<?php $total_c_tiga = $pembelian['harga_beli']['tiga'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_c_tiga, 2, ',', '.') ?></td>
			<?php $total_c_empat = $pembelian['harga_beli']['empat'] ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_c_empat, 2, ',', '.') ?></td>
			<?php
			$total_beli = $pembelian['harga_beli_fix'];
			?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_beli, 2, ',', '.') ?></td>
		</tr>


		<tr role="row" class="heading">
			<td class="no-sort" width="70px"><b>Laba Bersih (Pembayaran - Pembelian)</b></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_satu - $total_c_satu, 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_dua - $total_c_dua, 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_tiga - $total_c_tiga, 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_b_empat - $total_c_empat, 2, ',', '.') ?></td>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_peng - $total_beli, 2, ',', '.') ?></td>
		</tr>
		<tr role="row" class="heading">
			<td class="no-sort" width="70px" colspan="5"><b>Zakat</b></td>
			<?php $total_laba = $summary['laba']; ?>
			<?php $nominal_zakat = ($total_laba * 2.5) / 100 ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($nominal_zakat, 2, ',', '.') ?></td>
		</tr>
		<tr role="row" class="heading">
			<td class="no-sort" width="70px" colspan="5"><b>Laba Bersih dari Hasil Perhitungan Zakat (2.5 %)</b></td>
			<?php $total_laba = $total_laba - $nominal_zakat; ?>
			<td class="text-right" width="100px"><?php echo 'Rp. ' . number_format($total_laba, 2, ',', '.') ?></td>
		</tr>
	</tbody>
</table>
