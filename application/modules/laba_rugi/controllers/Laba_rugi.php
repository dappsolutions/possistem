<?php

class Laba_rugi extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'laba_rugi';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
			'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/min/moment.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/laba_rugi.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'pembeli';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Laba Rugi";
		$data['title_content'] = 'Data Laba Rugi';
		// $data['kas'] = $this->getDataKasMasuk();
		// $data['faktur'] = $this->getDataFakturTotalMasuk();
		// $data['faktur_pelanggan'] = $this->getDataFakturPelanggan();
		$data['pembelian'] = $this->getDataPembelianBarang();
		$data['summary'] = $this->getSummaryDataLabaRugi($data);
		// echo '<pre>';
		// print_r($data['pembelian']);
		// die;
		// $data['tagihan'] = $this->getDataTagihan();
		// $data['vendor'] = $this->getDataBayarVendor();
		// $data['lain'] = $this->getDataBayarLain();

		echo Modules::run('template', $data);
	}

	public function getDataPembelianBarang()
	{
		$filter_tgl = "";
		$filter_date_now = "and p.createddate like '%" . date('Y') . "%'";
		if (isset($_POST['keyword'])) {
			list($date_awal, $date_akhir) = explode('-', $_POST['keyword']);
			$date_awal = date('Y-m-d', strtotime(trim($date_awal)));
			$date_akhir = date('Y-m-d', strtotime(trim($date_akhir)));
			$filter_tgl = "and p.createddate BETWEEN '" . $date_awal . "' and '" . $date_akhir . "'";
			$filter_date_now = "";
		}

		$sql = "	select 
		(pi.harga * pi.qty) as harga_beli_total
		, QUARTER(p.createddate) AS quartal
		from product_has_price_procurement phpr
		join procurement_item pi 
			on pi.id = phpr.procurement_item
			and pi.deleted = 0
		join procurement p
			on p.id = pi.procurement 
		where p.deleted = 0 " . $filter_date_now . " " . $filter_tgl . " ";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		// echo '<pre>';
		// print_r($data);
		// die;

		$result['harga_beli_fix'] = 0;
		$result['harga_beli']['satu'] = 0;
		$result['harga_beli']['dua'] = 0;
		$result['harga_beli']['tiga'] = 0;
		$result['harga_beli']['empat'] = 0;
		$invoice = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				$result['harga_beli_fix'] += $value['harga_beli_total'];
				switch ($value['quartal']) {
					case '1':
						$result['harga_beli']['satu'] += $value['harga_beli_total'];
						break;
					case '2':
						$result['harga_beli']['dua'] += $value['harga_beli_total'];
						break;
					case '3':
						$result['harga_beli']['tiga'] += $value['harga_beli_total'];
						break;
					case '4':
						$result['harga_beli']['empat'] += $value['harga_beli_total'];
						break;

					default:
						# code...
						break;
				}
			}
		}

		// echo '<pre>';
		// print_r($result);
		// die;

		return $result;
	}

	public function getData()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Laba Rugi";
		$data['title_content'] = 'Data Laba Rugi';
		// echo '<pre>';
		// print_r($_POST);
		// die;
		// $data['kas'] = $this->getDataKasMasuk();
		// $data['faktur'] = $this->getDataFakturTotalMasuk();
		// $data['faktur_pelanggan'] = $this->getDataFakturPelanggan();
		// $data['tagihan'] = $this->getDataTagihan();
		// $data['vendor'] = $this->getDataBayarVendor();
		// $data['lain'] = $this->getDataBayarLain();
		$data['pembelian'] = $this->getDataPembelianBarang();
		$data['summary'] = $this->getSummaryDataLabaRugi($data);

		echo $this->load->view('data_detail', $data, true);
	}

	public function getTotalDataLabaRugi($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.nama', $keyword),
				array('p.no_hp', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*'),
			'like' => $like,
			'is_or_like' => true,
		));

		return $total;
	}

	public function getDataFakturTotalMasuk()
	{
		$keyword = date('Y');
		$where = "WHERE p.createddate LIKE '%$keyword%' OR pha.tgl_angsuran LIKE '%" . $keyword . "%' OR phc.tgl_bayar LIKE '%" . $keyword . "%'";
		if (!empty($_POST)) {
			list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
			$awal = date('Y-m-d', strtotime($awal));
			$akhir = date('Y-m-d', strtotime($akhir));
			$where = "WHERE (p.createddate >= '" . $awal . "' and p.createddate <= '" . $akhir . "') OR (pha.tgl_angsuran >= '" . $awal . "' and pha.tgl_angsuran <= '" . $akhir . "') OR (phc.tgl_bayar >= '" . $awal . "' and phc.tgl_bayar <= '" . $akhir . "')";
		}

		$query = <<<QUERY
  SELECT 
	p.id AS pembelian 
	, QUARTER(p.createddate) AS quartal
	, p.no_invoice
	, pm.nama AS nama_pembeli
	, sp.status AS status_beli
	, r.product
	, rhhjc.harga AS harga_cash
	, rhhjk.harga AS harga_kredit
	, phr.id AS pembeli_has_product
	, pha.total_bayar AS bayar_angsuran
	, phc.total_bayar AS bayar_cash
FROM pembelian p
JOIN pembeli_has_product phr ON p.id = phr.pembelian
JOIN pembeli pm ON phr.pembeli = pm.id
JOIN status_pembelian sp ON phr.status_pembelian = sp.id
JOIN product r ON phr.product = r.id
LEFT JOIN product_has_harga_jual_pokok rhhjc ON r.id = rhhjc.product AND rhhjc.period_end IS NULL
LEFT JOIN product_has_harga_jual_tunai rhhjk ON r.id = rhhjk.product AND rhhjk.period_end IS NULL
JOIN pembayaran_product pr ON phr.id = pr.pembeli_has_product
LEFT JOIN pembayaran_has_cash phc ON pr.id = phc.pembayaran_product
LEFT JOIN pembayaran_has_angsuran pha ON pr.id = pha.pembayaran_product
$where
QUERY;

		$data = Modules::run('database/get_custom', $query);

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		$total_quartal_satu = 0;
		$total_quartal_dua = 0;
		$total_quartal_tiga = 0;
		$total_quartal_empat = 0;
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$total = $value['bayar_angsuran'] == '' ? $value['bayar_cash'] : $value['bayar_angsuran'];
				switch ($value['quartal']) {
					case 1:
						$total_quartal_satu += $total;
						break;
					case 2:
						$total_quartal_dua += $total;
						break;
					case 3:
						$total_quartal_tiga += $total;
						break;
					case 4:
						$total_quartal_empat += $total;
						break;
				}
				array_push($result, $value);
			}
		}

		$total = array();
		$total['total_satu'] = $total_quartal_satu;
		$total['total_dua'] = $total_quartal_dua;
		$total['total_tiga'] = $total_quartal_tiga;
		$total['total_empat'] = $total_quartal_empat;
		$total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
		return $total;
	}

	public function getDataKasMasuk()
	{
		$keyword = date('Y');
		$where = "WHERE k.createddate LIKE '%" . $keyword . "%'";
		if (!empty($_POST)) {
			list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
			$awal = date('Y-m-d', strtotime($awal));
			$akhir = date('Y-m-d', strtotime($akhir));
			$where = "WHERE k.createddate >= '" . $awal . "' and k.createddate <= '" . $akhir . "'";
		}

		$query = <<<QUERY
  SELECT 
k.jumlah 
, QUARTER(k.createddate) AS quartal
FROM kas k
$where
QUERY;

		$data = Modules::run('database/get_custom', $query);

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		$total_quartal_satu = 0;
		$total_quartal_dua = 0;
		$total_quartal_tiga = 0;
		$total_quartal_empat = 0;
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$total = $value['jumlah'];
				switch ($value['quartal']) {
					case 1:
						$total_quartal_satu += $total;
						break;
					case 2:
						$total_quartal_dua += $total;
						break;
					case 3:
						$total_quartal_tiga += $total;
						break;
					case 4:
						$total_quartal_empat += $total;
						break;
				}
				array_push($result, $value);
			}
		}

		$total = array();
		$total['total_satu'] = $total_quartal_satu;
		$total['total_dua'] = $total_quartal_dua;
		$total['total_tiga'] = $total_quartal_tiga;
		$total['total_empat'] = $total_quartal_empat;

		$total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
		return $total;
	}

	public function getSummaryDataLabaRugi($params = array())
	{
		$filter_tgl = "";
		$filter_date_now = "and p.createddate like '%" . date('Y') . "%'";
		if (isset($_POST['keyword'])) {
			list($date_awal, $date_akhir) = explode('-', $_POST['keyword']);
			$date_awal = date('Y-m-d', strtotime(trim($date_awal)));
			$date_akhir = date('Y-m-d', strtotime(trim($date_akhir)));
			$filter_tgl = "and p.createddate BETWEEN '" . $date_awal . "' and '" . $date_akhir . "'";
			$filter_date_now = "";
		}
		$sql = "
		select 
		distinct
		i.no_faktur 
		,pi.jumlah_bayar 
		,(pit.harga * pit.qty) as harga_beli_total
		, ip.sub_total as total_faktur
		, isa.jumlah  as sisa_hutang
		, ist.status 
		, QUARTER(p.createddate) AS quartal
		from payment p
		join payment_item pi
			on p.id = pi.payment 
		join invoice i
			on i.id = pi.invoice 
		join metode_bayar mb
			on mb.id = i.metode_bayar 
		join invoice_product ip
			on ip.invoice = i.id 
		join product_satuan ps
			on ps.id = ip.product_satuan
		join (select max(id) id, invoice from invoice_sisa group by invoice) isa_group
			on isa_group.invoice = i.id 
		join invoice_sisa isa
			on isa.id = isa_group.id 	
		join (select max(id) id, invoice from invoice_status group by invoice) ist_group
			on ist_group.invoice = i.id 
		join invoice_status ist
			on ist.id = ist_group.id
		left join (
			select max(id) id, product, satuan from procurement_item group by product, satuan
		) pit_max
			on pit_max.product = ps.product 
			and pit_max.satuan = ps.satuan 
		left join procurement_item pit
			on pit.id = pit_max.id
		where p.deleted = 0 
		" . $filter_tgl . "	
		" . $filter_date_now . "	
		order by p.id desc";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result['laba'] = 0;
		$result['rugi'] = 0;
		$result['hutang'] = 0;
		$result['hutang_terbayar_fix'] = 0;
		$result['hutang_terbayar']['satu'] = 0;
		$result['hutang_terbayar']['dua'] = 0;
		$result['hutang_terbayar']['tiga'] = 0;
		$result['hutang_terbayar']['empat'] = 0;
		$result['pemasukan_fix'] = 0;
		$result['pemasukan']['satu'] = 0;
		$result['pemasukan']['dua'] = 0;
		$result['pemasukan']['tiga'] = 0;
		$result['pemasukan']['empat'] = 0;
		$result['harga_beli_fix'] = 0;
		$result['harga_beli']['satu'] = 0;
		$result['harga_beli']['dua'] = 0;
		$result['harga_beli']['tiga'] = 0;
		$result['harga_beli']['empat'] = 0;
		$result['total_faktur_fix'] = 0;
		$result['total_faktur']['satu'] = 0;
		$result['total_faktur']['dua'] = 0;
		$result['total_faktur']['tiga'] = 0;
		$result['total_faktur']['empat'] = 0;
		$result['total_transaksi'] = 0;
		$invoice = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				if (!in_array($value['no_faktur'], $invoice)) {
					$result['total_transaksi'] += 1;
					if ($value['status'] == 'PAID') {
						$total_faktur = $value['total_faktur'];
						$result['total_faktur_fix'] += $total_faktur;
						switch ($value['quartal']) {
							case '1':
								$result['total_faktur']['satu'] += $total_faktur;
								break;
							case '2':
								$result['total_faktur']['dua'] += $total_faktur;
								break;
							case '3':
								$result['total_faktur']['tiga'] += $total_faktur;
								break;
							case '4':
								$result['total_faktur']['empat'] += $total_faktur;
								break;

							default:
								# code...
								break;
						}
					} else {
						$total = $value['sisa_hutang'];
						$result['hutang'] += $total;

						$result['hutang_terbayar_fix'] += $value['jumlah_bayar'];
						switch ($value['quartal']) {
							case '1':
								$result['hutang_terbayar']['satu'] += $value['jumlah_bayar'];
								break;
							case '2':
								$result['hutang_terbayar']['dua'] += $value['jumlah_bayar'];
								break;
							case '3':
								$result['hutang_terbayar']['tiga'] += $value['jumlah_bayar'];
								break;
							case '4':
								$result['hutang_terbayar']['empat'] += $value['jumlah_bayar'];
								break;

							default:
								# code...
								break;
						}
					}

					$total_faktur_pemasukan = $value['total_faktur'];
					$result['pemasukan_fix'] += $total_faktur_pemasukan;
					switch ($value['quartal']) {
						case '1':
							$result['pemasukan']['satu'] += $total_faktur_pemasukan;
							break;
						case '2':
							$result['pemasukan']['dua'] += $total_faktur_pemasukan;
							break;
						case '3':
							$result['pemasukan']['tiga'] += $total_faktur_pemasukan;
							break;
						case '4':
							$result['pemasukan']['empat'] += $total_faktur_pemasukan;
							break;

						default:
							# code...
							break;
					}

					$result['harga_beli_fix'] += $value['harga_beli_total'];
					switch ($value['quartal']) {
						case '1':
							$result['harga_beli']['satu'] += $value['harga_beli_total'];
							break;
						case '2':
							$result['harga_beli']['dua'] += $value['harga_beli_total'];
							break;
						case '3':
							$result['harga_beli']['tiga'] += $value['harga_beli_total'];
							break;
						case '4':
							$result['harga_beli']['empat'] += $value['harga_beli_total'];
							break;

						default:
							# code...
							break;
					}
					$invoice[] = $value['no_faktur'];
				}
			}
		}

		$perhitungan_total = ($result['hutang_terbayar_fix'] + $result['total_faktur_fix']) - $params['pembelian']['harga_beli_fix'];
		$result['laba'] = $perhitungan_total > 0 ? $perhitungan_total : 0;
		$result['rugi'] = $perhitungan_total < 0 ? $perhitungan_total * -1 : 0;

		// echo '<pre>';
		// print_r($result);
		// die;

		return $result;
	}

	public function getDataFakturPelanggan()
	{
		$keyword = date('Y');
		$where = "where i.createddate like '%" . $keyword . "%' and i.deleted = 0";
		if (!empty($_POST)) {
			list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
			$awal = date('Y-m-d', strtotime($awal));
			$akhir = date('Y-m-d', strtotime($akhir));
			$where = "where (i.createddate >= '" . $awal . "' and i.createddate <= '" . $akhir . "') and i.deleted = 0";
		}

		$query = <<<QUERY
  select 
		i.total
		, QUARTER(i.createddate) AS quartal
		from invoice i
		$where
QUERY;

		$data = Modules::run('database/get_custom', $query);

		$result = array();
		$total_quartal_satu = 0;
		$total_quartal_dua = 0;
		$total_quartal_tiga = 0;
		$total_quartal_empat = 0;
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$total = $value['total'];
				switch ($value['quartal']) {
					case 1:
						$total_quartal_satu += $total;
						break;
					case 2:
						$total_quartal_dua += $total;
						break;
					case 3:
						$total_quartal_tiga += $total;
						break;
					case 4:
						$total_quartal_empat += $total;
						break;
				}
				array_push($result, $value);
			}
		}

		$total = array();
		$total['total_satu'] = $total_quartal_satu;
		$total['total_dua'] = $total_quartal_dua;
		$total['total_tiga'] = $total_quartal_tiga;
		$total['total_empat'] = $total_quartal_empat;

		$total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
		return $total;
	}

	public function getDataTagihan()
	{
		$keyword = date('Y');
		$where = "WHERE pt.createddate LIKE '%" . $keyword . "%'";
		if (!empty($_POST)) {
			list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
			$awal = date('Y-m-d', strtotime($awal));
			$akhir = date('Y-m-d', strtotime($akhir));
			$where = "WHERE pt.createddate >= '" . $awal . "' and pt.createddate <= '" . $akhir . "'";
		}


		$query = <<<QUERY
  
SELECT 
pt.total
, QUARTER(pt.createddate) AS quartal
FROM pembayaran_tagihan pt
$where
QUERY;

		$data = Modules::run('database/get_custom', $query);

		$result = array();
		$total_quartal_satu = 0;
		$total_quartal_dua = 0;
		$total_quartal_tiga = 0;
		$total_quartal_empat = 0;
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$total = $value['total'];
				switch ($value['quartal']) {
					case 1:
						$total_quartal_satu += $total;
						break;
					case 2:
						$total_quartal_dua += $total;
						break;
					case 3:
						$total_quartal_tiga += $total;
						break;
					case 4:
						$total_quartal_empat += $total;
						break;
				}
				array_push($result, $value);
			}
		}

		$total = array();
		$total['total_satu'] = $total_quartal_satu;
		$total['total_dua'] = $total_quartal_dua;
		$total['total_tiga'] = $total_quartal_tiga;
		$total['total_empat'] = $total_quartal_empat;
		$total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
		return $total;
	}

	public function getDataBayarVendor()
	{
		$keyword = date('Y');
		$where = "WHERE pv.createddate LIKE '%" . $keyword . "%'";
		if (!empty($_POST)) {
			list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
			$awal = date('Y-m-d', strtotime($awal));
			$akhir = date('Y-m-d', strtotime($akhir));
			$where = "WHERE pv.createddate >= '" . $awal . "' and pv.createddate <= '" . $akhir . "'";
		}


		$query = <<<QUERY
    SELECT 
    pv.total
    , QUARTER(pv.createddate) AS quartal
    FROM pembayaran_vendor pv
    $where
QUERY;

		$data = Modules::run('database/get_custom', $query);

		$result = array();
		$total_quartal_satu = 0;
		$total_quartal_dua = 0;
		$total_quartal_tiga = 0;
		$total_quartal_empat = 0;
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$total = $value['total'];
				switch ($value['quartal']) {
					case 1:
						$total_quartal_satu += $total;
						break;
					case 2:
						$total_quartal_dua += $total;
						break;
					case 3:
						$total_quartal_tiga += $total;
						break;
					case 4:
						$total_quartal_empat += $total;
						break;
				}
				array_push($result, $value);
			}
		}

		$total = array();
		$total['total_satu'] = $total_quartal_satu;
		$total['total_dua'] = $total_quartal_dua;
		$total['total_tiga'] = $total_quartal_tiga;
		$total['total_empat'] = $total_quartal_empat;
		$total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
		return $total;
	}

	public function getDataBayarLain()
	{
		$keyword = date('Y');
		$where = "WHERE pl.createddate LIKE '%" . $keyword . "%'";
		if (!empty($_POST)) {
			list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
			$awal = date('Y-m-d', strtotime($awal));
			$akhir = date('Y-m-d', strtotime($akhir));
			$where = "WHERE pl.createddate >= '" . $awal . "' and pl.createddate <= '" . $akhir . "'";
		}

		$query = <<<QUERY
    
    SELECT 
    pl.total
    , QUARTER(pl.createddate) AS quartal
    FROM pembayaran_lain_lain pl
    $where
QUERY;

		$data = Modules::run('database/get_custom', $query);

		$result = array();
		$total_quartal_satu = 0;
		$total_quartal_dua = 0;
		$total_quartal_tiga = 0;
		$total_quartal_empat = 0;
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$total = $value['total'];
				switch ($value['quartal']) {
					case 1:
						$total_quartal_satu += $total;
						break;
					case 2:
						$total_quartal_dua += $total;
						break;
					case 3:
						$total_quartal_tiga += $total;
						break;
					case 4:
						$total_quartal_empat += $total;
						break;
				}
				array_push($result, $value);
			}
		}

		$total = array();
		$total['total_satu'] = $total_quartal_satu;
		$total['total_dua'] = $total_quartal_dua;
		$total['total_tiga'] = $total_quartal_tiga;
		$total['total_empat'] = $total_quartal_empat;
		$total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
		return $total;
	}

	public function getDataLabaRugi($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.nama', $keyword),
				array('p.no_hp', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*'),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataLabaRugi($keyword)
		);
	}

	public function getDetailDataLabaRugi($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' kr',
			'where' => "kr.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah LabaRugi";
		$data['title_content'] = 'Tambah LabaRugi';
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataLabaRugi($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah LabaRugi";
		$data['title_content'] = 'Ubah LabaRugi';
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = $this->getDetailDataLabaRugi($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail LabaRugi";
		$data['title_content'] = 'Detail LabaRugi';
		echo Modules::run('template', $data);
	}

	public function getPostDataHeader($value)
	{
		$data['nama'] = $value->nama;
		$data['no_hp'] = $value->no_hp;
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		$id = $this->input->post('id');
		$is_valid = false;
		$tipe_product = $id;
		$this->db->trans_begin();
		try {
			$post_tipe_product = $this->getPostDataHeader($data);
			if ($id == '') {
				$tipe_product = Modules::run('database/_insert', $this->getTableName(), $post_tipe_product);
			} else {
				//update
				Modules::run('database/_update', $this->getTableName(), $post_tipe_product, array('id' => $id));
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'tipe_product' => $tipe_product));
	}
}
