<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Syarat
     </div>
     <div class='col-md-3'>
      <?php echo $syarat ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Kategori Akad
     </div>
     <div class='col-md-3'>
      <?php echo $kategori ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Jenis Akad
     </div>
     <div class='col-md-3'>
      <?php echo $jenis ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>    
    <hr/>

    <div class="row">
     <div class='col-md-3'>
      <b><u>Data Berkas Persyaratan</u></b>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-12'>
      <table class="table table-striped table-bordered" id="list_berkas">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>Jenis Berkas</th>
         <th>Lampirkan Berkas</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($detail)) { ?>
         <?php foreach ($detail as $value) { ?>
          <tr>
           <td>
            <?php echo $value['nama_berkas'] ?>
           </td>
           <td>
            <?php echo $value['status'] == 'Y' ? 'Wajib' : 'Tidak' ?>
           </td>
          </tr>
         <?php } ?>
        <?php } ?>        
       </tbody>
      </table>

     </div>     
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="Persyaratan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
