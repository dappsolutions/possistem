<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class="row">
					<div class='col-md-3 text-bold'>
						Produk
					</div>
					<div class='col-md-3'>
						<?php echo $nama_product ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Satuan
					</div>
					<div class='col-md-3'>
						<?php echo $nama_satuan ?>
					</div>
				</div>
				<br />
				<!-- <div class="row">
					<div class='col-md-3 text-bold'>
						Gudang
					</div>
					<div class='col-md-3'>
						<?php echo $nama_gudang ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Rak
					</div>
					<div class='col-md-3'>
						<?php echo $nama_rak ?>
					</div>
				</div>
				<br /> -->
				<div class="row">
					<div class='col-md-3 text-bold'>
						Stok
					</div>
					<div class='col-md-3'>
						<?php echo number_format($stock, 2, ',', '.') ?>
					</div>
				</div>
				<br />
				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="ProductStock.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="padding-16">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h5>Histori Stok</h5>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered">
									<thead>
										<tr class="bg-warning">
											<th>No</th>
											<th>Status</th>
											<th>Keterangan</th>
											<th>Qty</th>
											<th>Tanggal</th>
										</tr>
									</thead>
									<tbody>
										<?php $no = 1  ?>
										<?php foreach ($data_history as $key => $value) { ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $value['status'] ?></td>
												<td><?php echo $value['keterangan'] ?></td>
												<td><?php echo $value['qty'] ?></td>
												<td><?php echo $value['createddate'] ?></td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>