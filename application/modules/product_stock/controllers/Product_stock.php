<?php

class Product_stock extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'product_stock';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/product_stock.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'product_stock';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Stok";
		$data['title_content'] = 'Data Stok';
		$content = $this->getDataStok();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		// echo '<pre>';
		// print_r($data);
		// die;
		echo Modules::run('template', $data);
	}

	public function getTotalDataStok($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.product', $keyword),
				array('p.kode_product', $keyword),
				array('k.stock', $keyword),
				array('s.nama_satuan', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => 'product p',
			'field' => array(
				'k.*', 'p.product as nama_product',
				'ps.satuan', 'g.nama_gudang',
				'r.nama_rak', 's.nama_satuan', 'p.kode_product'
			),
			'join' => array(
				array('product_satuan ps', 'ps.product = p.id and ps.deleted = 0'),
				array('(select max(id) id, product from product_stock group by product) k_max', 'k_max.product = p.id'),
				array('product_stock k', 'k.id = k_max.id'),
				array('gudang g', 'k.gudang = g.id', 'left'),
				array('rak r', 'k.rak = r.id', 'left'),
				array('satuan s', 'ps.satuan = s.id', 'left'),
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => "(k.deleted is null or k.deleted = 0)
			and ps.satuan_terkecil = '1'"
		));

		return $total;
	}

	public function getDataStok($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.kode_product', $keyword),
				array('p.product', $keyword),
				array('k.stock', $keyword),
				array('s.nama_satuan', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => 'product p',
			'field' => array(
				'k.*', 'p.product as nama_product',
				'ps.satuan', 'g.nama_gudang',
				'r.nama_rak', 's.nama_satuan', 'p.kode_product'
			),
			'join' => array(
				array('product_satuan ps', 'ps.product = p.id and ps.deleted = 0'),
				array('(select max(id) id, product from product_stock group by product) k_max', 'k_max.product = p.id'),
				array('product_stock k', 'k.id = k_max.id'),
				array('gudang g', 'k.gudang = g.id', 'left'),
				array('rak r', 'k.rak = r.id', 'left'),
				array('satuan s', 'ps.satuan = s.id', 'left'),
			),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
			'where' => "(k.deleted is null or k.deleted = 0) and ps.satuan_terkecil = '1'"
		));
		// echo "<pre>";
		// print_r($data);
		// die;
		// die;
		$result = array();
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $value) {
				if (!in_array($value['kode_product'], $temp)) {
					if ($value['stock'] < 0) {
						Modules::run('database/_update', 'product_stock', array('stock' => 0), array('id' => $value['id']));
						$post_inus['stock'] = $value['stock'];
						$post_inus['product_stock'] = $value['id'];
						Modules::run('database/_insert', 'product_stock_minus', $post_inus, array('id' => $value['id']));
						$value['stock'] = 0;
					}
					array_push($result, $value);
					$temp[] = $value['kode_product'];
				}
				//    $total_return = $value['stok_retur'] == '' ? 0 : $value['stok_retur'];
				//    $total_keluar = $value['stok_keluar'] == '' ? 0 : $value['stok_keluar'];
				//    $value['stock'] = $value['stock']+ $total_return - $total_keluar;
			}
		}

		//   echo '<pre>';
		//   print_r($result);die;
		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataStok($keyword)
		);
	}

	public function getDetailDataStok($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' kr',
			'field' => array(
				'kr.*', 'p.product as nama_product', 'ps.satuan',
				'ps.product', 'r.nama_rak',
				'g.nama_gudang', 's.nama_satuan', 'sp.nama_satuan as satuan_parent'
			),
			'join' => array(
				array('product_satuan ps', 'kr.product_satuan = ps.id'),
				array('product p', 'ps.product = p.id'),
				array('gudang g', 'kr.gudang = g.id', 'left'),
				array('rak r', 'kr.rak = r.id', 'left'),
				array('satuan s', 's.id = ps.satuan', 'left'),
				array('satuan sp', 'sp.id = s.parent', 'left')
			),
			'where' => "kr.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListProduct()
	{
		$data = Modules::run('database/get', array(
			'table' => 'product p',
			'field' => array('p.*'),
			'where' => "p.deleted = 0 or p.deleted is null"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListGudang()
	{
		$data = Modules::run('database/get', array(
			'table' => 'gudang p',
			'field' => array('p.*'),
			'where' => "p.deleted = 0 or p.deleted is null and p.id = 1"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListRak()
	{
		$data = Modules::run('database/get', array(
			'table' => 'rak p',
			'field' => array('p.*'),
			'where' => "p.deleted = 0 or p.deleted is null and p.id = 1"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListSatuan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'satuan sa',
			'field' => array('sa.*', 's.nama_satuan as satuan_parent'),
			'join' => array(
				array('satuan s', 'sa.parent = s.id', 'left')
			),
			'where' => "sa.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Stok";
		$data['title_content'] = 'Tambah Stok';
		$data['list_product'] = $this->getListProduct();
		$data['list_gudang'] = $this->getListGudang();
		$data['list_rak'] = $this->getListRak();
		$data['list_satuan'] = array();

		// echo '<pre>';
		// print_r($data);
		// die;
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataStok($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Stok";
		$data['title_content'] = 'Ubah Stok';
		$data['list_product'] = $this->getListProduct();
		$data['list_satuan'] = $this->getDataSatuan($data['product']);
		//   echo '<pre>';
		//  print_r($data['list_satuan']);die;
		$data['list_gudang'] = $this->getListGudang();
		$data['list_rak'] = $this->getListRak();
		//   $data['list_satuan'] = $this->getListSatuan(); 
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = $this->getDetailDataStok($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Stok";
		$data['title_content'] = 'Detail Stok';
		$data['data_history'] = $this->getDetailHistoryTransaksi($data);
		echo Modules::run('template', $data);
	}

	public function getPostDataHeader($value)
	{
		$data['product'] = $value->product;
		$data['product_satuan'] = $value->product_satuan;
		$data['stock'] = str_replace('.', '', $value->stock);
		if ($value->gudang != '') {
			$data['gudang'] = $value->gudang;
		}
		if ($value->rak != '') {
			$data['rak'] = $value->rak;
		}
		return $data;
	}

	public function cekDataSudahAdaStok($data)
	{
		$data_db = Modules::run('database/get', array(
			'table' => 'product_stock ps',
			'field' => array('ps.*'),
			'join' => array(
				array('product_satuan pst', 'pst.id = ps.product_satuan'),
				array('product p', 'p.id = pst.product'),
			),
			'where' => "ps.deleted = 0 and ps.product_satuan = '" . $data->product_satuan . "'",
			'orderby' => "ps.id desc"
		));

		$result = array();
		if (!empty($data_db)) {
			$data_db = $data_db->row_array();
			$result = $data_db;
		}

		return $result;
	}

	public function getDetailHistoryTransaksi($data)
	{
		// echo '<pre>';
		// print_r($data);
		// die;
		$data_db = Modules::run('database/get', array(
			'table' => 'product_log_stock pls',
			'field' => array('pls.*'),
			'join' => array(
				array('product_satuan ps', 'ps.id = pls.product_satuan'),
				array('product p', 'p.id = ps.product'),
			),
			'where' => "pls.deleted = 0 and p.id = '" . $data['product'] . "'",
			'orderby' => "pls.id desc",
		));

		// echo '<pre>';
		// echo $this->db->last_query();
		// die;

		$result = array();
		if (!empty($data_db)) {
			foreach ($data_db->result_array() as $val) {
				array_push($result, $val);
			}
		}

		return $result;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		$id = $this->input->post('id');
		$is_valid = false;

		// echo '<pre>';
		// print_r($data);
		// die;
		$checkIsExist = $this->cekDataSudahAdaStok($data);
		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader($data);
			if ($id == '') {
				if (empty($checkIsExist)) {
					$id = Modules::run('database/_insert', $this->getTableName(), $post_data);
				} else {
					$id = $this->generateStokProduct($data);
				}
			} else {
				//update
				Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}


	public function generateStokProduct($data)
	{
		$data_db = Modules::run('database/get', array(
			'table' => 'product_stock ps',
			'field' => array('ps.*'),
			'join' => array(
				array('product_satuan pst', 'pst.id = ps.product_satuan'),
				array('product p', 'p.id = pst.product'),
			),
			'where' => "ps.deleted = 0 and ps.product_satuan = '" . $data->product_satuan . "'",
			'orderby' => "ps.id desc"
		));

		// echo '<pre>';
		// print_r($this->db->last_query());
		// die;

		$result = array();
		$stok = 0;
		if (!empty($data_db)) {
			$data_db = $data_db->row_array();
			$stok = $data_db['stock'];
		}
		$parms = array();
		$parms['product_satuan'] = $data->product_satuan;
		$parms['status'] = 'IN';
		$parms['qty'] = $data->stock;
		$parms['keterangan'] = 'Lain';
		Modules::run('database/_insert', 'product_log_stock', $parms);

		$post['stock'] = $stok + ($data->stock);
		$post['product'] = $data->product;
		$post['product_satuan'] = $data->product_satuan;
		$post['gudang'] = 1;
		$post['rak'] = 1;
		return Modules::run('database/_insert', 'product_stock', $post);
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Stok";
		$data['title_content'] = 'Data Stok';
		$content = $this->getDataStok($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/' . $keyword . '/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function getDataSatuan($product)
	{
		$data = Modules::run('database/get', array(
			'table' => 'product_satuan ps',
			'field' => array('ps.*', 's.nama_satuan', 'sp.nama_satuan as satuan_parent'),
			'join' => array(
				array('satuan s', 'ps.satuan = s.id', 'left'),
				array('satuan sp', 's.parent = sp.id', 'left')
			),
			'where' => "ps.deleted = 0 and ps.product = '" . $product . "' and ps.satuan_terkecil = 1",
			'orderby' => 'ps.id asc',
			'limit' => 1
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getSatuanProduk()
	{
		$product = $_POST['product'];
		$data_satuan = $this->getDataSatuan($product);
		$data['list_satuan'] = $data_satuan;
		echo $this->load->view('list_satuan', $data, true);
	}
}
