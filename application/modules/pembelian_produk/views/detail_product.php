<div class="content">
 <div class="animated fadeIn">
  <!--<div class="card">-->
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Produk Produk</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Tipe Produk
     </div>
     <div class='col-md-3'>
      <?php echo $tipe ?>
     </div>     
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3'>
      Nama Produk
     </div>
     <div class='col-md-3'>
      <?php echo $product ?>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3'>
      Produk Kategori
     </div>
     <div class='col-md-3'>
      <?php echo $product_kategori ?>
     </div>
    </div>
    <br/>


    <div class='row'>
     <div class='col-md-3'>
      Keterangan Produk
     </div>
     <div class='col-md-3'>
      <?php $keterangan ?>
     </div>
    </div>
    <br/>

    <hr/>

    <div class='row'>
     <div class='col-md-3'>
      Harga Cash Produk
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp. ' . number_format($harga_cash, 2, ',', '.') ?>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3'>
      Harga Kredit Produk
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp. ' . number_format($harga_kredit, 2, ',', '.') ?>
     </div>
    </div>
    <br/>
    <hr/>
    
    <div class='row'>
     <div class='col-md-12'>
      <center>Foto Produk Produk</center>
     </div>
    </div>
    <br/>    

    <div class='row'>
     <?php foreach ($data_image as $v_image) { ?>
      <div class='col-md-4'>
       <div class=''>
        <img src="<?php echo $v_image['foto'] ?>" width="300" height="300"/>
       </div>       
      </div>
     <?php } ?>
    </div>
    <br/>
    <hr/>

    <div class='row'>
     <div class='col-md-3'>
      <u>Detail Harga Angsuran</u>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft" id="list_ansuran">
       <thead>
        <tr>
         <th>No</th>
         <th>Harga Ansguran</th>
         <th>Tenor</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($detail)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($detail as $value) { ?>
          <tr class="edit"> 
           <td><?php echo $no++ ?></td>
           <td>
            <?php echo 'Rp. ' . number_format($value['harga'], 2, ',', '.') ?>
           </td>
           <td class="text-center">
            <?php echo $value['periode_tahun'] ?>
           </td>
          </tr>
         <?php } ?>
        <?php } ?>
       </tbody>
      </table>
     </div>
    </div>
    <hr/>
   </div>
  <!--</div>-->
 </div>
</div>
