<?php

class Invoice extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Invoice';
	}

	public function getTableName()
	{
		return 'invoice';
	}

	public function index()
	{
		echo 'invoice';
	}

	public function getListInvoice()
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' i',
			'field' => array(
				'i.*', 'isa.status',
				'sisa.jumlah as sisa_hutang',
				'isp.approve as approve_print',
				'p.nama as nama_pembeli',
			),
			'join' => array(
				array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
				array('invoice_status isa', 'isa.id = iss.id'),
				array('invoice_sisa sisa', 'sisa.invoice = i.id', 'left'),
				array('(select max(id) id, invoice from invoice_print group by invoice) issp', 'issp.invoice = i.id', 'left'),
				array('invoice_print isp', 'isp.id = issp.id', 'left'),
				array('pembeli p', 'p.id = i.pembeli'),
			),
			'where' => "(i.deleted is null or i.deleted = 0) and isp.approve = 'ASK'",
			'orderby' => 'i.id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['total_str'] = number_format($value['total']);
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}

	public function getListFakturPelanggan($pelanggan)
	{
		$data = Modules::run('database/get', array(
			'table' => 'invoice i',
			'field' => array('i.*', 'ist.status', 'isa.jumlah as sisa_hutang'),
			'join' => array(
				array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
				array('invoice_status ist', 'ist.id = iss.id'),
				array('(select max(id) id, invoice from invoice_sisa group by invoice) ss', 'ss.invoice = i.id', 'left'),
				array('invoice_sisa isa', 'isa.id = ss.id', 'left'),
			),
			'where' => "i.deleted = 0 and i.pembeli = '" . $pelanggan . "' and ist.status = 'DRAFT'",
			'orderby' => 'i.tanggal_faktur, i.tanggal_bayar'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				if ($value['sisa_hutang'] == '') {
					$value['sisa_hutang'] = $value['total'];
				}
				array_push($result, $value);
			}
		}

		//  echo '<pre>';
		//  print_r($result);die;

		echo json_encode(array(
			'data' => $result
		));
	}

	public function prosesApprove()
	{
		$is_valid = "0";

		$this->db->trans_begin();
		try {
			$post_status['user'] = $_POST['user'];
			$post_status['invoice'] = $_POST['invoice'];
			$post_status['approve'] = 'APPROVED';
			Modules::run('database/_insert', 'invoice_print', $post_status);

			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}
}
