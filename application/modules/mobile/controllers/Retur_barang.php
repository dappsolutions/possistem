<?php

class Retur_barang extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Retur_barang';
	}

	public function getTableName()
	{
		return 'retur_barang';
	}

	public function index()
	{
		echo 'Retur_barang';
	}

	public function getDay()
	{
		$day = date('l');
		switch (strtolower($day)) {
			case 'sunday':
				return 7;
				break;
			case 'monday':
				return 1;
				break;
			case 'tuesday':
				return 2;
				break;
			case 'wednesday':
				return 3;
				break;
			case 'thursday':
				return 4;
				break;
			case 'friday':
				return 5;
				break;
			case 'saturday':
				return 6;
				break;
			default:
				return 1;
				break;
		}
	}

	public function getListGudang() {
  $data = Modules::run('database/get', array(
              'table' => 'gudang g',
              'field' => array('g.*'),
              'where' => "g.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  echo json_encode(array(
			'data'=> $result
		));
	}
	
	public function getListReturBarang()
	{
		// echo $day;die;
		// $user = "3";
		$user = $_POST['user'];
		$now = date('Y-m-d');
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' ro',
			'field' => array('ro.*', 'isa.status', 'p.nama as nama_pembeli'),
			'join' => array(
				array('(select max(id) id, `retur_barang` from retur_barang_status group by `retur_barang`) iss', 'iss.retur_barang = ro.id'),
				array('retur_barang_status isa', 'isa.id = iss.id'),
				array('pembeli p', 'ro.pembeli = p.id')
			),
			'where' => "ro.deleted is null or ro.deleted = 0",
			'orderby' => 'ro.id desc',
			'where' => "ro.createdby = '" . $user . "' and ro.createddate = '" . $now . "'"
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		// echo '<pre>';
		// print_r($result);die;
		echo json_encode(array(
			'data' => $result
		));
	}
	
	public function getListReturItem($retur_id)
	{
		// echo $day;die;
		// $user = "3";
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' ro',
			'field' => array('rti.*', 'isa.status', 'p.nama as nama_pembeli', 
			'sk.kategori as kategori_stok', 'gd.nama_gudang', 'st.nama_satuan', 'ps.harga as harga_produk', 'pr.product'),
			'join' => array(
				array('(select max(id) id, `retur_barang` from retur_barang_status group by `retur_barang`) iss', 'iss.retur_barang = ro.id'),
				array('retur_barang_status isa', 'isa.id = iss.id'),
				array('pembeli p', 'ro.pembeli = p.id'),
				array('retur_barang_item rti', 'rti.retur_barang = ro.id'),
				array('stok_kategori sk', 'sk.id = rti.stok_kategori'),
				array('gudang gd', 'gd.id = rti.gudang'),
				array('product_satuan ps', 'ps.id = rti.product_satuan'),
				array('satuan st', 'st.id = ps.satuan'),
				array('product pr', 'pr.id = ps.product'),
			),
			'where' => "ro.deleted is null or ro.deleted = 0",
			'orderby' => 'ro.id desc',
			'where' => "ro.id = '" . $retur_id . "'"
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		// echo '<pre>';
		// print_r($result);die;
		echo json_encode(array(
			'data' => $result
		));
	}

	public function getPostDataHeader()
	{
		$data['no_retur'] = Modules::run('no_generator/generateNoReturBarang');
		$data['pembeli'] = $_POST['pembeli'];
		$data['tanggal_retur'] = date('Y-m-d');
		$data['total'] = str_replace('.', '', $_POST['total']);
		$data['createdby'] = $_POST['user'];
		$data['createddate'] = date('Y-m-d');
		return $data;
	}

	public function prosesSimpan()
	{
		$is_valid = "0";

		$data_item = json_decode($_POST['data_item']);
		// echo '<pre>';
		// print_r($data_item);
		// die;
		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader();
			$this->db->insert($this->getTableName(), $post_data);
			// echo '<pre>';
			// echo $this->db->last_query();die;
			$id = $this->db->insert_id();

			//invoice product item    
			if (!empty($data_item)) {
				foreach ($data_item as $value) {
					$post_item['retur_barang'] = $id;
					$post_item['product_satuan'] = $value->product_satuan;
					$post_item['stok_kategori'] = $value->jenis_stok;
					$post_item['qty'] = $value->qty;
					$post_item['gudang'] = $value->gudang_id;
					$post_item['sub_total'] = str_replace(',', '', $value->sub_total);

					Modules::run('database/_insert', 'retur_barang_item', $post_item);


					//insert into product_log_stock
					if ($value->jenis_stok == '1') {
						$current_satuan = Modules::run('retur_pelanggan/getDetailCurrentSatuan', $value->product_satuan);
						if (!empty($current_satuan)) {
							$stock_barang_id = $current_satuan['cur_product_stock'];
							$stok_current = $current_satuan['stock'];
							$total_stok = $stok_current + $value->qty;							
							Modules::run('database/_update', 'product_stock', array('stock' => $total_stok), array('id' => $stock_barang_id));
						}
					}


					$post_log_stok['product_satuan'] = $value->product_satuan;
					$post_log_stok['status'] = 'RETUR';
					$post_log_stok['stok_kategori'] = $value->jenis_stok;
					$post_log_stok['qty'] = $value->qty;
					$post_log_stok['keterangan'] = 'Retur Barang Customer';
					Modules::run('database/_insert', 'product_log_stock', $post_log_stok);
				}
			}



			//retur barang status
			$post_status['retur_barang'] = $id;
			$post_status['status'] = 'DRAFT';
			Modules::run('database/_insert', 'retur_barang_status', $post_status);

			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function searchProduk()
	{
		$keyword = trim($_POST['keyword']);
		$data = Modules::run('database/get', array(
			'table' => 'product p',
			'field' => array(
				'p.*', 's.nama_satuan',
				'ps.harga', 'ps.id as product_satuan'
			),
			'join' => array(
				array('product_satuan ps', 'p.id = ps.product'),
				array('satuan s', 's.id = ps.satuan'),
			),
			'where' => "(p.deleted is null or p.deleted = 0 and ps.deleted = 0) 
			and (p.kode_product like '%" . $keyword . "%' or p.product like '%" . $keyword . "%') "
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['val'] = $value['product'] . ' - ' . $value['nama_satuan'] . ' - ' . number_format($value['harga']);
				$value['harga_str'] = number_format($value['harga']);
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}


	public function searchCustomer()
	{
		$keyword = trim($_POST['keyword']);
		// $keyword = "sum";
		$data = Modules::run('database/get', array(
			'table' => 'pembeli p',
			'field' => array('p.*'),
			'like' => array(
				array("p.nama", $keyword)
			),
			'inside_brackets' => true,
			'is_or_like' => true,
			'where' => "p.deleted is null or p.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}
}
