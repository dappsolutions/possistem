<?php

class Dashboard extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Dashboard';
	}

	public function getTableName()
	{
		return 'order';
	}

	public function index()
	{
		echo 'Dashboard';
	}

	public function getTotalPenjualan()
	{
		$user = isset($_POST['user']) ? $_POST['user'] : '';
		$now = date('Y-m-d');
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' o',
			'field' => array('sum(o.total) as total'),
			'join' => array(
				array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
				array('order_status isa', 'isa.id = iss.id'),
			),
			'where' => "o.deleted is null or o.deleted = 0 and isa.status = 'DRAFT' 
		and (o.createdby = '" . $user . "' or o.createdby is null) and o.tanggal_faktur = '" . $now . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				if ($value['total'] == '') {
					$value['total'] = "0";
				} else {
					$value['total'] = number_format($value['total']);
				}
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}

	public function getTotalReturBarang()
	{
		$user = isset($_POST['user']) ? $_POST['user'] : '6';
		$now = date('Y-m-d');
		// echo $now;die;
		$data = Modules::run('database/get', array(
			'table' => 'retur_barang ro',
			'field' => array('sum(rpi.qty) as total'),
			'join' => array(
				array('(select max(id) id, `retur_barang` from retur_barang_status group by `retur_barang`) iss', 'iss.retur_barang = ro.id'),
				array('retur_barang_status isa', 'isa.id = iss.id'),
				array('pembeli p', 'ro.pembeli = p.id'),
				array('retur_barang_item rpi', 'ro.id = rpi.retur_barang'),
			),
			'where' => "ro.deleted is null or ro.deleted = 0",
			'orderby' => 'ro.id desc',
			'where' => "ro.createdby = '".$user."' and ro.createddate = '".$now."'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				if ($value['total'] == '') {
					$value['total'] = "0";
				}
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}
}
