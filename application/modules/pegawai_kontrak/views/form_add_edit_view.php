<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Pegawai
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="pegawai" error="Pegawai">
       <option value="">Pilih Pegawai</option>
       <?php if (!empty($list_pegawai)) { ?>
        <?php foreach ($list_pegawai as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($pegawai)) { ?>
          <?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Awal
     </div>
     <div class='col-md-3'>
      <input type='text' readonly="" name='' id='tanggal_awal' class='form-control required' 
             value='<?php echo isset($tanggal_awal) ? $tanggal_awal : '' ?>' error="Tanggal Awal"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Akhir
     </div>
     <div class='col-md-3'>
      <input type='text' readonly="" name='' id='tanggal_akhir' class='form-control required' 
             value='<?php echo isset($tanggal_akhir) ? $tanggal_akhir : '' ?>' error="Tanggal Akhir"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="PegawaiKontrak.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="PegawaiKontrak.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
