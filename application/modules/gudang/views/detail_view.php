<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Kode
     </div>
     <div class='col-md-3'>
      <?php echo $kode ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Gudang
     </div>
     <div class='col-md-3'>
      <?php echo $nama_gudang ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Panjang (m)
     </div>
     <div class='col-md-3'>
      <?php echo $panjang ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Lebar (m)
     </div>
     <div class='col-md-3'>
      <?php echo $lebar ?>
     </div>     
    </div>
    <br/>
   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tinggi (m)
     </div>
     <div class='col-md-3'>
      <?php echo $tinggi ?>
     </div>     
    </div>
    <br/>
   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Maksimal Tonase (Kg)
     </div>
     <div class='col-md-3'>
      <?php echo $max_tonase ?>
     </div>     
    </div>
    <br/>
    
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="Gudang.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
