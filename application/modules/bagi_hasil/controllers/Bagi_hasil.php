<?php

class Bagi_hasil extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'bagi_hasil';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/min/moment.min.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/bagi_hasil.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pembeli';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Bagi Hasil";
  $data['title_content'] = 'Data Bagi Hasil';
  $data['kas'] = $this->getDataKasMasuk();
  $data['faktur'] = $this->getDataFakturTotalMasuk();
  $data['tagihan'] = $this->getDataTagihan();
  $data['vendor'] = $this->getDataBayarVendor();
  $data['lain'] = $this->getDataBayarLain();
  $data['internal'] = $this->getDataBagiHasil();

  $data['hasil_total'] = ($data['kas']['total'] + $data['faktur']['total']) -
          ($data['tagihan']['total'] + $data['vendor']['total'] + $data['lain']['total']);

  echo Modules::run('template', $data);
 }

 public function getData() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Bagi Hasil";
  $data['title_content'] = 'Data Bagi Hasil';
  $data['kas'] = $this->getDataKasMasuk();
  $data['faktur'] = $this->getDataFakturTotalMasuk();
  $data['tagihan'] = $this->getDataTagihan();
  $data['vendor'] = $this->getDataBayarVendor();
  $data['lain'] = $this->getDataBayarLain();
  $data['internal'] = $this->getDataBagiHasil();

  $data['hasil_total'] = ($data['kas']['total'] + $data['faktur']['total']) -
          ($data['tagihan']['total'] + $data['vendor']['total'] + $data['lain']['total']);

  echo $this->load->view('detail_data', $data, true);
 }

 public function getDataBagiHasil() {
  $where = "deleted is null or deleted = 0";
  if(!empty($_POST)){
   list($awal, $akhir) = explode(' - ', $_POST['keyword']);
   $awal = date('Y-m-d', strtotime($awal));
   $akhir = date('Y-m-d', strtotime($akhir));
   $where = "deleted is null or deleted = 0 and (createddate >= '".$awal."' and createddate <= '".$akhir."')";
  }
  $data = Modules::run('database/get', array(
              'table' => 'kerja_sama_internal',
              'where' => $where
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getTotalDataLabaRugi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.no_hp', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
  ));

  return $total;
 }

 public function getDataFakturTotalMasuk() {
   $keyword = date('Y');
  $where = "WHERE p.createddate LIKE '%".$keyword."%' OR pha.tgl_angsuran LIKE '%" . $keyword . "%' OR phc.tgl_bayar LIKE '%" . $keyword . "%'";
  if (!empty($_POST)) {
   list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
   $awal = date('Y-m-d', strtotime($awal));
   $akhir = date('Y-m-d', strtotime($akhir));
   $where = "WHERE (p.createddate >= '" . $awal . "' and p.createddate <= '" . $akhir . "') OR (pha.tgl_angsuran >= '" . $awal . "' and pha.tgl_angsuran <= '" . $akhir . "') OR (phc.tgl_bayar >= '" . $awal . "' and phc.tgl_bayar <= '" . $akhir . "')";
  }

  $query = <<<QUERY
  SELECT 
	p.id AS pembelian 
	, QUARTER(p.createddate) AS quartal
	, p.no_invoice
	, pm.nama AS nama_pembeli
	, sp.status AS status_beli
	, r.product
	, rhhjc.harga AS harga_cash
	, rhhjk.harga AS harga_kredit
	, phr.id AS pembeli_has_product
	, pha.total_bayar AS bayar_angsuran
	, phc.total_bayar AS bayar_cash
FROM pembelian p
JOIN pembeli_has_product phr ON p.id = phr.pembelian
JOIN pembeli pm ON phr.pembeli = pm.id
JOIN status_pembelian sp ON phr.status_pembelian = sp.id
JOIN product r ON phr.product = r.id
LEFT JOIN product_has_harga_jual_pokok rhhjc ON r.id = rhhjc.product AND rhhjc.period_end IS NULL
LEFT JOIN product_has_harga_jual_tunai rhhjk ON r.id = rhhjk.product AND rhhjk.period_end IS NULL
JOIN pembayaran_product pr ON phr.id = pr.pembeli_has_product
LEFT JOIN pembayaran_has_cash phc ON pr.id = phc.pembayaran_product
LEFT JOIN pembayaran_has_angsuran pha ON pr.id = pha.pembayaran_product
$where
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['bayar_angsuran'] == '' ? $value['bayar_cash'] : $value['bayar_angsuran'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;
  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataKasMasuk() {
  $keyword = date('Y');
  $where = "WHERE k.createddate LIKE '%".$keyword."%'";
  if(!empty($_POST)){
   list($awal, $akhir) = explode(' - ', $_POST['keyword']);
   $awal = date('Y-m-d', strtotime($awal));
   $akhir = date('Y-m-d', strtotime($akhir));
   $where = "WHERE k.createddate >= '".$awal."' and k.createddate <= '".$akhir."'";
  }

  $query = <<<QUERY
  SELECT 
k.jumlah 
, QUARTER(k.createddate) AS quartal
FROM kas k
$where
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['jumlah'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;

  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataTagihan() {
  $keyword = date('Y');
  $where = "WHERE pt.createddate LIKE '%".$keyword."%'";
  if (!empty($_POST)) {
   list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
   $awal = date('Y-m-d', strtotime($awal));
   $akhir = date('Y-m-d', strtotime($akhir));
   $where = "WHERE pt.createddate >= '".$awal."' and pt.createddate <= '".$akhir."'";
  }
  $query = <<<QUERY
  
SELECT 
pt.total
, QUARTER(pt.createddate) AS quartal
FROM pembayaran_tagihan pt
$where
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['total'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;
  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataBayarVendor() {
   $keyword = date('Y');
  $where = "WHERE pv.createddate LIKE '%".$keyword."%'";
  if (!empty($_POST)) {
   list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
   $awal = date('Y-m-d', strtotime($awal));
   $akhir = date('Y-m-d', strtotime($akhir));
   $where = "WHERE pv.createddate >= '".$awal."' and pv.createddate <= '".$akhir."'";
  }
  $query = <<<QUERY
    SELECT 
    pv.total
    , QUARTER(pv.createddate) AS quartal
    FROM pembayaran_vendor pv
    $where
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['total'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;
  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataBayarLain() {
  $keyword = date('Y');
  $where = "WHERE pl.createddate LIKE '%".$keyword."%'";
  if (!empty($_POST)) {
   list($awal, $akhir) = explode(' - ', $this->input->post('keyword'));
   $awal = date('Y-m-d', strtotime($awal));
   $akhir = date('Y-m-d', strtotime($akhir));
   $where = "WHERE pl.createddate >= '".$awal."' and pl.createddate <= '".$akhir."'";
  }
  $query = <<<QUERY
    
    SELECT 
    pl.total
    , QUARTER(pl.createddate) AS quartal
    FROM pembayaran_lain_lain pl
    $where
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['total'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;
  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataLabaRugi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.no_hp', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataLabaRugi($keyword)
  );
 }

 public function getDetailDataLabaRugi($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah LabaRugi";
  $data['title_content'] = 'Tambah LabaRugi';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataLabaRugi($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah LabaRugi";
  $data['title_content'] = 'Ubah LabaRugi';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataLabaRugi($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail LabaRugi";
  $data['title_content'] = 'Detail LabaRugi';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['nama'] = $value->nama;
  $data['no_hp'] = $value->no_hp;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $tipe_product = $id;
  $this->db->trans_begin();
  try {
   $post_tipe_product = $this->getPostDataHeader($data);
   if ($id == '') {
    $tipe_product = Modules::run('database/_insert', $this->getTableName(), $post_tipe_product);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_tipe_product, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'tipe_product' => $tipe_product));
 }

}
