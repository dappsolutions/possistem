<?php

class General extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'general';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/general.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'general';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data General";
  $data['title_content'] = 'Data General';
  $content = $this->getDataGeneral();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataGeneral($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('i.title', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' i',
              'field' => array('i.*'),
              'like' => $like,
              'is_or_like' => true,
              'where' => "i.deleted is null or i.deleted = 0"
  ));

  return $total;
 }

 public function getDataGeneral($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('i.title', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' i',
              'field' => array('i.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "i.deleted is null or i.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataGeneral($keyword)
  );
 }

 public function getDetailDataGeneral($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*', 'kr.title as nama_perusahaan'),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListPriveledge() {
  $data = Modules::run('database/get', array(
              'table' => 'priveledge',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah General";
  $data['title_content'] = 'Tambah General';
  $data['list_akses'] = $this->getListPriveledge();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataGeneral($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah General";
  $data['title_content'] = 'Ubah General';
  $data['list_akses'] = $this->getListPriveledge();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataGeneral($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail General";
  $data['title_content'] = 'Detail General';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['title'] = $value->title;
  $data['alamat'] = $value->alamat;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($_FILES);die;
  $id = $this->input->post('id');
  $file = $_FILES;

  $is_valid = false;
  $is_save = true;
  $message = "";
  $pengguna = $id;
  $this->db->trans_begin();
  try {
   $post_user = $this->getPostDataHeader($data);
   if ($id == '') {
    $pengguna = Modules::run('database/_insert', $this->getTableName(), $post_user);
   } else {
    //update

    if (!empty($file)) {
     if ($data->file_str != $file['file']['name']) {
      $response_upload = $this->uploadData('file');
      if ($response_upload['is_valid']) {
       $post_user['logo'] = $file['file']['name'];
      } else {
       $is_save = false;
       $message = $response_upload['response'];
      }
     }
    }
    if ($is_save) {
     Modules::run('database/_update', $this->getTableName(), $post_user, array('id' => $id));
    }
   }

   if ($is_save) {
    $this->db->trans_commit();
    $is_valid = true;
   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'pengguna' => $pengguna, 'message' => $message));
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'user', array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data General";
  $data['title_content'] = 'Data General';
  $content = $this->getDataGeneral($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/general/';
  $config['allowed_types'] = 'png|jpg|jpeg';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);

  $is_valid = false;
  if (!$this->upload->do_upload($name_of_field)) {
   $response = $this->upload->display_errors();
  } else {
   $response = $this->upload->data();
   $is_valid = true;
  }

  return array(
      'is_valid' => $is_valid,
      'response' => $response
  );
 }

 public function showLogo() {
  $foto = str_replace(' ', '_', $this->input->post('foto'));
  $data['foto'] = $foto;
//  echo '<pre>';
//  print_r($data);die;
  echo $this->load->view('foto', $data, true);
 }

}
