<?php

class Dashboard extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function getModuleName()
	{
		return 'dashboard';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/faktur_pelanggan.js"></script>'
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'v_index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Dashboard";
		$data['title_content'] = 'Dashboard';
		$data['penjualan'] = $this->getTopPenjualan();
		$data['total_pj'] = $this->getPenjualan();
		$data['total_pemasukan'] = $this->getTotalPemasukan();
		$data['tagihan'] = Modules::run('laba_rugi/getDataTagihan');
		$data['vendor'] = Modules::run('laba_rugi/getDataBayarVendor');
		$data['lain'] = Modules::run('laba_rugi/getDataBayarLain');
		$data['total_product'] = count($this->getTotalProduct());
		$data['total_customer'] = count($this->getTotalCustomer());
		$data['total_user'] = count($this->getTotalUser());
		$data['data_penjualan'] = $this->getDataGrafikPenjualan();
		$data['data_kas_kredit'] = $this->getDataGrafikKasKredit();
		$data['data_kas_debit'] = $this->getDataGrafikKasDebit();
		$data['year'] = date('Y');
		$data['pembelian'] = $this->getDataPembelianBarang();
		$data['summary_penjualan'] = $this->getDataSummaryPenjualan($data);
		// echo '<pre>';
		// print_r($data['summary_penjualan']);
		// die;
		$data['date_now'] = Modules::run('helper/getIndoDate', date('Y-m-d'), false);
		echo Modules::run('template', $data);
	}

	public function getDataPembelianBarang()
	{
		$filter_tgl = "";
		$filter_date_now = "and p.createddate like '%" . date('Y') . "%'";
		if (isset($_POST['keyword'])) {
			list($date_awal, $date_akhir) = explode('-', $_POST['keyword']);
			$date_awal = date('Y-m-d', strtotime(trim($date_awal)));
			$date_akhir = date('Y-m-d', strtotime(trim($date_akhir)));
			$filter_tgl = "and p.createddate BETWEEN '" . $date_awal . "' and '" . $date_akhir . "'";
			$filter_date_now = "";
		}

		$sql = "	select 
		(pi.harga * pi.qty) as harga_beli_total
		, QUARTER(p.createddate) AS quartal
		from product_has_price_procurement phpr
		join procurement_item pi 
			on pi.id = phpr.procurement_item
			and pi.deleted = 0
		join procurement p
			on p.id = pi.procurement 
		where p.deleted = 0 " . $filter_date_now . " " . $filter_tgl . " ";

		// echo '<pre>';
		// echo $sql;
		// die;

		$data = $this->db->query($sql);

		// echo '<pre>';
		// print_r($data);
		// die;

		$result['harga_beli_fix'] = 0;
		$result['harga_beli']['satu'] = 0;
		$result['harga_beli']['dua'] = 0;
		$result['harga_beli']['tiga'] = 0;
		$result['harga_beli']['empat'] = 0;
		$invoice = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				$result['harga_beli_fix'] += $value['harga_beli_total'];
				switch ($value['quartal']) {
					case '1':
						$result['harga_beli']['satu'] += $value['harga_beli_total'];
						break;
					case '2':
						$result['harga_beli']['dua'] += $value['harga_beli_total'];
						break;
					case '3':
						$result['harga_beli']['tiga'] += $value['harga_beli_total'];
						break;
					case '4':
						$result['harga_beli']['empat'] += $value['harga_beli_total'];
						break;

					default:
						# code...
						break;
				}
			}
		}

		// echo '<pre>';
		// print_r($result);
		// die;

		return $result;
	}

	public function getDataSummaryPenjualan($params = array())
	{
		$filter_date_now = "and p.createddate like '%" . date('Y') . "%'";

		$sql = "
	
		select 
		distinct
		i.no_faktur 
		,pi.jumlah_bayar 
		,(ps.harga_beli * ip.qty) as harga_beli_total 
		, ip.sub_total as total_faktur
		, isa.jumlah  as sisa_hutang
		, ist.status 
		from payment p
		join payment_item pi
			on p.id = pi.payment 
		join invoice i
			on i.id = pi.invoice 
		join metode_bayar mb
			on mb.id = i.metode_bayar 
		join invoice_product ip
			on ip.invoice = i.id 
		join product_satuan ps
			on ps.id = ip.product_satuan
		join (select max(id) id, invoice from invoice_sisa group by invoice) isa_group
			on isa_group.invoice = i.id 
		join invoice_sisa isa
			on isa.id = isa_group.id 	
		join (select max(id) id, invoice from invoice_status group by invoice) ist_group
			on ist_group.invoice = i.id 
		join invoice_status ist
			on ist.id = ist_group.id
		where p.deleted = 0 
		" . $filter_date_now . "
		order by p.id desc";

		$data = $this->db->query($sql);

		$result['laba'] = 0;
		$result['rugi'] = 0;
		$result['hutang'] = 0;
		$result['pemasukan'] = 0;
		$result['total_faktur'] = 0;
		$result['hutang_terbayar'] = 0;
		$result['harga_beli'] = 0;
		$result['total_transaksi'] = 0;
		$invoice = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				if (!in_array($value['no_faktur'], $invoice)) {
					$result['total_transaksi'] += 1;
					if ($value['status'] == 'PAID') {
						$total_faktur = $value['total_faktur'];
						$total = $total_faktur - $value['harga_beli_total'];
						$result['total_faktur'] += $total_faktur;
						if ($total < 0) {
							$total *= -1;
							$result['rugi'] += $total;
						} else {
							$result['laba'] += $total;
						}
					} else {
						$total = $value['sisa_hutang'];
						$result['hutang'] += $total;

						$result['hutang_terbayar'] += $value['jumlah_bayar'];
					}

					$total_faktur_pemasukan = $value['total_faktur'];
					$result['pemasukan'] += $total_faktur_pemasukan;

					$result['harga_beli'] += $value['harga_beli_total'];
					$invoice[] = $value['no_faktur'];
				}
			}
		}

		// echo '<pre>';
		// print_r($result);
		// die;

		$perhitungan_total = ($result['hutang_terbayar'] + $result['total_faktur']) - $params['pembelian']['harga_beli_fix'];
		$result['laba'] = $perhitungan_total > 0 ? $perhitungan_total : 0;
		$result['rugi'] = $perhitungan_total < 0 ? $perhitungan_total * -1 : 0;

		return $result;
	}

	public function getTotalProduct()
	{
		$data = Modules::run('database/get', array(
			'table' => 'product',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getTotalCustomer()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pembeli',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getTotalUser()
	{
		$data = Modules::run('database/get', array(
			'table' => 'user',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getPenjualan()
	{
		$date = date('Y');
		$query = <<<QUERY
  
select sum(total) as total from invoice where tanggal_faktur like '%$date%' and deleted = 0
QUERY;

		$data = Modules::run('database/get_custom', $query);

		$total = 0;
		if (!empty($data)) {
			$total = $data->row_array()['total'];
		}

		$query = <<<QUERY
  
select count(*) as jumlah from invoice where tanggal_faktur like '%$date%' and deleted = 0
QUERY;

		$data = Modules::run('database/get_custom', $query);

		$unit = 0;
		if (!empty($data)) {
			$unit = $data->row_array()['jumlah'];
		}

		return array(
			'unit' => $unit,
			'total' => $total
		);
	}

	public function getDataGrafikPenjualan()
	{

		$date = date('Y') . '-';

		$result = array();
		$total = 0;
		$total_max = 0;
		for ($i = 1; $i < 12; $i++) {
			$i = $i < 10 ? '0' . $i : $i;
			$date_str = $date . $i;
			$query = "SELECT * FROM invoice WHERE createddate like '%" . $date_str . "%' and deleted = 0";

			$data = Modules::run("database/get_custom", $query);


			$result[] = empty($data) ? 0 : count($data->result_array());
			$total += empty($data) ? 0 : count($data->result_array());

			if (!empty($data)) {
				if ($total_max > count($data->result_array())) {
					$total_max = count($data->result_array());
				}
			}
		}
		$str = implode(',', $result);
		return array(
			'data' => $str,
			'total' => $total_max
		);
	}

	public function getDataGrafikKasKredit()
	{

		$date = date('Y') . '-';

		$result = array();
		$total = 0;
		$total_max = 0;
		for ($i = 1; $i < 12; $i++) {
			$i = $i < 10 ? '0' . $i : $i;
			$date_str = $date . $i;
			$query = "select js.id, c.keterangan  
	, ct.jenis
from jurnal_detail jd
join jurnal_struktur js
	on jd.jurnal_struktur = js.id
join feature f
	on js.feature = f.id
join coa c
	on c.id = js.coa
join coa_type ct
	on ct.id = js.coa_type
	where c.keterangan = 'Bank & Kas' and jd.deleted = 0 and 
 jd.createddate like '%" . $date_str . "%' and ct.jenis = 'KREDIT'";

			$data = Modules::run("database/get_custom", $query);
			//   echo "<pre>";
			//   echo $this->db->last_query();
			//   die;

			$result[] = empty($data) ? 0 : count($data->result_array());
			$total += empty($data) ? 0 : count($data->result_array());

			if (!empty($data)) {
				if ($total_max > count($data->result_array())) {
					$total_max = count($data->result_array());
				}
			}
		}
		$str = implode(',', $result);
		return array(
			'data' => $str,
			'total' => $total_max
		);
	}

	public function getDataGrafikKasDebit()
	{

		$date = date('Y') . '-';

		$result = array();
		$total = 0;
		$total_max = 0;
		for ($i = 1; $i < 12; $i++) {
			$i = $i < 10 ? '0' . $i : $i;
			$date_str = $date . $i;
			$query = "select js.id, c.keterangan  
	, ct.jenis
from jurnal_detail jd
join jurnal_struktur js
	on jd.jurnal_struktur = js.id
join feature f
	on js.feature = f.id
join coa c
	on c.id = js.coa
join coa_type ct
	on ct.id = js.coa_type
	where c.keterangan = 'Bank & Kas' and jd.deleted = 0 and 
 jd.createddate like '%" . $date_str . "%' and ct.jenis = 'DEBIT'";

			$data = Modules::run("database/get_custom", $query);
			//   echo "<pre>";
			//   echo $this->db->last_query();
			//   die;

			$result[] = empty($data) ? 0 : count($data->result_array());
			$total += empty($data) ? 0 : count($data->result_array());

			if (!empty($data)) {
				if ($total_max > count($data->result_array())) {
					$total_max = count($data->result_array());
				}
			}
		}
		$str = implode(',', $result);
		return array(
			'data' => $str,
			'total' => $total_max
		);
	}

	public function getTotalPemasukan()
	{
		$date = date('Y');
		$query = "select
	sum(i.total) as total
	from invoice i
	join (select max(id) id, invoice from invoice_status group by invoice) iss
		on iss.invoice = i.id
	join invoice_status isa on isa.id = iss.id
	where isa.status = 'PAID' and i.tanggal_faktur like '%$date%' and i.deleted = 0
";

		//  echo '<pre>';
		//  echo $query;die;
		$data = Modules::run('database/get_custom', $query);

		$total = 0;
		if (!empty($data)) {
			$total = $data->row_array()['total'];
		}
		//  echo $total;die;
		return $total;
	}

	public function getTopPenjualan($keyword = '')
	{
		$data = Modules::run('database/get', array(
			'table' => 'invoice i',
			'field' => array(
				'i.*', 'p.nama as nama_pembeli',
				'p.no_hp', 'p.alamat', 'isa.status'
			),
			'join' => array(
				array('pembeli p', 'i.pembeli = p.id'),
				array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'i.id = iss.invoice'),
				array('invoice_status isa', 'isa.id= iss.id'),
			),
			'limit' => 5,
			'orderby' => 'i.id desc',
			'where' => 'i.deleted = 0'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataPenjualanStr()
	{
		$date = date('Y-m') . '-';

		$result = array();
		$total = 0;
		for ($i = 1; $i < 32; $i++) {
			$i = $i < 10 ? '0' . $i : $i;
			$date_str = $date . $i;
			$query = "SELECT * FROM pembelian WHERE createddate = '" . $date_str . "'";

			$data = Modules::run("database/get_custom", $query);


			$result[] = empty($data) ? 0 : count($data->result_array());
			$total += empty($data) ? 0 : count($data->result_array());
		}
		$str = implode(',', $result);
		return array(
			'data' => $str,
			'total' => $total
		);
	}

	public function getDataKredit()
	{
		$date = date('Y') . '-';

		$result = array();
		$total = 0;
		for ($i = 1; $i < 13; $i++) {
			$i = $i < 10 ? '0' . $i : $i;
			$date_str = $date . $i;
			$query = "SELECT * FROM pembayaran_product WHERE createddate LIKE '%" . $date_str . "%'";

			$data = Modules::run("database/get_custom", $query);

			$result[] = empty($data) ? 0 : count($data->result_array());
			$total += empty($data) ? 0 : count($data->result_array());
		}
		$str = implode(',', $result);
		return array(
			'data' => $str,
			'total' => $total
		);
	}
}
