<input type="hidden" value="<?php echo date('Y') ?>" id="tahun" class="form-control" />
<input type="hidden" value="<?php echo $data_penjualan['data'] ?>" id="data-penjualan" class="form-control" />
<input type="hidden" value="<?php echo $data_penjualan['total'] ?>" id="total-data-penjualan" class="form-control" />

<input type="hidden" debit="<?php echo $data_kas_debit['data'] ?>" kredit="<?php echo $data_kas_kredit['data'] ?>" id="data-kas" class="form-control" />
<input type="hidden" total="<?php echo $data_kas_kredit['total'] ?>" id="total-data-kas" class="form-control" />

<div class="row">
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h5><?php echo  'Rp, ' . number_format($summary_penjualan['pemasukan'], 2, ',', '.') ?></h5>

				<p>Total Penjualan</p>
			</div>
			<div class="icon">
				<i class="ion ion-person"></i>
			</div>
			<a href="<?php echo base_url() . 'faktur_pelanggan' ?>" class="small-box-footer"><?php echo $summary_penjualan['total_transaksi'] ?> Transaksi <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				<h5><?php echo  'Rp, ' . number_format($summary_penjualan['hutang'], 2, ',', '.') ?></h5>

				<p>Total Hutang Customer</p>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="<?php echo base_url() . 'faktur_pelanggan' ?>" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-yellow">
			<div class="inner">
				<h5><?php echo  'Rp, ' . number_format($summary_penjualan['laba'], 2, ',', '.') ?></h5>

				<p>Total Laba Bersih</p>
			</div>
			<div class="icon">
				<i class="ion ion-android-menu"></i>
			</div>
			<a href="<?php echo base_url()  ?>" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				<h5><?php echo  'Rp, ' . number_format($summary_penjualan['rugi'], 2, ',', '.') ?></h5>

				<p>Total Rugi</p>
			</div>
			<div class="icon">
				<i class="ion ion-pie-graph"></i>
			</div>
			<a href="<?php echo base_url() . 'pelanggan' ?>" class="small-box-footer">Customer <?php echo $total_customer ?> <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
</div>
