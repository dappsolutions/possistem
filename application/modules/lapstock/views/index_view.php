<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
<!--     <div class="col-md-4">
      <input type="text" id="tanggal" class="form-control" readonly>
					</div>-->
					<div class="col-md-8">
<!--      <button id="tampil" class="btn btn-primary" onclick="LapStock.tampilkan(this)">Tampilkan</button>
      &nbsp;-->
      <a class="btn btn-success" download="<?php echo 'Laporan Stok' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Stok');">Export</a>
					</div>
    </div>
    <br/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-striped table-bordered table-list-draft" id="tb_laporan">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>Produk</th>
          <th>Tipe Produk</th>
          <th>Satuan</th>
          <th>Gudang</th>
          <th>Rak</th>
          <th>Stok</th>
          <th>Stok Keluar</th>
          <th>Stok Retur</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($data_stok)) { ?>
          <?php $no =  1; ?>
          <?php foreach ($data_stok as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['kode_product'] . ' - ' . $value['nama_product'] ?></td>
            <td><?php echo $value['tipe_produk'] ?></td>
            <td><?php echo $value['nama_satuan'] ?></td>
            <td><?php echo $value['nama_gudang'] ?></td>
            <td><?php echo $value['nama_rak'] ?></td>
            <td class="text-right"><?php echo $value['stock'] ?></td>
            <td class="text-right"><?php echo $value['stok_keluar'] == '' ? 0 : $value['stok_keluar'] ?></td>
            <td class="text-right"><?php echo $value['stok_retur'] == '' ? 0 : $value['stok_retur'] ?></td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td colspan="10" class="text-center">Tidak ada data ditemukan</td>
          </tr>
         <?php } ?>

        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>

  </div>
 </div>
</div>
