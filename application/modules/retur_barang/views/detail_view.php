<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Retur
     </div>
     <div class='col-md-3'>
      <?php echo $no_retur ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Pelanggan
     </div>
     <div class='col-md-3'>
      <?php echo $nama_pembeli ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Retur
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_retur ?>
     </div>     
    </div>
    <br/>     
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft" id="tb_product">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>Produk</th>
          <th>Jumlah</th>
          <th>Kategori Stok</th>
          <th>Alokasi Gudang</th>
          <th>Sub Total</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($retur_item)) { ?>
          <?php foreach ($retur_item as $value) { ?>
           <tr> 
            <td><?php echo $value['kode_product'].'-'.$value['nama_product'] . '-' . $value['nama_satuan'] . '-[' . number_format($value['harga']) . ']' ?></td>
            <td><?php echo $value['qty'] ?></td>
            <td><?php echo $value['kategori_stok'] ?></td>
            <td><?php echo $value['nama_gudang'] ?></td>
            <td><?php echo number_format($value['sub_total']) ?></td>
           </tr>
          <?php } ?>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-12 text-right">
      <h4>Total : Rp, <label id="total"><?php echo number_format($total) ?></label></h4>
     </div>
    </div>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger" onclick="ReturBarang.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="ReturBarang.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
