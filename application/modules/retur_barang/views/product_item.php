<td>
 <select class="form-control required" id="product<?php echo $index ?>" 
         error="Produk" onchange="ReturBarang.hitungSubTotal(this, 'product')">
  <option value="" harga="0">Pilih Produk</option>
  <?php if (!empty($list_product)) { ?>
   <?php foreach ($list_product as $value) { ?>
    <?php $selected = '' ?>
    <option harga="<?php echo $value['harga'] ?>" <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_product'].'-'.$value['nama_product'] . '-' . $value['nama_satuan'] . '-[Rp, ' . number_format($value['harga']) . ']' ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>
<td class="text-center">
 <input type="number" value="1" min="1" id="jumlah" 
        class="form-control text-right" 
        onkeyup="ReturBarang.hitungSubTotal(this, 'jumlah')" 
        onchange="ReturBarang.hitungSubTotal(this, 'jumlah')"/>
</td>      
<td class="text-center">
 <select class="form-control required" id="stok_kategori" error="Kategori Stok">
  <?php if (!empty($list_stok)) { ?>
   <?php foreach ($list_stok as $value) { ?>
    <?php $selected = '' ?>
    <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>  
<td class="text-center">
 <select class="form-control required" id="gudang" error="Gudang">
  <?php if (!empty($list_gudang)) { ?>
   <?php foreach ($list_gudang as $value) { ?>
    <?php $selected = '' ?>
    <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_gudang'] ?></option>
   <?php } ?>
  <?php } ?>
 </select>
</td>  
<td class="text-center">
 <label id="sub_total">0</label>
</td>      
<td class="text-center">
 <i class="mdi mdi-delete mdi-18px" onclick="ReturBarang.deleteItem(this)"></i>
</td>


<script>
 $(function () {
  $("#product<?php echo $index ?>").select2();
  $("#metode<?php echo $index ?>").select2();
  $("#pajak<?php echo $index ?>").select2();
 });
</script>