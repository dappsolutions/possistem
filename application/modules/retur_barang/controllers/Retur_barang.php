<?php

class Retur_barang extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'retur_barang';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/retur_barang.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'retur_barang';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Retur";
  $data['title_content'] = 'Data Retur';
  $content = $this->getDataReturorder();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataReturorder($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('ro.no_retur', $keyword),
       array('ro.tanggal_retur', $keyword),
       array('pb.nama', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' ro',
              'field' => array('ro.*', 'isa.status', 'pb.nama as nama_pembeli'),
              'join' => array(
                  array('(select max(id) id, `retur_barang` from retur_barang_status group by `retur_barang`) iss', 'iss.retur_barang = ro.id'),
                  array('retur_barang_status isa', 'isa.id = iss.id'),
                  array('pembeli pb', 'pb.id = ro.pembeli'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "ro.deleted is null or ro.deleted = 0",
              'orderby' => 'ro.id desc'
  ));

  return $total;
 }

 public function getDataReturorder($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('ro.no_retur', $keyword),
       array('ro.tanggal_retur', $keyword),
       array('pb.nama', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' ro',
              'field' => array('ro.*', 'isa.status', 'pb.nama as nama_pembeli'),
              'join' => array(
                  array('(select max(id) id, `retur_barang` from retur_barang_status group by `retur_barang`) iss', 'iss.retur_barang = ro.id'),
                  array('retur_barang_status isa', 'isa.id = iss.id'),
                  array('pembeli pb', 'pb.id = ro.pembeli'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "ro.deleted is null or ro.deleted = 0",
              'orderby' => 'ro.id desc'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataReturorder($keyword)
  );
 }

 public function getDetailDataReturorder($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' rb',
              'field' => array('rb.*',
                  'p.nama as nama_pembeli', 'isa.status'),
              'join' => array(
                  array('pembeli p', 'rb.pembeli = p.id'),
                  array('(select max(id) id, `retur_barang` from retur_barang_status group by `retur_barang`) iss', 'iss.retur_barang = rb.id'),
                  array('retur_barang_status isa', 'isa.id = iss.id'),                  
              ),
              'where' => "rb.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListProduct() {
  $data = Modules::run('database/get', array(
              'table' => 'product_satuan ps',
              'field' => array('ps.*', 'p.product as nama_product', 's.nama_satuan', 'p.kode_product'),
              'join' => array(
                  array('product p', 'ps.product = p.id'),
                  array('satuan s', 'ps.satuan = s.id', 'left'),
              ),
              'where' => "ps.deleted = 0 or ps.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPelanggan() {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null and p.pembeli_kategori = 2"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPajak() {
  $data = Modules::run('database/get', array(
              'table' => 'pajak p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListStokKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'stok_kategori sk',
              'field' => array('sk.*'),
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListMetodeBayar() {
  $data = Modules::run('database/get', array(
              'table' => 'metode_bayar',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Retur";
  $data['title_content'] = 'Tambah Retur';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function bayar() {
  $data['view_file'] = 'form_bayar';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Bayar Returorder";
  $data['title_content'] = 'Bayar Returorder';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataReturorder($id);
//  echo $data['total'];die;
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Returorder";
  $data['title_content'] = 'Ubah Returorder';
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['order_item'] = $this->getListInvoiceItem($id);
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataReturorder($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Retur";
  $data['title_content'] = 'Detail Retur';
  $data['retur_item'] = $this->getListReturBarangItem($id);
//  echo '<pre>';
//  print_r($data['retur_item']);die;
  echo Modules::run('template', $data);
 }

 public function getListReturBarangItem($retur_barang) {
  $data = Modules::run('database/get', array(
              'table' => 'retur_barang_item roi',
              'field' => array('roi.*', 'ps.satuan', 'ps.harga',
                  'p.product as nama_product', 's.nama_satuan', 
                  'sk.kategori as kategori_stok', 'gd.nama_gudang', 'p.kode_product'),
              'join' => array(
                  array('product_satuan ps', 'ps.id = roi.product_satuan'),
                  array('product p', 'p.id = ps.product'),
                  array('satuan s', 's.id = ps.satuan', 'left'),
                  array('stok_kategori sk', 'sk.id = roi.stok_kategori', 'left'),
                  array('gudang gd', 'roi.gudang = gd.id', 'left'),
              ),
              'where' => "roi.retur_barang = '" . $retur_barang . "' and roi.deleted = 0",
              'orderby' => 'roi.id'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPostDataHeader($value) {
  $data['no_retur'] = Modules::run('no_generator/generateNoReturBarang');
  $data['pembeli'] = $value->pembeli;
  $data['tanggal_retur'] = date('Y-m-d', strtotime($value->tanggal_faktur));
  $data['total'] = str_replace('.', '', $value->total);
  return $data;
 }

 public function penambahanStok($data_item) {
  $current_satuan = Modules::run('retur_pelanggan/getDetailCurrentSatuan', $data_item->product_satuan);

  if (!empty($current_satuan)) {
   
   $stock_barang_id = $current_satuan['cur_product_stock'];
   $stok_current = $current_satuan['stock'];

   $total_stok = $stok_current + $data_item->qty;
   Modules::run('database/_update', 'product_stock', array('stock' => $total_stok), array('id' => $stock_barang_id));
  }
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);

    //order product item    
    if (!empty($data->product_item)) {
     foreach ($data->product_item as $value) {
      $post_item['retur_barang'] = $id;
      $post_item['product_satuan'] = $value->product_satuan;
      $post_item['stok_kategori'] = $value->stok_kategori;
      $post_item['qty'] = $value->qty;
      $post_item['gudang'] = $value->gudang;
      $post_item['sub_total'] = str_replace(',', '', $value->sub_total);

      Modules::run('database/_insert', 'retur_barang_item', $post_item);

      //insert into product_log_stock
      if ($value->stok_kategori == '1') {
       $this->penambahanStok($value);
      }


      $post_log_stok['product_satuan'] = $value->product_satuan;
      $post_log_stok['status'] = 'RETUR';
      $post_log_stok['stok_kategori'] = $value->stok_kategori;
      $post_log_stok['qty'] = $value->qty;
      $post_log_stok['keterangan'] = 'Retur Barang Customer';
      Modules::run('database/_insert', 'product_log_stock', $post_log_stok);
     }
    }

    //retur barang status
    $post_status['retur_barang'] = $id;
    $post_status['status'] = 'DRAFT';
    Modules::run('database/_insert', 'retur_barang_status', $post_status);
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Retur ";
  $data['title_content'] = 'Data Retur';
  $content = $this->getDataReturorder($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getListGudang() {
  $data = Modules::run('database/get', array(
              'table' => 'gudang g',
              'field' => array('g.*'),
              'where' => "g.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function addItem() {
  $data['list_product'] = $this->getListProduct();
  $data['list_pelanggan'] = $this->getListPelanggan();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['list_stok'] = $this->getListStokKategori();
  $data['list_gudang'] = $this->getListGudang();
  $data['index'] = $_POST['index'];
  echo $this->load->view('product_item', $data, true);
 }

 public function getListBank() {
  $data = Modules::run('database/get', array(
              'table' => 'bank',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getMetodeBayar() {
  $data['list_bank'] = $this->getListBank();
  $data['index'] = $_POST['index'];
  echo $this->load->view('bank_akun', $data, true);
 }

 public function printReturorder($id) {
  $data['retur'] = $this->getDetailDataReturorder($id);
  $data['retur_item'] = $this->getListReturBarangItem($id);

//    echo '<pre>';
//  print_r($data);die;
  $mpdf = Modules::run('mpdf/getInitPdf');
  $data['self'] = Modules::run('general/getDetailDataGeneral', 1);
//  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('Retur Customer - ' . date('Y-m-d') . '.pdf', 'I');
 }

 public function cancelReturorder() {
  $order_id = $_POST['order_id'];
  $this->db->trans_begin();
  try {
   //order status
   $post_status['order'] = $order_id;
   $post_status['status'] = 'CANCEL';
   Modules::run('database/_insert', 'order_status', $post_status);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
