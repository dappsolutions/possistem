<?php

class Lacak_salesman extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'lacak_salesman';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRjkdMDx94jbFdDKloaSAgTbS1Q2fByng&callback=LacakSalesman.initSDK" async defer></script>',
      '<script src="https://www.gstatic.com/firebasejs/6.2.0/firebase-app.js"></script>',
      '<script src="https://www.gstatic.com/firebasejs/6.2.0/firebase-auth.js"></script>',
      '<script src="https://www.gstatic.com/firebasejs/6.2.0/firebase-firestore.js"></script>',
      '<script src="https://www.gstatic.com/firebasejs/6.2.0/firebase-database.js"></script>',
//      '<script src="https://www.gstatic.com/firebasejs/6.2.0/firebase.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/lacak_salesman.js"></script>',      
  );

  return $data;
 }

 public function getTableName() {
  return 'log_user_position';
 }

 public function index($sales = '0') {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Lacak Salesman";
  $data['title_content'] = 'Lacak Salesman';
  $data['list_user'] = $this->getListUser();

  $data['sales_id'] = $sales;
  echo Modules::run('template', $data);
 }

 public function getLogUserLocation($user) {
  if ($user !== '000') {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' ul',
               'where' => "ul.user = '" . $user . "'",
               'orderby' => 'ul.id desc'
   ));

//        echo "<pre>";
//        echo $this->db->last_query();
//        die;
  } else {
   $data = Modules::run('database/get', array(
               'table' => $this->getTableName() . ' ul'
   ));
  }

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }else{
   $post['user'] = $user;
   $post['lat'] = 0;
   $post['lng'] = 0;
   array_push($result, $post);
  }
//    echo '<pre>';
//    print_r($result);die;
//    var_dump($result);
//    return $result; 

  echo json_encode(array('is_valid' => true, 'result' => $result));
 }

 public function getListUser() {
  $data = Modules::run('database/get', array(
              'table' => 'user p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }
  //var_dump($result);
  return $result;
 }

}
