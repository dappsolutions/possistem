<select type="<?php echo $filter ?>" class="form-control required" error="Data" id="filter_data" multiple="">
 <option value="">Pilih</option>
 <?php if (!empty($list_filter)) { ?>
  <?php foreach ($list_filter as $value) { ?>
   <?php $selected = '' ?>
   <?php if (isset($value['hak_akses'])) { ?> 
    <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'].' - '.$value['hak_akses'] ?></option>
   <?php }else{ ?>    
    <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
   <?php } ?>    
  <?php } ?>
 <?php } ?>
</select>

