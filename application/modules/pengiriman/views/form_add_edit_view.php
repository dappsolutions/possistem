<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Berdasarkan
     </div>
     <div class='col-md-3'>
      <select class="form-control required"
              id="filter_by" error="Berdasarkan" onchange="Pengiriman.getFilterBy(this)">
       <option value="">Berdasarkan</option>
       <option value="sales">Sales</option>
       <option value="pelanggan">Pelanggan</option>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      
     </div>
     <div class='col-md-3' id="content_filter">
      
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Order
     </div>
     <div class='col-md-3'>
      <input type='text' readonly="" name='' id='tanggal_order' class='form-control required' 
             value='<?php echo isset($tanggal_order) ? $tanggal_order : '' ?>' error="Tanggal Order"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Kirim
     </div>
     <div class='col-md-3'>
      <input type='text' readonly="" name='' id='tanggal' class='form-control required' 
             value='<?php echo isset($tanggal_pengiriman) ? $tanggal_pengiriman : '' ?>' error="Tanggal"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Sopir
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='sopir' class='form-control required' 
             value='<?php echo isset($sopir) ? $sopir : '' ?>' error="Sopir"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Polisi
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='no_polisi' class='form-control required' 
             value='<?php echo isset($no_polisi) ? $no_polisi : '' ?>' error="No Polisi"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class="col-md-6">
      <u>Data Produk</u>
     </div>
     <div class="col-md-6 text-right">
      <button class="btn btn-primary" onclick="Pengiriman.getDataListProduct()">Tampilkan</button>
     </div>
    </div>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <div class="form-item">
       <?php echo $this->load->view('form_product'); ?>
      </div>      
     </div>
    </div>
    <br/>
    
    <div class="row">
     <div class="col-md-12 text-right">
      <h4>Total : Rp. <label id="total"><?php echo isset($total) ? number_format($total, 0, ',', '.') : '0' ?></label></h4>
     </div>
    </div>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Pengiriman.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Pengiriman.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
