<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-list-draft" id="tb_product">
				<thead>
					<tr class="bg-primary-light text-white">
						<!-- <th>Sales</th> -->
						<th>Produk</th>
						<!-- <th>Tanggal Order</th> -->
						<th>Satuan</th>
						<th>Harga</th>
						<th>Jumlah</th>
						<th>Sub Total</th>
					</tr>
				</thead>
				<tbody>
					<?php if (!empty($invoice_item)) { ?>
						<?php $index = 0; ?>
						<?php $temp = ''; ?>
						<?php foreach ($invoice_item as $value) { ?>
							<tr data_id="<?php echo $value['id'] ?>" product_satuan="<?php echo $value['product_satuan'] ?>">
								<!-- <td>
									<?php echo $value['sales'] ?>
								</td> -->
								<td>
									<?php echo $value['nama_product'] == $temp ? '' : $value['nama_product'] ?>
								</td>
								<!-- <td>
									<?php echo $value['tgl_order'] ?>
								</td> -->
								<td>
									<?php echo $value['satuan_str'] ?>
								</td>
								<td harga="" class="text-right">
									<?php echo $value['harga_str'] ?>
								</td>
								<td class="text-center">
									<?php echo $value['qty_str'] ?>
								</td>
								<td class="text-right"><?php echo 'Rp. ' . number_format($value['sub_total_str'], 0, ',', '.') ?></td>
							</tr>
							<?php $index += 1; ?>
							<?php $temp = $value['nama_product']; ?>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
