<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-header">
				<div class="row">
					<div class="col-md-10">
						<div class="box-box-title middle-left">
							<strong class="box-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
						</div>
					</div>
					<div class="col-sm-2 text-right"></div>
				</div>
			</div>
			<div class="box-body box-block">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Tipe Produk</u>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class='col-md-3'>
						Tipe Produk
					</div>
					<div class='col-md-3'>
						<?php echo $tipe ?>
					</div>
				</div>
				<br />
				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="TipeProduk.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
