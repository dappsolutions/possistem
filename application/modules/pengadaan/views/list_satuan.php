<div class="select_content">
	<div class="top">
		<select class="form-control required" id="satuan<?php echo $index ?>" error="Satuan" onchange="Pengadaan.hitungSubTotal(this, 'satuan')">
			<option value="">Pilih Satuan</option>
			<?php if (!empty($list_satuan)) { ?>
				<?php foreach ($list_satuan as $value) { ?>
					<?php $selected = '' ?>
					<option <?php echo $selected ?> konversi="<?php echo $value['konversi'] ?>" value="<?php echo $value['satuan'] ?>"><?php echo $value['nama_satuan'] ?></option>
				<?php } ?>
			<?php } ?>
		</select>
	</div>
	<div class="text-right" id="action_remove">

	</div>
</div>
