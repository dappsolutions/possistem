<?php

class Pengadaan extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'pengadaan';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/pengadaan.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'procurement';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Pengadaan";
		$data['title_content'] = 'Data Pengadaan';
		$content = $this->getDataPengadaan();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function getTotalDataPengadaan($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.no_faktur', $keyword),
				array('v.nama_vendor', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*', 'v.nama_vendor'),
			'join' => array(
				array('vendor v', 'v.id = p.vendor')
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => "p.deleted is null or p.deleted = 0"
		));

		return $total;
	}

	public function getDataPengadaan($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.no_faktur', $keyword),
				array('v.nama_vendor', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*', 'v.nama_vendor'),
			'join' => array(
				array('vendor v', 'v.id = p.vendor')
			),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
			'where' => "p.deleted is null or p.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataPengadaan($keyword)
		);
	}

	public function getDetailDataPengadaan($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' p',
			'field' => array('p.*', 'v.nama_vendor', 'v.alamat', 'ps.status'),
			'join' => array(
				array('vendor v', 'v.id = p.vendor'),
				array('(select max(id) id, procurement from procurement_status group by procurement) pss', 'pss.procurement = p.id'),
				array('procurement_status ps', 'ps.id = pss.id'),
			),
			'where' => "p.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListVendor()
	{
		$data = Modules::run('database/get', array(
			'table' => 'vendor',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Pengadaan";
		$data['title_content'] = 'Tambah Pengadaan';
		$data['list_vendor'] = $this->getListVendor();
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataPengadaan($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Pengadaan";
		$data['title_content'] = 'Ubah Pengadaan';
		$data['list_vendor'] = $this->getListVendor();
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = $this->getDetailDataPengadaan($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pengadaan";
		$data['title_content'] = 'Detail Pengadaan';
		$data['list_proc_item'] = $this->getProcItem($id);
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function getProcItem($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'procurement_item pi',
			'field' => array(
				'pi.*',
				'p.product as nama_product', 's.nama_satuan',
				'p.kode_product'
			),
			'join' => array(
				array('product p', 'pi.product = p.id'),
				array('satuan s', 'pi.satuan = s.id'),
			),
			'where' => "pi.deleted = 0 and pi.procurement = '" . $id . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getPostDataHeader($value)
	{
		$data['no_faktur'] = Modules::run('no_generator/generateNoFakturPengadaan');
		$data['vendor'] = $value->vendor;
		$data['tanggal'] = date('Y-m-d', strtotime($value->tanggal));
		$data['total'] = str_replace('.', '', $value->total);
		$data['keterangan'] = $value->keterangan;
		return $data;
	}

	public function updateProductSatuan($value, $procurment_id, $proc_item_id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'product_satuan ps',
			'field' => array(
				'ps.*',
				'pst.stock',
				'pst.id as product_stock'
			),
			'join' => array(
				array('product_stock pst', 'pst.product_satuan = ps.id'),
				array('satuan s', 's.id = ps.satuan'),
			),
			'where' => "ps.deleted = 0 and ps.product = '" . $value->product . "' and ps.satuan_terkecil = '1'"
		));

		// echo '<pre>';
		// echo $this->db->last_query();
		// die;

		if (!empty($data)) {
			$data = $data->row_array();
			$total_stock = $data['stock'] + ($value->jumlah * $value->konversi);
			$post['stock'] = $total_stock;
			Modules::run('database/_update', 'product_stock', $post, array('id' => $data['product_stock']));


			//insert into product_log_stock
			$post_log_stok['product_satuan'] = $data['id'];
			$post_log_stok['status'] = 'IN';
			$post_log_stok['stok_kategori'] = '1';
			$post_log_stok['qty'] = $value->jumlah * $value->konversi;
			$post_log_stok['keterangan'] = 'Pengadaan';
			Modules::run('database/_insert', 'product_log_stock', $post_log_stok);

			//update harga beli satuan
			// $post = array();
			// $post['harga_beli'] = $value->harga;
			// Modules::run('database/_update', 'product_satuan', $post, array('product' => $value->product, 'satuan' => $value->satuan));

			$params = array();
			$params['product_satuan'] = $data['id'];
			$params['procurement'] = $procurment_id;
			$params['procurement_item'] = $proc_item_id;
			Modules::run('database/_insert', 'product_has_price_procurement', $params);

			$this->generateStokProduct($data, $value);
		}
	}

	public function generateStokProduct($data, $params)
	{
		$data_db = Modules::run('database/get', array(
			'table' => 'product_stock ps',
			'field' => array('ps.*'),
			'join' => array(
				array('product_satuan pst', 'pst.id = ps.product_satuan'),
				array('product p', 'p.id = pst.product'),
			),
			'where' => "ps.deleted = 0 and ps.product_satuan = '" . $data['id'] . "'",
			'orderby' => "ps.id desc"
		));

		// echo '<pre>';
		// print_r($this->db->last_query());
		// die;

		$result = array();
		$stok = 0;
		if (!empty($data_db)) {
			$data_db = $data_db->row_array();
			$stok = $data_db['stock'];
		}

		$post['stock'] = $stok + ($params->jumlah * $params->konversi);
		$post['product'] = $params->product;
		$post['product_satuan'] = $data['id'];
		$post['gudang'] = 1;
		$post['rak'] = 1;
		Modules::run('database/_insert', 'product_stock', $post);
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));

		// echo '<pre>';
		// print_r($data);
		// die;
		$id = $this->input->post('id');
		$is_valid = false;


		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader($data);
			if ($id == '') {
				$id = Modules::run('database/_insert', $this->getTableName(), $post_data);

				//procuremtn item
				if (!empty($data->proc_item)) {
					foreach ($data->proc_item as $value) {
						$proc_item['procurement'] = $id;
						$proc_item['product'] = $value->product;
						$proc_item['satuan'] = $value->satuan;
						$proc_item['harga'] = $value->harga;
						$proc_item['qty'] = $value->jumlah;
						$proc_item['sub_total'] = $value->sub_total;

						$proc_item_id = Modules::run('database/_insert', 'procurement_item', $proc_item);

						$this->updateProductSatuan($value, $id, $proc_item_id);
					}
				}

				//procurement status
				$proc_status['procurement'] = $id;
				$proc_status['status'] = 'DRAFT';
				Modules::run('database/_insert', 'procurement_status', $proc_status);
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Pengadaan";
		$data['title_content'] = 'Data Pengadaan';
		$content = $this->getDataPengadaan($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function getListProduct()
	{
		$data = Modules::run('database/get', array(
			'table' => 'product p',
			'field' => array('p.*'),
			'where' => "p.deleted = 0 or p.deleted is null"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListSatuan($product = '')
	{

		$where = "ps.deleted = 0 or ps.deleted is null";
		if ($product != '') {
			$where = "(ps.deleted = 0 or ps.deleted is null) and ps.product = '" . $product . "'";
		}
		$data = Modules::run('database/get', array(
			'table' => 'product_satuan ps',
			'field' => array(
				's.nama_satuan',
				's.id as satuan',
				'sp.nama_satuan as satuan_parent',
				'ps.qty as konversi'
			),
			'join' => array(
				array('product p', 'ps.product = p.id'),
				array('satuan s', 'ps.satuan = s.id', 'left'),
				array('satuan sp', 'sp.id = s.parent', 'left'),
			),
			'where' => $where
		));

		//  echo "<pre>";
		//  echo $this->db->last_query();
		//  die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function addItem()
	{
		$data['list_product'] = $this->getListProduct();
		//		$data['list_satuan'] = $this->getListSatuan();
		$data['list_satuan'] = array();
		$data['index'] = $_POST['index'];
		echo $this->load->view('product_item', $data, true);
	}

	public function getListSatuanData()
	{
		$product = $_POST['product'];
		$content['list_satuan'] = array();
		if ($product != '') {
			$content['list_satuan'] = $this->getListSatuan($product);
		}

		$content['index'] = '';
		echo $this->load->view('list_satuan', $content, true);
	}

	public function addSatuanContent()
	{
		$data['list_satuan'] = $this->getListSatuan();
		$data['tr_index'] = $_POST['tr_index'];
		$data['urutan'] = $_POST['urutan'];
		echo $this->load->view('list_item', $data, true);
	}

	public function printFaktur($id)
	{

		$data['proc'] = $this->getDetailDataPengadaan($id);
		$data['proc_item'] = $this->getProcItem($id);
		$data['self'] = Modules::run('general/getDetailDataGeneral', 1);
		echo $this->load->view('cetak_html', $data, true);
		// echo '<pre>';
		// print_r($data);
		// die;
		// $mpdf = Modules::run('mpdf/getInitPdf');

		// //  $pdf = new mPDF('A4');
		// $view = $this->load->view('cetak', $data, true);
		// $mpdf->WriteHTML($view);
		// $mpdf->Output('Nota Pengadaan - ' . date('Y-m-d') . '.pdf', 'I');
	}
}
