<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='nama' class='form-control required' 
             value='<?php echo isset($nama_vendor) ? $nama_vendor : '' ?>' error="Nama"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      No HP
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='no_hp' class='form-control required' 
             value='<?php echo isset($no_hp) ? $no_hp : '' ?>' error="No HP"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Email
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='email' class='form-control required' 
             value='<?php echo isset($email) ? $email : '' ?>' error="Email"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Status
     </div>
     <div class='col-md-3'>
      <select id="status" error="Status" class="form-control required">
       <option value="1" <?php echo isset($status) ? $status ? 'selected' : '' : '' ?>>Aktif</option>
       <option value="0" <?php echo isset($status) ? !$status ? 'selected' : '' : '' ?>>Tidak Aktif</option>
      </select>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Alamat
     </div>
     <div class='col-md-3'>
      <textarea class="form-control required" error="Alamat" id="alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
     </div>
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <textarea class="form-control" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
     </div>     
    </div>
    <br/>
    
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Vendor.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Vendor.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
