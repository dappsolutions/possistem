<table class="table table-striped table-bordered table-list-draft" id="tb_product">
 <thead>
  <tr>
   <th>Produk</th>
   <th>Pajak</th>
   <th>Metode Bayar</th>
   <th>Jumlah</th>
   <th>Sub Total</th>
  </tr>
 </thead>
 <tbody>
  <?php if (!empty($invoice_item)) { ?>
   <?php foreach ($invoice_item as $value) { ?>
    <tr> 
     <td><?php echo $value['nama_product'] . '-' . $value['satuan'] . '-[' . number_format($value['harga']) . ']' ?></td>
     <td>
      <?php if ($value['persentase'] != '0') { ?>
       <?php echo $value['jenis'] ?>
      <?php } else { ?>
       <?php echo $value['jenis'] ?>
      <?php } ?>
     </td>
     <td><?php echo $value['metode'] ?></td>
     <td><?php echo $value['qty'] ?></td>
     <td><?php echo number_format($value['sub_total']) ?></td>
    </tr>

    <?php if ($value['bank'] != '0' && $value['bank'] != '') { ?>
     <tr>
      <td colspan="7"><?php echo $value['nama_bank'] . '-' . $value['no_rekening'] . '-' . $value['akun'] ?></td>
     </tr>
    <?php } ?>
   <?php } ?>
  <?php } ?>         
 </tbody>
</table>