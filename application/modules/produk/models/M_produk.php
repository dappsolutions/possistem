
<?php

class M_produk extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	public function getListSatuan($params = array())
	{
		$sql = "select * from satuan s where deleted = 0";

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}
	
	public function getListProdukSatuan($id)
	{
		$sql = "
		select 
		ps.*
		, s.nama_satuan 
		from product_satuan ps 
		join product p 
			on p.id = ps.product
		join satuan s 
			on s.id = ps.satuan 
		where ps.deleted = 0
		and p.id = ".$id;

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);

		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->result_array();
		}

		return $result;
	}
}
