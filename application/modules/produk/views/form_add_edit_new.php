<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-header with-border" style="margin-top: 12px;">
				<h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
			</div>
			<br />
			<div class="box-body box-block">
				<div class="row">
					<div class='col-md-3 text-bold'>
						Tipe Produk
					</div>
					<div class='col-md-3'>
						<select id="tipe" error="Tipe" class="form-control required">
							<?php if (!empty($list_tipe)) { ?>
								<?php foreach ($list_tipe as $v_tip) { ?>
									<?php
									$selected = "";
									if (isset($tipe_product)) {
										if ($v_tip['id'] == $tipe_product) {
											$selected = 'selected';
										}
									}
									?>
									<option <?php echo $selected ?> value="<?php echo $v_tip['id'] ?>"><?php echo $v_tip['tipe'] ?></option>
								<?php } ?>
							<?php } else { ?>
								<option value="">Tidak Ada Tipe Produk</option>
							<?php } ?>
						</select>
					</div>

					<div class='col-md-3 text-bold hide'>
						Produk Kategori
					</div>
					<div class='col-md-3 hide'>
						<select id="kategori" error="Produk Kategori" class="form-control required">
							<?php if (!empty($list_rk)) { ?>
								<?php foreach ($list_rk as $v_rk) { ?>
									<?php
									$selected = "";
									if (isset($kategori_product)) {
										if ($v_rk['id'] == $kategori_product) {
											$selected = 'selected';
										}
									}
									?>
									<option <?php echo $selected ?> value="<?php echo $v_rk['id'] ?>"><?php echo $v_rk['kategori'] ?></option>
								<?php } ?>
							<?php } else { ?>
								<option value="">Tidak Ada Produk Kategori</option>
							<?php } ?>
						</select>
					</div>

					<div class='col-md-3 text-bold'>
						Keterangan Produk
					</div>
					<div class='col-md-3'>
						<textarea class="form-control required" error="Detail Produk Produk" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
					</div>
				</div>
				<br />

				<div class='row'>
					<div class='col-md-3 text-bold'>
						Kode Produk
					</div>
					<div class='col-md-3'>
						<input type='text' name='' id='kode_product' class='form-control required' error="Kode Produk" value='<?php echo isset($kode_product) ? $kode_product : '' ?>' placeholder="" />
					</div>

					<div class='col-md-3 text-bold'>
						Nama Produk
					</div>
					<div class='col-md-3'>
						<input type='text' name='' id='product' class='form-control required' error="Nama Produk" value='<?php echo isset($product) ? $product : '' ?>' placeholder="" />
					</div>
				</div>
				<br />

				<div class='row'>
					<div class='col-md-3 text-bold'>
						Kode Barcode
					</div>
					<div class='col-md-3'>
						<input type='text' name='' id='kode_barcode' class='form-control' value='<?php echo isset($kodebarcode) ? $kodebarcode : '' ?>' placeholder="" />
					</div>
				</div>
				<br />

				<hr />

				<div class='row'>
					<div class='col-md-3 text-bold'>
						<u>Foto Produk Produk</u>
					</div>
				</div>
				<br />

				<div class='row'>
					<?php if (isset($data_image)) { ?>
						<?php foreach ($data_image as $v_image) { ?>
							<div class='col-md-4' id='<?php echo $v_image['id'] ?>'>
								<div class='text-right hover' onclick="Produk.removeImage(this)">
									<i class="mdi mdi-close mdi-24px"></i>
								</div>
								<div class=''>
									<img src="<?php echo $v_image['foto'] ?>" width="150" height="150" />
								</div>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
				<br />

				<div class='row'>
					<div class='col-md-12'>
						<div class="images">
							<div class="pic">
								add
							</div>
						</div>
					</div>
				</div>
				<br />
				<hr />
				<div class="row">
							<div class="col-md-12">
								<?php echo $this->load->view('form_produk_satuan') ?>
							</div>
				</div>
				<br />
				<hr />
				
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="Produk.simpan('<?php echo isset($id) ? $id : '' ?>')"><i class="fa fa-check"></i>&nbsp;Simpan</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="Produk.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>