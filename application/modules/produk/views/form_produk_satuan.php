<div class="row">
 <div class="col-md-12">
  <div class="table-responsive">
   <table class="table table-bordered" id="table-satuan-produk">
    <thead>
     <tr class="bg-warning">
      <th>Satuan</th>
      <th>Harga Beli</th>
      <th>Harga Jual</th>
      <th>Konversi Jml.</th>
      <th>Satuan Terkecil</th>
      <th>Stok</th>
      <th>keterangan</th>
      <th></th>
     </tr>
    </thead>
    <tbody>
     <?php foreach ($list_produk_satuan as $key => $v_ps) { ?>
      <tr>
       <td>
        <select name="" id="satuan" error="Satuan" class="required">
         <option value=""></option>
         <?php foreach ($list_satuan as $key => $value) { ?>
          <option value="<?php echo $value['id'] ?>" <?php echo $v_ps['satuan'] == $value['id'] ? 'selected' : '' ?>><?php echo $value['nama_satuan'] ?></option>
         <?php } ?>
        </select>
       </td>
       <td>
        <input type="text" id="harga-beli" class="required" error="Harga Beli" value="<?php echo $v_ps['harga_beli'] ?>">
       </td>
       <td>
        <input type="text" id="harga-jual" class="required" error="Harga Jual" value="<?php echo $v_ps['harga'] ?>">
       </td>
       <td>
        <input type="text" id="konversi" class="required" error="Konversi" value="<?php echo $v_ps['qty'] ?>">
       </td>
       <td class="text-center">
        <input type="radio" id="satuan_terkecil" name="satuan_terkecil" <?php echo $v_ps['satuan_terkecil'] == '1' ? 'checked' : '' ?>>
       </td>
       <td>
        <input type="text" id="stok" class="required" error="Stok">
       </td>
       <td>
        <input type="text" id="keterangan" value="<?php echo $v_ps['ket_harga'] ?>">
       </td>
       <td class="text-center">
        <i class="mdi mdi-delete mdi-18px" onclick="Produk.removeItem(this)"></i>
       </td>
      </tr>
     <?php } ?>
     <tr>
      <td colspan="7" class="">
       <a href="" onclick="Produk.addItem(this, event)">Tambah Item</a>
      </td>
     </tr>
    </tbody>
   </table>
  </div>
 </div>
</div>