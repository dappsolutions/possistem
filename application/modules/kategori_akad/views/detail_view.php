<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Kategori
     </div>
     <div class='col-md-3'>
      <?php echo $kategori ?>
     </div>     
    </div>
    <br/>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="KategoriAkad.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
