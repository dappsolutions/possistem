<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type='hidden' name='' id='module' class='form-control' value='add_order' />

<div class="padding-16">
	<div class="row">
		<div class="col-md-5">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h5>Daftar Produk Tersedia</h5>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">
							<label for=""><i>* Tekan Enter jika data produk barcode tidak muncul</i></label>
						</div>
						<div class="col-md-12">
							<input type="text" placeholder="Scan Barcode" id="barcode" onkeyup="Order.scanBarcodeProduk(this, event)" class="form-control">
						</div>
						<div class="col-md-12">
							<br>
							<input type="text" placeholder="Cari Barang" id="cari_barang" onkeyup="Order.scanCariProduk(this, event)" class="form-control">
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive" style="max-height: 300px;overflow: auto;">
								<table class="table table-striped" id="tb_daftar_produk">
									<thead>
										<tr class="bg-primary">
											<th>Kode Product</th>
											<th>Produk</th>
											<th>Stok</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<?php if (!empty($list_product)) { ?>
											<?php foreach ($list_product as $v) { ?>
												<tr stock="<?php echo $v['stock'] ?>" data_id="<?php echo $v['id'] ?>" kode_product="<?php echo $v['kode_product'] ?>" kodebarcode="<?php echo $v['kodebarcode'] ?>" nama_product="<?php echo $v['product'] ?>">
													<td><?php echo $v['kode_product'] ?></td>
													<td><?php echo $v['product'] ?></td>
													<td><?php echo number_format($v['stock']) ?></td>
													<td class="text-center">
														<label class="label label-success" onclick="Order.chooseProduk(this)">Pilih</label>
													</td>
												</tr>
											<?php } ?>
										<?php } else { ?>
											<tr>
												<td colspan="3">Tidak ada data ditemukan</td>
											</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h5>Form Transaksi</h5>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-12">

							<!-- <div class="row">
								<div class="col-md-12">
									<u>Daftar Item</u>
								</div>
							</div>
							<br /> -->

							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table class="table table-striped table-list-draft" id="tb_product">
											<thead>
												<tr class="bg-primary-light text-white">
													<th>Produk</th>
													<th>Satuan</th>
													<th>Jumlah</th>
													<th>Potongan</th>
													<th>Sub Total</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>

												<tr data_id="">
													<td colspan="7">
														<label id="add_detail" style="display: none;">
															<a href="#" onclick="Order.addItem(this, event)">Tambah Item</a>
														</label>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-md-12 text-right">
									<h2>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h2>
								</div>
							</div>
							<hr>

							<div class="row">
								<div class='col-md-3 text-bold'>
									Pelanggan
								</div>
								<div class='col-md-9'>
									<select class="form-control required" id="pembeli" error="Pelanggan">
										<option value="">Pilih Pelanggan</option>
										<?php $index = 0; ?>
										<?php if (!empty($list_pelanggan)) { ?>
											<?php foreach ($list_pelanggan as $value) { ?>
												<?php $selected = '' ?>
												<?php if (isset($pembeli)) { ?>
													<?php $selected = $pembeli == $value['id'] ? 'selected' : '' ?>
												<?php } else { ?>
													<?php $selected = $index == 0 ? 'selected' : '' ?>
												<?php } ?>
												<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
												<?php $index += 1 ?>
											<?php } ?>
										<?php } ?>
									</select>
								</div>
							</div>
							<br />

							<div class="row">
								<div class='col-md-3 text-bold'>
									Metode Bayar
								</div>
								<div class='col-md-9'>
									<select class="form-control required" id="metode_bayar" error="Metode Bayar" onchange="Order.setMetodeBayar(this)">
										<?php if (!empty($list_metode)) { ?>
											<?php foreach ($list_metode as $value) { ?>
												<?php $selected = '' ?>
												<?php if (isset($metode_bayar)) { ?>
													<?php $selected = $metode_bayar == $value['id'] ? 'selected' : '' ?>
												<?php } ?>
												<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['metode'] ?></option>
											<?php } ?>
										<?php } ?>
									</select>
								</div>
							</div>
							<br />

							<?php $hide = isset($no_rekening) ? $no_rekening == '' ? 'hide' : '' : 'hide' ?>
							<div class="content-rekening <?php echo $hide ?>">
								<div class="row">
									<div class='col-md-3 text-bold'>
										No Rekening
									</div>
									<div class='col-md-9'>
										<input type='text' id='no_rekening' class='form-control text-right' value='<?php echo isset($no_rekening) ? $no_rekening : '' ?>' />
									</div>
								</div>
								<br />
							</div>

							<div class="row">
								<div class='col-md-3 text-bold'>
									Potongan
								</div>
								<div class='col-md-9'>
									<select class="form-control required" id="potongan_invoice" error="Potongan" onchange="Order.hitungTotal()">
										<?php if (!empty($list_potongan)) { ?>
											<?php foreach ($list_potongan as $value) { ?>
												<?php $selected = '' ?>
												<?php if (isset($potongan)) { ?>
													<?php $selected = $potongan == $value['id'] ? 'selected' : '' ?>
												<?php } ?>
												<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['potongan'] ?></option>
											<?php } ?>
										<?php } ?>
									</select>
								</div>
							</div>
							<br />

							<div class="row">
								<div class='col-md-3 text-bold'>
									Nilai Potongan
								</div>
								<div class='col-md-9'>
									<input type='number' min="0" name='' id='nilai_potongan' class='form-control text-right' value='<?php echo isset($pot_faktur) ? $pot_faktur : '0' ?>' error="Nilai Potongan" onkeyup="Order.hitungSubTotal(this, 'potongan')" />
								</div>
							</div>
							<br />
							<hr />

							<div class='row'>
								<div class='col-md-12 text-right'>
									<button id="btn-simpan" class="btn btn-success" onclick="Order.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan F8</button>
									&nbsp;
									<button id="" class="btn btn-baru" onclick="Order.back()">Kembali</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>