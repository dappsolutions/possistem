<td class="input" produk="<?php echo $product ?>" id="produk_td" stock="<?php echo $stock ?>" satuan_terkeci_id="<?php echo $data_satuan_terkecil['id'] ?>">
	<input type="checkbox" checked disabled>
	<br>
	Kode : <label for=""><?php echo $kode_product ?></label>
	<br>
	Produk : <label for="" id="nama_product"><?php echo $nama_product ?></label>
	<br>
	<?php if (!empty($list_satuan)) { ?>
		Harga : <label for="" id="harga"><?php echo $list_satuan[0]['harga'] ?></label>
	<?php } else { ?>
		Harga : <label for="" id="harga"></label>
	<?php } ?>
</td>
<td satuan="">
	<select name="" id="product_satuan" onchange="Order.setGantiSatuanHarga(this)">
		<?php if (!empty($list_satuan)) { ?>
			<?php foreach ($list_satuan as $key => $value) { ?>
				<option value="<?php echo $value['id'] ?>" harga="<?php echo $value['harga'] ?>" konversi="<?php echo $value['konversi'] ?>"><?php echo $value['nama_satuan'] ?> <?php echo $value['keterangan'] != '' ? ' - ' . $value['keterangan'] : '' ?></option>
			<?php } ?>
		<?php } else { ?>
			<option value="">Tidak Ada Satuan</option>
		<?php } ?>
	</select>
</td>
<td class="text-center">
	<input type="text" style="width: 50px;" value="1" min="1" id="jumlah" class="text-right" onkeyup="Order.hitungSubTotal(this, 'jumlah')" onchange="Order.hitungSubTotal(this, 'jumlah')" />
</td>
<td class="text-center">
	<a href="#" onclick="Order.getPotongan(this, event)">Potongan</a>
</td>
<td class="text-center" id="td_sub_total">
	<label id="sub_total">0</label>
</td>
<td class="text-center">
	<i class="mdi mdi-delete mdi-18px" onclick="Order.deleteItem(this)"></i>
</td>


<script>
	$(function() {
		$("#product<?php echo $index ?>").select2();
		$("#metode<?php echo $index ?>").select2();
		$("#pajak<?php echo $index ?>").select2();
	});
</script>
