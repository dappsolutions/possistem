<input type="hidden" value="<?php echo $index ?>" id="index_pot" class="form-control" />


<div class="panel panel-default">
  <div class="panel-heading">
    <h5>Tambah Potongan</h5>
  </div>
  <div class="panel-body">
    <div class="row">
      <div class="col-md-12 padding-16">
        <div class="row">
          <div class='col-md-3 text-bold'>
            Potongan
          </div>
          <div class='col-md-9'>
            <select class="form-control" id="potongan" error="Potongan">
              <?php if (!empty($list_potongan)) { ?>
                <?php foreach ($list_potongan as $value) { ?>
                  <?php $selected = '' ?>
                  <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['potongan'] ?></option>
                <?php } ?>
              <?php } ?>
            </select>
          </div>
        </div>
        <br />

        <div class="row">
          <div class='col-md-3 text-bold'>
            Nilai
          </div>
          <div class='col-md-9'>
            <input type='text' name='' id='nilai_pot' class='form-control required' error="Nilai" />
          </div>
        </div>
        <br />
        <hr />
        <div class='row'>
          <div class='col-md-12 text-right'>
            <button id="" class="btn btn-success" onclick="Order.submitPot(this, 'potongan')">Proses</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>