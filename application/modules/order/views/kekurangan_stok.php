<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h5>Daftar Kekurangan Stok Barang</h5>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
									<tr class="bg-primary">
										<th>No</th>
										<th>Produk</th>
										<th>Qty</th>
										<th>Stok Ready (PCS)</th>
										<th>Stok Jual (PCS)</th>
										<th>Stok Kurang (PCS)</th>
									</tr>
								</thead>
								<tbody>
									<?php $no = 1 ?>
									<?php foreach ($data as $key => $value) { ?>
										<tr>
											<td><?php echo $no++ ?></td>
											<td><?php echo $value->nama_produk ?></td>
											<td><?php echo $value->qty ?></td>
											<td><?php echo $value->stok ?></td>
											<td><?php echo $value->qty * $value->konversi ?></td>
											<td class="text-danger"><?php echo ($value->qty * $value->konversi) - $value->stok ?></td>
										</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>