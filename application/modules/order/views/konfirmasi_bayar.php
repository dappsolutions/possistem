<input type="hidden" id="order-id" value="<?php echo $order ?>">

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h4>Konfirmasi Pembayaran Total Harus Dibayar : <?php echo $totalBayar ?></h4>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<input type="text" name="" id="bayar" onkeyup="Order.checkUangPembayaran(this, event)" class="form-control required text-right" error="Uang Bayar" placeholder="Uang Bayar" value="">
						<i>
							<p id="pesan-pembayaran"></p>
						</i>
					</div>
					<div class="col-md-12 text-right">
						<br>
						<button class="btn btn-success" id="btn-bayar" onclick="Order.execPembayaran(this)">Proses Enter</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>