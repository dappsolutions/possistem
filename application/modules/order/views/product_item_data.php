<?php if (!empty($list_product)) { ?>
	<?php foreach ($list_product as $v) { ?>
		<tr stock="<?php echo $v['stock'] ?>" data_id="<?php echo $v['id'] ?>" kode_product="<?php echo $v['kode_product'] ?>" kodebarcode="<?php echo $v['kodebarcode'] ?>" nama_product="<?php echo $v['product'] ?>">
			<td><?php echo $v['kode_product'] ?></td>
			<td><?php echo $v['product'] ?></td>
			<td><?php echo number_format($v['stock']) ?></td>
			<td class="text-center">
				<label class="label label-success" id="btn-pilih" onclick="Order.chooseProduk(this)">Pilih</label>
			</td>
		</tr>
	<?php } ?>
<?php } else { ?>
	<tr>
		<td colspan="3">Tidak ada data ditemukan</td>
	</tr>
<?php } ?>