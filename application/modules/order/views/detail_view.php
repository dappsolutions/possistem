<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type='hidden' name='' id='product_satuan' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type='hidden' name='' id='module' class='form-control' value='detail_order' />

<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class="row">
					<div class='col-md-3 text-bold'>
						No Order
					</div>
					<div class='col-md-3'>
						<?php echo $no_order ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Pelanggan
					</div>
					<div class='col-md-3'>
						<?php echo $nama_pembeli ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Tanggal Order
					</div>
					<div class='col-md-3'>
						<?php echo $tanggal_faktur ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Metode Bayar
					</div>
					<div class='col-md-3' id="metode_bayar">
						<?php echo $metode ?>
					</div>
				</div>
				<br />

				<?php if ($metode == 'TRANSFER') { ?>
					<div class="row">
						<div class='col-md-3 text-bold'>
							No Rekening
						</div>
						<div class='col-md-3' id="no_rekening">
							<?php echo $no_rekening ?>
						</div>
					</div>
					<br />
				<?php } ?>

				<div class="row">
					<div class='col-md-3 text-bold'>
						Potongan
					</div>
					<div class='col-md-3'>
						<?php echo $jenis_potongan ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Nilai
					</div>
					<div class='col-md-3'>
						<?php echo $pot_faktur ?>
					</div>
				</div>
				<br />
				<hr />

				<div class="row">
					<div class="col-md-12">
						<u>Data Produk</u>
					</div>
				</div>
				<br />

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-list-draft" id="tb_product">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>Produk</th>
										<th>Jumlah</th>
										<th>Sub Total</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($order_item)) { ?>
										<?php foreach ($order_item as $value) { ?>
											<tr product_id="<?php echo $value['product_id'] ?>" product_satuan="<?php echo $value['product_satuan'] ?>" qty="<?php echo $value['qty'] ?>" konversi="<?php echo $value['konversi'] ?>" satuan_terkecil_id="<?php echo $value['satuan_terkecil_id']['id'] ?>">
												<td><?php echo $value['nama_product'] . '-' . $value['nama_satuan'] . '-[' . number_format($value['harga']) . ']' ?></td>
												<td><?php echo $value['qty'] ?></td>
												<td><?php echo number_format($value['sub_total']) ?></td>
											</tr>
											<?php if (!empty($value['pot_item'])) { ?>
												<?php foreach ($value['pot_item'] as $v_i) { ?>
													<tr>
														<td colspan="3"><?php echo 'Potongan ' . $v_i['jenis_potongan'] . ' : ' . $v_i['nilai'] ?></td>
													</tr>
												<?php } ?>
											<?php } ?>

											<?php if ($status != 'CANCEL' && $status != 'VALIDATE') { ?>
												<?php if (!empty($value['stok_data'])) { ?>
													<?php if ($value['stok_data']['is_valid'] == 0) { ?>
														<tr class="text-danger">
															<td>Sisa Stok : <?php echo $value['stok_data']['stock'] ?></td>
															<td class="text-center"><i class="fa fa-rotate-left" order="<?php echo $value['order'] ?>" data_id="<?php echo $value['id'] ?>" onclick="Order.cancelItem(this)"></i>&nbsp;Batal</td>
															<td>
																<i class="fa fa-pencil" data_id="<?php echo $value['id'] ?>" product_satuan="<?php echo $value['product_satuan'] ?>" qty="<?php echo $value['qty'] ?>" harga="<?php echo $value['harga'] ?>" order="<?php echo $value['order'] ?>" onclick="Order.ubahQty(this)"></i>
															</td>
														</tr>
													<?php } ?>
												<?php } ?>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-right">
						<h4>Total : Rp, <label id="total"><?php echo number_format($total) ?></label></h4>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-12 text-right'>
						<?php if ($status != 'CANCEL') { ?>
							<?php if ($status != 'VALIDATE') { ?>
								<button id="btn-cancel" class="btn btn-danger" onclick="Order.cancel('<?php echo isset($id) ? $id : '' ?>')">Cancel ESC</button>
								&nbsp;
								<?php if ($status == 'DRAFT') { ?>
									<button id="btn-simpan" class="btn btn-success" onclick="Order.confirm('<?php echo isset($id) ? $id : '' ?>')">Konfirmasi Pembayaran F8</button>
								<?php } ?>
								&nbsp;
							<?php } ?>
						<?php } ?>
						<button id="" class="btn btn-baru" onclick="Order.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>