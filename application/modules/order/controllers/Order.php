<?php

class Order extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->limit = 10;
		$this->load->model('m_order', 'order');
	}

	public function getModuleName()
	{
		return 'order';
	}

	public function getHeaderJSandCSS()
	{
		//versioning
		$version = str_shuffle("1234567890abcdefghijklmnopqrstuvwxyz");
		$version = substr($version, 0, 11);
		//versioning

		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/order_v1.js?v=' . $version . '"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'order';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Order";
		$data['title_content'] = 'Data Order';
		$content = $this->getDataOrder();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function getTotalDataOrder($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('o.no_order', $keyword),
				array('o.tanggal_faktur', $keyword),
				array('pb.nama', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => $this->getTableName() . ' o',
			'field' => array('o.*', 'isa.status', 'pb.nama as nama_pembeli'),
			'join' => array(
				array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
				array('order_status isa', 'isa.id = iss.id'),
				array('pembeli pb', 'pb.id = o.pembeli'),
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => "o.deleted is null or o.deleted = 0",
			'orderby' => 'o.id desc'
		));

		return $total;
	}

	public function getDataOrder($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('o.no_order', $keyword),
				array('o.tanggal_faktur', $keyword),
				array('pb.nama', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' o',
			'field' => array('o.*', 'isa.status', 'pb.nama as nama_pembeli'),
			'join' => array(
				array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
				array('order_status isa', 'isa.id = iss.id'),
				array('pembeli pb', 'pb.id = o.pembeli'),
			),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
			'where' => "o.deleted is null or o.deleted = 0",
			'orderby' => 'o.id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		//  echo '<pre>';
		//  print_r($result);die;
		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataOrder($keyword)
		);
	}

	public function getDetailDataOrder($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' o',
			'field' => array(
				'o.*',
				'p.nama as nama_pembeli', 'isa.status',
				'pt.potongan as jenis_potongan',
				'mb.metode'
			),
			'join' => array(
				array('pembeli p', 'o.pembeli = p.id'),
				array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
				array('order_status isa', 'isa.id = iss.id'),
				array('metode_bayar mb', 'mb.id = o.metode_bayar'),
				array('potongan pt', 'pt.id = o.potongan', 'left'),
			),
			'where' => "o.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListProduct()
	{
		$data = Modules::run('database/get', array(
			'table' => 'product_satuan ps',
			'field' => array(
				'ps.*',
				'p.product as nama_product',
				's.nama_satuan',
				'pst.stock',
				'p.kode_product'
			),
			'join' => array(
				array('product p', 'ps.product = p.id'),
				array('satuan s', 'ps.satuan = s.id', 'left'),
				array('product_stock pst', 'p.id = pst.product'),
			),
			'where' => "ps.deleted = 0 or ps.deleted is null"
		));

		// echo '<pre>';
		// print_r($this->db->last_query());
		// die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		// echo '<pre>';
		// print_r($result);
		// die;

		return $result;
	}

	public function getListPelanggan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pembeli p',
			'field' => array('p.*'),
			'where' => "p.deleted = 0 or p.deleted is null and p.pembeli_kategori = 2"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPajak()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pajak p',
			'field' => array('p.*'),
			'where' => "p.deleted = 0 or p.deleted is null"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListMetodeBayar()
	{
		$data = Modules::run('database/get', array(
			'table' => 'metode_bayar',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPotongan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'potongan',
			'orderby' => 'id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Order";
		$data['title_content'] = 'Tambah Order';
		// $data['list_product'] = $this->getListProduct();
		// echo '<pre>';
		// print_r($data['list_product']);
		// die;
		$data['list_pelanggan'] = $this->getListPelanggan();
		$data['list_metode'] = $this->getListMetodeBayar();
		$data['list_pajak'] = $this->getListPajak();
		$data['list_potongan'] = $this->getListPotongan();
		$data['list_product'] = $this->order->getDataProdukOutstanding();
		// echo '<pre>';
		// print_r($data);
		// die;
		echo Modules::run('template', $data);
	}

	public function bayar()
	{
		$data['view_file'] = 'form_bayar';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Bayar Order";
		$data['title_content'] = 'Bayar Order';
		$data['list_product'] = $this->getListProduct();
		$data['list_pelanggan'] = $this->getListPelanggan();
		$data['list_metode'] = $this->getListMetodeBayar();
		$data['list_pajak'] = $this->getListPajak();
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataOrder($id);
		//  echo $data['total'];die;
		//  echo '<pre>';
		//  print_r($data);die;
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Order";
		$data['title_content'] = 'Ubah Order';
		$data['list_product'] = $this->getListProduct();
		$data['list_pelanggan'] = $this->getListPelanggan();
		$data['list_metode'] = $this->getListMetodeBayar();
		$data['list_pajak'] = $this->getListPajak();
		$data['order_item'] = $this->getListInvoiceItem($id);
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = $this->getDetailDataOrder($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Order";
		$data['title_content'] = 'Detail Order';
		$data['order_item'] = $this->getListInvoiceItem($id);
		// echo '<pre>';
		// print_r($_SESSION);
		// die;
		echo Modules::run('template', $data);
	}

	public function getListInvoiceItem($order, $id = '')
	{
		$where = "ip.order = '" . $order . "' and ip.deleted = 0";
		if ($id != '') {
			$where = "ip.order = '" . $order . "' and ip.deleted = 0 and ip.id = '" . $id . "'";
		}
		$data = Modules::run('database/get', array(
			'table' => 'order_product ip',
			'field' => array(
				'ip.*', 'ps.satuan', 'ps.harga',
				'p.product as nama_product', 's.nama_satuan',
				'ps.qty as konversi',
				'p.id as product_id'
			),
			'join' => array(
				array('product_satuan ps', 'ps.id = ip.product_satuan'),
				array('product p', 'p.id = ps.product'),
				//                  array('bank b', 'b.id = ip.bank', 'left'),
				//                  array('pajak pj', 'pj.id = ip.pajak'),
				//                  array('metode_bayar m', 'm.id = ip.metode_bayar'),
				array('satuan s', 's.id = ps.satuan', 'left')
			),
			'where' => $where,
			'orderby' => 'ip.id'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['pot_item'] = $this->getListPotonganItem($value['id']);
				$value['stok_data'] = $this->cekStokFix($value);
				$params['product'] = $value['product_id'];
				$value['satuan_terkecil_id'] = $this->order->getDataSatuanTerkecilProduk($params);
				// echo '<pre>';
				// print_r($value);
				// die;
				array_push($result, $value);
			}
		}


		// echo '<pre>';
		// print_r($result);die;
		return $result;
	}

	public function getListPotonganItem($order_product)
	{
		$data = Modules::run('database/get', array(
			'table' => 'order_pot_product ipp',
			'field' => array('ipp.*', 'pt.potongan as jenis_potongan'),
			'join' => array(
				array('potongan pt', 'pt.id = ipp.potongan', 'left')
			),
			'where' => "ipp.deleted = 0 and ipp.order_product = '" . $order_product . "'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getDetailCurrentSatuan($product_satuan)
	{
		$data = Modules::run('database/get', array(
			'table' => 'product_satuan ps',
			'field' => array(
				'ps.*', 'pst.stock',
				's.nama_satuan as current_satuan',
				'sp.nama_satuan as parent_satuan',
				'psp.id as parent_product_satuan',
				'psp.qty as qty_parent',
				'pstp.stock as parent_stock',
				'pst.id as cur_product_stock',
				'pstp.id as parent_product_stock',
				'pdc.product as nama_product'
			),
			'join' => array(
				array('product_stock pst', 'pst.product_satuan = ps.id'),
				array('satuan s', 's.id = ps.satuan'),
				array('satuan sp', 'sp.id = s.parent', 'left'),
				array('product_satuan psp', 'psp.satuan = sp.id and psp.product = ps.product', 'left'),
				array('product_stock pstp', 'pstp.product_satuan = psp.id', 'left'),
				array('product pdc', 'pdc.id = ps.product'),
			),
			'where' => "ps.deleted is null or ps.deleted = 0 and ps.id = '" . $product_satuan . "'",
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}

		return $result;
	}

	public function penguranganStok($data_item)
	{
		$current_satuan = $this->getDetailCurrentSatuan($data_item->product_satuan);
		$current_order = $data_item->qty;
		$message = "";
		$is_valid = true;

		if (!empty($current_satuan)) {
			$current_stok = $current_satuan['stock'];
			$parent_stok = $current_satuan['parent_stock'] == '' ? 0 : $current_satuan['parent_stock'];
			$current_qty = $current_satuan['qty'];
			// $parent_qty = $current_satuan['qty_parent'] == '' ? 0 : $current_satuan['qty_parent'];

			$konversi_stok = $parent_stok * $current_qty;
			$sisa_current = $current_stok - $current_order;
			if ($sisa_current < 0) {
				$sisa_current *= -1;

				$total_parent_stok = $konversi_stok - $sisa_current;

				//untuk update current stok
				$current_stok_change = intval($total_parent_stok) % intval($current_qty);

				//untuk update parent stok
				$parrent_stok_change = ($total_parent_stok - $current_stok_change) / $current_qty;

				// $parrent_stok_change = $parent_stok - $parrent_stok_change;
				//update product stok current

				if ($parrent_stok_change >= 0) {
					// Modules::run('database/_update', 'product_stock', array('stock' => $current_stok_change), array(
					// 	'id' => $current_satuan['cur_product_stock']
					// ));
					// //update product stok parrent			
					// if ($current_satuan['parent_product_stock'] != '') {
					// 	Modules::run('database/_update', 'product_stock', array('stock' => $parrent_stok_change), array(
					// 		'id' => $current_satuan['parent_product_stock']
					// 	));
					// }
				} else {
					$is_valid = false;
					$message = "Stok " . $current_satuan['parent_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
				}
			} else {
				//update product stok current
				$current_stok_change = $current_stok - $current_order;
				if ($current_stok_change >= 0) {
					// Modules::run('database/_update', 'product_stock', array('stock' => $current_stok_change), array(
					// 	'id' => $current_satuan['cur_product_stock']
					// ));
				} else {
					$is_valid = false;
					$message = "Stok " . $current_satuan['current_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
				}
			}
		}

		return array(
			'is_valid' => $is_valid,
			'message' => $message
		);
	}

	public function getPostDataHeader($value)
	{
		$data['no_order'] = Modules::run('no_generator/generateNoOrder');
		$data['pembeli'] = $value->pembeli;
		$data['potongan'] = $value->potongan;
		$data['metode_bayar'] = $value->metode_bayar;
		if ($value->pot_faktur != '') {
			$data['pot_faktur'] = $value->pot_faktur;
		}
		if ($value->no_rekening != '') {
			$data['no_rekening'] = $value->no_rekening;
		}
		$data['tanggal_faktur'] = date('Y-m-d H:i:s');
		$data['total'] = str_replace('.', '', $value->total);
		return $data;
	}

	public function checkStokProduct($product_item)
	{
		$result = array();

		// echo '<pre>';
		// print_r($product_item);
		// die;
		$detail = array();
		if (!empty($product_item)) {
			$temp = array();
			foreach ($product_item as $key => $value) {
				$product_id = $value->product;
				$summary_qty_product = 0;
				if (!in_array($product_id, $temp)) {
					foreach ($product_item as $val_pro) {
						if ($val_pro->product == $product_id) {
							$summary_qty_product += ($val_pro->qty * $val_pro->konversi);
						}

						$detail[$product_id]['qty'] = $summary_qty_product;
					}
					$temp[] = $product_id;
				}
			}

			// echo '<pre>';
			// print_r($detail);
			// die;
			if (!empty($detail)) {
				foreach ($detail as $key => $value) {
					$product_id = $key;
					foreach ($product_item as $val) {
						if ($val->product == $key) {
							if ($value['qty'] > $val->stok) {
								array_push($result, $val);
							}
						}
					}
				}
			}
		}

		return $result;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));

		// echo '<pre>';
		// print_r($data);
		// die;
		$checkStok = $this->checkStokProduct($data->product_item);

		// echo '<pre>';
		// print_r($checkStok);
		// die;

		$id = $this->input->post('id');
		$is_valid = false;
		$is_save = true;
		$message = "";
		$view = '';

		if (empty($checkStok)) {
			$this->db->trans_begin();
			try {
				$post_data = $this->getPostDataHeader($data);
				if ($id == '') {
					$id = Modules::run('database/_insert', $this->getTableName(), $post_data);

					//order product item    
					if (!empty($data->product_item)) {
						foreach ($data->product_item as $value) {
							$post_item['order'] = $id;
							$post_item['product_satuan'] = $value->product_satuan;
							$post_item['qty'] = $value->qty;
							$post_item['sub_total'] = str_replace(',', '', $value->sub_total);
							$order_product = Modules::run('database/_insert', 'order_product', $post_item);

							//insert potongan
							if (!empty($value->potongan_item)) {
								foreach ($value->potongan_item as $v_pot) {
									$post_pot_item['order_product'] = $order_product;
									$post_pot_item['potongan'] = $v_pot->potongan;
									$post_pot_item['nilai'] = $v_pot->nilai;

									Modules::run('database/_insert', 'order_pot_product', $post_pot_item);
								}
							}
						}
					}
					//order status
					$post_status['order'] = $id;
					$post_status['status'] = 'DRAFT';
					Modules::run('database/_insert', 'order_status', $post_status);
				} else {
					//update
					unset($post_data['no_faktur']);
					Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));


					if (!empty($data->product_item)) {
						foreach ($data->product_item as $value) {
							$post_item['order'] = $id;
							$post_item['product_satuan'] = $value->product_satuan;
							$post_item['qty'] = $value->qty;
							$post_item['sub_total'] = str_replace(',', '', $value->sub_total);
							if ($value->id != '') {
								if ($value->deleted == 1) {
									$post_item['deleted'] = 1;
								} else {
									$post_item['deleted'] = 0;
								}

								Modules::run('database/_update', 'order_product', $post_item, array('id' => $value->id));
							} else {
								Modules::run('database/_insert', 'order_product', $post_item);
							}
						}
					}
				}
				if ($is_save) {
					$this->db->trans_commit();
					$is_valid = true;
				}
			} catch (Exception $ex) {
				$this->db->trans_rollback();
			}
		} else {
			$data_produk_habis['data'] = $checkStok;
			$view = $this->load->view('kekurangan_stok', $data_produk_habis, true);
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id, 'message' => $message, 'view' => $view));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Order";
		$data['title_content'] = 'Data Order';
		$content = $this->getDataOrder($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/' . $keyword . '/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function addItem()
	{
		$data['list_product'] = $this->getListProduct();
		$data['list_pelanggan'] = $this->getListPelanggan();
		$data['list_metode'] = $this->getListMetodeBayar();
		$data['list_pajak'] = $this->getListPajak();
		$data['index'] = $_POST['index'];
		echo $this->load->view('product_item', $data, true);
	}

	public function getListBank()
	{
		$data = Modules::run('database/get', array(
			'table' => 'bank',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getMetodeBayar()
	{
		$data['list_bank'] = $this->getListBank();
		$data['index'] = $_POST['index'];
		echo $this->load->view('bank_akun', $data, true);
	}

	public function printOrder($id)
	{
		$data_pembayaran = array('data');
		$data = end($data_pembayaran['data']);

		$data['order'] = $this->getDetailDataOrder($id);
		$data['order_item'] = $this->getListInvoiceItem($id);
		$mpdf = Modules::run('mpdf/getInitPdf');

		$post_print['user'] = $this->session->userdata('user_id');
		$post_print['order'] = $id;
		Modules::run('database/_insert', 'order_print', $post_print);

		//  $pdf = new mPDF('A4');
		$view = $this->load->view('cetak', $data, true);
		$mpdf->WriteHTML($view);
		$mpdf->Output('Nota Customer - ' . date('Y-m-d') . '.pdf', 'I');
	}

	public function cancelOrder()
	{
		$order_id = $_POST['order_id'];
		$this->db->trans_begin();
		try {
			//order status
			$post_status['order'] = $order_id;
			$post_status['status'] = 'CANCEL';
			Modules::run('database/_insert', 'order_status', $post_status);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function getLisOrderItem($order)
	{
		$sql = "select op.*
, roi.qty as retur_qty
, roi.sub_total as retur_sub_total
, ps.harga
from order_product op
join `order` o
	on o.id = op.`order`
left join retur_order_item roi
	on roi.order_product = op.id
join product_satuan ps
 on ps.id = op.product_satuan
where o.id = '" . $order . "'";

		$data = Modules::run('database/get_custom', $sql);
		//echo "<pre>";
		//echo $this->db->last_query();
		//die;
		//  echo $sql;die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		//  echo '<pre>';
		//  print_r($result);die;
		return $result;
	}

	public function generateStokProduct($data)
	{
		$data_db = Modules::run('database/get', array(
			'table' => 'product_stock ps',
			'field' => array('ps.*'),
			'join' => array(
				array('product_satuan pst', 'pst.id = ps.product_satuan'),
				array('product p', 'p.id = pst.product'),
				array('satuan s', 's.id = pst.satuan'),
			),
			'where' => "ps.deleted = 0 and p.id = '" . $data->product_id . "' and pst.satuan_terkecil = 1",
			'orderby' => "ps.id desc"
		));

		// echo '<pre>';
		// print_r($this->db->last_query());
		// die;

		$result = array();
		$stok = 0;
		if (!empty($data_db)) {
			$data_db = $data_db->row_array();
			$stok = $data_db['stock'];
			// $post['stock'] = $data_db['stock'] - ($data->qty * $data->konversi);
			// $post['product'] = $data->product_id;
			// $post['product_satuan'] = $data->product_satuan;
			// $post['gudang'] = 1;
			// $post['rak'] = 1;
			// Modules::run('database/_update', 'product_stock', $post, array('id' => $data_db['id']));			
			// echo '<pre>';
			// print_r($this->db->last_query());
			// die;
		}

		$post['stock'] = $stok - ($data->qty * $data->konversi);
		$post['product'] = $data->product_id;
		$post['product_satuan'] = $data->satuan_terkecil_id;
		$post['gudang'] = 1;
		$post['rak'] = 1;
		Modules::run('database/_insert', 'product_stock', $post);
	}

	public function confirmBayar()
	{
		$order = $_POST['order_id'];
		$uang_bayar = $_POST['uang_bayar'];
		$total_tagihan = $_POST['total_tagihan'];
		$uang_kembali = $_POST['uang_kembali'];
		$data = json_decode($_POST['data']);

		// echo '<pre>';
		// print_r($_POST);
		// die;

		$is_valid = false;
		$this->db->trans_begin();
		try {

			// insert into product_log_stock
			if (!empty($data->product_item)) {
				foreach ($data->product_item as $key => $value) {
					// echo count($value);die';
					// echo '<pre>';
					// print_r($value);
					// die;
					if (isset($value->product_id)) {
						$post_log_stok['product_satuan'] = $value->product_satuan;
						$post_log_stok['status'] = 'ORDER';
						$post_log_stok['qty'] = $value->qty * $value->konversi;
						$post_log_stok['keterangan'] = 'Order Pelanggan';
						$post_log_stok['reference_id'] = $order;
						Modules::run('database/_insert', 'product_log_stock', $post_log_stok);


						$this->generateStokProduct($value);
					}
				}
			}

			$post_status_order['order'] = $order;
			$post_status_order['status'] = 'CONFIRM';
			Modules::run('database/_insert', 'order_status', $post_status_order);


			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}


		$total_kembali = $uang_bayar - $total_tagihan;
		// echo $total_kembali;
		// die;
		echo json_encode(array('is_valid' => $is_valid, 'id' => $order, 'total_kembali' => $total_kembali));
	}

	public function penguranganStokFix($data_item)
	{
		$current_satuan = $this->getDetailCurrentSatuan($data_item['product_satuan']);
		$current_order = $data_item['qty'];
		$message = "";
		$is_valid = 1;

		if (!empty($current_satuan)) {
			$current_stok = $current_satuan['stock'];
			$parent_stok = $current_satuan['parent_stock'] == '' ? 0 : $current_satuan['parent_stock'];
			$qty_parent = $current_satuan['qty_parent'] == '' ? 1 : $current_satuan['qty_parent'];
			// $parent_qty = $current_satuan['qty_parent'] == '' ? 0 : $current_satuan['qty_parent'];

			$konversi_stok = $parent_stok * $qty_parent;
			$sisa_current = $current_stok - $current_order;
			if ($sisa_current < 0) {
				$sisa_current *= -1;

				$total_parent_stok = $konversi_stok - $sisa_current;

				//untuk update current stok
				$current_stok_change = intval($total_parent_stok) % intval($qty_parent);

				//untuk update parent stok
				$parrent_stok_change = ($total_parent_stok - $current_stok_change) / $qty_parent;

				// $parrent_stok_change = $parent_stok - $parrent_stok_change;
				//update product stok current

				if ($parrent_stok_change >= 0) {
					Modules::run('database/_update', 'product_stock', array('stock' => $current_stok_change), array(
						'id' => $current_satuan['cur_product_stock']
					));

					//update product stok parrent			
					if ($current_satuan['parent_product_stock'] != '') {
						Modules::run('database/_update', 'product_stock', array('stock' => $parrent_stok_change), array(
							'id' => $current_satuan['parent_product_stock']
						));
					}
				} else {
					$is_valid = 0;
					$message = "Stok " . $current_satuan['nama_product'] . ' ' . $current_satuan['parent_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
				}
			} else {
				//update product stok current
				$current_stok_change = $current_stok - $current_order;
				if ($current_stok_change >= 0) {
					Modules::run('database/_update', 'product_stock', array('stock' => $current_stok_change), array(
						'id' => $current_satuan['cur_product_stock']
					));
				} else {
					$is_valid = 0;
					$message = "Stok " . $current_satuan['nama_product'] . ' ' . $current_satuan['current_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
				}
			}
		}

		return array(
			'is_valid' => $is_valid,
			'message' => $message
		);
	}

	public function cekStokFix($data_item)
	{
		$current_satuan = $this->getDetailCurrentSatuan($data_item['product_satuan']);


		$current_order = $data_item['qty'];
		$message = "";
		$is_valid = 1;
		$stock_valid = 0;
		//  echo '<pre>';
		//  print_r($current_satuan);die;
		if (!empty($current_satuan)) {
			$current_stok = $current_satuan['stock'];
			$parent_stok = $current_satuan['parent_stock'] == '' ? 0 : $current_satuan['parent_stock'];
			$qty_parent = $current_satuan['qty_parent'] == '' ? 1 : $current_satuan['qty_parent'];
			// $parent_qty = $current_satuan['qty_parent'] == '' ? 0 : $current_satuan['qty_parent'];
			//   echo $current_stok.' - '.$parent_stok.' - '.$current_qty;die;
			//  echo $parent_stok;die;
			//     echo $current_qty;die; 
			//   echo $parent_stok;die;
			$konversi_stok = $parent_stok * $qty_parent;

			//  echo $konversi_stok;die;
			$sisa_current = $current_stok - $current_order;

			//	  echo $sisa_current;die;
			if ($sisa_current < 0) {
				$sisa_current *= -1;

				//    echo $sisa_current;die;
				$total_parent_stok = $konversi_stok - $sisa_current;

				//    echo $total_parent_stok;die;
				// echo $total_parent_stok.' '.$konversi_stok.' '.$sisa_current;die;
				//untuk update current stok
				$current_stok_change = intval($total_parent_stok) % intval($qty_parent);

				//    echo $current_stok_change;die;
				//    echo $current_stok_change;die;
				//untuk update parent stok
				$parrent_stok_change = ($total_parent_stok - $current_stok_change) / $qty_parent;
				//echo $parrent_stok_change;die;
				// $parrent_stok_change = $parent_stok - $parrent_stok_change;
				//update product stok current
				//     echo $parrent_stok_change;die;
				if ($parrent_stok_change >= 0) {
					//          

					$stock_valid = $parent_stok . $current_satuan['parent_satuan'] . '/' . $current_stok . '' . $current_satuan['current_satuan'];
					//     echo $current_stok;die;
					//     echo $current_stok_change;die;
					if ($parrent_stok_change <= 0 && $current_stok_change <= 0) {
						$is_valid = 0;
						if ($parrent_stok_change <= 0) {
							$message = "Stok " . $current_satuan['parent_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
						}

						if ($current_stok_change <= 0) {
							$message = "Stok " . $current_satuan['current_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
						}
					}
				} else {
					$is_valid = 0;
					$message = "Stok " . $current_satuan['parent_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
				}
			} else {
				//update product stok current
				$current_stok_change = $current_stok - $current_order;
				if ($current_stok_change >= 0) {
					//
					$stock_valid = $current_stok . ' ' . $current_satuan['current_satuan'];
				} else {
					$is_valid = 0;
					$message = "Stok " . $current_satuan['current_satuan'] . ' Kosong, Isi Stok Terlebih Dahulu';
				}
			}
		}

		return array(
			'is_valid' => $is_valid,
			'message' => $message,
			'stock' => $stock_valid
		);
	}

	public function validateBayar()
	{
		$order = $_POST['order_id'];
		$hutang = $_POST['hutang'];
		$uang_bayar = $_POST['uang_bayar'];
		$uang_kembali = $_POST['uang_kembali'];

		$hutang = $hutang < 0 ? $hutang * -1 : $hutang;

		$order_data = $this->getDetailDataOrder($order);
		$order_item = $this->getLisOrderItem($order);

		// echo '<pre>';
		// print_r($order_data);die;
		// echo '<pre>';
		// print_r($order_item);die;
		$invoice = 0;
		$is_valid = false;
		$is_save = true;
		$message = "";

		$this->db->trans_begin();
		try {

			$post_invoice['no_faktur'] = Modules::run('no_generator/generateNoFaktur');
			$post_invoice['pembeli'] = $order_data['pembeli'];
			$post_invoice['tanggal_faktur'] = $order_data['tanggal_faktur'];
			$post_invoice['tanggal_bayar'] = date('Y-m-d');
			$post_invoice['ref'] = $order;
			$post_invoice['potongan'] = $order_data['potongan'];
			if ($order_data['no_rekening'] != '') {
				$post_invoice['no_rekening'] = $order_data['no_rekening'];
			}
			$post_invoice['metode_bayar'] = $order_data['metode_bayar'];
			$post_invoice['pot_faktur'] = $order_data['pot_faktur'];
			$post_invoice['total'] = str_replace('.', '', $order_data['total']);
			if ($uang_kembali != '') {
				$post_invoice['uang_kembali'] = $uang_kembali;
			}

			$invoice = Modules::run('database/_insert', 'invoice', $post_invoice);

			$total = 0;
			foreach ($order_item as $value) {
				$post_item['qty'] = $value['qty'] - $value['retur_qty'];
				$post_item['sub_total'] = $value['sub_total'] - $value['retur_sub_total'];
				$post_item['product_satuan'] = $value['product_satuan'];
				$post_item['invoice'] = $invoice;
				$invoice_product = Modules::run('database/_insert', 'invoice_product', $post_item);


				$pot_item = $this->getListPotonganItem($value['id']);
				if (!empty($pot_item)) {
					foreach ($pot_item as $v_pot) {
						$post_pot['invoice_product'] = $invoice_product;
						$post_pot['potongan'] = $v_pot['potongan'];
						$post_pot['nilai'] = $v_pot['nilai'];
						Modules::run('database/_insert', 'invoice_pot_product', $post_pot);
					}
				}
				$total += $post_item['sub_total'];
			}

			//  Modules::run('database/_update', 'invoice', array('total' => $total), array('id' => $invoice));
			//invoice status
			$post_status['invoice'] = $invoice;
			$post_status['user'] = $this->session->userdata('user_id');
			$post_status['status'] = $hutang == 0 ? 'PAID' : 'DRAFT';
			Modules::run('database/_insert', 'invoice_status', $post_status);

			//invoice sisa
			$post_sisa['invoice'] = $invoice;
			$post_sisa['jumlah'] = str_replace('.', '', $hutang);
			Modules::run('database/_insert', 'invoice_sisa', $post_sisa);

			$post_status_order['order'] = $order;
			$post_status_order['status'] = 'VALIDATE';
			Modules::run('database/_insert', 'order_status', $post_status_order);

			//payment 
			$post = array();
			$post['no_faktur_bayar'] = Modules::run('no_generator/generateNoFakturBayar');
			$post['tanggal_faktur'] = date('Y-m-d H:i:s');
			$post['tanggal_bayar'] = date('Y-m-d');
			$post['jumlah'] = str_replace('.', '', $uang_bayar);
			$payment = Modules::run('database/_insert', 'payment', $post);

			//payment item
			$post = array();
			$post['payment'] = $payment;
			$post['invoice'] = $invoice;
			$post['jumlah_bayar'] = str_replace('.', '', $uang_bayar);
			Modules::run('database/_insert', 'payment_item', $post);

			if ($is_save) {
				$this->db->trans_commit();
				$is_valid = true;
			} else {
				$this->db->trans_rollback();
			}
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}


		echo json_encode(array('is_valid' => $is_valid, 'id' => $invoice, 'message' => $message));
	}

	public function getPotongan()
	{
		$data['list_potongan'] = $this->getListPotongan();
		$data['index'] = $_POST['index'];
		//  echo '<pre>';
		//  print_r($data);die;
		echo $this->load->view('potongan_view', $data, true);
	}

	public function getDetailOrderProduct($order_product)
	{
		$sql = "select op.*
	, roi.qty as retur_qty
	, roi.sub_total as retur_sub_total
	, ps.harga
	from order_product op
	join `order` o
		on o.id = op.`order`
	left join retur_order_item roi
		on roi.order_product = op.id
	join product_satuan ps
	 on ps.id = op.product_satuan
	where op.id = '" . $order_product . "'";

		$data = Modules::run('database/get_custom', $sql);
		//echo "<pre>";
		//echo $this->db->last_query();
		//die;
		//  echo $sql;die;
		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}

		//  echo '<pre>';
		//  print_r($result);die;
		return $result;
	}

	public function cancelItem()
	{
		$order = $_POST['order'];
		$id = $_POST['data_id'];

		$order_data = $this->getDetailDataOrder($order);
		$order_item = $this->getLisOrderItem($order);
		$order_detail_product = $this->getDetailOrderProduct($id);

		$pot_item = $this->getListPotonganItem($order_detail_product['id']);

		$is_valid = false;
		$this->db->trans_begin();
		try {

			$total_pot = 0;
			$harga_non_potongan = $order_detail_product['harga'] * $order_detail_product['qty'];

			if (!empty($pot_item)) {
				foreach ($pot_item as $value) {
					if ($value['jenis_potongan'] == 'Persentase') {
						$nilai_persentase = $value['nilai'];
						$dipotong = ($nilai_persentase * $harga_non_potongan) / 100;
						$total_pot += $dipotong;
					}

					if ($value['jenis_potongan'] == 'Nominal') {
						$total_pot += $value['nilai'];
					}
				}
			}

			//  echo $total_pot;die;	
			Modules::run('database/_delete', 'order_pot_product', array('order_product' => $id));
			Modules::run('database/_delete', 'order_product', array('id' => $id));

			$data_order = $this->getLisOrderItem($order);
			$total = 0;
			if (!empty($data_order)) {
				foreach ($data_order as $value) {
					$total += $value['sub_total'];
				}

				$total += $total_pot;
			}

			$post_update['total'] = $total;
			Modules::run('database/_update', $this->getTableName(), $post_update, array('id' => $order));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function execUbahQty()
	{
		//  echo '<pre>';
		//  print_r($_POST);die;
		//  $data_order_product = $this->getListInvoiceItem($_POST['order']);

		$message = "";

		$is_valid = false;
		$this->db->trans_begin();

		try {
			//update invoice_product
			$post_ip['qty'] = $_POST['qty_ubah'];
			$post_ip['sub_total'] = $_POST['sub_total'];
			Modules::run('database/_update', 'order_product', $post_ip, array('id' => $_POST['order_product']));

			$data_order_product = $this->getListInvoiceItem($_POST['order'], $_POST['order_product']);
			$valid_stok = $data_order_product[0]['stok_data']['is_valid'];
			$message = $data_order_product[0]['stok_data']['message'];

			//   echo '<pre>';
			//   print_r($data_order_product);die;
			$data_order_product = $this->getListInvoiceItem($_POST['order']);
			//   echo '<pre>';
			//   print_r($data_order_product);die;
			$total = 0;
			foreach ($data_order_product as $value) {
				$total += $value['sub_total'];
			}

			//update invoice
			$post_invoice['total'] = $total;
			Modules::run('database/_update', $this->getTableName(), $post_invoice, array('id' => $_POST['order']));

			$this->db->trans_commit();
			if ($valid_stok == 0) {
				$is_valid = false;
			} else {
				$is_valid = true;
			}
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'message' => $message));
	}

	public function showKonfirmasiPembayaran()
	{
		$order = $_POST['order_id'];
		$data['order'] = $order;
		$data['totalBayar'] = $_POST['totalBayar'];
		echo $this->load->view('konfirmasi_bayar', $data, true);
	}

	public function chooseProduk()
	{
		$data = $_POST;
		$data['list_satuan'] = $this->order->getDataProdukSautanHarga($data['product']);
		$data['data_satuan_terkecil'] = $this->order->getDataSatuanTerkecilProduk($data);
		// echo '<pre>';
		// print_r($data);
		// die;
		echo $this->load->view('product_item_new', $data, true);
	}

	public function scanBarcodeProduk()
	{
		$data = $_POST;
		$data['list_product'] = $this->order->getDataProdukOutstanding($data);
		echo $this->load->view('product_item_data', $data, true);
	}

	public function scanCariProduk()
	{
		$data = $_POST;
		$data['list_product'] = $this->order->getDataProdukOutstandingBarang($data);
		echo $this->load->view('product_item_data', $data, true);
	}
}
