<?php

class M_order extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getDataProdukOutstanding($params = array())
	{
		$filter = "";
		if (!empty($params)) {
			if ($params['barcode'] != '') {
				$filter = "and p.kodebarcode = '" . $params['barcode'] . "'";
			}
		}
		$sql = "
		select 
		p.*
		, ps.stock 
		from product p 
		left join (select max(id) id, product from product_stock group by product) ps_max
			on  ps_max.product = p.id
		left join product_stock ps 
			on ps.id = ps_max.id
		left join product_satuan pst
			on ps.product_satuan = pst.id 
		left JOIN satuan s 
			on s.id = pst.satuan 
		where p.deleted = 0
		and 
		pst.satuan_terkecil = 1
		and pst.deleted = 0
		" . $filter;

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);
		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataProdukOutstandingBarang($params = array())
	{
		$filter = "";
		if (!empty($params)) {
			if ($params['barang'] != '') {
				$filter = "and p.product like '%" . $params['barang'] . "%'";
			}
		}
		$sql = "
		select 
		p.*
		, ps.stock 
		from product p 
		left join (select max(id) id, product from product_stock group by product) ps_max
			on  ps_max.product = p.id
		left join product_stock ps 
			on ps.id = ps_max.id
		left join product_satuan pst
			on ps.product_satuan = pst.id 
		left JOIN satuan s 
			on s.id = pst.satuan 
		where p.deleted = 0
		and 
		pst.satuan_terkecil = 1
		and pst.deleted = 0
		" . $filter;

		// echo '<pre>';
		// print_r($sql);
		// die;

		$data = $this->db->query($sql);
		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataProdukSautanHarga($product)
	{
		$sql = "select 
		ps.*
		, s.nama_satuan 
		, ps.qty as konversi
		, ps.ket_harga as keterangan
		from product_satuan ps
		join satuan s 
			on s.id = ps.satuan 
		where ps.product = " . $product . " and ps.deleted = 0";

		$data = $this->db->query($sql);
		$result = array();
		if (!empty($data->result_array())) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataSatuanTerkecilProduk($params)
	{
		$sql = "select 
		ps.*
		, s.nama_satuan 
		, ps.qty as konversi
		, ps.ket_harga as keterangan
		from product_satuan ps
		join satuan s 
			on s.id = ps.satuan 
		where ps.product = " . $params['product'] . " and ps.deleted = 0
		and ps.satuan_terkecil = 1";

		$data = $this->db->query($sql);
		$result = array();
		if (!empty($data->result_array())) {
			$result = $data->row_array();
		}

		return $result;
	}
}
