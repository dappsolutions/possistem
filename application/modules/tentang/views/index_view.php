<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class="row">
     <div class="col-md-12 text-center">
      <p><img class="align-content" src="<?php echo base_url() ?>assets/images/logo/I-Muamalah-logo.png" alt="Logo Menu" id="logo" width="175" height="34"><br>versi 1.0 (Startup)</p>
      Dikembangkan oleh GreenholeTech (CV. Greenhole Technology Solution)
      <br>
      Email: <a href="mailto:support@greenholetech.com">support@greenholetech.com</a>
      <br>
      WA / Telp: <a href="https://wa.me/6281328185987?text=Ingin%20upgrade%20versi%20iMuamalah%20..." target="_blank">0813.2818.5987</a>
      <br>
      Website : <a href="https://greenholetech.com" target="_blank">www.greenholetech.com</a>
     </div>          
    </div> 
   </div>
  </div>
 </div>
</div>
