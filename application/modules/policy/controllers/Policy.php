<?php

class Policy extends MX_Controller {

 public function __construct() {
  parent::__construct();
 }

 public function getModuleName() {
  return 'policy';
 }

 public function index() {
  echo $this->load->view('index');
 }

}
