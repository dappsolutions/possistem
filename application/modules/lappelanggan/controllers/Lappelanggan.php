<?php

class Lappelanggan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'lappelanggan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
						'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
						'<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
						'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">',
						'<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/min/moment.min.js"></script>',
						'<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/lappelanggan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pembeli';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pelanggan";
  $data['title_content'] = 'Data Pelanggan';

//  echo 'asdasd';die;
  $content = $this->getDataPelangganDetail();
  $data['data_pelanggan'] = $content['data'];
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pelanggan";
  $data['title_content'] = 'Data Pelanggan';
  $content = $this->getDataPelangganDetail($keyword);
  $data['data_pelanggan'] = $content['data'];  
  
  echo Modules::run('template', $data);
 }

 public function getDataPelangganDetail($keyword = '') {
  $sql = "select * from pembeli where deleted = 0 or deleted is null
 ";

		$data = Modules::run('database/get_custom', $sql);
//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
  );
 }
	
	public function getDataPelangganDetailSearch($tgl_awal, $tgl_akhir) {

		$where = "where i.createddate >= '".$tgl_awal."' and i.createddate <= '".$tgl_akhir."' and i.deleted = 0";

  $sql = "select 
		i.no_faktur
		, ipt.createddate as tgl_cetak
		, i.tanggal_bayar as tgl_jatuh_tempo
		, pb.nama as customer
  , i.tanggal_faktur
		, pb.alamat 
		, 'RETAIL' as jenis_customer
		, o.no_order 	
		, pg.nama as nama_sales
		, gd.nama_gudang
		, p.kode_product
		, p.product as nama_product	
		, kp.kategori
		, tp.tipe as tipe_product
		, vd.nama_vendor
		, ip.qty as total_qty
		, st.nama_satuan as current_satuan
		, ps.qty as total_current_qty
		, ip.sub_total
		, ipp.nilai total_potongan
		, pt.potongan as jenis_potongan
		, 'Pelanggan' as jenis_transaksi
		, ps.harga as harga_jual
	from invoice i
	join invoice_product ip
		on ip.invoice = i.id
	left join (select max(id) id, invoice from invoice_print group by invoice) iprt
		on iprt.invoice = i.id
	left join invoice_print ipt
		on ipt.id = iprt.id
	join pembeli pb
		on pb.id = i.pembeli
	left join `order` o
		on i.`ref` = o.id
	left join `user` us
		on us.id = o.createdby
	left join pegawai pg
		on pg.id = us.pegawai
	join product_satuan ps
		on ps.id = ip.product_satuan
	join product_stock psst
		on psst.product_satuan = ps.id
	join product p
		on p.id = ps.product
	join gudang gd
		on gd.id = psst.gudang
	join kategori_product kp
		on kp.id = p.kategori_product
	join tipe_product tp
		on tp.id = p.tipe_product
	left join (select max(id) id, product from procurement_item group by product) proc_item
		on proc_item.product = p.id
	left join procurement_item pit
		on pit.id = proc_item.id
	left join procurement pr
		on pr.id = pit.procurement
	left join vendor vd
		on vd.id = pr.vendor
	join satuan st
		on ps.satuan = st.id
	left join (select max(id) id, invoice_product from invoice_pot_product group by invoice_product) ippt
		on ippt.invoice_product = ip.id
	left join invoice_pot_product ipp
		on ipp.id = ippt.id
	left join potongan pt
		on pt.id = ipp.potongan ".$where;

		$data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
  );
 }

 public function getData() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pelanggan";
  $data['title_content'] = 'Data Pelanggan';
  $data['kas'] = $this->getDataKasMasuk();

  echo $this->load->view('detail_data', $data, true);
 }

 public function getDataBagiHasil() {
  $data = Modules::run('database/get', array(
              'table' => 'kerja_sama_internal',
              'where' => "deleted is null or deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getTotalDataLabaRugi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.no_hp', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
  ));

  return $total;
 }

 public function getDataFakturTotalMasuk() {
  if (!empty($_POST)) {
   $keyword = date('Y', strtotime($this->input->post('keyword')));
  } else {
   $keyword = date('Y');
  }

  $query = <<<QUERY
  SELECT 
	p.id AS pembelian 
	, QUARTER(p.createddate) AS quartal
	, p.no_invoice
	, pm.nama AS nama_pembeli
	, sp.status AS status_beli
	, r.product
	, rhhjc.harga AS harga_cash
	, rhhjk.harga AS harga_kredit
	, phr.id AS pembeli_has_product
	, pha.total_bayar AS bayar_angsuran
	, phc.total_bayar AS bayar_cash
FROM pembelian p
JOIN pembeli_has_product phr ON p.id = phr.pembelian
JOIN pembeli pm ON phr.pembeli = pm.id
JOIN status_pembelian sp ON phr.status_pembelian = sp.id
JOIN product r ON phr.product = r.id
LEFT JOIN product_has_harga_jual_pokok rhhjc ON r.id = rhhjc.product AND rhhjc.period_end IS NULL
LEFT JOIN product_has_harga_jual_tunai rhhjk ON r.id = rhhjk.product AND rhhjk.period_end IS NULL
JOIN pembayaran_product pr ON phr.id = pr.pembeli_has_product
LEFT JOIN pembayaran_has_cash phc ON pr.id = phc.pembayaran_product
LEFT JOIN pembayaran_has_angsuran pha ON pr.id = pha.pembayaran_product
WHERE p.createddate LIKE '%$keyword%' OR pha.tgl_angsuran LIKE '%$keyword%' OR phc.tgl_bayar LIKE '%$keyword%'
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['bayar_angsuran'] == '' ? $value['bayar_cash'] : $value['bayar_angsuran'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;
  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataKasMasuk() {
  if (!empty($_POST)) {
   $keyword = date('Y', strtotime($this->input->post('keyword')));
  } else {
   $keyword = date('Y');
  }

  $query = <<<QUERY
  SELECT 
k.jumlah 
, QUARTER(k.createddate) AS quartal
FROM kas k
WHERE k.createddate LIKE '%$keyword%'
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['jumlah'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;

  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataTagihan() {
  if (!empty($_POST)) {
   $keyword = date('Y', strtotime($this->input->post('keyword')));
  } else {
   $keyword = date('Y');
  }
  $query = <<<QUERY
  
SELECT 
pt.total
, QUARTER(pt.createddate) AS quartal
FROM pembayaran_tagihan pt
WHERE pt.createddate LIKE '%$keyword%'
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['total'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;
  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataBayarVendor() {
  if (!empty($_POST)) {
   $keyword = date('Y', strtotime($this->input->post('keyword')));
  } else {
   $keyword = date('Y');
  }
  $query = <<<QUERY
    SELECT 
    pv.total
    , QUARTER(pv.createddate) AS quartal
    FROM pembayaran_vendor pv
    WHERE pv.createddate LIKE '%$keyword%'
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['total'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;
  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataBayarLain() {
  if (!empty($_POST)) {
   $keyword = date('Y', strtotime($this->input->post('keyword')));
  } else {
   $keyword = date('Y');
  }
  $query = <<<QUERY
    
    SELECT 
    pl.total
    , QUARTER(pl.createddate) AS quartal
    FROM pembayaran_lain_lain pl
    WHERE pl.createddate LIKE '%$keyword%'
QUERY;

  $data = Modules::run('database/get_custom', $query);

  $result = array();
  $total_quartal_satu = 0;
  $total_quartal_dua = 0;
  $total_quartal_tiga = 0;
  $total_quartal_empat = 0;
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $total = $value['total'];
    switch ($value['quartal']) {
     case 1:
      $total_quartal_satu += $total;
      break;
     case 2:
      $total_quartal_dua += $total;
      break;
     case 3:
      $total_quartal_tiga += $total;
      break;
     case 4:
      $total_quartal_empat += $total;
      break;
    }
    array_push($result, $value);
   }
  }

  $total = array();
  $total['total_satu'] = $total_quartal_satu;
  $total['total_dua'] = $total_quartal_dua;
  $total['total_tiga'] = $total_quartal_tiga;
  $total['total_empat'] = $total_quartal_empat;
  $total['total'] = $total_quartal_satu + $total_quartal_dua + $total_quartal_tiga + $total_quartal_empat;
  return $total;
 }

 public function getDataLabaRugi($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.nama', $keyword),
       array('p.no_hp', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataLabaRugi($keyword)
  );
 }

 public function getDetailDataLabaRugi($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah LabaRugi";
  $data['title_content'] = 'Tambah LabaRugi';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataLabaRugi($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah LabaRugi";
  $data['title_content'] = 'Ubah LabaRugi';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataLabaRugi($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail LabaRugi";
  $data['title_content'] = 'Detail LabaRugi';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['nama'] = $value->nama;
  $data['no_hp'] = $value->no_hp;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $tipe_product = $id;
  $this->db->trans_begin();
  try {
   $post_tipe_product = $this->getPostDataHeader($data);
   if ($id == '') {
    $tipe_product = Modules::run('database/_insert', $this->getTableName(), $post_tipe_product);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_tipe_product, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'tipe_product' => $tipe_product));
 }

	public function tampilkan(){
		list($tgl_awal, $tgl_akhir) = explode('-', $_POST['tanggal']);
		$tgl_awal = date('Y-m-d', strtotime(trim($tgl_awal)));
		$tgl_akhir = date('Y-m-d', strtotime(trim($tgl_akhir)));
		
		$data_laporan = $this->getDataPelangganDetailSearch($tgl_awal, $tgl_akhir);
		// echo '<pre>';
		// print_r($data_laporan);die;
		$data['data_Pelanggan'] = $data_laporan['data'];
		echo $this->load->view('table_laporan', $data, true);
	}
}
