<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class="row">
					<div class="col-md-8">
						<a class="btn btn-success" download="<?php echo 'Laporan Pembelian' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Pelanggan');">Export</a>
					</div>
				</div>
				<br />
				<br />
				<div class="row">
					<div class="col-md-12">
						<div class="input-group">
							<input type="text" class="form-control" onkeyup="LapPembelian.search(this, event)" id="keyword" placeholder="Pencarian">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
						</div>
					</div>
				</div>
				<br />
				<div class='row'>
					<div class='col-md-12'>
						<?php if (isset($keyword)) { ?>
							<?php if ($keyword != '') { ?>
								Cari Data : "<b><?php echo $keyword; ?></b>"
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12">
						<div class='table-responsive' id="data_detail">
							<table class="table table-bordered" id="tb_laporan">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>No</th>
										<th>No Pembelian</th>
										<th>Produk</th>
										<th>Harga Beli (Rp)</th>
										<th>Supplier</th>
										<th>Jumlah</th>
										<th>Total</th>
										<th>Tanggal</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($data_pembelian)) { ?>
										<?php $no = $pagination['last_no'] + 1; ?>
										<?php foreach ($data_pembelian as $value) { ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $value['no_faktur'] ?></td>
												<td><?php echo $value['product'] ?></td>
												<td><?php echo number_format($value['harga']) ?></td>
												<td><?php echo $value['nama_vendor'] ?></td>
												<td><?php echo $value['qty'] ?></td>
												<td><?php echo number_format(($value['qty'] * $value['harga'])) ?></td>
												<td><?php echo $value['createddate'] ?></td>
											</tr>
										<?php } ?>
									<?php } else { ?>
										<tr>
											<td colspan="10" class="text-center">Tidak ada data ditemukan</td>
										</tr>
									<?php } ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="box-footer clearfix">
				<ul class="pagination pagination-sm no-margin pull-right">
					<?php echo $pagination['links'] ?>
				</ul>
			</div>
		</div>
	</div>
</div>
