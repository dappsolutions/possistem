<?php

class Template extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		if ($this->session->userdata('user_id') == '') {
			redirect(base_url());
		}
	}

	public function index($data)
	{
		//  echo '<pre>';
		//  print_r($_SESSION);die;
		$data['general_title'] = $this->getDataGeneralTitle();
		$data['notif_faktur'] = $this->getNotifikasiFaktur();
		$data['notif_order'] = $this->getNotifikasiOrder();
		$data['notif_retur'] = $this->getNotifikasiReturOrder();
		//  echo '<pre>';
		//  print_r($data);die;
		$data['total_notif'] = count($data['notif_faktur']) +
			count($data['notif_order']) +
			count($data['notif_retur']);

		echo $this->load->view('template_view', $data, true);
	}

	public function getDataGeneralTitle()
	{
		$data = Modules::run('database/get', array(
			'table' => 'general',
		));

		$title = "";
		if (!empty($data)) {
			$title = $data->row_array()['title'];
		}


		return $title;
	}

	public function getNotifikasiFaktur()
	{
		$sql = "select i.*
	from invoice i
	join (SELECT max(id) id, invoice from invoice_status group by invoice) iss
		on iss.invoice = i.id
	join invoice_status isa 
		on isa.id = iss.id
	where isa.status = 'DRAFT' and i.deleted = 0 order by i.id desc";


		$data = Modules::run('database/get_custom', $sql, 5);

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getNotifikasiOrder()
	{
		$sql = "select o.*
	from `order` o
 join (select max(id) id, `order` from order_status group by `order`) oss
  on oss.order = o.id
 join order_status os
  on os.id = oss.id
  where os.status = 'DRAFT' and o.deleted = 0
	order by o.id desc";


		$data = Modules::run('database/get_custom', $sql, 5);

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getNotifikasiReturOrder()
	{
		$sql = "select ro.*
	from retur_order ro
join `order` o
	on o.id = ro.id
 join (select max(id) id, `order` from order_status group by `order`) oss
  on oss.order = o.id
 join order_status os
  on os.id = oss.id
	where ro.deleted = 0 and os.status = 'DRAFT' and ro.deleted = 0
	order by ro.id desc";


		$data = Modules::run('database/get_custom', $sql, 5);

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}
}
