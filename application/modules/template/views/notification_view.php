<li>
 <!-- inner menu: contains the actual data -->
 <ul class="menu">
  <?php if (!empty($notif_faktur)) { ?>
   <?php foreach ($notif_faktur as $value) { ?>
    <li>
     <a href="<?php echo base_url() . 'faktur_pelanggan/detail/' . $value['id'] ?>">
      <i class="fa fa-file-text-o text-warning"></i> Faktur <b><?php echo $value['no_faktur'] ?></b> Telah Dibuat
     </a>
    </li>
   <?php } ?>
  <?php } ?>  

  <?php if (!empty($notif_order)) { ?>
   <?php foreach ($notif_order as $value) { ?>
    <li>
     <a href="<?php echo base_url() . 'order/detail/' . $value['id'] ?>">
      <i class="fa fa-file-text-o text-danger"></i> Order <b><?php echo $value['no_order'] ?></b> Telah Dibuat
     </a>
    </li>
   <?php } ?>
  <?php } ?>  
  
   <?php if (!empty($notif_retur)) { ?>
   <?php foreach ($notif_retur as $value) { ?>
    <li>
     <a href="<?php echo base_url() . 'returorder/detail/' . $value['id'] ?>">
      <i class="fa fa-file-text-o text-danger"></i> Retur Order <b><?php echo $value['no_retur'] ?></b> 
     </a>
    </li>
   <?php } ?>
  <?php } ?>  
 </ul>
</li>