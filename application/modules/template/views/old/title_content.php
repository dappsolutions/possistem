<div class="breadcrumbs" style="margin-top: 12px;">
 <div class="col-sm-8">
  <div class="page-header float-left">
   <div class="page-title">
    <h1><?php echo isset($title_content) ? $title_content : '' ?></h1>
   </div>
  </div>
 </div>
 <div class="col-sm-8">
  <div class="page-header float-right">
   <div class="page-title">
    <ol class="breadcrumb text-right">
     <li class="active"><?php echo isset($title_content) ? $title_content : '' ?></li>
    </ol>
   </div>
  </div>
 </div>
</div>
