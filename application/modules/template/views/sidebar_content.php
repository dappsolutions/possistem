<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo base_url() ?>assets/images/images.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo ucfirst($this->session->userdata('username')) ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<?php echo $this->load->view('akses_superadmin'); ?>
		<?php echo $this->load->view('akses_kasir'); ?>
		<?php //echo $this->load->view('akses_sales'); 
		?>
		<?php //echo $this->load->view('akses_hrd'); 
		?>
		<?php //echo $this->load->view('akses_akunting'); 
		?>
		<?php //echo $this->load->view('akses_direktur'); 
		?>
	</section>
</aside>
