<?php if ($this->session->userdata('hak_akses') == "kasir") { ?>
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Penjualan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'order' ?>"><i class="fa fa-file-text-o"></i> Order </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Penagihan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'faktur_pelanggan' ?>"><i class="fa fa-file-text-o"></i> Faktur </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Inventori</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'product_stock' ?>"><i class="fa fa-file-text-o"></i> Stok </a></li>
			</ul>
		</li>
		<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	</ul>

<?php } ?>
