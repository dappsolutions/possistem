<?php if ($this->session->userdata('hak_akses') == "superadmin") { ?>
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>
		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Penjualan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'order' ?>"><i class="fa fa-file-text-o"></i> Order </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Penagihan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'faktur_pelanggan' ?>"><i class="fa fa-file-text-o"></i> Faktur </a></li>
				<li><a href="<?php echo base_url() . 'payment' ?>"><i class="fa fa-file-text-o"></i> Histori Cicilan </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Pembelian</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'supplier' ?>"><i class="fa fa-file-text-o"></i> Supplier </a></li>
				<li><a href="<?php echo base_url() . 'pengadaan' ?>"><i class="fa fa-file-text-o"></i> Pengadaan </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'laba_rugi' ?>"><i class="fa fa-file-text-o"></i> Laba Rugi </a></li>
				<li><a href="<?php echo base_url() . 'lappenjualan' ?>"><i class="fa fa-file-text-o"></i> Penjualan </a></li>
				<li><a href="<?php echo base_url() . 'lappembelian' ?>"><i class="fa fa-file-text-o"></i> Pembelian </a></li>
				<li><a href="<?php echo base_url() . 'lappelanggan' ?>"><i class="fa fa-file-text-o"></i> Pelanggan </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Inventori</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<!-- <li><a href="<?php echo base_url() . 'gudang' ?>"><i class="fa fa-file-text-o"></i> Gudang </a></li>
				<li><a href="<?php echo base_url() . 'rak' ?>"><i class="fa fa-file-text-o"></i> Rak </a></li> -->
				<li><a href="<?php echo base_url() . 'pelanggan' ?>"><i class="fa fa-file-text-o"></i> Pelanggan </a></li>
				<li><a href="<?php echo base_url() . 'produk' ?>"><i class="fa fa-file-text-o"></i> Produk </a></li>
				<li><a href="<?php echo base_url() . 'tipe_produk' ?>"><i class="fa fa-file-text-o"></i> Tipe Produk </a></li>
				<li><a href="<?php echo base_url() . 'unit' ?>"><i class="fa fa-file-text-o"></i> Satuan </a></li>
				<li><a href="<?php echo base_url() . 'satuan' ?>"><i class="fa fa-file-text-o"></i> Produk Satuan </a></li>
				<li><a href="<?php echo base_url() . 'product_stock' ?>"><i class="fa fa-file-text-o"></i> Stok </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Personalia</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'pegawai' ?>"><i class="fa fa-file-text-o"></i> Karyawan </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Pengaturan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'general' ?>"><i class="fa fa-file-text-o"></i> Umum </a></li>
				<li><a href="<?php echo base_url() . 'pengguna' ?>"><i class="fa fa-file-text-o"></i> Pengguna </a></li>
				<!--<li><a href="<?php echo base_url() . 'tanggal' ?>"><i class="fa fa-file-text-o"></i> Tanggal </a></li>-->
			</ul>
		</li>
		<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	</ul>

<?php } ?>
