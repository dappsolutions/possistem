<?php

class Satuan extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'satuan';
	}

	public function getHeaderJSandCSS()
	{
		//versioning
		$version = str_shuffle("1234567890abcdefghijklmnopqrstuvwxyz");
		$version = substr($version, 0, 11);
		//versioning

		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/satuan.js?v=' . $version . '"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'product_satuan';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Satuan";
		$data['title_content'] = 'Data Satuan';
		$content = $this->getDataSatuan();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function getTotalDataSatuan($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.product', $keyword),
				array('s.nama_satuan', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => $this->getTableName() . ' k',
			'field' => array(
				'k.*',
				'p.product as nama_product', 's.nama_satuan'
			),
			'join' => array(
				array('product p', 'k.product = p.id'),
				array('satuan s', 's.id = k.satuan', 'left'),
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => "k.deleted is null or k.deleted = 0"
		));

		return $total;
	}

	public function getDataSatuan($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('p.product', $keyword),
				array('s.nama_satuan', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' k',
			'field' => array(
				'k.*',
				'p.product as nama_product', 's.nama_satuan',
				'hg.harga as harga_jual_fix'
			),
			'join' => array(
				array('product p', 'k.product = p.id'),
				array('(select max(id) id, product_satuan from product_has_harga_jual group by product_satuan) hg_max', 'hg_max.product_satuan = k.id'),
				array('product_has_harga_jual hg', 'hg.id = hg_max.id'),
				array('satuan s', 's.id = k.satuan', 'left'),
			),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
			'where' => "k.deleted is null or k.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataSatuan($keyword)
		);
	}

	public function getDetailDataSatuan($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' kr',
			'field' => array(
				'kr.*', 'p.product as nama_product',
				's.nama_satuan', 'sp.nama_satuan as satuan_parent',
				'hg.harga as harga_jual_fix',
				'hgs.harga harga_grosir_fix'
			),
			'join' => array(
				array('product p', 'kr.product = p.id'),
				array('(select max(id) id, product_satuan from product_has_harga_jual group by product_satuan) hg_max', 'hg_max.product_satuan = kr.id'),
				array('product_has_harga_jual hg', 'hg.id = hg_max.id'),
				array('(select max(id) id, product_satuan from product_has_harga_grosir group by product_satuan) hgs_max', 'hgs_max.product_satuan = kr.id', 'left'),
				array('product_has_harga_grosir hgs', 'hgs.id = hgs_max.id', 'left'),
				array('satuan s', 's.id = kr.satuan', 'left'),
				array('satuan sp', 'sp.id = s.parent', 'left')
			),
			'where' => "kr.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListProduct()
	{
		$data = Modules::run('database/get', array(
			'table' => 'product p',
			'field' => array('p.*'),
			'where' => "p.deleted = 0 or p.deleted is null"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListSatuan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'satuan sa',
			'field' => array('sa.*', 's.nama_satuan as satuan_parent'),
			'join' => array(
				array('satuan s', 'sa.parent = s.id', 'left')
			),
			'where' => "sa.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListHargaJual($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'product_has_harga_jual hg',
			'field' => array('hg.*'),
			'where' => array('hg.product_satuan' => $id),
			'orderby' => 'hg.id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListHargaJualGrosir($id)
	{
		$data = Modules::run('database/get', array(
			'table' => 'product_has_harga_grosir hg',
			'field' => array('hg.*'),
			'where' => array('hg.product_satuan' => $id),
			'orderby' => 'hg.id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Satuan";
		$data['title_content'] = 'Tambah Satuan';
		$data['list_product'] = $this->getListProduct();
		$data['list_satuan'] = $this->getListSatuan();
		//  echo '<pre>';
		//  print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function ubahHargaJual($id)
	{
		$data['module'] = $this->getModuleName();
		$data['data'] = $this->getListHargaJual($id);
		$data['product_satuan'] = $id;

		echo $this->load->view('form_harga_jual', $data, true);
	}

	public function ubahHargaGrosir($id)
	{
		$data['module'] = $this->getModuleName();
		$data['data'] = $this->getListHargaJualGrosir($id);
		$data['product_satuan'] = $id;

		echo $this->load->view('form_harga_grosir', $data, true);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataSatuan($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Satuan";
		$data['title_content'] = 'Ubah Satuan';
		$data['list_product'] = $this->getListProduct();
		$data['list_satuan'] = $this->getListSatuan();
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = $this->getDetailDataSatuan($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Satuan";
		$data['title_content'] = 'Detail Satuan';
		echo Modules::run('template', $data);
	}

	public function getPostDataHeader($value)
	{
		$data['product'] = $value->product;
		$data['satuan'] = $value->satuan;
		//  $data['level'] = $value->level;
		if ($value->qty != '') {
			$data['qty'] = $value->qty;
		}
		$data['harga'] = str_replace('.', '', $value->harga);
		$data['harga_beli'] = str_replace('.', '', $value->harga_beli);
		$data['ket_harga'] = str_replace('.', '', $value->ket_harga);
		$data['satuan_terkecil'] = $value->satuan_terkecil;
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		$id = $this->input->post('id');
		$is_valid = false;

		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader($data);
			if ($id == '') {
				$id = Modules::run('database/_insert', $this->getTableName(), $post_data);
				//insert product has harga jual	


				$post_data = array();
				$post_data['product_satuan'] = $id;
				$post_data['harga'] = str_replace('.', '', $data->harga);
				Modules::run('database/_insert', 'product_has_harga_jual', $post_data);
			} else {
				//update
				Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Satuan";
		$data['title_content'] = 'Data Satuan';
		$content = $this->getDataSatuan($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function execUbahHargaJual()
	{
		$product_satuan = $_POST['product_satuan'];
		$harga = $_POST['harga'];
		$is_valid = false;
		$this->db->trans_begin();
		try {

			$post_data = array();
			$post_data['product_satuan'] = $product_satuan;
			$post_data['harga'] = str_replace('.', '', $harga);
			Modules::run('database/_insert', 'product_has_harga_jual', $post_data);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function execUbahHargaJualGrosir()
	{
		$product_satuan = $_POST['product_satuan'];
		$harga = $_POST['harga'];
		$is_valid = false;
		$this->db->trans_begin();
		try {

			$post_data = array();
			$post_data['product_satuan'] = $product_satuan;
			$post_data['harga'] = str_replace('.', '', $harga);
			Modules::run('database/_insert', 'product_has_harga_grosir', $post_data);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}
}
