<input type="hidden" id="product_satuan" value="<?php echo $product_satuan ?>">

<div class="row">
	<div class="col-md-4">
		<input type="text" id="harga_jual_ubah" class="form-control required col-sm-6" error="Harga Jual" placeholder="Harga Jual">
	</div>
	<div class="col-md-6">
		<button class="btn btn-success" onclick="Satuan.execUbahHargaJualGrosir(this)">Proses</button>
	</div>
</div>
<br>

<div class="row">
	<div class="col-md-12">
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead class="bg-primary">
					<tr>
						<th>No</th>
						<th>Harga</th>
						<th>Dibuat</th>
					</tr>
				</thead>
				<tbody>
					<?php if (!empty($data)) { ?>
						<?php $no = 1 ?>
						<?php foreach ($data as $key => $value) { ?>
							<?php $bg_active = $key == 0 ? 'bg-success' : 'bg-danger' ?>
							<tr class="<?php echo $bg_active ?>">
								<td><?php echo $no++ ?></td>
								<td><?php echo number_format($value['harga']) ?></td>
								<td><?php echo $value['createddate'] ?></td>
							</tr>
						<?php } ?>
					<?php } else { ?>
						<tr>
							<td colspan="2">Tidak ada data ditemukan</td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
