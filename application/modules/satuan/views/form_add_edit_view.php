<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Stok</u>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Produk
					</div>
					<div class='col-md-3'>
						<select class="form-control required" id="product" error="Produk">
							<option value="">Pilih Produk</option>
							<?php if (!empty($list_product)) { ?>
								<?php foreach ($list_product as $value) { ?>
									<?php $selected = '' ?>
									<?php if (isset($product)) { ?>
										<?php $selected = $product == $value['id'] ? 'selected' : '' ?>
									<?php } ?>
									<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['kode_product'] . ' - ' . $value['product'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Satuan
					</div>
					<div class='col-md-3'>
						<select class="form-control required" id="satuan" error="Satuan">
							<option value="">Pilih Satuan</option>
							<?php if (!empty($list_satuan)) { ?>
								<?php foreach ($list_satuan as $value) { ?>
									<?php $selected = '' ?>
									<?php if (isset($satuan)) { ?>
										<?php $selected = $satuan == $value['id'] ? 'selected' : '' ?>
									<?php } ?>
									<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_satuan'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Jumlah
					</div>
					<div class='col-md-3'>
						<input type='text' name='' placeholder="" id='qty' class='form-control required text-right' value='<?php echo isset($qty) ? $qty : '' ?>' error="Jumlah Isi" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Satuan Terkecil
					</div>
					<div class='col-md-3 text-right'>
						<input type="checkbox" id="satuan_terkecil" <?php echo isset($satuan_terkecil) ? $satuan_terkecil == '1' ? 'checked' : '' : '' ?>>
					</div>
				</div>
				<br />

				<?php $readonly = isset($id) ? 'readonly' : '' ?>
				<div class="row">
					<div class='col-md-3 text-bold'>
						Harga Beli
					</div>
					<div class='col-md-3'>
						<input type='text' name='' placeholder="" id='harga_beli' class='form-control required text-right' value='<?php echo isset($harga_beli) ? $harga_beli : '' ?>' error="Harga Beli" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Harga Jual
					</div>
					<div class='col-md-3'>
						<input type='text' <?php echo $readonly ?> name='' placeholder="" id='harga' class='form-control required text-right' value='<?php echo isset($harga_jual_fix) ? $harga_jual_fix : '' ?>' error="Harga Jual" />
						<?php if (isset($id)) { ?>
							<div class="text-right">
								<button style="margin-top: 12px;font-size: 10px;" class="btn btn-info" onclick="Satuan.ubahHargaJual(<?php echo $id ?>)">Ubah Harga Jual</button>
							</div>
						<?php } ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Keterangan Harga Jual
					</div>
					<div class='col-md-3'>
						<input type='text' name='' placeholder="" id='keterangan' class='form-control text-right' value='<?php echo isset($ket_harga) ? $ket_harga : '' ?>' placeholder="Keterangan Harga" />
					</div>
				</div>
				<br />
				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="Satuan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="Satuan.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
