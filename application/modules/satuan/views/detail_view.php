<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Satuan</u>
					</div>
				</div>
				<hr />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Produk
					</div>
					<div class='col-md-3'>
						<?php echo $nama_product ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Satuan
					</div>
					<div class='col-md-3'>
						<?php echo  $nama_satuan ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Jumlah
					</div>
					<div class='col-md-3'>
						<?php echo $qty ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Satuan Terkecil
					</div>
					<div class='col-md-3'>
						<?php echo $satuan_terkecil == '1' ? 'Ya' : '' ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Harga Beli
					</div>
					<div class='col-md-3'>
						<?php echo 'Rp, ' . number_format($harga_beli) ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Harga Jual
					</div>
					<div class='col-md-3'>
						<?php echo 'Rp, ' . number_format($harga_jual_fix) ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Keterangan Harga
					</div>
					<div class='col-md-3'>
						<?php echo $ket_harga ?>
					</div>
				</div>
				<br />
				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="Satuan.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
