<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type='hidden' name='' id='no_faktur' class='form-control' value='<?php echo isset($no_faktur) ? $no_faktur : '' ?>' />
<input type='hidden' name='' id='tgl_bayar_old' class='form-control' value='<?php echo isset($tanggal_bayar) ? $tanggal_bayar : '' ?>' />
<input type='hidden' name='' id='tgl_faktur_old' class='form-control' value='<?php echo isset($tanggal_faktur) ? $tanggal_faktur : '' ?>' />
<input type='hidden' name='' id='module' class='form-control' value='detail_faktur_pelanggan' />

<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class="row">
					<div class="col-md-4">
						&nbsp;
					</div>
					<div class="col-md-4">
						&nbsp;
					</div>
					<div class="col-md-4">
						<?php if ($approve != '') { ?>
							<?php echo $this->load->view('pesan') ?>
						<?php } ?>
					</div>
				</div>
				<div class="row">
					<div class='col-md-3 text-bold'>
						No Faktur
					</div>
					<div class='col-md-3'>
						<?php echo $no_faktur ?>
					</div>
				</div>
				<br />

				<?php if ($no_order != '') { ?>
					<div class="row">
						<div class='col-md-3 text-bold'>
							No Ref Order
						</div>
						<div class='col-md-3'>
							<?php echo $no_order ?>
						</div>
					</div>
					<br />
				<?php } ?>

				<div class="row">
					<div class='col-md-3 text-bold'>
						Pelanggan
					</div>
					<div class='col-md-3'>
						<?php echo $nama_pembeli ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Tanggal Faktur
					</div>
					<div class='col-md-3'>
						<?php echo $tanggal_faktur ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Tanggal Bayar
					</div>
					<div class='col-md-3'>
						<?php echo $tanggal_bayar ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Metode Bayar
					</div>
					<div class='col-md-3'>
						<?php echo $metode ?>
					</div>
				</div>
				<br />

				<?php if ($no_rekening != '') { ?>
					<div class="row">
						<div class='col-md-3 text-bold'>
							No Rekening
						</div>
						<div class='col-md-3'>
							<?php echo $no_rekening ?>
						</div>
					</div>
					<br />
				<?php } ?>

				<div class="row">
					<div class='col-md-3 text-bold'>
						Potongan
					</div>
					<div class='col-md-3'>
						<?php echo $jenis_potongan ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Nilai
					</div>
					<div class='col-md-3'>
						<?php if ($jenis_potongan == 'Nominal') { ?>
							<?php echo number_format($pot_faktur) ?>
						<?php } else { ?>
							<?php if ($jenis_potongan == 'Tidak ada potongan') { ?>
							<?php } else { ?>
								<?php echo $pot_faktur . ' %' ?>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<br />
				<hr />

				<div class="row">
					<div class="col-md-12">
						<u>Data Produk</u>
					</div>
				</div>
				<br />

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-list-draft" id="tb_product">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>Produk</th>
										<th>Jumlah</th>
										<th>Harga</th>
										<!--<th>Metode Bayar</th>-->
										<th>Sub Total</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($invoice_item)) { ?>
										<?php foreach ($invoice_item as $value) { ?>
											<tr data_id="<?php echo $value['id'] ?>">
												<td><?php echo $value['kode_product'] . '-' . $value['nama_product'] . '-' . $value['nama_satuan'] ?></td>
												<td><?php echo $value['qty'] ?></td>
												<td><?php echo number_format($value['harga']) ?></td>
												<!--<td><?php echo $value['metode'] ?></td>-->
												<td><?php echo number_format($value['sub_total']) ?></td>
												<!-- <td> -->
												<?php //if ($status == 'DRAFT') { 
												?>
												<!-- <i class="fa fa-pencil" data_id="<?php echo $value['id'] ?>" product_satuan="<?php echo $value['product_satuan'] ?>" qty="<?php echo $value['qty'] ?>" harga="<?php echo $value['harga'] ?>" invoice="<?php echo $value['invoice'] ?>" onclick="FakturPelanggan.ubahQty(this)"></i> -->
												<!--             &nbsp; Ubah Pembatalan Jumlah-->
												<?php //} 
												?>
												<!-- </td> -->
											</tr>

											<?php if ($value['bank'] != '0' && $value['bank'] != '') { ?>
												<tr>
													<td colspan="7" class="text-primary"><?php echo $value['nama_bank'] . '-' . $value['no_rekening'] . '-' . $value['akun'] ?></td>
												</tr>
											<?php } ?>

											<?php if (!empty($value['pot_item'])) { ?>
												<?php foreach ($value['pot_item'] as $v_i) { ?>
													<?php if ($v_i['jenis_potongan'] == 'Nominal') { ?>
														<tr>
															<td colspan="6"><?php echo 'Potongan ' . $v_i['jenis_potongan'] . ' : ' . number_format($v_i['nilai']) ?></td>
														</tr>
													<?php } else { ?>
														<tr>
															<td colspan="6"><?php echo 'Potongan ' . $v_i['jenis_potongan'] . ' : ' . $v_i['nilai'] . ' %' ?></td>
														</tr>
													<?php } ?>
												<?php } ?>
											<?php } ?>
										<?php } ?>
									<?php } ?>

									<!-- <tr data_id="" class='simpan_item_add'>
										<td colspan="7">
											<label id="add_detail">
												<a href="#" onclick="FakturPelanggan.addItem(this, event)">Tambah Item</a>
												&nbsp;
												<a href="#" class='hide' id='simpan_item' onclick="FakturPelanggan.simpanItem(this, event)">Simpan Item</a>
											</label>
										</td>
									</tr> -->
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-right">
						<h4>Total : Rp, <label id="total"><?php echo number_format($total) ?></label></h4>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="btn-cetak" class="btn btn-danger" onclick="FakturPelanggan.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak F8</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="FakturPelanggan.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
