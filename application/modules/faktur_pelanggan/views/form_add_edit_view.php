<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<input type='hidden' name='' id='module' class='form-control' value='add_faktur_pelanggan' />
<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-header with-border" style="margin-top: 12px;">
				<h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
			</div>
			<div class="box-body box-block">
				<div class="row">
					<div class='col-md-3 text-bold'>
						Pelanggan
					</div>
					<div class='col-md-3'>
						<select class="form-control required" id="pembeli" error="Pelanggan">
							<option value="">Pilih Pelanggan</option>
							<?php if (!empty($list_pelanggan)) { ?>
								<?php foreach ($list_pelanggan as $value) { ?>
									<?php $selected = '' ?>
									<?php if (isset($pembeli)) { ?>
										<?php $selected = $pembeli == $value['id'] ? 'selected' : '' ?>
									<?php } ?>
									<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Tanggal Faktur
					</div>
					<div class='col-md-3'>
						<input type='text' name='' readonly="" id='tanggal_faktur' class='form-control required' value='<?php echo isset($tanggal_faktur) ? $tanggal_faktur : '' ?>' error="Tanggal Faktur" />
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Tanggal Bayar
					</div>
					<div class='col-md-3'>
						<input type='text' name='' readonly="" id='tanggal_bayar' class='form-control required' value='<?php echo isset($tanggal_bayar) ? $tanggal_bayar : '' ?>' error="Tanggal Bayar" />
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3 text-bold'>
						Metode Bayar
					</div>
					<div class='col-md-3'>
						<select class="form-control required" id="jenis_bayar" error="Metode Bayar">
							<?php if (!empty($list_jenis_bayar)) { ?>
								<?php foreach ($list_jenis_bayar as $value) { ?>
									<?php $selected = '' ?>
									<?php if (isset($metode)) { ?>
										<?php $selected = $metode == $value['id'] ? 'selected' : '' ?>
									<?php } ?>
									<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Potongan
					</div>
					<div class='col-md-3'>
						<select class="form-control required" id="potongan_invoice" error="Potongan" onchange="FakturPelanggan.hitungTotal()">
							<?php if (!empty($list_potongan)) { ?>
								<?php foreach ($list_potongan as $value) { ?>
									<?php $selected = '' ?>
									<?php if (isset($potongan)) { ?>
										<?php $selected = $potongan == $value['id'] ? 'selected' : '' ?>
									<?php } ?>
									<option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['potongan'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3 text-bold'>
						Nilai Potongan
					</div>
					<div class='col-md-3'>
						<input type='text' min="0" name='' id='nilai_potongan' class='form-control' value='<?php echo isset($pot_faktur) ? $pot_faktur : '' ?>' error="Nilai Potongan" onkeyup="FakturPelanggan.hitungSubTotal(this, 'potongan')" />
					</div>
				</div>
				<br />
				<hr />

				<div class="row">
					<div class="col-md-12">
						<u>Data Produk</u>
					</div>
				</div>
				<br />

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table class="table table-striped table-bordered table-list-draft" id="tb_product">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>Produk</th>
										<th>Jumlah</th>
										<th>Harga</th>
										<th>Potongan</th>
										<th>Pajak</th>
										<th>Sub Total</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($invoice_item)) { ?>
										<?php $index = 0; ?>
										<?php foreach ($invoice_item as $value) { ?>
											<tr data_id="<?php echo $value['id'] ?>">
												<td>
													<select class="form-control select2 required" id="product<?php echo $index ?>" error="Produk" onchange="FakturPelanggan.hitungSubTotal(this, 'product')">
														<option value="" harga="0">Pilih Produk</option>
														<?php if (!empty($list_product)) { ?>
															<?php foreach ($list_product as $v_p) { ?>
																<?php $selected = $v_p['id'] == $value['product_satuan'] ? 'selected' : '' ?>
																<option harga="<?php echo $v_p['harga'] ?>" <?php echo $selected ?> value="<?php echo $v_p['id'] ?>"><?php echo $v_p['nama_product'] . '-' . $v_p['satuan'] . '-[Rp, ' . number_format($v_p['harga']) . ']' ?></option>
															<?php } ?>
														<?php } ?>
													</select>
												</td>
												<td>
													<select class="form-control select2 required" id="pajak<?php echo $index ?>" error="Pajak" onchange="FakturPelanggan.hitungSubTotal(this, 'pajak')">
														<option value="" persentase="0">Pilih Pajak</option>
														<?php if (!empty($list_pajak)) { ?>
															<?php foreach ($list_pajak as $v_pj) { ?>
																<?php $selected = $v_pj['id'] == $value['pajak'] ? 'selected' : '' ?>
																<option persentase="<?php echo $v_pj['persentase'] ?>" <?php echo $selected ?> value="<?php echo $v_pj['id'] ?>"><?php echo $v_pj['jenis'] ?></option>
															<?php } ?>
														<?php } ?>
													</select>
												</td>
												<td>
													<input type="number" value="<?php echo $value['qty'] ?>" min="1" id="jumlah" class="form-control text-right" onkeyup="FakturPelanggan.hitungSubTotal(this, 'jumlah')" onchange="FakturPelanggan.hitungSubTotal(this, 'jumlah')" />
												</td>
												<td><?php echo number_format($value['sub_total']) ?></td>
												<td class="text-center">
													<i class="mdi mdi-delete mdi-18px" onclick="FakturPelanggan.deleteItem(this)"></i>
												</td>
											</tr>

											<?php if ($value['bank'] != '0' && $value['bank'] != '') { ?>
												<tr data_bank="<?php echo $value['bank'] ?>">
													<td colspan="7"><?php echo $value['nama_bank'] . '-' . $value['no_rekening'] . '-' . $value['akun'] ?></td>
												</tr>
											<?php } ?>

											<?php $index += 1; ?>
										<?php } ?>
									<?php } ?>
									<tr data_id="">
										<td colspan="7">
											<label id="add_detail">
												<a href="#" onclick="FakturPelanggan.addItem(this, event)">Tambah Item</a>
											</label>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-12 text-right">
						<h4>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h4>
					</div>
				</div>
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="FakturPelanggan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="FakturPelanggan.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
